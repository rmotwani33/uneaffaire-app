package com.uneaffaire;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import com.uneaffaire.MainActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.react.ReactApplication;
import com.bugsnag.BugsnagReactNative;
import com.facebook.react.ReactInstanceManager;
import com.microsoft.codepush.react.CodePush;
import com.inprogress.reactnativeyoutube.ReactNativeYouTube;
import com.microsoft.codepush.react.ReactInstanceHolder;
import cl.json.RNSharePackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.goldenowl.twittersignin.TwitterSigninPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.taessina.paypal.RNPaypalWrapperPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.oblador.vectoricons.VectorIconsPackage;
// import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.pusherman.networkinfo.RNNetworkInfoPackage;
import com.pusherman.networkinfo.RNNetworkInfoPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
// import com.imagepicker.permissions.OnImagePickerPermissionsCallback;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
// import com.goldenowl.twittersignin.TwitterSigninPackage;
// import co.apptailor.googlesignin.RNGoogleSigninPackage;
// import com.facebook.reactnative.androidsdk.FBSDKPackage;
// import com.imagepicker.ImagePickerPackage;
// import com.meedan.ShareMenuPackage;
// import cl.json.RNSharePackage;
// import com.github.yamill.orientation.OrientationPackage;
// import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
// import com.facebook.soloader.SoLoader;
import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.controllers.ActivityCallbacks;
import com.airbnb.android.react.maps.MapsPackage;
import com.evollu.react.fcm.FIRMessagingPackage;
//import com.gcrabtree.rctsocketio.SocketIoPackage;  // <--- import

// import io.fabric.sdk.android.Fabric;
import java.util.Arrays;
import java.util.List;

 public class MainApplication extends NavigationApplication implements ReactInstanceHolder {

   

     @Override
     public boolean isDebug() {
         // Make sure you are using BuildConfig from your own application
         return BuildConfig.DEBUG;
     }

     private static CallbackManager mCallbackManager = CallbackManager.Factory.create();
     private String CODEPUSH_STAGING = "M8JTsewo0w0UebM2oPOid1bB3WvXSyb7WbJ94";
     private String CODEPUSH_PRODUCTION = "X7ch7ZFl3m0mmOb-KZgnvGtjq1hzB1bbXbZJc4";

     protected static CallbackManager getCallbackManager() {
         return mCallbackManager;
     }



     @Override
     public void onCreate() {
         super.onCreate();
        //  Fabric.with(getApplicationContext(), new Crashlytics());
         FacebookSdk.sdkInitialize(getApplicationContext());
         // If you want to use AppEventsLogger to log events.
         //AppEventsLogger.activateApp(this);
         setActivityCallbacks(new ActivityCallbacks() {
             @Override
             public void onActivityResumed(Activity activity) {
                 // Do stuff
             }

             @Override
             public void onActivityPaused(Activity activity) {
                 // Do stuff
             }

             @Override
             public void onActivityResult(int requestCode, int resultCode, Intent data) {
                     super.onActivityResult(requestCode, resultCode, data);
                        //Fabric.with(getApplicationContext(), new Crashlytics());
                         //.onActivityResult(requestCode, resultCode, data);                   
                        mCallbackManager.onActivityResult(requestCode, resultCode, data);
                  
                 
             }
           
         });
     }

     @Override
     public String getJSBundleFile() {
         // Override default getJSBundleFile method with the one CodePush is providing
         return CodePush.getJSBundleFile();
     }
    // @Override
    // public void onNewIntent (Intent intent) {
    //         super.onNewIntent(intent);
    //     setIntent(intent);
    // }

     protected List<ReactPackage> getPackages() {
         // Add additional packages you require here
         // No need to add RnnPackage and MainReactPackage
         return Arrays.<ReactPackage>asList(
                 // eg. new VectorIconsPackage()
                new VectorIconsPackage(),
                //  new OrientationPackage(),
                new RNGoogleSigninPackage(), // <-- add this
                new FBSDKPackage(mCallbackManager),
                new TwitterSigninPackage(),
                new RNDeviceInfo(),
                 new FIRMessagingPackage(),
                new MapsPackage(),
                //  new ImagePickerPackage(), // <-- add this line
                new PickerPackage(),
                new RNPaypalWrapperPackage(),
                new RNNetworkInfoPackage(), // <-- add this line
                BugsnagReactNative.getPackage(),
                new CodePush(CODEPUSH_STAGING, getApplicationContext(), BuildConfig.DEBUG)
                //new SocketIoPackage()   // <--- Add here!
         );
     }

     @Override
     public List<ReactPackage> createAdditionalReactPackages() {
         return getPackages();
     }

      @Override
    public String getJSMainModuleName() {
        return "index";
    }

    @Override
    public ReactInstanceManager getReactInstanceManager() {
        // CodePush must be told how to find React Native instance
        return getReactNativeHost().getReactInstanceManager();
    }

  // @Override
//    public ReactInstanceManager getReactInstanceManager() {
//        // CodePush must be told how to find React Native instance
//        return getReactNativeHost().getReactInstanceManager();
//    }

 }
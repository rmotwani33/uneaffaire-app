import {settings} from './settings';
import _ from 'lodash';
import io from 'socket.io-client';
window.navigator.userAgent = 'ReactNative';

function connectD(token, chatActions,authActions) {
  let socket = io(settings.socketUrl, {
    jsonp: false,
    transports: ['websocket'],
    path: '/socket.io',
    // path: '',
  });

  let socketData = {};
  socket.on('connect_error', (error) => {
    console.log('connect error' + error);
  });

  socket.on('connect', () => {
    chatActions.setSocket(socket);
    console.log('socket -> connected');
    //console.log('token '+token);
    socket.emit('connectD', {'token': token});
  });

  socket.on('disconnect', () => {
    console.log('socket disconnected');
  });

  socket.on('connectD', (data) => {
    console.log('socket on -> connectD');
  // console.log(data);
    socket.conncetD=true;
    chatActions.setSocket(socket);
    chatActions.setData(data);
    if(data.user_data !== undefined && data.user_data !== null && Object.keys(data.user_data).length > 0){
      let userdata = data.user_data;
      userdata.token= token;
     //console.log(userdata);
      authActions.setUser(userdata);
    }

    if (_.isArray(data.recent)) {
      chatActions.setConv(data.recent);
    }
  });

  socket.on('userOffline', (uid) => {
    console.log('userOffline -> '+uid.uid);
    chatActions.setUserStatus(uid.uid,false);
  });
  socket.on('userOnline', (uid) => {
    console.log('userOnline -> '+uid.uid);
    chatActions.setUserStatus(uid.uid,true);
  });

  socket.on('isTyping',(data)=>{
    console.log('isTyping -> ');
    //console.log(data);
    if(data.typing){
      chatActions.setUserTyping(data.ad,true);
    }else{
      chatActions.setUserTyping(data.ad,false);
    }
  });

  socket.on('onMessage', (msg) => {
    console.log('onMessage -> ');
    //console.log(msg.msg);
    let nmsg=msg.msg;
    nmsg.typing=false;
    chatActions.onNewMessage(nmsg);
  });

  socket.on('messages_app',(response)=>{
    console.log('messages_app -> ');
    //console.log(response);
    if(response.status){
      chatActions.setDetail(response.message);
    }
    //chatActions.setDetail
  });

  socket.on('updateUserData',(response)=>{
    console.log('updateUserData -> ');
    //console.log(response);
    let userdata = response.user;
    userdata.token= token;
    //console.log(userdata);
    authActions.setUser(userdata);
  });
}

function emitD(event,soc,data){
  if(soc !== undefined && soc !== null && Object.keys(soc).length > 0){
  console.log(" emitted -> "+event);
  //console.log(soc);
  //console.log(data);
  soc.emit(event,data);
  }
}

// socket.on('connectD', (data) => {   //console.log(data);   //return data; });

export {connectD,emitD};

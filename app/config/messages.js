const messages = {
    noUsername:'Pseudo ou mot de passe ne peut pas être vide.',
    //noUsername:'Entrez un email valide',
    password8:'Le mot de passe doit contenir 8 caractères.',
    password16:'Le mot de passe ne doit pas contenir plus de 16 caractères.',
    //validEmail:'Entrez un pseudo valide.',
    validEmail:'Entrez un email valide',
    repass:'Entrez à nouveau la même valeur s\'il vous plait.'
};
export default messages;
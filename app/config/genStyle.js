import React, { Component } from 'react';
import {
  StyleSheet,Dimensions,Platform
} from 'react-native';
import { MediaQueryStyleSheet } from "react-native-responsive";
import { colors } from './styles';
const { height, width } = Dimensions.get('window');
const FORTAB = width < 1025 && width > 721;
const TABLANDSCAPE = width < 1025 && width > 768;
const TABPORTRAIT = width > 721 && width < 769;
export default StyleSheet.create({

    logo:{
        width:140,
        height:160,
    },
    slatebgColor:{
        backgroundColor:'#808a94',
    },
    absoluteBottomRight:{
        backgroundColor:'rgba(255,255,255,0.7)',
        position:'absolute',
        right:0,
        bottom:0,
        flexDirection:'row',
        paddingBottom:5,
        paddingTop:5,
    },
    genButton:{
        padding:8,
        alignSelf: 'stretch',
        color:'#fff',
        textAlign:'center',
        fontSize:FORTAB ? 22:18,
        fontFamily:'Montserrat',
        borderColor:'#eee',
    },
    genButton2:{
        color:'#fff',
        textAlign:'center',
        fontSize:FORTAB ? 22:18,
        fontFamily:'Montserrat',
        
    },
    btnCont:{
        padding:8,
        borderColor:'#eee',
        borderRadius:5
    },
    genCheck:{  
        paddingBottom:10,
        paddingTop:10,
    },

    brandColor:{
        backgroundColor:'#f15a23',
        borderRadius:3,
        alignSelf:'stretch',
    },
    FLONEHALF:{
        flex:TABLANDSCAPE ? 1.4 : TABPORTRAIT ? 0.8:1.4,
    },
    FLDOUBLE:{
        flex:2,
    },
    main:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#f8f8f8',
        paddingTop:15,
    },
    main1:{
        flex: 1,
        flexDirection: 'column',
        backgroundColor:'#f8f8f8',
        paddingTop:15,
    },
    loginMain:{
        flex:1.5,
        alignSelf:'stretch',
    },
    outlineBtn:{
        borderColor:'#f15a23',
        borderWidth:1,
        borderRadius: 2,
        height:FORTAB ? 55:45,
        justifyContent:'center',
        alignSelf:'stretch',
        alignItems:'center',
    },
    smallOutlineBtn:{
        borderColor:'#f15a23',
        borderWidth:1,
        borderRadius: 2,
        height:30,
        justifyContent:'center',
        alignSelf:'stretch',
        alignItems:'center',
    },
    logoCont:{
        flexDirection: 'column',
        justifyContent: 'center',
    },
    /**************WELCOME STYLE SHEET**********/
    Social:{
        color:'#f15a23',
        fontFamily:'icomoon',
        fontSize:20,
    },
    SocialBtn:{
        width:60,
        height:60,
        //borderColor:'#f15a23',
        //borderWidth:1,
        borderRadius: 30,
        alignItems:'center',
        justifyContent:'center',
        marginLeft:5,
        marginRight:5,
    },
    faceBookBtn:{
        backgroundColor:"#3B579D",
    },
    googleBtn:{
        backgroundColor:"#D34836",
    },
    twitterBtn:{
        backgroundColor:"#1DA1F2",
    },
    bottomView:{
        flex:0.8,
        flexDirection:'row',
        alignItems:'center',
        paddingLeft:0,
        paddingRight:0,
        paddingBottom:0,
    },

    /***********SIGN IN & SIGN UP STYLE***********/
    signsocialTxt:{
        marginTop:40,  
        alignSelf: 'stretch',
        textAlign: 'center',
        bottom:0,
        paddingBottom:10,
        fontFamily: 'Montserrat'
    },
    skipText:{
            color:'#f15a23',
            fontSize:FORTAB ? 18:16,
            fontFamily:'Montserrat',
            bottom:10,
    },
    fieldIcoPadding:{
        
    },
    genInput:{
        borderWidth:0,
        //marginTop:10,
        height:40,
        fontSize:FORTAB ? 14:12,
        fontFamily: 'Montserrat',
        color:'#666',
        //paddingLeft:30,
        //backgroundColor:"#FFF"
    },
    genInput3:{
        marginBottom:10
    },  
    genInput2:{
        // borderWidth:0,
        // //marginTop:10,
        // height:40,
        // fontSize:FORTAB ? 14:12,
        // fontFamily: 'Montserrat',
        // color:'#666',
        backgroundColor:"#FFF",
        //marginTop:5
        //paddingLeft:30,
    },
    fieldIco:{
        //position:'absolute',
        //left:0,
        //bottom:14,
        fontSize:20,
    },
    genTextarea:{
        borderWidth:0,
        marginTop:10,
        fontSize:FORTAB ? 14:12,
        fontFamily: 'Montserrat',
        color:'#666',
  },
  errorTxtLbl:{
        fontSize:FORTAB ? 12:10,
        color:'#ff0000'
  },
  successTxtLbl:{
        fontSize:FORTAB ? 12:10,
        color:'#008000'
  },
  forgotTxt:{
    marginTop:30,  
    alignSelf: 'stretch',
    textAlign: 'right',
    bottom:10,
    paddingBottom:10,
    fontFamily: 'Montserrat'
  },
  signupButton:{
    marginTop:15,
    borderRadius:3,
  },
  /************SIGN UP STYLESHEET**********/
  container:{
     flex:1,
     alignSelf:'stretch',
  },
  signupMain:{
    flex:1.3,
    alignSelf:'stretch',
  },
  signupMainFirst:{
    flex:0.8,
    alignSelf:'stretch',
  },
  genChk:{
    paddingBottom:0,
    paddingTop:0,
  },
  btnBlock:{
    flex:1,
    alignItems:'center',
    justifyContent: 'flex-end',
    alignSelf:'stretch',
    flexDirection:'column',
  },
  signupTxt:{
    bottom:10,
    fontFamily: 'Montserrat',
    textAlign:'center', 
  },
  signTxt:{
    fontWeight:'bold',
    color:'#606060',
    fontFamily: 'Montserrat',
  },
  tabs:{
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'space-between',
    marginLeft:-1,
    marginRight:-1,
  },
  widthHalf:{
    width:(width/2)-15,
  },
  widthHalfPro:{
    width:(width/2)-8,
  },
  widthHalfWithSpace:{
    width:(width/2)-30,
    borderRadius:3,
  },
  widthThree:{
    width:(width/3)-6,
  },
  widthQuater:{
    width:(width/4)-8,
  },
  tabText:{
    fontSize:FORTAB ? 16:9,  
    fontFamily:'Montserrat',
    textAlign:'center',
    paddingLeft:5,
    paddingRight:5,
    paddingTop:10,
    paddingBottom:10,
    borderColor:'#ddd',
    backgroundColor:'#eee',
    alignSelf:'stretch',
    borderWidth:1,
    
  },
  tabTextSelected:{
    fontSize:FORTAB ? 16:9,  
    textAlign:'center',
    paddingLeft:5,
    paddingRight:5,
    paddingTop:10,
    paddingBottom:10,
    //backgroundColor:'rgba(0,0,0,0)',
    alignSelf:'stretch',
    borderColor:'#f15a23',
    borderBottomColor:'#f8f8f8',
    borderWidth:1,
    borderBottomWidth:1,
    fontFamily:'Montserrat',
  },
  tabSelected:{
    alignItems:'center',
    position:'relative',
    zIndex:2,
    paddingLeft:1,
    paddingRight:1,
    top:1,
  },
  tabnotSelected:{
    alignItems:'center',
    paddingLeft:1,
    paddingRight:1,
    position:'relative',
    top:1,
  },
  botomOrangeLine:{
    width:width-34,
    height:1,
    backgroundColor:'#f15a23',
    position:'absolute',
    bottom:14,
    left:0,
    right:0,
    zIndex:1,
  },
  forproduct:{
    width:width-16,
  },
  tabParent:{
    paddingBottom:15,
    position:'relative',
    overflow:'hidden',
    backgroundColor: '#f8f8f8'
  },
  /*************FORGOTPASS STYLE************/
  btnRow:{
    flex:1,
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  forgotEmailtxt:{
    color:'#666',
    fontSize:14,
    fontFamily:'Montserrat',
  },
  forgotMain:{ alignSelf: 'stretch'},
  /**************PRODUCT STYLESHEET**************/
    proSign:{
        width:35,
        height:20,
    },
    proSignAbsolute:{
        position:'absolute',
        right:0,
        paddingTop:5,
        width:20,
        height:10,
    },
    proImg:{    
        width:null,
        resizeMode:'cover',
        height:TABLANDSCAPE ? 135 : TABPORTRAIT ? 135:110,
    },
    progridWidth:{
        height:151,
        width:null,
        resizeMode:'cover',
    },
    proName:{
        fontSize:FORTAB ? 14:12,
        paddingBottom:0,
        color:'#333',
        fontFamily:'Montserrat',
    },
    camIco:{
        fontSize:18,
        zIndex:-1,
    },
    canvasContainer:{
        flex:1,
        alignItems: 'stretch',
        padding:0,
        position:'relative',
    },
    bottomDetail:{
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    bottomTxt:{
        fontSize:10,
    },
    globalIcon:{
        fontFamily:'icomoon',
        fontSize:12,
        justifyContent:'center'
    },
    specialIco:{
        fontSize:25,
        color:'#f15a23',
        marginRight:10,
    },
    proList:{
        borderWidth:1,
        borderColor:'#ddd',
        backgroundColor:'#fff',
        elevation: 3,
    },
    proMain:{
        flex:1,
        flexDirection: 'column',
        alignItems: 'stretch',
        backgroundColor: '#f8f8f8',
        //paddingBottom:2,
    },
    proCol:{    
        paddingLeft:4,
        paddingRight:4,
        paddingBottom:8,
        position:'relative',
        backgroundColor: 'rgba(0,0,0,0)'
    },
    proRow:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        position:'relative',
        marginLeft:0,
        marginRight:0,
        //backgroundColor:'#fff',
        flex:1,
    },
    proCaption:{
        paddingLeft:8,
        paddingRight:8,
        paddingBottom:5,
        paddingTop:5,
        backgroundColor:"#FFF",
        width:null,
        alignSelf:'stretch',
    },
    rightAlign:{
        top:2,
        right:0,
    },
    leftAlign:{
        fontSize:12,
        fontFamily:'Montserrat',
        justifyContent: 'space-between',
    },
    bold:{
        fontWeight:'600',
    },
    orangeColor:{
        color:'#f15a23',
    },
    greenColor:{
        color:'#72bb53',
    },
    listParent:{
        paddingLeft:0,
        paddingRight:0,
        paddingBottom:15,
        height:height-70,
        //height:height-120,
        //height:height,
    },
    searchHolder:{
        flexDirection:'row',
        paddingRight:10,
        paddingLeft:10,
        marginRight:-10,
        marginLeft:-10,
        paddingBottom:0,
        position:'relative',
        height:45,
        alignSelf:'stretch',
        backgroundColor:"#FFF",
        paddingTop:7,
        marginTop:-20,
        justifyContent:'flex-end',
        alignItems:'center',
    },
    searchAdvanceIco:{
        marginRight:-15,
        marginLeft:-15,
        marginTop:0,

    },
    
    searchHolderScrolled:{
        flexDirection:'row',
        paddingRight:0,
        paddingBottom:12,
        position:'relative',
        height:35,
        alignSelf:'stretch',
        marginTop:-50,
    },
    searchinputHolder:{ 
        height:45,
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },
    searchInput:{
        alignSelf:'stretch',
        height:40,
        //paddingLeft:5,
        //paddingRight:30,
    },
    uneSym:{
        left:0,
        top:0,
        width:20,
        zIndex:1,
        height:25,
    },
    genIco:{
        fontFamily:'icomoon',
        fontSize:20,
    },
    icoholderRight:{
        alignItems:'center',
        justifyContent:'center',
        //marginRight:10,
        // position:'absolute',
        // flexDirection:'row',
        // top:5,
        // zIndex:-1,
    },
    searchIco:{
        fontSize:20,
        //marginRight:10,
        //height:30, 
        //alignItems:"center",
        //justifyContent:"center",
        //alignSelf:"center",
        //lineHeight:30,
        //zIndex:-1,
        //position:'relative',
    },
    prolistImg:{
        flex:1,
        alignItems: 'stretch',
        padding:8,
    },
    circle:{
        width:10,
        height:10,
        backgroundColor:'#eee',
        borderRadius:5,
        top:0,
        zIndex:1,
    },
    online:{
        backgroundColor:'#72bb53',
    },
    offline:{
        backgroundColor:'#ccc',
    },
    favIco:{
        position:'absolute',
        right:10,
        top:10,
        color:'#ccc',
        fontSize:FORTAB ? 22:18,
    },
    imageCanvas:{
        position:'relative',
        overflow:'hidden',
        flex:1,
        borderWidth:1,
        borderBottomWidth:0,
        borderColor:'#ddd',
    },
    imgInner:{
        position:'relative',
        flex:1,
    },
    bottomCaption:{
        position:'relative',
        bottom:0,
        backgroundColor:'#333',
        paddingLeft:10,
        paddingRight:10,
        paddingTop:2,
        paddingBottom:4,
        flex:1,
        left:0,
        right:0,
        justifyContent:'space-between',
        flexDirection:'row',
        alignItems:'center'
    },
    listCol:{
        paddingLeft:0,
        paddingRight:0,
        paddingTop:0,
        paddingBottom:0,
        position:'relative',
    },
    hasVideo:{
        width:50,
        height:50,
        position:'absolute',    
        left:0,
        top:-5,
        zIndex:1,
    },
    hasVideo1:{
        width:20,
        height:20,
        position:'absolute',    
        left:3,
        //opacity:0.8,
        // top:-4,
        zIndex:1,
    },
    prolistRow:{
        borderBottomWidth:1,
        borderLeftWidth:0,
        borderRightWidth:0,
        borderTopWidth:0,
        borderColor:'#eee',
        backgroundColor:'#fff',
        marginTop:10,
        paddingTop:10,
        elevation: 3,
    },
    productdataMain:{
        backgroundColor:'#f8f8f8',  
        paddingLeft:FORTAB ? 15:8,
        paddingRight:FORTAB ? 15:8,
        paddingTop:15,
    },
    userChat:{
        //paddingTop:3,
        //paddingRight:2,
    },
    sliderItem:{
        marginLeft:-15,
        marginRight:-15,
    },
    bottomFixed:{
        paddingLeft:15,
        paddingRight:15,
        paddingTop:10,
        paddingBottom:10,
        borderRadius:0,
        borderColor:'#d6d6d6',
        borderWidth:1,
        backgroundColor:'#fff', 
        position:'relative',
    },
    varientButton:{
        marginLeft:5,
        marginRight:5,
        position:'relative',
    },
    varientRadio:{
        position:'absolute',
        top:0,
        bottom:0,
        left:0,
        right:0,
        backgroundColor:'red',
    },
    boxApear:{
        width:width,
        borderWidth:1,
        borderColor:'#aaaaaa',
        alignSelf:'center',
        paddingTop:10,
        backgroundColor:'#fff',
    },
    popupView:{
        position:'absolute',
        width:width,
        height:height,
        backgroundColor:'rgba(50,50,50,0.7)',
        top:0,
        bottom:0,
        left:0,
        right:0,
    },
    varientBlock:{
        paddingBottom:20,
        paddingLeft:15,
        paddingRight:15, 
    },
    addImg:{
        flex:1,
        width:null,
        resizeMode:'cover'
    },
    addHolder:{
        paddingBottom:15,
        paddingTop:15,
        flex:1,
        width:width-30,
    },
    modalMain:{
        padding:10,
        backgroundColor:'#fff',
        alignSelf:'center',
        width:width-80,
    },
    /*****************CART STYLESHEET*****************/
    cartItemImg:{
        height:FORTAB ? 104:120,
        width:null,
        resizeMode:'cover'
    },
    /*************MY ACCOUNT STYLESHEET**************/
    proCont:{
      flex:2,
    },
  smbtn:{
      padding:5,
  },
  scrollInner:{
      flex:1,
      alignItems:'stretch',
       backgroundColor:"#f8f8f8",
  },
  genText:{
    color:'#333',
    fontSize:FORTAB ? 18:14,
    fontFamily:'Montserrat',
  },
  onlineTxt:{
    color:'#333',
    fontSize:13,
    fontFamily:'Montserrat',
  },
  blockTitle:{
    fontSize:FORTAB ? 22:18,
    color:'#333',
    paddingBottom:10,
    alignSelf:'flex-start'
  },
  dataRow:{
    flexDirection:'row',
    justifyContent: 'space-between',
    paddingBottom:5,
    alignSelf:'stretch',
  },
  labelText:{
    fontSize:FORTAB ? 16:12,
    textAlign:'right',
    color:'#333',
  },
  valText:{
    fontSize:12,
    right:0,
    color:'#333',
  },
  statasticksRow:{
    marginRight:-10,
    marginLeft:-10,
  },
  statastcksCol:{
    paddingLeft:10,
    paddingRight:10,
  },
  accountdataBlock:{
    paddingBottom:20,
    flexDirection:'column',
  },
  inlineDatarow:{
      flexDirection:'row',
      alignItems:'center',
      alignSelf:'stretch',
      overflow:'visible',
  },
  calendar: {
    borderTopWidth: 1,
    paddingTop: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
    height: 350
  },
 ItemTop:{
    alignItems:'flex-start',
  }, 
  inlineWithico:{
    flexDirection:'row',
    paddingRight:10,
    marginRight:10,
    justifyContent:'center',
    alignItems:'center',
  },
  icoText:{
    fontSize:12,
    color:'#333',
  },
  label: {
        fontSize: 12,
 },
  myAccountData:{
      flex:1,
  },
  /*********productlist style*********/
    gridParent:{
        paddingLeft:0,
        paddingRight:0,
        paddingBottom:15,
        height:height-210,
        // height:height-70,
        //height:height-120,
    },
    colInner:{
        position:'relative',
    },
    bottmData:{
        paddingLeft:5,
        paddingTop:2,
    },
    justRight:{
        justifyContent: 'flex-end',
        alignSelf:'stretch'
    },
    justCenter:{
        alignItems: 'center',
        justifyContent: 'center',
    },
    noItemsCont:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        height:200,
    },
    noItemsText:{
        fontSize:18,
        color:"#ccc"
    },
    Valign:{
        alignItems: 'center',
    },
    justLeft:{
        justifyContent: 'flex-start',
        alignSelf:'flex-start'
    },
    alignSelf:{
        alignSelf:'center',
    },
    valignTop:{
        alignItems:'flex-start',
    },
    relativeCaption:{
        position:'relative',
        backgroundColor:'#333',
        bottom:1,
    },
    relativeIco:{
        position:'relative',
        bottom:0,
        top:5,
    },
    mainScollview:{
        alignSelf:'stretch',
        backgroundColor:'#f8f8f8',
    },
    readiText:{
        fontSize:FORTAB ? 14:12,
        marginLeft:8, 
        marginTop:-2,
        alignSelf:'center',
        marginRight:5,
        flexWrap: "wrap"
    },
    readiText2:{
        fontSize:FORTAB ? 14:12,
        marginLeft:8,
        marginTop:-4,
        alignSelf:'center',
        //marginRight:5,
    },
    readiCont:{
        paddingTop:5,
        paddingBottom:5, 
    },
    smallBtn:{
        fontSize:FORTAB ? 18:14,
        paddingTop:5,
        paddingBottom:5,
        paddingRight:12,
        paddingLeft:12,
        borderRadius:2,
        backgroundColor:'#f15a23',
        color:'#fff',
        textAlign:'center',

    },
    smallBtn2:{
        //fontSize:FORTAB ? 18:14,
        height:30,
        paddingTop:5,
        paddingBottom:5,
        paddingRight:12,
        paddingLeft:12,
        borderRadius:2,
        backgroundColor:'#f15a23',
        //color:'#fff',
        //textAlign:'center',

    },
    smBtn:{
        backgroundColor:'#f15a23',
        borderRadius:3,
        paddingVertical:5,
        paddingHorizontal:12,
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:5,
    },
    smBtnText:{
        fontSize:12,
        color:"#fff"
    },
    w120:{
        width:120,
    },
    smallDesc:{
        fontSize:FORTAB ? 16:12,
        color:'#666666'
    },

    postAdSection:{
        marginTop:10,
        borderColor:"#ddd",
        borderWidth:0.7,
        paddingVertical:7,
        backgroundColor:"#FFF",
        marginTop:10,
    },

    subTitle:{        
        fontSize:FORTAB ? 20:16,
        color:'#666',
        //backgroundColor:"#f15a25",
        padding:5,
        //paddingHorizontal:-5,
        //marginTop:10,
    },
    subTitle2:{        
        fontSize:FORTAB ? 20:16,
        color:'#fff',
        backgroundColor:"#f15a25",
        padding:5,
        //paddingHorizontal:-5,
        //marginTop:10,
    },
    contData:{
        paddingBottom:10,
    },
    contBlock:{
        paddingBottom:15,
    },
    borderBottom:{
        borderBottomWidth:1,
        borderBottomColor:'#d6d6d6'
    },
    pdfData:{
        flex:1,
        position:'relative',
    },
    date:{
        position:'absolute',
        right:5,
        bottom:5,
    },
    imgCol:{
        marginRight:15,
    },
    pdfRow:{
        borderWidth:1,
        borderColor:'#d6d6d6',
        paddingTop:8,
        paddingLeft:8,
        paddingRight:8,
        paddingBottom:32,
        marginTop:10,
    },
    pdfSymbol:{
        width:42,
        height:39,
    },
    dateText:{
        fontSize:12,
        position:'absolute',
        right:5,
        bottom:5,
        left:8,
        color:'#999',
    },
    Center: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    fullDate:{
        fontSize:12,
        color:'#999',
    },
    /*************favoris page style*********/
    dropDown:{
        width:150,
        alignSelf:'stretch',
    },
    gridCheck:{
        position:'absolute',
        right:null,
        left:0,
        top:0,
        paddingBottom:2,
        paddingLeft:2,
        paddingRight:2,
        paddingTop:2,
        backgroundColor:'rgba(255,255,255,0.9)',
        borderWidth:1,
        borderColor:'#ddd',
    },
    listingProname:{
        fontSize:FORTAB ? 20:14,
        color:'#333'
    },
    notiCol:{
        paddingLeft:5,
        paddingRight:5,
    },
    cardNotiRow:{
        marginLeft:-5,
        marginRight:-5,
    },
    subscribeImgHolder:{
        height:TABLANDSCAPE ? 180 : TABPORTRAIT ? 160:100 ,
    },
    subImg:{
        width:null,
        resizeMode:'cover',
        height:TABLANDSCAPE ? 190 : TABPORTRAIT ? 170:100,
    },
    subOwnerImg:{
        width:null,
        resizeMode:'cover',
        height:TABLANDSCAPE ? 80 : TABPORTRAIT ? 60:40,
    },
    subscribeHolder:{
        borderWidth:1,
        borderColor:'#ddd',
        backgroundColor:'#fff',
    },
    subOwnerHolder:{
        padding:3,
        bottom:3,
        backgroundColor:'#fff',
        borderWidth:1,
        width:TABLANDSCAPE ? 80 : TABPORTRAIT ? 60:40,
        borderColor:'#ccc',
        left:3,
    },
    optHandler:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:TABLANDSCAPE ? -30 : TABPORTRAIT ? -30:-30,
    },
    delSubscriber:{
        padding:3,
        borderWidth:1,
        borderColor:'#ccc',
        height:FORTAB ? 40:30,
        backgroundColor:'#fff',
        color:'#f15a25',
        fontSize:FORTAB ? 28:20,
    },
    subCol:{
        padding:5,
    },
    subRow:{
        marginLeft:-5,
        marginRight:-5,
    },
    /****************HOMEPAGE STYLE***************/
    mapView:{
        height:TABLANDSCAPE ? height-200 : TABPORTRAIT ? height-280:350,
        alignSelf:'center',
        width:TABLANDSCAPE ? 530: TABPORTRAIT ? width-120:338,
        alignItems:'center',
        overflow:'hidden'
    },
    advmapView:{
        height:TABLANDSCAPE ? height+100 : TABPORTRAIT ? height-300:218,
        alignSelf:'center',
        width:TABLANDSCAPE ? 700: TABPORTRAIT ? width:200,
        alignItems:'center',
        overflow:'hidden',
    },
    mapHolder:{
        alignSelf:'center',
        paddingTop:5,
        height:TABLANDSCAPE ? height-200  : TABPORTRAIT ? height-280:394,
        width:TABLANDSCAPE ? 530: TABPORTRAIT ? width-120:338,
    },
    advmapHolder:{
        alignSelf:'center',
        paddingTop:5,
        height:TABLANDSCAPE ? height+75  : TABPORTRAIT ? height-180:240,
        width:TABLANDSCAPE ? 700: TABPORTRAIT ? width:200,
    },
    mapSym:{
        width:20,
        height:20,
    },
    dropBorder:{
        paddingBottom:5,
        borderBottomWidth:1,
        borderBottomColor:'#999999',
        alignItems:'flex-start',
    },
    rowListing:{
        paddingTop:10,
        paddingBottom:10,
        borderTopWidth:1,
        borderTopColor:'#ddd',
        paddingLeft:10,
        paddingRight:10,
    },
    vertList:{
        flexDirection:'column',
        borderLeftWidth:1,
        borderLeftColor:'#ddd',
        height:height-185,
        marginTop:10,
        marginRight:-15,
    },
    verticalList:{
        height:height-185,
        flex:1,
        borderTopWidth:1,
        borderTopColor:'#ddd',
    },
    postAddButton:{
        width:50,
        height:50,
        position:'absolute',
        right:5,
        bottom:10,
        borderWidth:1,
        borderColor:'#ddd',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:25,
        backgroundColor:'#fff',
        elevation:3,
    },
    addPost:{
        color:'#f15a25',
        fontSize:18,
    },
    searchholderIco:{
        position:'relative',
        zIndex:111,
    },
    videoRow:{
        paddingBottom:20,
        paddingTop:20,
        borderColor:'#ddd',
        borderWidth:1,
        justifyContent:'center',
        borderRadius:3,
        marginTop:10,
    },
    videoIco:{
        height:80,
        width:80,
        alignSelf:'center',
    },
    /***************FASTSEARCH STYLE************/
    historyRow:{
        paddingBottom:7,
        paddingTop:7,
        borderBottomWidth:1,
        paddingLeft:8,
        paddingRight:8,
        borderColor:'#eee',
        backgroundColor:'#fff'
    },
    proThumbMain:{
        paddingLeft:5,
        paddingRight:5,
    },
    proThumb:{
        width:100,
        height:70,
    },
    smallTitle:{
        fontSize:14,
    },
/*********************Manage ad row***************/
ManageadRow:{
    paddingTop:10,
    paddingBottom:10,
    borderBottomWidth:1,
    borderColor:'#ddd'
},
  /****  GHANSHYAM CSS  ******/

    /*********Aprops style*********/
    apropsMain:{
        flex:1,
    },
    conditionMain:{
        flex:1,
    },
    titleAprops:{
        fontSize:18,
        paddingBottom:5,
    },
    titleCondition:{
        fontSize:18,
        paddingBottom:5,
    },
    contentAprops:{
        paddingTop:5,
        paddingBottom:10
    },
    conditionTitle:{
        paddingBottom:15,
    },
    conditionsubTitle:{
        paddingBottom:10,
        paddingTop:10,
        borderTopWidth:1,
        borderColor:'#c0c0c0',
        flexDirection:'row',
        justifyContent:'space-between',
    },
    conditionsubtext:{
        fontSize:16,    
    },
    conditionContent:{
        paddingBottom:10,
        paddingLeft:2,
    },  
    plusTab:{
        marginRight:5,
        fontWeight:'bold',
        fontSize:18,
    },
    
          
    /*********Aprops style*********/    
    contactTiltle:{
        paddingBottom:10,
        fontFamily:'Montserrat',
    },
    contactTiltle1:{
        paddingBottom:0,
        paddingTop:10,
    },
    contactText:{
        fontFamily:'Montserrat',
        paddingBottom:5, 
    },
    contactTextMain:{
        paddingBottom:10,  
    },
    rowBtnMain:{
        flexDirection:'row', 
        flex:1, 
    },
    btnSquare:{
        borderWidth:0.7,
        height:35,
        width:35,
        borderColor:'#c9c9c9',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#FFFFFF',
    },
    rowMesalerts:{
        paddingTop:5,
        paddingBottom:5,
        borderBottomWidth:1,
        borderBottomColor:'#DDD',
        justifyContent:'space-between',
        alignItems:'center',
    },
    cardNotiAlert:{
        padding:7,
        borderWidth:0.8,
        borderColor:'#c0c0c0',
    },
    notiCardTitle:{
        fontSize:16,
        fontFamily:'Montserrat',
        fontWeight:'bold',
    },
    notiCardDate:{
        fontSize:12,
        color:'#999999',
    },
    icNoti:{
        paddingRight:10,
        paddingLeft:10,
    },
    favouritesTitle:{
        fontSize:16,
        fontFamily:'montserrat',
    },
    favouritesCard:{
        flex:1,
        paddingBottom:10,
        borderBottomWidth:1,
        borderBottomColor:'#ddd',
        position:'relative',
    },
    favorisCard:{
        flex:1,
        paddingBottom:20,
        borderBottomWidth:1,
        borderBottomColor:'#ddd',
        position:'relative',
        backgroundColor:'#fff',
        padding:10,
        marginTop:10,
    },
    rightarrowdiv:{
        position:'absolute',
        right:-3,
    },
    calenderAvant:{
        fontSize:16,
    },
    leftbnImg:{
        width:100,
        height:100,
        borderWidth:1,
        borderColor:'#DDD',
    },
    categoryTitle:{
        paddingBottom:10,
        borderBottomWidth:1,
        borderBottomColor:'#d1d1d1',
    },
    linkAvant:{
        // marginTop:5,
        // paddingTop:5,
        // paddingBottom:5,
        // paddingLeft:5,
        // paddingRight:5,
        color:'#f15a23',
    },
    dropDownpostad:{
        borderBottomWidth:1,
        paddingBottom:5,
        borderBottomColor:'#999999',
        marginTop:10,
        alignContent:'stretch',
        width:width,
    },
    dropabsIco:{
        position:'absolute',
        right:5,
        alignSelf:'center',
        top:5,
        justifyContent:'center',
        zIndex:-1,
    },
    bottomFixdad:{
        borderTopWidth:1,
        borderTopColor:'#cccccc',
        width:width,
        paddingRight:20,
        paddingLeft:20,
        justifyContent:'center',
    },
    placeHolderview:{
     //   width:width/3 - 15,
        height:100,    
        marginRight:7,
        marginLeft:7,
        flex:1, 
        alignItems:'center',
    },
    placeHolderImg:{
         width:100,
        height:100,  
    },
    imgUploadView:{
        // flex:1,
        // height:180,
    },
    imgUpload:{
      //flex:1,
        height:FORTAB ? 380:270,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#ccc',
        padding:7,
    },
    textareaIco:{   
        top:20,
        position:'absolute',
        right:10,
    },
    prodataRow:{
        paddingTop:7,
        paddingBottom:7,
      //borderBottomWidth:1,
        borderTopWidth:1,
        borderColor:'#aaaaaa',
    },
    detailType:{
        color:'#888888',
        fontFamily:'Montserrat',
        fontSize:FORTAB ? 16 : 14,
        flexWrap:'wrap'
    },
    detailTypeValue:{
         color:'#555555',
         fontFamily:'Montserrat',
         fontSize:FORTAB ? 16 : 14,
         flexWrap:'wrap'
    },
    autodicoTxt:{
       // paddingBottom:7,
    },
    paypalImg:{
        width:70,
        height:20,
        resizeMode:'contain',
    },
    Prolistgray:{
        backgroundColor:"#FFF",
        elevation: 3,
    },
    discuterbtn:{
        borderWidth:0.5,
        paddingLeft:10, 
        paddingTop:3,
        paddingBottom:5,
        borderColor:"#777",
        marginTop:5,
    },
    picno:{
        position:'absolute',
        right:0,
        backgroundColor:'red',
    },
    framePstad:{    
      //  padding:5,
        borderWidth:1,
        borderColor:"#c9c9c9",
    },
    Borderbtmframe:{
        paddingBottom:5,
        borderBottomWidth:1,
        borderBottomColor:'#999999',
        alignItems:'flex-start',
        paddingLeft:5,
        paddingRight:5,
    },
    postadcamera:{
        position:'absolute',
        right:15,
        bottom:0,
        padding:13,
        borderRadius:50,
        backgroundColor:'#FFFFFF',
        borderWidth:1,
        borderColor:'#bbbbbb',
        elevation:3,
    },
    cameraAd:{
        color:'#f15a23',
        fontSize:22,
    },
    cancelImg:{
        position:'absolute',
        right:-7,
        top:-10,
        padding:5,
        borderRadius:20,
        borderWidth:1.5,
        borderColor:'#999',
        backgroundColor:'rgba(255,255,255,0.9)',
    },
    txtareaOpen:{
        borderTopWidth:2,
        borderTopColor:'#c1c1c1',
    },
    fullupImg:{
        width:FORTAB ? width-50:null,
        resizeMode:'cover',
        height:FORTAB ? 200:120,
    },
    fullupImgView:{
        height:FORTAB ? 200:120,
    },
    Carousel1:{
        //  justifyContent:'center', 
        //  width:FORTAB ? width-50:null,
        //  alignSelf:'stretch',
        //  flex:1,
        
    },
    abscamtxt:{
        position:'absolute',
        right:0,
        top:2,
        width:18,
        textAlign:'center',
        backgroundColor:"#0000"
        //zIndex:-1,
    },
    camIcoAbs:{
        marginLeft:15,
        zIndex:-1,
    },
    suprimeTitle:{
        fontSize:FORTAB ? 16:14,
        color:'#7a7a7a',
    },
    favIcoHeart:{
        fontSize:20,
    },
    locateIco:{
        fontSize:30,
    },
    deleteRemonterlist:{
        padding:5,
        backgroundColor:'#eeeeee',
        fontSize:20,
        color:'#f15a23',
        marginRight:5,
    },
    checkboxTxt:{
        marginLeft:5,
        marginTop:5,
    },
    socialIcon:{
        height:35,
        width:35,
    },
    /****** GHANSHYAM CSS  ******/
    /************** CHAT STYLESHEET *************/
    chatview: {
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: '#EEE',
        alignSelf:'center'
    },
    img: {
        height: 60,
        width: 60,
        borderRadius: 30,
    },
    leftview: {
        width: 70
    },
    centerview: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },
    timefont: {
        fontFamily:'montserrat',
        fontSize: 13,
        color: '#5683BA',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    chatname_date: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignSelf: 'stretch',
    },
    rightview: {
        flexDirection: 'column',
        alignItems: 'center',
        
    },
    namefont: {
        fontFamily:'montserrat',
        fontSize: FORTAB ? 16:14,
        paddingTop: 2,
        paddingBottom:4,
        color:'#333',
    },
    chatfont: {
        fontFamily:'montserrat',
        fontSize: FORTAB ? 14:12,
        color: '#666',
        paddingBottom:4,
        alignSelf:'stretch',
    },
    unread: {
        position: 'absolute',
        bottom: 6,
        left: 50,
        backgroundColor: 'red',
        fontFamily:'montserrat',
        fontSize: 12,
        color: '#FFF',
        borderRadius: 11,
        padding: 4,
        minWidth: 22,
        textAlign: 'center'
    },
    itemName: {
        fontFamily:'montserrat',
        fontSize: 12,
        color: '#444'
    },
    statusview: {
        marginTop: 9,
        height: 10,
        width: 10,
        backgroundColor: '#5683BA',
        borderRadius: 5,
    },
    separator: {
        height: 1,
        backgroundColor: '#bbb',
    },
    footerview: {
        backgroundColor: '#FFF',
    },
     header:{
        flexDirection: 'row',
        backgroundColor: '#ff0000',
        height: 50,
        alignItems: 'center'
    },
    hleft: {
        width: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    hcenter: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    hright: {
        width: 50
    },
 
    menuIco:{
            fontFamily:'montserrat',
            fontSize:25,
            color:'#fff'
    },
    textCenter:{
        color:'#ffffff',
        fontFamily:'montserrat',
        fontSize:14,
    },
    time: {
        fontFamily:'montserrat',
        fontSize:12,
        color: '#666'
    },
    dateTXT:{
        color:'#aaa',
        fontSize:12,
    },

    msgProImg:{
        width:35,
        height:35,
        borderRadius:17,
        marginRight:8,
    },
    attachPhoto : {
        width:100,
        height:100,
        borderRadius:5
    },
    canvas: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        padding:5
    },
    msgData:{
        paddingLeft:10,
        paddingRight:10,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:'#f0f0f0',
        borderRadius:2,
    },
    msgText:{
        fontSize:FORTAB ? 16:12,
    },
    senderText:{
        color:'#fff'
    },
    _DATE_:{
        fontSize:FORTAB ? 11:10,
        marginTop:2,
        textAlign:'right'
    },
    recRowMsg:{
        width:width-110,
    },
    senderRowMsg:{
        width:width-110,
        alignItems:'flex-end',
     
    },
    senderImg:{
        marginRight:0,
        marginLeft:8,
    },
    chatRecRow:{
        paddingBottom:25,
        justifyContent:'flex-start',
    },
    chatSenderRow:{
        paddingBottom:25,
        justifyContent:'flex-end',
    },
    msgImage:{
        width:125,
        height:80,
    },
    FooterRow:{
        //alignSelf:'stretch',
        marginLeft:-15,
        marginRight:-15,
        height:50,
        flexDirection:'row',
        borderTopWidth:1,
        borderColor:'#c0c0c0',
    },
    attachIco:{
        //marginLeft:10,
        //top:FORTAB ? 18:14,
        fontSize:18,
        //zIndex:111,
    },
    sendIco:{
        //marginRight:10,
        //top:FORTAB ? 18:12,
        fontSize:22,
        //zIndex:111,
    },
    msgInput:{  
        padding:5,
        //alignSelf:'stretch',
        flex:1,
    },
    docIco:{
        fontSize:30,
        color:'#fff',
        marginRight:8,
    },
    sizeText:{
        fontSize:12,
        color:'#fff'
        
    },
    /***************PRODUCT DETAIL PAGE***********/
    productCaption:{
        alignSelf:'stretch',
        paddingTop:10,
        paddingBottom:10,
        //width:width,
    },
    productName:{
        fontWeight:'600',
        color:'#333',
    },
    cartCountMain:{
        position:'relative',
    },
    cartCountView:{
        position:'absolute',
        right:-5,
        top:-5,
        width:16,
        height:16,
        backgroundColor:'#f15a23',
        borderRadius:8,
        zIndex:12,
        justifyContent:'center',
        alignItems:'center'
    },
    cartCount:{
        fontSize:10,
        borderRadius:8,
        textAlign:'center',
        backgroundColor:'#0000',
        color:'#ffffff',
    },
    sliderImage:{
        //width:width+5,
        width:width,
        height:FORTAB ? 500 : 300,
    },
    sliderParent:{
        height:FORTAB ? 500 : 300,
    },
    tabLandCaption:{
        width:width,
        height:height,
    },
    innerBlock:{
        paddingBottom:20,
    },
    inlineIcoBox:{
        alignItems:'center',
        marginLeft:5,
        marginRight:5,
    },
    map:{
        height:FORTAB ? 500 : 300,
        width:width,
        left:0, right: 0, top:0, bottom: 0, position: 'absolute',
    },
    mapWrap:{
        //width:width,
        height:FORTAB ? 500 : 300,
        alignSelf:'stretch',
        flex:1,
        position:'relative',
    },
    youVideo:{
        width:width,
        height:FORTAB ? 500 : 300,
        //alignSelf:'stretch',
        //flex:1,
    },
    youVideo2:{
        width:width,
        height:FORTAB ? 500 : 300,
    },
    videoHolder:{
        width:width,
        height:FORTAB ? 500 : 300,
        //backgroundColor:'#ff0000',
        alignSelf:'stretch',
        alignItems:'stretch',
    },
    cartClick:{
        overflow:'visible',
        paddingTop:5,
        paddingRight:5,
    },
    absoluteCircle:{
        position:'absolute',
        right:13,
        top:-3,
    },
    modalInner:{
        height:height,
        backgroundColor:'rgba(0,0,0,0.6)',
        justifyContent:'flex-end',
        paddingBottom:25,
    },
    autodicoImg:{
        resizeMode:'cover',
        width:90,
        marginRight:10,
        height:40,
    },
    modalApear:{
        justifyContent:'flex-end',
    },
    close:{
        position:'absolute',
        right:0,
        top:-30,
        color:'#fff',
        width:30,
        height:30,
        lineHeight:22,
        backgroundColor:'#f15a25',
        textAlign:'center',
    },
    modalWrap:{
        position:'relative',
    },

    modalButton:{
        height:30,
        width:100,
        backgroundColor:"#fff",
        borderRadius:5,
        alignItems:'center',
        justifyContent:'center',
        margin:5,
        borderColor:colors.navBarButtonColor,
        borderWidth:1
    },
    modalButtonSelected:{
        backgroundColor:colors.navBarButtonColor,
    },
    modalButtonText:{
        color:"#333"
    },
    modalButtonSelectedText:{
        color:"#FFF"
    },
    /****************Drawer stylesheet*************/
    drawerImage:{
        resizeMode:'contain',
        width:null,
        height:100,
    },
    drawerImgCont:{
        width:null,
        paddingTop:12,
        paddingBottom:12,
    },
    icoMenu:{
        fontSize:FORTAB ? 20:16,
        marginRight:20,
    },
    menuList:{
        flexDirection:'row',
        alignItems:'center',
        paddingTop:10,
        paddingBottom:10,
        paddingRight:15,
        paddingLeft:15,
    },
    menuListSelected:{
        backgroundColor:colors.navBarButtonColor,
    },
    drawerView:{
        backgroundColor:'#fff',
        width:260,
    },
    menuLink:{
        fontWeight:'600',
        //fontFamily:'opensans',
        fontSize:FORTAB ? 18:14,
    },
    userCaption:{
        paddingLeft:15,
        paddingRight:15,
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'#eee',
        paddingTop:8,
        paddingBottom:8,
        borderTopWidth:1,
        borderBottomWidth:1,
        borderColor:'#ddd',
    },
    loginUserIco:{
        color:'#666',
        fontSize:FORTAB ? 25:20,
        marginRight:15,
    },
    loginLink:{
        color:'#666',
        fontSize:FORTAB ? 18:12,
        fontWeight:FORTAB ? '600':'500',
    },
    drawerScroll:{
        height:height-140   
    },
    pageLink:{
        borderBottomColor:'#ddd',
        borderBottomWidth:1,
    },
    /*************overwrite style************/
    row:{
        alignItems:'flex-start',
        flex:1,
        position:'relative',
    },
    borderRight:{
        borderBottomWidth:0,
        borderLeftWidth:0,
        borderTopWidth:0,
        borderRightWidth:1,
        borderColor:'#ff0000',
    },
    red:{
        color:'#ff0000',
    },
    blueColor:{
        color:'#4789bf',
    },
    _MPR5_:{
        paddingRight:5,
        marginRight:5,
    },
    _MPR0_:{
        paddingRight:0,
        marginRight:0,
    },
      _MLR5D_:{
        marginLeft:-5,
        marginRight:-5,
    },
    white:{
        color:'#fff'    
    },
    orange:{
        backgroundColor:'#f15a25',
    },
    yellow:{
        backgroundColor:'#f4eb49',
    },
    small:{ 
        fontSize:FORTAB ? 12:10
    },
    midum:{
        fontSize:FORTAB ? 16:12,
    },
    big:{
        fontSize:FORTAB ? 18:14,
    },
    large:{
        fontSize:FORTAB ? 20:16
    },
   _BT0_:{
        borderTopWidth:0,
   },
    mdIco:{
        fontSize:20,
        color:'#f15a23',
    },
    bigIco:{
        fontSize:25,
        color:'#f15a23',
        marginRight:8,
    },
    xlIco:{
        fontSize:FORTAB ? 22:20,
    },
    smallIco:{
        fontSize:FORTAB ? 22:16,
    },
    largeIco:{
        fontSize:28,
        height:30,
    },
    _MPR10_:{
        paddingRight:10,
        marginRight:10,
    },
     _PR35_:{
        paddingRight:35,
    },
    _MR5_:{
        marginRight:5,
    },
    _ML5_:{
        marginLeft:5,
    },
   _PT10_:{
        paddingTop:10,
   },
   _PT5_:{
        paddingTop:5,
   },
   inputContact:{
        backgroundColor:'rgba(255,255,255,0)'
    },
    gendes:{
        fontSize:FORTAB ?16:14,
        color:'#666666',
    },
   stretch:{
       alignSelf:'stretch',
   },
   relative:{
        position:'relative', 
   },
   selfStart:{
        alignSelf:'flex-start'
   },
   absicoinput:{
       marginRight:20,
   },
   center:{
       textAlign:'center',
   },
    _w220_:{
        width:220,
    },
    _F20_:{
        fontSize:FORTAB ?25:20,
    },
     _F18_:{
        fontSize:FORTAB ? 22:18
    },
    _F16_:{
        fontSize:FORTAB ? 18:16,
    },
    _F14_:{
        fontSize:14,
    },
    _F12_:{
        fontSize:FORTAB ?  14:12,
    },
    _F22_:{
        fontSize:22,
    },
     _F24_:{
        fontSize:24,
    },
     _F26_:{
        fontSize:26,
    },
    _F28_:{
        fontSize:28,
    },
    flex:{
        flex:1,
        alignSelf:'stretch',
    }, 
    
    flexStart:{
            alignItems:'center'
    },
    _HP15_:{
        paddingLeft:15,
        paddingRight:15,
    },
    _MR0_:{
        marginRight:0,
    },
    _MLR15D_:{
        marginLeft:-15,
        marginRight:-15,
    },
    _MT10_:{
        marginTop:10,
    },
    _MB10_:{
        marginBottom:10,
    },
    _PT15_:{
        paddingTop:15,
    },
    _MT0_:{
        marginTop:0,
    },
    _PT0_:{
        paddingTop:0,
    },
    _PB0_:{
        paddingBottom:0,
    },
    _MT5_:{
        marginTop:5,
    },
    _PB10_:{
        paddingBottom:10,
    },
    _PB5_:{
        paddingBottom:5,
    },
    _MT20_:{
        marginTop:20,
    },
    _MR10_:{
        marginRight:10,
    },
    _P15_:{
        padding:15,
    },
    _MLR14DEC_:{
        marginLeft:-14,
        marginRight:-14,
    },
    _PR20_:{
        paddingRight:20,
    },
    _PL20_:{
        paddingLeft:20,
    },
    _PL10_:{
        paddingLeft:10,
    },
    _PR10_:{
        paddingRight:10,
    },
    _PR15_:{
        paddingRight:15,
    },
    _MR15_:{
        paddingRight:15,
    },
    _MT15_:{
        marginTop:15,
    },
    _PB15_:{
        paddingBottom:15,
    },
    _MLR5_:{
        marginLeft:-5,
        marginRight:-5,
    },
     textLeft:{
        textAlign:'left',
    },
    spaceBitw:{
       justifyContent:'space-between',
       alignSelf:'stretch',
    },
    grayColor:{
        color:'#888'
    },
    disableColor : {
        color:'#ccc'
    },
    blackColor:{
        color:'#333'
    },
    boldLabel:{
        fontWeight:'600',
        color:'#333',
    },
    boldLabelW:{
        //fontWeight:'600',
        color:'#333',
    },
    _BR1_:{
        borderRightWidth:0.5,
        borderColor:'#888888',
    },
    _PL0_:{
        paddingLeft:0,
    },
    _FFM_:{
        fontFamily:'Montserrat',
    },
    _PL5_:{
        paddingLeft:5,
    },
    _PB20_:{
        paddingBottom:20,
    },
    _HP0_:{
        paddingLeft:0,
        paddingRight:0,
    },
    hr:{
        borderBottomWidth:1,
        borderBottomColor:'#DDD',
        height:1,
    },
    absoluteCheck:{
        position:'absolute',
        zIndex:1,
        right:-1,
        top:0,
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingTop:0,
        paddingBottom:0,
    },
     _BR0_:{
        borderRightWidth:0,
    },
    _BRD0_:{
        borderRadius:0,
    },
    flexRow:{
        flexDirection:'row',
    },
    right20:{
        right:20,
    },
    proImgsmall:{
        height:FORTAB ? 105:90,
    },
    proImgremonter:{
        height:108,
    },
    proBigImg:{
        height:FORTAB ? 250:200, 
    },
    _T18_:{
        top:18,
    },
    _PTB10_:{
        paddingTop:10,
        paddingBottom:10,
    },
    _PR25_:{
        paddingRight:25,
    },
    baseOnline:{
        position:'absolute',
        height:10,
        width:10,
        backgroundColor:'red',
    },
    absCircle:{
        position:'absolute',
        right:-2,
        top:-3,
    },
    _w150_:{
        width:150,
    },
    jusRight:{
        justifyContent:'flex-end',
    }, 
    subTitleBgView:{
        backgroundColor:'#ddd',
        paddingLeft:5,
        paddingRight:5,
        width:null,
        paddingTop:3,
        paddingBottom:3,
        alignSelf:'stretch',
    },
    subTitleBg:{
        fontSize:16,
        fontFamily:'Montserrat',
        color:'#666666',
    },
    genSwitch:{
        transform: Platform.OS === 'ios' ?  [{scale: 0.5}] : FORTAB ? [{scale: 1}]:[{scale: 0.8}],
    },
    absoluteControl:{
        position:'absolute',
        bottom:0,
        right:0,
    },
    bottomArea:{
        paddingLeft:15,
        paddingRight:15,
        paddingTop:8,
        paddingBottom:8,
        backgroundColor:'#fff',
        borderTopWidth:1,
        borderColor:'#ddd',
    },
    modalClose:{
        position:'absolute',
        right:0,
        top:0,
        backgroundColor:'#f15a25',
        padding:5,
        color:'#fff',
        zIndex:10
    },
    absoluteTopLeft:{
        position:'absolute',
        left:10,
        top:8,
        zIndex:1,
    },
    orangeBorder:{
        borderWidth:2,
        borderColor:'#f15a25'
    },
    creditRadio:{
        flexWrap:'wrap'
    },
    CenterIndi:{
        width:width,
        flex:1
    },
    socialIco:{
        alignItems:'center',
        justifyContent:'center',
        height:40,
        width:40,
        marginRight:10,
        padding:5,
    }
});
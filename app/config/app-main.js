import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  AsyncStorage, ActivityIndicator, Platform, View,Text,Image,TouchableOpacity,TextInput,Button,Alert
} from 'react-native';
import { colors } from './styles';
import {Navigation} from 'react-native-navigation';
import { iconsMap, iconsLoaded } from './icons';
import {registerScreens ,registerScreenVisibilityListener} from '../routes';
import images from './images';
import ApiHelper from '../lib/ApiHelper/ApiHelper';
const SHOW_SHARED_ELEMENT_SCREENS = true;
const api=new ApiHelper;
import { Provider } from 'react-redux';
import configureStore from '../redux/store/configureStore';
//import {connectD} from './socket'; 
const store = configureStore();

export default function startMain(token,userdata){
  console.log('======== start app with params ========');
  console.log('token : '+token);
  console.log('======== user data ========');
  console.log(userdata);
    iconsLoaded.then(() => {
        registerScreens(store, Provider);
        if(__DEV__){
          registerScreenVisibilityListener();
        }
    let startPage='WelcomeContainer';
    if(token !== undefined && token !== null && token != '' ){
       startPage='HomeContainer';
    }
    Navigation.startTabBasedApp({
    tabs: [
    {
      label: 'Annonces',
      screen: startPage,
      icon: iconsMap['listing--big']
    },
    { 
      label: 'Discuter',
      screen: 'Chat',
      icon: iconsMap['conversation--big']
    },
    {
      label: 'Déposer',
      screen: 'postad',
      icon: iconsMap['deposer-annonce--big']
    }, 
    // {
    //   label: 'Favoris',
    //   screen: 'favoris',
    //   icon: iconsMap['like--big']
    // }, 
    {
      label: 'Favoris',
      screen: 'favoris',
      icon: iconsMap['like--big']
    },
    {
      label: 'Mon compte',
      screen: 'myAccount',
      icon: iconsMap['user--big']
    },

    ],
    tabsStyle:{
      navBarBackgroundColor: colors.navBarBackgroundColor,
      navBarTextColor: colors.navBarTextColor,
      navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
      navBarButtonColor: colors.navBarButtonColor,
      statusBarTextColorScheme: colors.statusBarTextColorScheme,
      statusBarColor: colors.statusBarColor,
      //tabBarHidden: true, // make the tab bar hidden
      tabBarBackgroundColor: colors.tabBarBackgroundColor,
      tabBarButtonColor: colors.tabBarButtonColor,
      tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
      navBarSubtitleColor: colors.navBarSubtitleColor,
   },
   appStyle: {
      navBarBackgroundColor: colors.navBarBackgroundColor,
      navBarTextColor: colors.navBarTextColor,
      navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
      navBarButtonColor: colors.navBarButtonColor,
      statusBarTextColorScheme: colors.statusBarTextColorScheme,
      statusBarColor: colors.statusBarColor,
      tabBarBackgroundColor: colors.tabBarBackgroundColor,
      tabBarButtonColor: colors.tabBarButtonColor,
      tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
      navBarSubtitleColor: colors.navBarSubtitleColor,
      //screenBackgroundColor:"#f00",
      forceTitlesDisplay: true,
      statusBarHidden: false,
      tabBarHidden : startPage == 'WelcomeContainer' ? true :false, 
    },
    drawer: { 
      left: { 
        screen: 'Drawer', 
        passProps: {
          token:token,
          userdata:userdata,
        }, 
      },
      disableOpenGesture: true
    },
    passProps: {
      token:token,
      userdata:userdata,
    }, 
    animationType: 'fade' // optional, add transition animation to root change: 'none', 'slide-down', 'fade'
    });

    });
}

import React, {Component} from 'react';
import {StyleSheet, Image, Text, View, Dimensions,TouchableOpacity} from 'react-native';

import {colors} from '../../config/styles';
import images from '../../config/images';
import moment from 'moment';
import styles from '../../config/genStyle';
var _ = require('lodash');
// redux specific
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const dimen = Dimensions.get('window');

class ChatSticky extends Component {

  constructor(props) {
    super(props);
    this.active = true;
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  onNavigatorEvent(event) {
    //console.log(event);
  }

  componentWillReceiveProps(nextProps) {
    if(!(_.isEqual(this.props.chat.notification,nextProps.chat.notification))){
      this.active = true;
    }
  }

  goto(page,data) {
    //console.log(this.props);
    // this.props.navigator.push({
    //   screen: page,
    //   passProps:{
    //     data:data
    //   }
    // });
    if(this.active === true){
      this.props.navigator.handleDeepLink({link: 'Singlechat',payload:data});
      this.active = false;
    }
  }

  _renderChat(data) {
    return (
      <TouchableOpacity
        style={StyleSheet.flatten(styles.chatview)}
        activeOpacity={0.7}
        onPress={() => {this.goto('Singlechat',data)}}>
        <View style={StyleSheet.flatten(styles.leftview)}>
          <Image
            source={{
            uri: data.avatar
          }}
            style={StyleSheet.flatten(styles.img)}></Image>
          {data.userOnline
            ? <View
                style={{
                position: 'absolute',
                bottom: 10,
                right: 14,
                backgroundColor: "#34C24C",
                height: 10,
                width: 10,
                borderRadius: 5
              }}></View>
            : null}
        </View>
        <View style={StyleSheet.flatten(styles.centerview)}>
          <View style={StyleSheet.flatten(styles.chatname_date)}>
            <Text style={StyleSheet.flatten(styles.namefont)}>{data.fullname}</Text>
            <Text style={styles.dateTXT}>{moment(data.createdAt).fromNow()}</Text>
          </View>
          <Text style={StyleSheet.flatten(styles.chatfont)} numberOfLines={1}>{data.adName}</Text>
          {data.typing
            ? <Text style={StyleSheet.flatten(styles.chatfont)} numberOfLines={1}>{'Typing...'}</Text>
            : <Text style={StyleSheet.flatten(styles.chatfont)} numberOfLines={1}>{data.message}</Text>}
          {data.msg_count > 0
            ? <View
                style={{
                position: 'absolute',
                right: 20,
                top: 25,
                backgroundColor: "#FF7E00",
                height: 20,
                width: 20,
                borderRadius: 10,
                alignItems: 'center',
                justifyContent: 'center'
              }}>
                <Text style={{
                  color: "#FFF"
                }}>{data.msg_count}</Text>
              </View>
            : null}
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {notification} = this.props.chat;
    //console.log(notification);
    return (
      <View style={{padding:10}}>
        <View style={{backgroundColor:"#f8f8f8",borderRadius:15}}>
        {this._renderChat(notification)}
        </View>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {chat: state.chat, auth: state.auth};
}

function mapDispatchToProps(dispatch) {
  return {
    chatActions: bindActionCreators(chatActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatSticky);

const mainStyles = StyleSheet.create({
  container: {
    //position:'absolute',
    width: dimen.width,
    //flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor:'#0008',
    top: 0,
    height: 50,
    backgroundColor: "#f00",
    zIndex: 44
  }
});
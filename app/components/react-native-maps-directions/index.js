'use strict'
import React, { Component } from 'react';
import { Linking } from 'react-native'
import ActionSheet from 'react-native-actionsheet';


const isValidLatLong = (num, range) => typeof num === 'number' && num <= range && num >= (-1 * range)
const isValidCoordinates = (coords) => isValidLatLong(coords.latitude, 90) && isValidLatLong(coords.longitude, 180)

const isValidSourceDest = (args) => {
  const validSource = args.source && isValidCoordinates(args.source)
  const vaidDestination = args.destination && isValidCoordinates(args.destination)
  return vaidDestination && validSource
}

const getParameterString = (params) => {
  let paramsStr = ''

  if (!params || !params.map) return paramsStr

  params.map(({key, value}) => {
    paramsStr += `&${key}=${value}`
  })

  return paramsStr
}


export default class GetDirections extends Component {
    constructor(props) {
        super(props);
        this.state = {
          ActionSheet: {

          },
          args: null,
          showopts: false
        };
        let asopts = [];
        let keyTrans = {
          'waze://' : 'Open in WAZE',
          'http://' : 'Open in Google maps',
          'maps://' : 'Open in iMap'
        };
        for(let key in navigator.activeMaps){
          asopts.push(keyTrans[navigator.activeMaps[key]]);
        }
        asopts.push('cancel');
        this.ActionSheetOptions={
            CANCEL_INDEX : (asopts.length - 1),
            DESTRUCTIVE_INDEX : (asopts.length - 1),
            options : asopts,
            title : 'Open map with',
        };
    }

    openAction(type){
      const {destination, source, params} = this.state.args;
      const paramsStr = getParameterString(params);
      if(type == 'maps://'){

        // http://maps.apple.com/?ll=<lat>,<long>
        //'maps://' + destination.latitude +','+ destination.longitude
        Linking.openURL('http://maps.apple.com/?ll='+destination.latitude+','+destination.longitude);
      }else if(type == 'http://'){
        Linking.openURL(`http://maps.google.com/maps?saddr=${source.latitude},${source.longitude}&daddr=${destination.latitude},${destination.longitude}${paramsStr}`);
      }else if(type == 'waze://'){
        Linking.openURL('waze://?ll=' + destination.latitude +','+ destination.longitude+'&navigate=yes');
      }
    }

    exec (args) {
      if (!isValidSourceDest(args)) {
        return Promise.reject(new Error('Invalid arguments provided'))
      }
      this.setState({args: args},()=>{
        this.doAll(args);
      });
      
  }

  doAll(args){
    const {destination, source, params} = args
    const paramsStr = getParameterString(params)
    if(navigator.activeMaps.length == 0){
      return Promise.reject(new Error(`Could not open the url: ${url}`))
    }else{
      if(navigator.activeMaps.length == 1){
        this.openAction(navigator.activeMaps[0]);
      }else{
          this.ActionSheet.show();
      // http://maps.google.com/maps?daddr=lat,long&amp;ll=
      }
    }
    // const url = `http://maps.google.com/maps?saddr=${source.latitude},${source.longitude}&daddr=${destination.latitude},${destination.longitude}${paramsStr}`

    // return Linking.canOpenURL(url).then((supported) => {
    //   if (!supported) {
    //     return Promise.reject(new Error(`Could not open the url: ${url}`))
    //   } else {
    //     return Linking.openURL(url)
    //   }
    // })
  }

  ActionOptions(i){
    this.openAction(navigator.activeMaps[i]);
  }

  render(){
      return <ActionSheet
            ref={o => this.ActionSheet = o}
            title={this.ActionSheetOptions.title}
            options={this.ActionSheetOptions.options}
            cancelButtonIndex={this.ActionSheetOptions.CANCEL_INDEX}
            destructiveButtonIndex={this.ActionSheetOptions.DESTRUCTIVE_INDEX}
            onPress={this.ActionOptions.bind(this)}
          />;
  }
}

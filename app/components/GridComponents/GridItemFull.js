import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, Image, View} from 'react-native';
import {colors} from '../../config/styles';
import images from '../../config/images';
//import styles from '../../config/genStyle';
import {SharedElementTransition} from 'react-native-navigation';
import {FORTAB,TABLANDSCAPE,TABPORTRAIT} from '../../config/MQ'; 
import {Column as Col, Row} from 'react-native-flexbox-grid';
import FaIcon from 'react-native-vector-icons/FontAwesome';

export default class GridItemFull extends Component {

  constructor(props) {
    super(props);
  }

  getDispPrice(number){
    if(!isNaN(parseFloat(number))){
        if(number >= 1000){
            let ret='';
            let factor= parseInt(number/1000);
            ret+=factor
            ret+=' ';
            ret+=('000' + (number - (factor*1000))).slice(-3);
            ret+=' €';
            return ret;
        }else{
            let oret='';
            oret+=number;
            oret+=' €';
            return oret;
        }
    }
  }

 render() {
     const {data, vkey, onPress, onFav, onChat, currentUser , searchType} = this.props;
     return(
     <Row  key={`grid_item_full_${vkey}`} size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_,styles.Prolistgray]}>
                        <Col sm={12} md={12} lg={12} style={[styles.proCol,styles.listCol]} >
                           <View style={styles.proList}>
                                <TouchableOpacity style={styles.canvasContainer} 
                                onPress={()=>onPress(data)} 
                                activeOpacity={0.8}>
                                {typeof data.video != 'undefined' && data.video != "" ?
                        (<Image style={styles.hasVideo} source={images.video} />):(null)}
                                    <SharedElementTransition sharedElementId={'SharedTextId'+data.id}>
                                    <Image
                                        style={[styles.proImg,styles.progridWidth,styles.proBigImg]}
                                        source={{uri:data.single_image}}
                                        />
                                    </SharedElementTransition>
                                    {data.user_type=='professional' ? 
                                    (<Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />):(null)}
                                    <View style={styles.absoluteBottomRight}>
                                        {data.is_urgent == 1 && 
                                            <Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}>y</Text>
                                        }
                                        {data.is_direct_sale == 1 && 
                                            <Text style={[styles.globalIcon,styles.inlineWithico,styles.red]}>w</Text> 
                                        } 
                                    </View>
                                </TouchableOpacity>
                                <View style={styles.proCaption}>
                                    <TouchableOpacity onPress={()=>onPress(data)} activeOpacity={0.8}>
                                    {searchType === 'trocs' ? (
                                            <View style={{justifyContent:'center',flex:1}}>
                                                <View style={{flexDirection:'row'}}>
                                                    <FaIcon name="mail-forward" color={'#34c24c'} size={15} style={{marginRight:5,marginTop:3}}/>
                                                    <Text style={[styles.proName,styles._F14_]} numberOfLines={1}>{data.title}</Text>
                                                </View>
                                                <View style={{flexDirection:'row'}}>
                                                    <FaIcon name="reply" color={'#2e5f9b'} size={15} style={{marginRight:5,marginTop:3}}/>
                                                    <Text style={[styles.proName,styles._F14_]} numberOfLines={1}>{data.title_contre}</Text>
                                                </View>
                                            </View>) 
                                        : 
                                        <Text style={[styles.proName,styles._F14_]} numberOfLines={1}>{data.title}</Text>
                                    }
                                    </TouchableOpacity>
                                    <View style={styles.bottomDetail}>
                                        {/* <View style={styles.priceView}> */}
                                <Text style={[styles.priceText]}>{(data.price)}</Text>
                                <Text style={[styles.globalIcon,styles.camIco,styles.big]}>{'['}</Text>
                                <Text style={styles.bold}>{data.totalImageCount}</Text>
                            {/* </View> */}
                            {/* <View style={styles.camView}>
                                <Text style={[styles.globalIcon, styles.camIco]}>s</Text> 
                                <Text style={[styles.abscamtxt]}>{data.totalImageCount}</Text>
                            </View> */}
                            <View style={{flex:1}}></View>
                            <View style={styles.userChat}>
                                <Text style={data.havefave == 1 ? [styles.globalIcon, styles.smallIco, styles.orangeColor] : [styles.globalIcon, styles.smallIco]} onPress={()=>onFav(vkey,data.havefave)}>{data.havefave == 1 ? 'n' : 'm'}</Text>
                            </View>
                            {data.user_id  != currentUser.id ? 
                                (<TouchableOpacity
                                    style={styles.userChat}
                                    activeOpacity={0.8}
                                    onPress={()=>onChat(data)}>
                                    <Text style={[styles.globalIcon, styles.smallIco]}>f</Text>
                                </TouchableOpacity>):(null)}
                                {data.user_id  != currentUser.id ? 
                                (<View style={[styles.chatCircle, data.userOnline == 1 ? styles.userOnline : null]}></View>):(null)}
                                        {/* <View style={styles.leftAlign}>
                                            <Text style={[styles.orangeColor,styles.bold,styles._F14_,styles._MR10_]}>{data.price}{' €'}</Text>
                                            <Text><Text style={[styles.globalIcon,styles.camIco,styles.big]}>{'['} </Text>
                                            <Text style={styles.bold}>{data.totalImageCount}</Text></Text>
                                        </View>    
                                       <View style={[styles.inlineDatarow,styles.justRight]}>
                                            <View style={[styles.inlineDatarow]}>
                                                <View style={styles._MR5_}>
                                                    <Text style={data.havefave==1 ? [styles.globalIcon,styles.xlIco,styles.orangeColor]:[styles.globalIcon,styles.xlIco]} 
                                                    onPress={()=>onFav(key,data.havefave)}>{data.havefave==1?'n':'m'}</Text>
                                                </View>
                                                <TouchableOpacity style={styles.userChat} activeOpacity={0.8} onPress={()=>onChat(data.id)}>
                                                    <View style={[styles.relative]}>
                                                        <Text style={[styles.globalIcon,styles.xlIco]}>f</Text>
                                                        <Text style={[styles.circle,styles.online,styles.absCircle]}></Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View> 
                                        </View>     */}
                                    </View>
                                </View>
                            </View>  
                        </Col>
            </Row>
    );
  }
}

const styles = StyleSheet.create({
     proRow:{
        flexDirection: 'row',
        //flexWrap: 'wrap',
        //position:'relative',
        marginLeft:0,
        marginRight:0,
        //backgroundColor:'#fff',
        //flex:1,
    },
    proCol: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 8,
        position: 'relative',
        backgroundColor: 'rgba(0,0,0,0)'
    },
    proList: {
        borderWidth: 1,
        borderColor: '#ddd',
        backgroundColor: '#fff',
        elevation: 3
    },
    canvasContainer: {
        flex: 1,
        alignItems: 'stretch',
        padding: 0,
        position: 'relative'
    },
    imgInner: {
        position: 'relative',
        flex: 1
    },
    proImg: {
        width: null,
        resizeMode: 'cover',
        height: TABLANDSCAPE ? 135 : TABPORTRAIT ? 135 : 110,
        backgroundColor:"#eee",
    },
    progridWidth: {
        height: 151,
        width: null,
        resizeMode: 'cover'
    },
    proSign: {
        width: 35,
        height: 20
    },
    proSignAbsolute: {
        position: 'absolute',
        right: 0,
        paddingTop: 5,
        width: 20,
        height: 10
    },
    absoluteBottomRight: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        position: 'absolute',
        right: 0,
        bottom: 0,
        flexDirection: 'row',
        paddingVertical:5,
    },
    globalIcon: {
        fontFamily: 'icomoon',
        fontSize: 12,
        justifyContent: 'center'
    },
    red: {
        color: '#ff0000'
    },
    proCaption: {
        padding:5,
        backgroundColor: "#FFF",
        //width: null,
        //alignSelf: 'stretch'
    },
    proName: {
        fontSize: FORTAB ? 14 : 12,
        paddingBottom: 0,
        color: '#333',
        fontFamily: 'Montserrat',
        //flex: 1,
    },
    bottomDetail: {
        //flex: 1,
        paddingTop:5,
        flexDirection: 'row',
        //justifyContent: 'space-between',
    },
    inlineDatarow: {
        flexDirection: 'row',
        alignItems: 'center',
        //alignSelf: 'stretch',
        overflow: 'visible'
    },
    priceText: {
        fontWeight:'600',
        fontSize:14,
        color: '#f15a23'
    },
    rightAlign: {
        top: 2,
        right: 0
    },
    camIco: {
        fontSize: 20,
        //zIndex: -1,
        paddingHorizontal:5,
        paddingBottom:3,
        width: 30,
        //position: 'absolute',
    },
    abscamtxt: {
        backgroundColor: "#0000",
        fontWeight:'600',
        fontSize:FORTAB ? 12:10
    },
    small: {
        fontSize: FORTAB ? 12 : 10
    },
    smallIco: {
        fontSize: FORTAB ? 22: 20
    },
    xlIco: {
        fontSize: FORTAB? 22: 20
    },
    orangeColor: {
        color: '#f15a23'
    },
    userChat: {
        width:26,
        marginLeft:5,
        //flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    chatCircle: {
        position:'absolute',
        width:10,
        height:10,
        borderRadius:5,
        backgroundColor:'#ccc',
        right:0,
        top:3,
        zIndex:1,
    },
    userOnline:{
        backgroundColor:'#72bb53',
    },
    priceView:{
        //flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
    },
    camView:{
        width:20,
        marginLeft:8,
        marginBottom:3,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    rightView:{
        flex:2,
        flexDirection: 'row',
        alignItems: 'center',
        overflow: 'visible',
        justifyContent:'flex-end'
    },
    flex:{
        flex:1
    },
    bold:{
        fontWeight:'600',
    },
    hasVideo:{
        width:20,
        height:20,
        position:'absolute',    
        left:3,
        //opacity:0.8,
        // top:-4,
        zIndex:1,
    },
});
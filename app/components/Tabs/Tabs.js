import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,TouchableOpacity,ActivityIndicator,Dimensions,ScrollView,RefreshControl,Platform
} from 'react-native';
//import styles from '../../config/genStyle';

export default class Tabs extends Component {

  constructor(props) {
    super(props);
    this.height = Dimensions.get('window').height;
    this.width = Dimensions.get('window').width;
    this.state = {
        selected:typeof this.props.defaultTabIndex != 'undefined' ?  this.props.defaultTabIndex:''
    };
  }

  componentDidMount() {
    this.height = Dimensions.get('window').height;
    this.width = Dimensions.get('window').width;
  }

  getFixWidth = () => {
    return (this.width / 3) - 20;
  };

  calculateWidth(len){
      let nWidth;
      if(len > 1){
        nWidth=(this.width/len)-(len*1);
      }else{
        nWidth=(this.width/3)-(len*1);
      }
      return {width:nWidth}
  }
tabchange(i){
    this.setState({selected:i},()=>this.props.onTabPress(i));
}

setTab(i){ 
  this.setState({selected:i});
}
removeTab(i){
  this.setState({selected:i-1},()=>{
    this.forceUpdate();
  });
}
onLayoutCb(e){
  //console.log(e.nativeEvent);
  // this.height=e.nativeEvent.layout.height;
  // this.width=e.nativeEvent.layout.width;
}

renderTabs(){
  const {containerStyle,tabList,onTabPress,Scroll}=this.props;
  const {selected}=this.state;
     return(
      <View style={[styles.tabParent,containerStyle]} onLayout={(e)=>{this.onLayoutCb(e)}}>
                <View style={styles.botomOrangeLine}></View>  
                <View style={styles.tabs}>
                    {tabList.map((data,i)=>{
                        return selected == i ? 
                        <TouchableOpacity key={i} 
                        style={[styles.tabSelected,this.calculateWidth(tabList.length)]} 
                        onPress={()=>{this.tabchange(i)}} 
                        activeOpacity={0.8} >
                            <Text numberOfLines={2} style={[styles.tabTextSelected,styles.orangeColor]}>{data}</Text>
                        </TouchableOpacity>:
                        <TouchableOpacity key={i} 
                        style={[styles.tabnotSelected,this.calculateWidth(tabList.length)]} 
                        onPress={()=>{this.tabchange(i)}} 
                        activeOpacity={0.8}>
                            <Text numberOfLines={2} style={[styles.tabText]}>{data}</Text>
                        </TouchableOpacity>;
                    })}
                    {tabList.length == 1 ? (
                      <View style={{
                        flex:1,
                      }}>
                      </View>
                    ):(null)}
                </View>
            </View>
    );
  
}


renderScrollTab(){
  const {containerStyle,tabList,onTabPress,Scroll}=this.props;
  const {selected}=this.state; 
     return(
       <ScrollView contentContainerStyle={[styles.tabParent,containerStyle,styles.defaultScroll]} 
      automaticallyAdjustContentInsets={false}
      showsHorizontalScrollIndicator={false}
      horizontal
      onLayout={(e)=>{
        this.onLayoutCb(e)
         }}>
                <View style={[styles.botomOrangeLine,{bottom:Platform.OS === 'ios' ? 2: 1}]}></View>
                <View style={[styles.tabs,{flex:1}]}>
                    {tabList.map((data,i)=>{
                        return selected == i ? 
                        <TouchableOpacity key={i} 
                        style={[styles.tabSelected,Platform.OS === 'ios' ? {zIndex:2}:null,{minWidth:this.getFixWidth(),marginRight:5}]} 
                        onPress={()=>{this.tabchange(i)}} 
                        activeOpacity={0.8} >
                            <Text numberOfLines={2} style={[styles.tabTextSelected,styles.orangeColor]}>{data}</Text>
                        </TouchableOpacity>:
                        <TouchableOpacity key={i} 
                        style={[styles.tabnotSelected,{minWidth:this.getFixWidth(),marginRight:5}]} 
                        onPress={()=>{this.tabchange(i)}} 
                        activeOpacity={0.8}>
                            <Text numberOfLines={2} style={[styles.tabText]}>{data}</Text>
                        </TouchableOpacity>;
                    })}
                    {tabList.length < 4 ? (
                      <View style={{
                        flex:1,
                        }}>
                      </View>
                    ):(null)} 
                 </View>
            </ScrollView>
     );
}

 render() {
     const {Scroll}=this.props;
      return Scroll ? this.renderScrollTab() : this.renderTabs();
  }
}

const styles =StyleSheet.create({
  defaultScroll:{
    flexGrow:1,
    paddingVertical:2
  },
  tabParent:{
    //flex:1,
    //height:40,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor: '#f8f8f8',
    marginBottom:4,
    //borderBottomWidth:1,
    //borderBottomColor:"#f15a23"
  },
  tabs:{
    //flex:1,
    alignSelf:'stretch',
    flexDirection: 'row',
    alignItems:'center',
    //backgroundColor: '#f8f8f8',
    justifyContent:'space-between',
  },
  tabSelected:{
    alignItems:'center',
    justifyContent:'center',
    //position:'relative',
    paddingVertical:10,
    justifyContent:'center',
    backgroundColor:"#fff",
    //zIndex:2,
    backgroundColor:'#fff',
    //alignSelf:'stretch',
    borderTopColor:'#f15a23',
    borderLeftColor:'#f15a23',
    borderRightColor:'#f15a23',
    borderBottomColor:'#f8f8f8',
    borderTopWidth:1,
    borderLeftWidth:1,
    borderRightWidth:1,
   // borderWidth:1,
    borderBottomWidth:1,
    //top:1,
  },
  tabnotSelected:{
    //paddingLeft:5,
    //paddingRight:5,
    paddingVertical:10,
    borderColor:'#ddd',
    backgroundColor:'#eee',
    borderWidth:1,
    alignItems:'center',
    justifyContent:'center',
    borderBottomColor:'#f15a23',
    borderBottomWidth:1,
    //position:'relative',
    //top:1,
    zIndex:0,
  },
  botomOrangeLine:{
    width:'100%',
    height:1,
    backgroundColor:'#f15a23',
    position:'absolute',
    bottom:0,
    left:0,
    right:0,
    zIndex:0,
  },
  tabText:{
    fontSize:9,
    //fontSize:FORTAB ? 16:9,  
    fontFamily:'Montserrat',
    textAlign:'center',
    //paddingLeft:5,
    //paddingRight:5,
    //paddingTop:10,
    //paddingBottom:10,
    //borderColor:'#ddd',
    //backgroundColor:'#eee',
    //alignSelf:'stretch',
    //borderWidth:1,
  },
  tabTextSelected:{
    //fontSize:FORTAB ? 16:9,  
    fontSize:9,
    textAlign:'center',
    //paddingLeft:5,
    //paddingRight:5,
    //paddingTop:10,
    //paddingBottom:10,
    //backgroundColor:'rgba(0,0,0,0)',
    
    fontFamily:'Montserrat',
  },
  orangeColor:{
        color:'#f15a23',
  },
});
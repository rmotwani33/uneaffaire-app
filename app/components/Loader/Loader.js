import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,Dimensions
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
const dimen=Dimensions.get('window');

export default class Loader extends Component {

  constructor(props) {
    super(props);
  }

 render() {
     return(
     <View style={styles.container}>
        <View style={styles.iconContainer}>
            <Image style={styles.imagestyle} source={images.loader} />
        </View>
    </View>
    );
  }
}

const styles= StyleSheet.create({
    container:{
         position:'absolute',
         width:dimen.width,
         height:dimen.height,
         flex:1,
         alignItems:'center',
         justifyContent:'center',
         backgroundColor:'#0008'
    },
    iconContainer:{
        backgroundColor:"#fff",
        height:70,
        width:70,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10,
    },
    imagestyle:{
        height:63,
        width:50
    }
});
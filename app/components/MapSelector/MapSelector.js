import React, { Component } from 'react';
import {
  AppRegistry, SectionList, StyleSheet, Image,Text, TextInput, View,TouchableOpacity,Modal,ActivityIndicator,Dimensions,Platform
} from 'react-native';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import MapView from 'react-native-maps';
import images from '../../config/images';
import oauthconfig from '../../config/oauthconfig';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
const {height,width}=Dimensions.get("window");
const api= new ApiHelper;
export default class MapSelector extends Component {
constructor(props) {
    super(props);
    this.state={
        visible:false,
        load:false,
        address:'',
        region: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
    }
}

openMap(){
    this.setState({visible:true});
}
closeModal(){
    this.setState({visible:false});
}

onRegionChangeComplete = (region) =>{
    //console.log(region);
    this.setState({region:region},()=>{
        api.xConsole(region.latitude+" "+region.longitude);
        if(!this.state.load){
            this.setState({load:true},()=>{
                this.getPlaces(region.latitude,region.longitude).then((results)=>{
                    api.xConsole(results)
                    if(results.status === "OK"){
                        this.setState({load:false,address:results.results[0].formatted_address,region:region});
                    }else{
                        this.setState({load:false,region:region});
                    }
                }).catch((error)=>{
                    api.xConsole(error);
                    this.setState({load:false,region:region});
                });
            });
        }else{
            //this.setState({region:region});
        }
    });
   
    
}

getAddress(){
    return {
        address:this.state.address,
        lat:this.state.region.latitude,
        log:this.state.region.longitude,
    }
}


getPlaces = (lat,log) =>{
    return new Promise((resolve, reject) => {
        api.xConsole('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+log+'&key='+oauthconfig.googleMapKey);
        fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+log+'&key='+oauthconfig.googleMapKey).then((response) => response.json()).then((responseJson) => {
            resolve(responseJson);
        }).catch((error)=>{
            reject(error);
        });
    });
}

render() {
    const {onPress,containerStyle} = this.props;
     return(
        <View>
            {/* <TextInput
                underlineColorAndroid="transparent"
                style={[styles.valueText]}
                placeholder={"Ville ou code postal...."}
                value={this.state.address}
                defaultValue={this.props.defaultValue}
                autoCapitalize={'none'} autoCorrect={false}
                maxLength={this.props.maxLength}
                onFocus={() => this.openMap()}
                //onBlur={() => this.unsetFocus()}
                //onChangeText={(value) => this.setText(value)}
            /> */}

            <FloatLabelTextInput
                        editIco={'t'}
                        value={this.state.address}
                        placeholder={"Ville ou code postal...."}
                        onFocus={() => this.openMap()}
                        underlineColorAndroid={'#eee'}/>
            <Modal animationType={"slide"} transparent={false} visible={this.state.visible} onRequestClose={() =>this.closeModal()}>
                <View style={styles.container}>
                    <TouchableOpacity style={styles.absMarker}>
                    {/* <Image  style={styles.marker} source={images.marker} /> */}
                        </TouchableOpacity>
                    <MapView style={{flex:1}} region={this.state.region} onRegionChangeComplete={this.onRegionChangeComplete} >
                        {/* <MapView.Marker style={styles.absMarker}/> */}
                    </MapView>
                    <View style={styles.bottomView}>
                        <TouchableOpacity style={styles.button} activeOpacity={0.8} onPress={this.state.address != '' ? () =>this.closeModal(): null}>
                            <Text numberOfLines={1} style={styles.buttonText}>{this.state.address != '' ?this.state.address: "Select Location"}</Text>
                        </TouchableOpacity>
                    </View>
                    {/* <View style={{height:50,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                        <TouchableOpacity onPress={() =>this.modalShowHide()}>
                            <Text style={{ fontFamily:'icomoon',width:30,textAlign:'center',color:'#FD5009',fontSize:16}}>5</Text>
                        </TouchableOpacity>
                        <Text style={[styles.title]}>Choose category</Text>
                        <Text style={{width:30}}></Text>
                    </View> */}
                    {/* <SectionList
                    sections={this.state.SectionList}
                    renderItem={({item}) => <TouchableOpacity style={styles.item} onPress={()=>this.onItemPress(item)}><Text>{item.name}</Text></TouchableOpacity>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    /> */}
                </View>
            </Modal>
        </View>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    //paddingTop: 22
},
 valueText: {
    flex:1,
    height: (Platform.OS == 'ios' ? 20 : 60),
    fontSize: 16,
    color: '#333'
  },
bottomView:{
    height:60,
    backgroundColor:"#0000"
},
button:{
    flex:1,
    margin:10,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:"#ddd",
    borderRadius:5,
    padding:5
},
buttonText:{
    color:"#666",
    fontSize:14
},
absMarker:{
    position:'absolute',
    zIndex:2,
    //height:24,
    //width:24,
    top:(height-60)/2,
    left:width/2
},
marker:{
    height:24,
    width:24,
}
})
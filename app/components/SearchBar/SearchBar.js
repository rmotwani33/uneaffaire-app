import React, { Component } from 'react';
import {
  StyleSheet,
  Text,TouchableOpacity,ActivityIndicator
} from 'react-native';
import styles from '../../config/genStyle';

export default class SearchBar extends Component {

  constructor(props) {
    super(props);
  }

 render() {
     const {onPress,load,text,textStyle,BtnStyle,loadColor}=this.props;
     return(
     <TouchableOpacity style={[styles.justCenter,mainStyle.btnheight,BtnStyle]} onPress={!load ? onPress:null} activeOpacity={0.85}>
        {load ? <ActivityIndicator animating={true} size="small" color={loadColor ? loadColor:"#fff"}/>
      : <Text style={[mainStyle.text,textStyle]}>{text}</Text> } 
     </TouchableOpacity>
    );
  }
}

const mainStyle =StyleSheet.create({
btnheight:{
  height:40,
  backgroundColor:'#f15a23',
  borderRadius:3,
},
text:{
  color:'#fff',
  fontSize:18,
  fontFamily:'Montserrat',
}
});
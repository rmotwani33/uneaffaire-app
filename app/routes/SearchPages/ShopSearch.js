
import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,Keyboard,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,ActivityIndicator
} from 'react-native';
import Select from '../../components/Select/Select';
import { colors } from '../../config/styles';
import styles from '../../config/genStyle';
import CheckBox from '../../lib/react-native-check-box';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import ModalCat from '../../components/ModalCat/ModalCat';
import _ from 'lodash';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const api = new ApiHelper;

class ShopSearch extends Component {
//   static navigatorButtons = {
//      leftButtons: [{
//        icon: images.back,
//        id: 'back',
//        title:'back to welcome',
//      }],
//   };

  
static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
    //tabBarHidden:true,
};

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    let searchStr = '';
    let seactIntit  = false;
    let isUrgent = false;
    if(typeof this.props.params != 'undefined'){
        if(typeof this.props.params.search_string != 'undefined'){
            searchStr = this.props.params.search_string;
        }else{
            searchStr = '';
        }
    }else{
        searchStr = '';
    }
    if(typeof this.props.params != 'undefined'){
        if(typeof this.props.params.search_in_title != 'undefined'){
            seactIntit = this.props.params.search_in_title;
        }else{
            seactIntit = false;
        }
    }else{
        seactIntit = false;
    }
    if(typeof this.props.params != 'undefined'){
        if(typeof this.props.params.search_urgent != 'undefined'){
            isUrgent = this.props.params.search_urgent;
        }else{
            isUrgent = false;
        }
    }else{
        isUrgent = false;
    }
    this.state={
        scrollY: new Animated.Value(0),
        token:this.props.auth.token,
        searchInTitle:seactIntit,
        urgent:isUrgent,
        searchTxt:searchStr,
        catId:'',
        catLoad:false,
        attributeDataArray:[],
        dropdownData:{},
        location:{},
        in_history:true,
        load1:false,
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}
componentWillMount(){
    
    iconsLoaded.then(()=>{});
}
onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
    this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: false
    });
    this.props.navigator.toggleTabs({
        animated: false,
        to: 'hidden'
     });
    this.props.navigator.setStyle({navBarTitleTextCentered: true,drawUnderTabBar:true});
    this.props.navigator.setTitle({title: "Recherche rapide"});
    this.props.navigator.setButtons({
         leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title:'back to welcome',
        }],animated:false});      
    }
    if(event.id=='didAppear'){
        Keyboard.dismiss();
        // typeof this.props.dataSearch != 'undefined' ? 
        // this.setState({
        //     searchTxt:typeof this.props.dataSearch.search_string != 'undefined'? this.props.dataSearch.search_string :typeof this.props.dataSearch.searchTxt != 'undefined'? this.props.dataSearch.searchTxt : '',
        //     searchInTitle:typeof this.props.dataSearch.search_in_title != 'undefined' ?this.props.dataSearch.search_in_title:typeof this.props.dataSearch.searchInTitle != 'undefined' ?this.props.dataSearch.searchInTitle:'' ,
        //     urgent:typeof this.props.dataSearch.search_urgent != 'undefined' ? this.props.dataSearch.search_urgent:typeof this.props.dataSearch.urgent != 'undefined' ? this.props.dataSearch.urgent:''
        // }):'';
    }
    if(event.id == 'back2'){
        this.props.navigator.pop({
            animated: true // does the pop have transition animation or does it happen immediately (optional)
        });
    }
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }   
}

navigateTo(page,data){
    data.id = data.ad_id;
    if(page=='ProductDetail'){
        this.props.navigator.push({
            screen: page,
            sharedElements: ['SharedTextId'+data.id],
            passProps:{
                data:data,
                token:this.state.token, 
                userdata:this.props.auth.userdata,
            }            
        });
    }
}
goto(page){
    Keyboard.dismiss();
    let params = {
        search_string:this.state.searchTxt,
        search_urgent:this.state.urgent,
        search_in_title:this.state.searchInTitle,
        category_id:this.state.catId,
        category_filter:this.state.dropdownData, 
    }
    //console.log(params);
    let data={
        UserDetail:this.props.UserDetail
    }
    this.props.navigator.push({
        screen: page,
        passProps:{
            token:this.state.token,
            userdata:this.props.auth.userdata,
            data:data,   
            params:params    
        }
    });
    
}
// selectCat(item) {
//     //console.log(item);
//     this.setState({
//         catId: item
//     }, () => {
//         this.loadAttributes(item);
//     });
// }
// loadAttributes(item) {
//     let request = {
//         url: settings.endpoints.getAttributes,
//         method: 'POST',
//         params: {
//             category_id: item,
//             token: this.props.auth.token
//         }
//     }
//     this.setState({catLoad:true,dropdownData:{},attributeDataArray:[]},()=>{
//         api.FetchData(request).then((results) => {
//             let attributeData = [];
//             if (results.status) {
//                Object.keys(results.attrList).map((key) => {
//                if (results.attrList[key] != "") { 
//                        let attrib = {};
//                        attrib.id = results.attrList[key].name_lower;
//                        attrib.type = results.attrList[key].type;
//                        attrib.name = results.attrList[key].name;  
//                        attrib.num_of_drop=false;               
//                        if (results.attrList[key].type == 'dropdown') {
//                            if(results.attrList[key].predef != ''){
//                                 attrib.options = results.attrList[key].predef;
//                                 if(typeof results.attrList[key].predef[0] != 'undefined' && typeof results.attrList[key].predef[0].subAttr != 'undefined'){
//                                        attrib.hasSub = true;
//                                    }else{
//                                        attrib.hasSub = false;
//                                    }
//                            }else if(typeof results.attrList[key].incremental_element != 'undefined' && results.attrList[key].incremental_element != ''){
//                                let p = results.attrList[key];                                
//                                attrib.options = this.looping(p.min,p.max,p.incremental_element);
//                                attrib.num_of_drop=true; 
//                            }  
//                        }
//                        attributeData.push(attrib);
//                    }
//                });
//             }
//             this.setState({attributeDataArray: attributeData,catLoad:false});
//        })
//        .catch((error) => {
//           this.setState({catLoad:false});
//            //console.log(122);
//        })
//     })
        
// }
_renderSubCategoryDropDown(attribute,selectedParent){
    let subattr=[];
    let subID='';
    let subName='';
    attribute.options.map((data,i)=>{
        if(data.id === selectedParent){
            subattr = data.subAttr;
            subID = data.sub_id;
            subName= data.sub_name_lower
        }
    });

    return subattr.length > 0 && subID != '' && subName != ''? <Select vkey={selectedParent} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
            optionsList={subattr}
            header={'sélectionner'}
            selectedid={typeof this.state.dropdownData[subName] != 'undefined' ? this.state.dropdownData[subName]:''}
            onPress={(item) => {
            let dropdownData= this.state.dropdownData;
            dropdownData[subName] = item;
            this.setState({dropdownData: dropdownData}); 
            }}/> : null;
}
selectCat(item) {
    //console.log(item);
    this.setState({
        catId: item,dropdownData:{}
    }, () => {
        this.loadAttributes(item);
    });
}
loadAttributes(item) {
    let request = {
        url: settings.endpoints.getAttributes,
        method: 'POST',
        params: {
            category_id: item,
            token: this.props.auth.token
        }
    }
    this.setState({catLoad:true,attributeDataArray:[]},()=>{
        api.FetchData(request).then((results) => {
            let attributeData = [];
            if (results.status) {
               Object.keys(results.attrList).map((key) => {
               if (results.attrList[key] != "") { 
                       let attrib = {};
                       attrib.id = results.attrList[key].name_lower;
                       attrib.type = results.attrList[key].type;
                       attrib.name = results.attrList[key].name;  
                       attrib.num_of_drop=false;               
                       if (results.attrList[key].type == 'dropdown') {
                            if(results.attrList[key].num_of_drop === 2) attrib.num_of_drop=true;  
                            if(results.attrList[key].predef != ''){
                                attrib.options = results.attrList[key].predef;
                                if(typeof results.attrList[key].predef[0] != 'undefined' && typeof results.attrList[key].predef[0].subAttr != 'undefined'){
                                       attrib.hasSub = true;
                                   }else{
                                       attrib.hasSub = false;
                                   }
                            }else if(typeof results.attrList[key].attr_array != 'undefined' && results.attrList[key].attr_array) {
                                attrib.options =  this.setLoopData(results.attrList[key].attr_value_array);
                            }else if(typeof results.attrList[key].incremental_element != 'undefined' && results.attrList[key].incremental_element != ''){
                                let p = results.attrList[key];      
                                if(results.attrList[key].name ==='Année')                    
                                    attrib.options = (this.looping(p.min,p.max,p.incremental_element)).reverse();
                                else attrib.options = this.looping(p.min,p.max,p.incremental_element);
                            }  
                       }
                       attributeData.push(attrib);
                   }
               });
            }
            this.setState({attributeDataArray: attributeData,catLoad:false});
       })
       .catch((error) => {
          this.setState({catLoad:false});
           //console.log(122);
       })
    });
}

setLoopData(arrayData){
    let ary = [];
    if(_.isArray(arrayData) && !_.isEmpty(arrayData)) {
        arrayData.map((value)=>{
            ary.push({
                id:value,
                value:value
            });
        })
    }
    return ary;
}

looping(min,max,inc){
    let ary = [];
    for(i=min;i<=max;i+=inc){
        ary.push({
            id:i,
            value:i
        });
    }
    return ary;    
}

render() {
    const headerTranslate =  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    let _this = this; 

    return ( <View style={{flex:1}}>
            <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
                    <View >
                       <View style={[styles.productdataMain,styles._HP15_,styles._PT0_]}> 
                            <Animated.View style={[styles.searchHolder,styles.searchAdvanceIco,{
                                    transform: [{ translateY: headerTranslate },
                                    {
                                        scale: this.state.scrollY.interpolate({
                                            inputRange: [-50 , 0, 50],
                                            outputRange: [2, 1, 1]
                                        })
                                    }]
                                },
                            ]}>
                                <View style={styles.searchinputHolder}>
                                    <TextInput style={[styles.searchInput,styles._PL0_]} placeholder={"Rechercher..."} underlineColorAndroid={'#eee'}
                                    onChangeText={(text)=>this.setState({searchTxt:text})}
                                    value={typeof this.state.searchTxt != 'undefined' ? this.state.searchTxt:''}
                                />
                                </View>
                                <TouchableOpacity style={styles.icoholderRight} onPress={()=>this.state.searchTxt !=''? this.setState({searchTxt:''}) :null
                                //this.goto('ProductgridContainer',this.state.in_history,{})
                            } >
                            <Icon name={this.state.searchTxt =='' ? "search":'close'} color={'#888'} size={18} />
                            </TouchableOpacity>
                            </Animated.View>
                        <View style={[styles.row,styles._MT15_]}>
                                <CheckBox style={[styles.genChk]}
                                    isChecked={this.state.searchInTitle} 
                                    onClick={(checked) =>{this.setState({searchInTitle:checked})}}
                                    labelStyle={styles.midum}
                                    rightText={'Rechercher dans le titre'}
                                />
                        </View>
                        <View style={styles.accountdataBlock}>
                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                        <CheckBox style={[styles.genChk]}
                                            isChecked={this.state.urgent} 
                                            onClick={(checked) =>{this.setState({urgent:checked});}}
                                            labelStyle={styles.midum}
                                            rightText={'Annonces urgentes'}
                                        />
                                        <Text style={[styles.globalIcon,styles.small,styles.red]}> y</Text>
                                </View>
                            </View>  
                        </View>
                        <View style={styles.accountdataBlock}>
                                            {/* <View style={[styles.inlineDatarow,styles.spaceBitw,styles.categoryTitle]}>
                                                <ModalDropdown defaultValue={'select one'} textStyle={[styles.stretch,styles._F16_,{width:width-40}]} options={
                                                    {key1:'Catégorie1', key2:'Catégorie2',key3:'Catégorie3', key4:'Catégorie4'}
                                                    } 
                                                onSelect={this.onSelect.bind(this)}/>
                                                <Text style={[styles.globalIcon]}>6</Text>
                                            </View> */} 
                                                <ModalCat SectionList={this.props.auth.categoryList} containerStyle={{flex: 1}} onPress={(item)=>this.selectCat(item)}/>

                                                {this.state.catId !='' ? (
                                        <View style = {[styles._MT10_]}>
                                        {this.state.catLoad ? (
                                        <ActivityIndicator animating={true} color={colors.navBarButtonColor} size="small"/>) : (this.state.attributeDataArray.length > 0
                                        ? (this.state.attributeDataArray.map((attribute, i) => {
                                        if(attribute.num_of_drop){
                                            return (
                                            <View style={{flex:1,flexDirection:'row'}} key={i}>
                                                <View style={{flex:1,padding:7,paddingBottom:0}}>
                                                <Select vkey={i} containerStyle={{ backgroundColor: "#fff"}}
                                                    optionsList={attribute.options}
                                                    header={attribute.name+" min"}
                                                    showClose = {true}
                                                    selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id][0]:''}
                                                    onPress={(item) => {
                                                        let dropdownData = this.state.dropdownData;
                                                        //let obj={min:0,max}
                                                        if(dropdownData[attribute.id] === undefined){
                                                            dropdownData[attribute.id]=[];
                                                        }
                                                        dropdownData[attribute.id][0] = item;
                                                        this.setState({dropdownData: dropdownData});
                                                }}/>
                                                </View>
                                                <View style={{flex:1,padding:7,paddingBottom:0}}>
                                                <Select vkey={i} containerStyle={{ backgroundColor: "#fff"}}
                                                    optionsList={attribute.options}
                                                    header={attribute.name+" max"}
                                                    showClose = {true}
                                                    selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id][1]:''}
                                                    onPress={(item) => {
                                                        let dropdownData= this.state.dropdownData;
                                                        if(dropdownData[attribute.id] === undefined){
                                                            dropdownData[attribute.id]=[];
                                                        }
                                                        dropdownData[attribute.id][1] = item;
                                                        this.setState({dropdownData: dropdownData}); 
                                                }}/>
                                                </View>
                                            </View>)
                                        }else{
                                            return (
                                            <View style={{paddingHorizontal:7}} key={i}>
                                                <Select vkey={i} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                                                    optionsList={attribute.options}
                                                    header={attribute.name}
                                                    showClose = {true}
                                                    selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id]:''}
                                                    onPress={(item) => {
                                                        let dropdownData= this.state.dropdownData;
                                                        dropdownData[attribute.id] = item;
                                                        this.setState({dropdownData: dropdownData}); 
                                                }}/>
                                                {attribute.hasSub && typeof this.state.dropdownData[attribute.id] != 'undefined' ? (
                                     this._renderSubCategoryDropDown(attribute,this.state.dropdownData[attribute.id])
                                    ):(null)}
                                            </View>)
                                        }
                                        
                                            }))
                                            :(<View><Text>No Attributes Found</Text></View>
                                            ))
                                            }
                                            </View>
                                            ):(null)} 
                                        </View>
                    </View>
                </ScrollView>
                <View style={styles.bottomArea}>
                    <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8} onPress={()=>this.goto('UserAnnonces')}>
                        <Text style={[styles.genButton]}>Lancer la recherche</Text>
                    </TouchableOpacity>
                </View>
            </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopSearch);

import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
import {Navigation,Screen} from 'react-native-navigation';
import CheckBox from 'react-native-check-box';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
export default class AdvanceSearch extends Component {
//   static navigatorButtons = {
//      leftButtons: [{
//        icon: require('../../images/back.png'),
//        id: 'back',
//        title:'back to welcome',
//      }],
//   };

  
// static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
//     navBarTextFontFamily: colors.navBarTextFontFamily,
// };

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        like:1,
        scrolled:true,
        scrollY: new Animated.Value(0),
        value:1,
        value1:1,
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

 onNavigatorEvent(event) {
    //console.log(event)
    this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: true
    });
    if(event.id == 'back'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }

    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title: "Recherche avancée",

    });
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }
   
  }

  goto(page){   
        this.props.navigator.push({
        screen: page
        }); 
  }
  tabchange(tab){
        this.setState({tabs:tab,tabSelected:tab});
  }
 handleOnPress(value){
        this.setState({value:value})
 }
 handleOnPress1(value1){
        this.setState({value1:value1})
 }
handleOnPress2(value2){
    this.setState({value2:value2})
}
handleOnPress3(value3){
    this.setState({value3:value3})
}

  onScroll(event){
        var currentOffset = event.nativeEvent.contentOffset.y;
        var direction = currentOffset > this.offset ? this.setState({scrolled:true}) : this.setState({scrolled:false});
        this.offset = currentOffset;       
}

gettrans(bool){
if(bool){
      return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    }else{
       return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [-50, 0],
        extrapolate: 'clamp',
    });  
    }
}
  render() {
    
    const headerTranslate =  this.state.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });
        let type =  <View style={[styles.inlineDatarow,styles._MLR5_,styles._MT10_]}><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View><View style={styles.proThumbMain}><Image style={styles.proThumb} source={images.homestay} /></View></View>;

     // Screen.togg(this, {})
    return (
        <View style={[styles.main,styles._HP15_]}>
                {/* <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                        <View style={styles.scrollInner}>
                            <View style={styles.myAccountData}>
                                <Animated.View style={[styles.searchHolder,{
                                        transform: [{ translateY: headerTranslate },
                                        {
                                            scale: this.state.scrollY.interpolate({
                                                inputRange: [-50 , 0, 50],
                                                outputRange: [2, 1, 1]
                                            })
                                        }]
                                    },
                                ]}>
                                    <View style={styles.icoholderRight}>
                                        <Text style={[styles.genIco,styles.searchIco]}>v</Text>
                                    </View>
                                    <View style={styles.searchinputHolder}>
                                        <TextInput style={[styles.searchInput,styles._PL0_,styles._PR35_]} placeholder={"Rechercher..."} underlineColorAndroid={'#eee'}/>
                                    </View>
                                </Animated.View>
                            <View style={[styles.row,styles._MT15_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check1}
                                        rightText={'Rechercher dans le titre'}
                                    />
                            </View>
                            <View style={styles.accountdataBlock}>
                                    <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                            <CheckBox style={[styles.genChk]}
                                                onClick={(checked) =>{this.setState({check1:!checked})}}
                                                labelStyle={styles.midum} isChecked={this.state.check1}
                                                rightText={'Recherches préférées'}
                                            />
                                            <Text style={[styles.globalIcon,styles.small,styles.orangeColor]}> y</Text>
                                    </View>
                            </View>
                            <View style={styles.accountdataBlock}>
                                <View style={styles.inlineDatarow}>
                                    <RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES OFFERS</Text></RadioButton>
                                    <RadioButton currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES DEMANDS</Text></RadioButton>
                                </View>
                                <View style={[styles.inlineDatarow,styles._MT10_]}>
                                    <CheckBox style={[,styles._MR10_]}
                                                onClick={(checked) =>{this.setState({check1:!checked})}}
                                                labelStyle={styles.midum} isChecked={this.state.check1}
                                                rightText={'Particuliers'}
                                            />
                                    <CheckBox
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check1}
                                        rightText={'Professionnels'}
                                    />
                                </View>
                            </View>
                            <View style={styles.accountdataBlock}>
                                <View style={[styles.inlineDatarow,styles.spaceBitw,styles.categoryTitle]}>
                                    <Text style={styles.subTitle}>Categories</Text>
                                    <Text style={[styles.globalIcon]}>6</Text>
                                </View>
                                <View>
                                    <View style={[styles.inlineDatarow]}>
                                        <FloatLabelTextInput style={[styles.genInput,]} placeholder={"Prix de"} underlineColorAndroid={'#eee'}/>                                    
                                        <FloatLabelTextInput style={[styles.genInput,styles._ML5_]} placeholder={"Prix a"} underlineColorAndroid={'#eee'}/>
                                    </View>
                                    <View style={[styles.inlineDatarow]}>
                                        <FloatLabelTextInput style={[styles.genInput,]} placeholder={"Km de"} underlineColorAndroid={'#eee'}/>                                    
                                        <FloatLabelTextInput style={[styles.genInput,styles._ML5_]} placeholder={"Km a"} underlineColorAndroid={'#eee'}/>
                                    </View>
                                    <View style={[styles.inlineDatarow]}>
                                        <FloatLabelTextInput style={[styles.genInput,]} placeholder={"Anne de"} underlineColorAndroid={'#eee'}/>                                    
                                        <FloatLabelTextInput style={[styles.genInput,styles._ML5_]} placeholder={"Anne a"} underlineColorAndroid={'#eee'}/>
                                    </View>    
                                </View>
                            </View>
                            <View style={styles.accountdataBlock}>
                                <View style={[styles.mainMap]}>
                                    <View style={[styles._PB20_,styles.justCenter]}>
                                      <WebView source={{uri:'http://boumelita.eu/uneaffairecom/map_iframe_app.html'}} style={styles.mapView} /> 
                                    </View>
                                    <View>
                                          <CheckBox style={[styles.genChk]}
                                            onClick={(checked) =>{this.setState({check1:!checked})}}
                                            labelStyle={[styles.blockTitle,styles.orangeColor]} isChecked={this.state.check1}
                                            rightText={'TOUTE LA FRANCE'}
                                          />
                                          <View style={[styles.inlineDatarow,styles._MT15_]}>
                                               <Text style={[styles.globalIcon,styles._PR10_,styles._ML5_]}>6</Text>
                                               <Text style={styles._F14_}>Region</Text>
                                          </View>
                                          <View style={[styles._MT10_,,styles._ML5_]}>
                                               <RadioButton currentValue={this.state.value3} value3={1} onPress={this.handleOnPress3.bind(this)}><Text style={[styles.readiText,styles._PR10_]}>Aquitaine-Limousi.....</Text></RadioButton>
                                          </View>
                                          <View style={[styles.inlineDatarow,styles._MT15_]}>
                                               <Text style={[styles.globalIcon,styles._PR10_,styles._ML5_]}>6</Text>
                                               <Text style={styles._F14_}>Departments</Text>
                                          </View>
                                          <View style={[styles._MT10_,,styles._ML5_]}>
                                              <View style={styles._MT5_}>
                                                 <RadioButton currentValue={this.state.value2} value={0} onPress={this.handleOnPress2.bind(this)}><Text style={[styles.readiText,styles._PR10_,styles._PT10_]}>Departments1</Text></RadioButton>
                                              </View>
                                               <View style={styles._MT5_}>
                                                 <RadioButton currentValue={this.state.value2} value={1} onPress={this.handleOnPress2.bind(this)}><Text style={[styles.readiText,styles._PR10_,styles._PT10_]}>Departments2</Text></RadioButton>
                                              </View>
                                          </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView> */}
        </View>
    );
  }

}







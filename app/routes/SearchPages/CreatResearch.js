import React, { Component } from 'react';
import {
  StyleSheet,Keyboard,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,ActivityIndicator
} from 'react-native';
import { colors } from '../../config/styles';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ModalCat from '../../components/ModalCat/ModalCat';
import {settings} from '../../config/settings';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import Select from '../../components/Select/Select';
import { iconsMap, iconsLoaded } from '../../config/icons';
import FranceMap from '../../components/FranceMap/FranceMap';
import LocationSelect from '../../components/LocationSelect/LocationSelect';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import DropdownAlert from 'react-native-dropdownalert';
import CKeyboardSpacer from '../../components/CKeyboardSpacer';
import _ from 'lodash';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const { height, width } = Dimensions.get('window');
const FORTAB = width < 1025 && width > 721;
const api = new ApiHelper;

class CreatResearch extends Component {
  
static navigatorStyle = {
    tabBarHidden: true,
    drawUnderTabBar: true,
};

constructor(props) {
    super(props);
    //console.log(this.props);
    // if you want to listen on navigator events, set this up
     this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        like:1,
        scrolled:true,
        scrollY: new Animated.Value(0),
        value:'offers',
        value1:'all',
        catId:'',
        catLoad:false,
        attributeDataArray:[],
        dropdownData:{},
        name:'',
        keyword:'',
        load:false,
        isMap:false,
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillMount(){
    iconsLoaded.then(()=>{});
}

componentDidMount(){
    typeof this.props.id != 'undefined' && this.props.id != '' ? this.getEditData():null; 
}

onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
       this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: true
    });
    this.props.navigator.toggleTabs({
        animated: false,
        to: 'hidden'
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({title: "Créer une recherche préférée"});
    this.props.navigator.setButtons({
    leftButtons: [{
        icon: iconsMap['back'],
        id: 'back2',
        title:'back to welcome',
    }],animated:false})

    }
    // if(event.id=='didAppear'){
    //     typeof this.props.id != 'undefined' && this.props.id != '' ? this.getEditData():null; 
    // }   
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left',
            animated: true,
            to: 'open',
        });
    }   
}
getEditData(){
    let _this = this;
    let request={
         url:settings.endpoints.editUserFavSearch,
         method:'POST',
         params:{token:this.props.auth.token,search_id:this.props.id}
    }
    api.FetchData(request).then((result)=>{      
        if(result.status){
            this.loadAttributes(result.fav_search_data.category);
            this.ModalCat.setSelectedCat(result.fav_search_data.category_name);
            let dropdownData={};
            let map = false;
            Object.keys(result.fav_search_data.filter_data).map((key) => {
                if(typeof result.fav_search_data.filter_data[key].incremental_element != 'undefined' && result.fav_search_data.filter_data[key].incremental_element != ''){
                    dropdownData[key]=[];
                    dropdownData[key][0]=result.fav_search_data.filter_data[key].selected_min;
                    dropdownData[key][1]=result.fav_search_data.filter_data[key].selected_max;
                }else if(typeof result.fav_search_data.filter_data[key].attr_array != 'undefined' && result.fav_search_data.filter_data[key].attr_array){
                    dropdownData[key]=[];
                    dropdownData[key][0]=result.fav_search_data.filter_data[key].selected_min;
                    dropdownData[key][1]=result.fav_search_data.filter_data[key].selected_max;
                }else{
                    if(typeof result.fav_search_data.filter_data[key].predef[0] != 'undefined' 
                        && typeof result.fav_search_data.filter_data[key].predef[0].subAttr != 'undefined'){
                            let newk=result.fav_search_data.filter_data[key].predef;
                            newk.map((data,i)=>{
                                if(data.subAttr !="" && typeof data.selected_sub_val != 'undefined'){
                                    let subat='';
                                    dropdownData[data.sub_id]=data.selected_sub_val;
                                }
                            });
                        }
                    dropdownData[key]=result.fav_search_data.filter_data[key].selected_val;
                }
             });
            if(result.fav_search_data.location.isCurrent !==undefined){
                this.LocationSelect.setLocationData(result.fav_search_data.location);
            }
            if(result.fav_search_data.location.france !==undefined){
                map = true;
                this.FranceMap.setLocationData(result.fav_search_data.location);
            }
            this.setState({
                value:result.fav_search_data.ad_type,
                value1:result.fav_search_data.user_type,
                name:result.fav_search_data.name,
                keyword:result.fav_search_data.keyword,
                catId:result.fav_search_data.category,
                dropdownData:dropdownData,
                loading:false,
                isMap : map        
            })
        }else{
            this.setState({ loading:false});
        }               
    }).catch((error)=>{ 
         console.log(error);
    });
}
handleOnPress(value){
    this.setState({value:value})
}
handleOnPress1(value1){
    this.setState({value1:value1})
}
selectCat(item) {
    //console.log(item);
    this.setState({
        catId: item,dropdownData:{}
    }, () => {
        this.loadAttributes(item);
    });
}
getcategoryattributedata(request){
    return new Promise((resolve,reject)=>{
        api.FetchData(request).then((results)=>{
            resolve(results);
        }).catch((error)=>{
            reject(error);
        })
    });
}
loadAttributes(item) {
    let request = {
        url: settings.endpoints.getAttributes,
        method: 'POST',
        params: {
            category_id: item,
            token: this.props.auth.token
        }
    }
    this.setState({catLoad:true,attributeDataArray:[]},()=>{
        api.FetchData(request).then((results) => {
            let attributeData = [];
            if (results.status) {
               Object.keys(results.attrList).map((key) => {
               if (results.attrList[key] != "") { 
                       let attrib = {};
                       attrib.id = key;
                       attrib.type = results.attrList[key].type;
                       attrib.name = results.attrList[key].name;                       
                       if (results.attrList[key].type == 'dropdown') {
                            if(results.attrList[key].num_of_drop === 2) attrib.num_of_drop=true;  
                            if(results.attrList[key].predef != ''){
                                attrib.options = results.attrList[key].predef;
                                if(typeof results.attrList[key].predef[0] != 'undefined' && typeof results.attrList[key].predef[0].subAttr != 'undefined'){
                                       attrib.hasSub = true;
                                   }else{
                                       attrib.hasSub = false;
                                   }
                            }else if(typeof results.attrList[key].attr_array != 'undefined' && results.attrList[key].attr_array) {
                                attrib.options =  this.setLoopData(results.attrList[key].attr_value_array);
                            }else if(typeof results.attrList[key].incremental_element != 'undefined' && results.attrList[key].incremental_element != ''){
                               let p = results.attrList[key];      
                               if(results.attrList[key].name ==='Année')                    
                                   attrib.options = (this.looping(p.min,p.max,p.incremental_element)).reverse();
                               else attrib.options = this.looping(p.min,p.max,p.incremental_element);
                            }                          
                       }
                       attributeData.push(attrib);
                   }
               });
            }
            this.setState({attributeDataArray: attributeData,catLoad:false});
       })
       .catch((error) => {
          this.setState({catLoad:false});
           //console.log(122);
       })
    });
        
}

setLoopData(arrayData){
    let ary = [];
    if(_.isArray(arrayData) && !_.isEmpty(arrayData)) {
        arrayData.map((value)=>{
            ary.push({
                id:value,
                value:value
            });
        })
    }
    return ary;
}

looping(min,max,inc){
    let ary = [];
    for(i=min;i<=max;i+=inc){
        ary.push({
            id:i,
            value:i
        });
    }
    return ary;    
}
createUpdateFav(){
    Keyboard.dismiss();
    const { keyword,name } = this.state;
    let location={};
    if(this.state.isMap){
        location=this.FranceMap.getAddress();
    }else{
        location=this.LocationSelect.getAddress();
    }
    if(this.state.name == ''){
        this.dropdown.alertWithType('error', 'Error', 'Remplissez tous les champs');
        return false;
    }
    
    let params = {token:this.props.auth.token,fav_search_name:name,fav_search_category:this.state.catId,adtype:this.state.value,usertype:this.state.value1,attribute_id:this.state.dropdownData,location:location,fav_search_query:keyword}
    if(typeof this.props.id != 'undefined' && this.props.id != '' ){
        params.search_id=this.props.id;
    }
    let request={
        url:settings.endpoints.createFavSearch,
        method:'POST',
        params:params       
    }    
    this.setState({load:true},()=>{
        api.FetchData(request).then((result)=>{
            this.setState({load:false},()=>{
                if(result.status){
                    this.props.navigator.push({
                        screen: 'favoris',
                        passProps:{
                              token:this.props.auth.token, 
                              userdata:this.props.auth.userdata,
                              tabData:3
                          } 
                    });
                }else{
                    this.dropdown.alertWithType('error', 'Error', result.message);
                }
            });
        }).catch((error)=>{
          //console.log(error);
            this.setState({
                load:false
            });
        });
    });
    
}

_renderSubCategoryDropDown(attribute,selectedParent){
        let subattr=[];
        let subID='';
        attribute.options.map((data,i)=>{
            if(data.id === selectedParent){
                subattr = data.subAttr;
                subID = data.sub_id;
            }
        });

        return subattr.length > 0 && subID!='' ? <Select vkey={selectedParent} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                optionsList={subattr}
                header={'sélectionner'}
                selectedid={typeof this.state.dropdownData[subID] != 'undefined' ? this.state.dropdownData[subID]:''}
                onPress={(item) => {
                let dropdownData= this.state.dropdownData;
                dropdownData[subID] = item;
                this.setState({dropdownData: dropdownData}); 
                }}/> : null;
    }

render() {
    return (
        <View style={styles.flex}>
                <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                        <View style={styles.scrollInner}>
                            <View style={[styles.myAccountData,styles._HP15_,styles._PT10,styles._MT10]}> 
                                <Row size={12} style={[styles.row]}>    
                                    <Col sm={12} md={5} lg={4}>
                                        <View style={[styles.accountdataBlock,{paddingBottom:0}]}>
                                             <View style={[styles._MT10_,styles.inlineDatarow]}>
                                                <FloatLabelTextInput style={[styles.genInput,styles._ML5_]}  placeholder={"Nom de I'alerte"}         underlineColorAndroid={'#eee'}
                                                    onChangeTextValue={(text) => this.setState({name:text})}
                                                    value={this.state.name}
                                                />
                                            </View>
                                            <View style={[styles._MT10_,styles.inlineDatarow]}>
                                                    <FloatLabelTextInput style={[styles.genInput,styles._ML5_]}  placeholder={"Mot(s) clé(s)"} underlineColorAndroid={'#eee'}
                                                    onChangeTextValue={(text) => this.setState({keyword:text})}
                                                    value={this.state.keyword}
                                                />
                                            </View>
                                            <View style={styles.accountdataBlock}>
                                                <View style={[styles.inlineDatarow,styles._MT10_]}>
                                                    <RadioButton currentValue={this.state.value} value={'offers'} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES OFFERS</Text></RadioButton>
                                                    <RadioButton currentValue={this.state.value} value={'demands'} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES DEMANDS</Text></RadioButton>
                                                    <RadioButton currentValue={this.state.value} value={'trocs'} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES TROCS</Text></RadioButton>
                                                </View>
                                                <View style={[styles.inlineDatarow,styles._MT10_]}>
                                                    <RadioButton currentValue={this.state.value1} value={'all'} onPress={this.handleOnPress1.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>Toutes </Text></RadioButton>
                                                        <RadioButton currentValue={this.state.value1} value={'personal'} onPress={this.handleOnPress1.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>Particuliers</Text></RadioButton>
                                                        <RadioButton currentValue={this.state.value1} value={'professional'} onPress={this.handleOnPress1.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>Professionnels</Text></RadioButton>
                                                </View> 
                                            </View>
                                             <View style = {styles._MT10_}>
                                            <ModalCat SectionList={this.props.auth.categoryList} ref={o=>this.ModalCat=o} containerStyle={{flex: 1}} onPress={(item) => {this.selectCat(item)}}/>
                                            </View>
                                        </View>

                                        {this.state.catId !='' ? (
                        <View style = {[styles._MT10_]}>
                            {this.state.catLoad ? (
                                <ActivityIndicator animating={true} color={colors.navBarButtonColor} size="small"/>) : (this.state.attributeDataArray.length > 0
                        ? (this.state.attributeDataArray.map((attribute, i) => {
                                if(attribute.num_of_drop){
                                            return (
                                            <View style={{flex:1,flexDirection:'row'}} key={i}>
                                                <View style={{flex:1,padding:7,paddingBottom:0}}>
                                                <Select vkey={i} containerStyle={{ backgroundColor: "#fff"}}
                                                    optionsList={attribute.options}
                                                    header={attribute.name+" min"}
                                                    selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id][0]:''}
                                                    onPress={(item) => {
                                                        let dropdownData = this.state.dropdownData;
                                                        //let obj={min:0,max}
                                                        if(dropdownData[attribute.id] === undefined){
                                                            dropdownData[attribute.id]=[];
                                                        }
                                                        dropdownData[attribute.id][0] = item;
                                                        this.setState({dropdownData: dropdownData});
                                                }}/>
                                                </View>
                                                <View style={{flex:1,padding:7,paddingBottom:0}}>
                                                <Select vkey={i} containerStyle={{ backgroundColor: "#fff"}}
                                                    optionsList={attribute.options}
                                                    header={attribute.name+" max"}
                                                    selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id][1]:''}
                                                    onPress={(item) => {
                                                        let dropdownData= this.state.dropdownData;
                                                        if(dropdownData[attribute.id] === undefined){
                                                            dropdownData[attribute.id]=[];
                                                        }
                                                        dropdownData[attribute.id][1] = item;
                                                        this.setState({dropdownData: dropdownData}); 
                                                }}/>
                                                </View>
                                            </View>)
                                        }else{
                                return (
                                <View style={{paddingHorizontal:7}} key={i}>
                                    <Select vkey={i} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                                        optionsList={attribute.options}
                                        header={attribute.name}
                                        selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id]:''}
                                        onPress={(item) => {
                                            let dropdownData= this.state.dropdownData;
                                            dropdownData[attribute.id] = item;
                                            this.setState({dropdownData: dropdownData}); 
                                    }}/>
                                    {attribute.hasSub && typeof this.state.dropdownData[attribute.id] != "undefined" ? (
                                     this._renderSubCategoryDropDown(attribute,this.state.dropdownData[attribute.id])
                                    ):(null)}
                                </View>);
                        }
                                }))
                                :(<View><Text>No Attributes Found</Text></View>
                                ))
                                }
                                </View>
                                ):(null)} 
                                    </Col>
                                      <Col sm={12} md={7} lg={8}>
                                      <View style={styles._MT10_}>
                                        <LocationSelect isEnable={!this.state.isMap} currentLocation={true} 
                                        isEnable={true}
                                        advance = {true}
                                        ref = {o => this.LocationSelect = o} 
                                        authActions={this.props.authActions}
                                        onChange={()=>{
                                                this.setState({isMap:false});
                                            }}/>
                                        </View>
                                    </Col>  
                                </Row>   

                                    <View style={styles._MT10_}>
                                <FranceMap isEnable={this.state.isMap} selectedRegion={this.state.region} ref = {o => this.FranceMap = o}  onChange={()=>{
                                                this.setState({isMap:true});
                                            }} /> 
                                    </View> 
                        </View>
                    </View>
                </ScrollView>
                <DropdownAlert
        ref={(ref) => this.dropdown = ref}
        onClose={(data) => {/*console.log(data);*/}} />
                <View style={styles.bottomArea}>
                    {/* <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8}>
                        <Text style={[styles.genButton,styles.smallBtn]}>Sauvegarder</Text>
                    </TouchableOpacity> */}
                    <LoaderButton load={this.state.load} onPress={()=>{this.createUpdateFav()}} BtnStyle={[styles.brandColor]} 
                        text={'Sauvegarder'} textStyle={[styles.genButton,styles.smallBtn]} />
                </View> 
                {Platform.OS === 'ios' ? <CKeyboardSpacer /> : (null)} 
        </View>
    );
}
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatResearch);
import React, { Component } from 'react';
import {    
  StyleSheet,Image,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,Text, View,AppRegistry,ActivityIndicator,Keyboard,KeyboardAvoidingView
} from 'react-native';

import { colors } from '../../config/styles';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
import { iconsMap, iconsLoaded ,Icon} from '../../config/icons';
import FaIcon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '../../lib/react-native-check-box';
//import SelectCustom from '../../components/Select/SelectCustom';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ModalCat from '../../components/ModalCat/ModalCat';
import LocationSelect from '../../components/LocationSelect/LocationSelect';
import Select from '../../components/Select/Select';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import CKeyboardSpacer from '../../components/CKeyboardSpacer';
import _ from 'lodash';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const api = new ApiHelper;
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class advancesearch extends Component {

 

  
static navigatorStyle = {
    
    drawUnderTabBar: true
};

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    //console.log(this.props);
    let searchStr = '';
    let seactIntit  = false;
    let isUrgent = false;
    let adTypes = '';
    if(typeof this.props.dataSearch != 'undefined'){
        if(typeof this.props.dataSearch.searchTxt != 'undefined'){
            searchStr = this.props.dataSearch.searchTxt;
        }else if(typeof this.props.dataSearch.search_string != 'undefined'){
            searchStr = this.props.dataSearch.search_string;
        }else{
            searchStr = '';
        }
    }else{
        searchStr = '';
    }
    if(typeof this.props.dataSearch != 'undefined'){
        if(typeof this.props.dataSearch.searchInTitle != 'undefined'){
            seactIntit = this.props.dataSearch.searchInTitle;
        }else if(typeof this.props.dataSearch.search_in_title != 'undefined'){
            seactIntit = this.props.dataSearch.search_in_title;
        }else{
            seactIntit = false;
        }
    }else{
        seactIntit = false;
    }
    if(typeof this.props.dataSearch != 'undefined'){
        if(typeof this.props.dataSearch.urgent != 'undefined'){
            isUrgent = this.props.dataSearch.urgent;
        }else if(typeof this.props.dataSearch.search_urgent != 'undefined'){
            isUrgent = this.props.dataSearch.search_urgent;
        }else{
            isUrgent = false;
        }
    }else{
        isUrgent = false;
    }
    if(typeof this.props.dataSearch != 'undefined'){
        if(typeof this.props.dataSearch.ad_type != 'undefined'){
            adTypes = this.props.dataSearch.ad_type;
        }else{
            adTypes = 'offers';
        }
    }else{
        adTypes = 'offers';
    }
    this.state={
        scrollY: new Animated.Value(0),        
        searchText:searchStr,
        searchInTitle:seactIntit,
        urgent:isUrgent,
        adType:adTypes,
        catId:'',
        catLoad:false,
        attributeDataArray:[],
        dropdownData:{},
        location:{},
        RegionData:{
            region:typeof this.props.region != 'undefined' && this.props.region !== '' ? this.props.region : '',
            department:'',
            france:false,
            regionDesp:'',
            departementDesp:''
        },
        isMap:false,
        selectCat : false,
        franceData:{},
        franceText : 'Toute La France',
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}
componentWillMount(){
    iconsLoaded.then(()=>{});
}

componentDidMount() {
    this.setData();
}

setData(){
    if(this.props.dataSearch !== undefined && this.props.dataSearch !== null && Object.keys(this.props.dataSearch).length > 0 &&  this.props.dataSearch != ''){ 
        let newData = JSON.parse(JSON.stringify(this.props.dataSearch));
        console.log(newData);
        if(newData.category_id !== undefined && newData.category_id !== null){
            this.setState({catId: newData.category_id},()=>{
                this.loadAttributes(newData.category_id);
            });
        }
        if(newData.category_filter !== undefined && newData.category_filter !== null){
            this.setState({dropdownData:newData.category_filter});
        }
        if(newData.category_id !== '' && newData.category_id !== 0) {
            if(this.ModalCat && this.ModalCat.setDataFromId) {
                this.setState({
                    selectCat : true
                },()=>{
                    this.ModalCat.setDataFromId(newData.category_id);
                });
            }
        }
        if(newData.location !== undefined && newData.location !== null){
            if(newData.location.isCurrent !== undefined && this.LocationSelect!== undefined && this.LocationSelect !== null){
                if(newData.location.zipcode !== undefined) {
                   // console.log(this.state.franceText);
                    this.setState({franceData:newData.location,franceText : this.state.franceText},()=>{
                        this.LocationSelect.setLocationData(newData.location);
                    })
                }
                // else {
                //     let setText = this.state.franceText;
                //     this.setState({franceData:{france:true},franceText : setText})
                // }
              
            }else if(newData.location.france !== undefined ){
                // this.FranceMap.setLocationData(newData.location);
                let setText = newData.location.departementDesp !== '' ? newData.location.departementDesp : newData.location.regionDesp !== '' ? newData.location.regionDesp : this.state.franceText;
                this.setState({franceData:newData.location,franceText : setText})
            }
        }
    } else if(this.props.dataSearch !== undefined && this.props.dataSearch !== null && Object.keys(this.props.dataSearch).length === 0){
        let newSetData = this.state.franceData;
        newSetData.france = true
        this.setState({
            franceData:newSetData,
            franceText : this.state.franceText
        })
    }
    const { franceData } = this.state;
//    console.log(franceData);
    
   if(this.props.region !== undefined && this.props.region !== ''  && !_.isEmpty(franceData) && franceData.france) {
        let regData = this.state.RegionData;
        regData.regionDesp = this.props.region;
        this.setState({
            franceData:regData,
        },()=>{
            // console.log(this.state.franceData);
        })
    }  else if(this.props.fromHome) {
        let newSetData = this.state.franceData;
        newSetData.france = true
        this.setState({
            franceData:newSetData,
            franceText : this.state.franceText
        })
    }

    if(typeof this.props.isCurrent !== undefined && this.props.isCurrent){
        // console.log(this.props.isCurrent);
        this.LocationSelect.showCurrent(true);
    }
}
onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
        api.setAction(this.props.chatActions,this.props.authActions);
        
        this.props.chatActions.setScreen(this.props.testID);
        this.props.navigator.toggleTabs({
        to: 'hidden',
        animated: true,
    });
    this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: true
    });
   
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({title: "Recherche Avancée"});
    this.props.navigator.setButtons({
         leftButtons: [{
        icon: iconsMap['back'],
        id: 'back2',
        title:'back to welcome',
      }],rightButtons: [{
        icon: iconsMap['sync'],
        id: 'right',
        title:'back to welcome',
      }],animated:false});
     
    }
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    if(event.id == 'right'){
        let newData = this.state.franceData;
        newData.france = false;
        newData.region = '';
        newData.department = '';
        newData.departementDesp = '';
        newData.regionDesp = '';
        this.setState({searchInTitle:'',searchText:'',urgent:'',category_id:'',catId:'',adType:'offers',attributeDataArray:{},franceText:'Toute La France',franceData:newData},()=>{
                if(this.ModalCat && this.ModalCat.resetWithoutProps) {
                    this.ModalCat.resetWithoutProps();
                    this.LocationSelect.reset();
                    //this.FranceMap.setLocationData({});
                }
        });
        // this.FranceMap.setLocationData({});
    }

    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left',
            animated: true,
            to: 'open',
        });
    }
  }

goto(page){   
    this.props.navigator.push({
        screen: page,
        passProps:{
            token:this.props.auth.token,
            userdata:this.props.auth.userdata
        }
    }); 
}
selectCat(item) {
    this.setState({
        catId: item,
        dropdownData:{}
    }, () => {
        this.loadAttributes(item);
    });
}
loadAttributes(item) {
    let request = {
        url: settings.endpoints.getAttributes,
        method: 'POST',
        params: {
            category_id: item,
            token: this.props.auth.token
        }
    }
    this.setState({catLoad:true,attributeDataArray:[]},()=>{
        api.FetchData(request).then((results) => {
            let attributeData = [];
            if (results.status) {
               Object.keys(results.attrList).map((key) => {
               if (results.attrList[key] != "") { 
                       let attrib = {};
                       attrib.id = results.attrList[key].name_lower;
                       attrib.type = results.attrList[key].type;
                       attrib.name = results.attrList[key].name;  
                       attrib.num_of_drop=false;               
                       if (results.attrList[key].type == 'dropdown') {
                            if(results.attrList[key].num_of_drop === 2) attrib.num_of_drop=true;  
                            if(results.attrList[key].predef != ''){
                                attrib.options = results.attrList[key].predef;
                                if(typeof results.attrList[key].predef[0] != 'undefined' && typeof results.attrList[key].predef[0].subAttr != 'undefined'){
                                       attrib.hasSub = true;
                                   }else{
                                       attrib.hasSub = false;
                                   }
                            }else if(typeof results.attrList[key].attr_array != 'undefined' && results.attrList[key].attr_array) {
                                attrib.options =  this.setLoopData(results.attrList[key].attr_value_array);
                            }else if(typeof results.attrList[key].incremental_element != 'undefined' && results.attrList[key].incremental_element != ''){
                                let p = results.attrList[key];      
                                if(results.attrList[key].name ==='Année')                    
                                    attrib.options = (this.looping(p.min,p.max,p.incremental_element)).reverse();
                                else attrib.options = this.looping(p.min,p.max,p.incremental_element);
                           }  
                       }
                       attributeData.push(attrib);
                   }
               });
               this.setState({attributeDataArray: attributeData,catLoad:false,selectCat : true});
            } else {
                this.setState({catLoad:false,selectCat : false});
            }
            
       })
       .catch((error) => {
          this.setState({catLoad:false});
           //console.log(122);
       })
    })
}

setLoopData(arrayData){
    let ary = [];
    if(_.isArray(arrayData) && !_.isEmpty(arrayData)) {
        arrayData.map((value)=>{
            ary.push({
                id:value,
                value:value
            });
        })
    }
    return ary;
}
looping(min,max,inc){
    let ary = [];
    for(i=min;i<=max;i+=inc){
        ary.push({
            id:i,
            value:i
        });
    }
    return ary;    
}
onSelect(key,val){
}
submitAll(){
    Keyboard.dismiss();
    if(this.state.searchInTitle){
        if(this.state.searchText == ''){
            return false;
        }   
    }

    const {franceData} = this.state;
    let obj={};
    obj.search_string=this.state.searchText;
    obj.search_in_title=this.state.searchInTitle;
    obj.search_urgent=this.state.urgent;
    obj.category_id=this.state.catId;
    obj.ad_type=this.state.adType;
    obj.category_filter=this.state.dropdownData;
    //obj.in_history=true;
    if(_.isObject(franceData) && !_.isEmpty(franceData)){
         obj.location=franceData;
    }else{
        obj.location=this.LocationSelect.getAddress();
    } 

    this.props.navigator.push({
        screen: 'ProductgridContainer',
        passProps:{
            token:this.props.auth.token,
            userdata:this.props.auth.userdata, 
            params:obj,
            region : franceData.region,
            filterType:'advance'       
        }
    });
}


_renderSubCategoryDropDown(attribute,selectedParent){
        let subattr=[];
        let subID='';
        let subName='';
        attribute.options.map((data,i)=>{
            if(data.id === selectedParent){
                subattr = data.subAttr;
                subID = data.sub_id;
                subName= data.sub_name_lower
            }
        });

        return subattr.length > 0 && subID != '' && subName != ''? <Select vkey={selectedParent} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                optionsList={subattr}
                header={'sélectionner'}
                selectedid={typeof this.state.dropdownData[subName] != 'undefined' ? this.state.dropdownData[subName]:''}
                onPress={(item) => {
                let dropdownData= this.state.dropdownData;
                dropdownData[subName] = item;
                this.setState({dropdownData: dropdownData}); 
                }}/> : null;
    }


render() {    
    const headerTranslate =  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    const {selectCat,franceData , franceText} = this.state;
    // console.log(_.isObject(franceData) && !_.isEmpty(franceData) &&  franceData.regionDesp !== '' && !franceData.france);
    // console.log(franceData);
    return (
        <View style={[styles.flex]}>
            <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                <View style={styles.scrollInner}>
                    <View style={[styles.myAccountData,styles._HP0_,styles._PT10,{paddingLeft:7,paddingRight:7}]}>
                        <Animated.View style={[styles.searchHolder,styles.searchAdvanceIco,
                                        {
                                            transform: [{ translateY: headerTranslate },
                                            {
                                                scale: this.state.scrollY.interpolate({
                                                    inputRange: [-50 , 0, 50],
                                                    outputRange: [2, 1, 1]
                                                })
                                            }]
                                        },
                                    ]}>
                                    <View style={[styles.searchinputHolder,{marginRight:2,marginLeft:5}]}>
                                        <TextInput style={[styles.searchInput,{paddingLeft:5}]} 
                                        value={this.state.searchText} placeholder={"Rechercher..."} underlineColorAndroid={'#eee'} onChangeText={(val)=>{
                                            this.setState({searchText:val});
                                        }}/>
                                    </View>
                                    <TouchableOpacity style={[styles.icoholderRight,{marginRight:10}]} onPress={()=>this.state.searchText !=''? this.setState({searchText:''}) :null
                                    //this.goto('ProductgridContainer',this.state.in_history,{})
                                } >
                                <Icon name={this.state.searchText =='' ? "search":'close'} color={colors.navBarButtonColor} size={this.state.searchText =='' ? 19 :16} />
                                </TouchableOpacity>
                                </Animated.View>
                                <Row size={12} style={[styles.row]}>    
                                    <Col sm={12} md={5} lg={3}>
                                        <View style={[styles.row,styles._MT15_]}>
                                                <View style={[styles.inlineDatarow,styles._PB5_]}>
                                                    <CheckBox style={[styles.genChk]}
                                                        onClick={(checked) =>{this.setState({searchInTitle:checked})}}
                                                        labelStyle={[styles.blockTitle,styles.orangeColor]} 
                                                        rightText={'Rechercher dans le titre'}
                                                        isChecked={this.state.searchInTitle}
                                                    />
                                                </View>
                                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_,styles.stretch,styles._PB5_]}>
                                                        <CheckBox style={[styles.genChk]}
                                                            onClick={(checked) =>{this.setState({urgent:checked})}}
                                                            labelStyle={[styles.blockTitle,styles.orangeColor]} 
                                                            rightText={'Annonces urgentes'}
                                                            isChecked={this.state.urgent}
                                                        />
                                                        <Text style={[styles.globalIcon,styles.small,styles.orangeColor]}> y</Text>
                                                </View>
                                        </View>
                                        <TouchableOpacity activeOpacity={0.8} onPress={()=>{this.goto('favourites')}} style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                            <View style={styles.inlineWithico}>
                                                <Text style={[styles.globalIcon,styles.mdIco]}>S </Text>
                                                <Text style={[styles.genText,styles.grayColor]}>{'Recherches Préférées'}</Text>
                                            </View>
                                            <Text style={[styles.globalIcon]}>6</Text>
                                        </TouchableOpacity>
                                        <View style={styles.accountdataBlock}>
                                            <View style={[styles.inlineDatarow,styles._MT10_]}>
                                                <RadioButton 
                                                currentValue={this.state.adType} 
                                                value={'offers'} 
                                                onPress={()=>{
                                                    this.setState({adType:'offers'});
                                                }}>
                                                    <Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES OFFRES</Text>
                                                </RadioButton>
                                                <RadioButton 
                                                currentValue={this.state.adType} 
                                                value={'demands'} 
                                                onPress={()=>{
                                                    this.setState({adType:'demands'});
                                                }}>
                                                    <Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES DEMANDES</Text>
                                                </RadioButton>
                                                <RadioButton 
                                                currentValue={this.state.adType} 
                                                value={'trocs'} 
                                                onPress={()=>{
                                                    this.setState({adType:'trocs'});
                                                }}>
                                                    <Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>LES TROCS</Text>
                                                </RadioButton>
                                            </View>
                                        </View>
                                    </Col>    
                                    <Col sm={12} md={7} lg={9}>
                                        <View style={styles.accountdataBlock}>
                                            {/* <View style={[styles.inlineDatarow,styles.spaceBitw,styles.categoryTitle]}>
                                                <ModalDropdown defaultValue={'select one'} textStyle={[styles.stretch,styles._F16_,{width:width-40}]} options={
                                                    {key1:'Catégorie1', key2:'Catégorie2',key3:'Catégorie3', key4:'Catégorie4'}
                                                    } 
                                                onSelect={this.onSelect.bind(this)}/>
                                                <Text style={[styles.globalIcon]}>6</Text>
                                            </View> */} 
                                                <ModalCat 
                                                    ref={o => { this.ModalCat = o; }}
                                                    SectionList={this.props.auth.categoryList} 
                                                    containerStyle={{flex: 1}} 
                                                    selectedCatId={!selectCat && this.props.dataSearch !== undefined && this.props.dataSearch !== null &&this.props.dataSearch.category_id !== undefined ? this.props.dataSearch.category_id : null }
                                                    onPress={(item)=>this.selectCat(item)}
                                                    close={this.props.dataSearch !== undefined && this.props.dataSearch !== null &&this.props.dataSearch.category_id !== undefined ? this.props.dataSearch.category_id : false}
                                                />

                                                {this.state.catId !='' ? (
                                        <View style = {[styles._MT10_]}>
                                        {this.state.catLoad ? (
                                        <ActivityIndicator animating={true} color={colors.navBarButtonColor} size="small"/>) : (this.state.attributeDataArray.length > 0
                                        ? (this.state.attributeDataArray.map((attribute, i) => {
                                        if(attribute.num_of_drop){
                                            return (
                                            <View style={{flex:1,flexDirection:'row'}} key={i}>
                                                <View style={{flex:1,padding:7,paddingBottom:0}}>
                                                <Select vkey={i} containerStyle={{ backgroundColor: "#fff"}}
                                                    optionsList={attribute.options}
                                                    header={attribute.name+" min"}
                                                    showClose = {true}
                                                    selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id][0]:''}
                                                    onPress={(item) => {
                                                        let dropdownData = this.state.dropdownData;
                                                        //let obj={min:0,max}
                                                        if(dropdownData[attribute.id] === undefined){
                                                            dropdownData[attribute.id]=[];
                                                        }
                                                        dropdownData[attribute.id][0] = item;
                                                        this.setState({dropdownData: dropdownData});
                                                }}/>
                                                </View>
                                                <View style={{flex:1,padding:7,paddingBottom:0}}>
                                                <Select vkey={i} containerStyle={{ backgroundColor: "#fff"}}
                                                    optionsList={attribute.options}
                                                    header={attribute.name+" max"}
                                                    showClose = {true}
                                                    selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id][1]:''}
                                                    onPress={(item) => {
                                                        let dropdownData= this.state.dropdownData;
                                                        if(dropdownData[attribute.id] === undefined){
                                                            dropdownData[attribute.id]=[];
                                                        }
                                                        dropdownData[attribute.id][1] = item;
                                                        this.setState({dropdownData: dropdownData}); 
                                                }}/>
                                                </View>
                                            </View>)
                                        }else{
                                            return (
                                            <View style={{paddingHorizontal:7}} key={i}>
                                                <Select vkey={i} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                                                    optionsList={attribute.options}
                                                    header={attribute.name}
                                                    showClose = {true}
                                                    selectedid={typeof this.state.dropdownData[attribute.id] != "undefined" ? this.state.dropdownData[attribute.id]:''}
                                                    onPress={(item) => {
                                                        let dropdownData= this.state.dropdownData;
                                                        dropdownData[attribute.id] = item;
                                                        this.setState({dropdownData: dropdownData}); 
                                                            }}/>
                                                            {attribute.hasSub && typeof this.state.dropdownData[attribute.id] != 'undefined' ? (
                                                this._renderSubCategoryDropDown(attribute,this.state.dropdownData[attribute.id])
                                            ):(null)}
                                            </View>)
                                        }
                                        
                                            }))
                                            :(null
                                            ))
                                            }
                                            </View>
                                            ):(null)} 
                                           
                                            <LocationSelect isEnable={!this.state.isMap} currentLocation={true} 
                                                ref = {o => this.LocationSelect = o} 
                                                authActions={this.props.authActions}
                                                showMap={true}
                                                franceData = {franceData}
                                                onFranceChange = {(franceData)=>{
                                                    console.log(franceData);
                                                    this.setState({
                                                        franceData
                                                    });
                                                }}
                                                onChange={(flag)=>
                                                {
                                                    this.setState({isMap:false,franceData:{},franceText : flag ? 'Choisir autre localisation' : 'Toute La France' });
                                                // this.FranceMap.check(true);
                                                }}
                                                advance = {true}
                                            />
                                            {
                                                _.isObject(franceData) && !_.isEmpty(franceData) && franceData.regionDesp !== undefined &&  franceData.regionDesp !== '' && !franceData.france ? (
                                                <TouchableOpacity 
                                                onPress={()=>{
                                                    if(this.LocationSelect !== undefined && this.LocationSelect !== null) {
                                                        this.LocationSelect._onFocus(false,0);
                                                    }
                                                }}
                                                style={{marginTop:10,padding:10,borderColor:'#ddd',borderWidth:1,width:'100%',justifyContent:'space-between',flexDirection:'row',alignContent:'center'}}>
                                                    <Text style={[styles.genText,styles.grayColor]}>{franceData.departementDesp !== '' ? franceData.departementDesp  : franceData.regionDesp }</Text>
                                                    <FaIcon  name='angle-right' size={18}/>
                                                </TouchableOpacity>) : ( 
                                                <TouchableOpacity 
                                                onPress={()=>{
                                                    if(this.LocationSelect !== undefined && this.LocationSelect !== null) {
                                                        this.LocationSelect._onFocus(true,0);
                                                    }
                                                }}
                                                style={{marginTop:10,padding:10,borderColor:'#ddd',borderWidth:1,width:'100%',justifyContent:'space-between',flexDirection:'row',alignContent:'center'}}>
                                                    <Text style={[styles.genText,styles.grayColor]}>{franceText}</Text>
                                                    <FaIcon  name='angle-right' size={18}/>
                                                </TouchableOpacity>)
                                            }
                                        </View>
                                    </Col>
                                </Row>    
                        </View>
                        {/* <FranceMap  ref = {o => this.FranceMap = o} isEnable={this.state.isMap} 
                        onChange={()=>
                        {
                            this.setState({isMap:true},()=>{
                                console.log('On change called');
                                this.LocationSelect.setAddress('');
                            });
                        }
                        }/> */}
                    </View>
                </ScrollView>
                <View style={styles.bottomArea}>
                    <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8} onPress={()=>this.submitAll()}>
                        <Text style={[styles.genButton]}>Lancer la recherche</Text>
                    </TouchableOpacity>
                </View>
                {Platform.OS === 'ios' ? <CKeyboardSpacer /> : (null)}
        </View>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(advancesearch);






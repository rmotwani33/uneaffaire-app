import React, {Component} from 'react';
import {Text, View, ScrollView} from 'react-native';
import {colors} from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import { iconsMap, iconsLoaded } from '../../config/icons';

// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const api = new ApiHelper;

class apropos extends Component {
  // static navigatorButtons = {
  //   leftButtons: [
  //     {
  //       icon: images.back,
  //       id: 'back',
  //       title: 'Back To Login'
  //     }
  //   ]
  // };
  constructor(props) {
    super(props);
    this.state = {
      check1: false,
      abtdata: [],
      title: ''
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  static navigatorStyle = {
    // navBarBackgroundColor: colors.navBarBackgroundColor,
    // navBarTextColor: colors.navBarTextColor,
    // navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    // navBarButtonColor: colors.navBarButtonColor,
    // statusBarTextColorScheme: colors.statusBarTextColorScheme,
    // statusBarColor: colors.statusBarColor,
    // tabBarBackgroundColor: colors.tabBarBackgroundColor,
    // tabBarButtonColor: colors.tabBarButtonColor,
    // tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    // navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
};


  componentWillMount(){
      iconsLoaded.then(()=>{});
  }

  onNavigatorEvent(event) {
    //console.log(event)
    if (event.id == 'willAppear') {
      api.setAction(this.props.chatActions,this.props.authActions);
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({to: 'shown', animated: true});
      this.props.navigator.toggleTabs({
        to: 'hidden',
        animated: true
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({title: 'A propos'});
      this.props.navigator.setButtons({
        leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title: 'Back To Login'
        }]
      })
    }
    if (event.id == 'didAppear') {

    }

    if (event.id == 'back2') {
      this.props.navigator.pop({
          animated: true // does the pop have transition animation or does it happen immediately (optional)
        });
    }
  }
  goto(page) {
    this.props.navigator.push({screen: page});
  }
  componentDidMount() {
    this.getAboutData();
  }
  getAboutData() {
    let request = {
      url: settings.endpoints.about,
      method: 'POST',
      params: { key: 'about' }
    }
    api.FetchData(request).then((result) => {
        if (result.status) {
          this.setState({
            abtdata: result.result,
            title: result.result.title});
        }
      })
      .catch((error) => {
        //console.log(error);
      });
  }
  render() {
    return (
      <View style={[styles.main, styles._HP15_, styles._PT15_]}>
        <ScrollView style={styles.apropsMain}>
          <Text style={styles.blockTitle}>{this.state.abtdata.name}</Text>
          <Text style={styles.contentAprops}>{this.state.abtdata.description}</Text> 
        </ScrollView>                 
      </View>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(apropos);

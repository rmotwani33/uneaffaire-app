import React, { Component } from 'react';
import {Text,View,TextInput,TouchableOpacity,ScrollView,Switch,Dimensions
} from 'react-native';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import { iconsMap, iconsLoaded } from '../../config/icons';
import DropdownAlert from 'react-native-dropdownalert';
import startMain from '../../config/app-main';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const { height, width } = Dimensions.get('window');
const api= new ApiHelper;
class RemoveAd extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: images.back,
  //      id: 'back',
  //      title:'back to welcome',
  //    }],
  // };
// static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
// };
  
  constructor(props) {
    super(props);
    this.state={
        value: 1,
        comment:'',
        load:false
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
}
  

  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
      api.setAction(this.props.chatActions,this.props.authActions);
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: true
      });
      this.props.navigator.toggleTabs({
          animated: false,
          to: 'hidden'
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({
        title: "Supprimer votre annonce",
      });
      this.props.navigator.setButtons({
      leftButtons: [{
          icon: iconsMap['back'],
          id: 'back2',
          title:'back to welcome',
      }],animated:false})
    }
    if(event.id=='didAppear'){

    }
    
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }

  handleOnPress(value){
    this.setState({value:value})
  }

  goto(page){
    this.props.navigator.push({
      screen: page
    });
  }
  deleteItem(){
    const { fromDash } = this.props;
    this.setState({load:true},()=>{
      let request={
        url:settings.endpoints.deleteAd,
        method:'POST',
        params:{token:this.props.auth.token,reason_id:this.state.value,ad_id:this.props.ad_id,comment_reason:this.state.comment}
      }
      api.FetchData(request).then((result)=>{  
        console.log(result);
        if(result.status){
          this.dropdown.alertWithType('success', 'Félicitation!', result.message);
          setTimeout(()=>{
            if(fromDash) {
              this.props.navigator.pop({
                animated: true // does the pop have transition animation or does it happen immediately (optional)
              });
            } else {
              startMain(this.props.auth.token,this.props.auth.userdata);
            }
          },2000);        
        }else{
            this.dropdown.alertWithType('error', 'Error', result.message);
        }
        this.setState({load:false});           
      }).catch((error)=>{
          console.log(error);
          this.setState({load:false});
      });
    });
    
  }
  render() {
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
            <View style={styles.scrollInner}>
                  <View style={[styles.productdataMain,styles._HP15_]}>
                      <View>
                          <TouchableOpacity style={styles._PB10_} activeOpacity={0.8}><RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML10_,styles._F16_]}>{'Article vendu / Loué sur UneAffaire.fr'}</Text></RadioButton></TouchableOpacity>
                          <TouchableOpacity style={styles._PB10_} activeOpacity={0.8}><RadioButton currentValue={this.state.value} value={2} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML10_,styles._F16_]}>{'Article vendu / Loué ailleurs'}</Text></RadioButton></TouchableOpacity>
                          <TouchableOpacity style={styles._PB10_} activeOpacity={0.8}><RadioButton currentValue={this.state.value} value={3} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML10_,styles._F16_]}>{'Modifier annonce / Remonter en tête'}</Text></RadioButton></TouchableOpacity>
                          <TouchableOpacity style={styles._PB10_} activeOpacity={0.8}><RadioButton currentValue={this.state.value} value={4} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML10_,styles._F16_]}>{'Autre raison'}</Text></RadioButton></TouchableOpacity>
                      </View>
                      <View>
                         <TextInput underlineColorAndroid='#CCCCCC'  multiline={true} numberOfLines={5} placeholder={"Commentaires"} onChangeText={(text) => this.setState({comment:text})} />
                         {/* <TouchableOpacity style={[styles.brandColor,styles._MT10_]} activeOpacity={0.85}>
                            <Text style={styles.genButton} onPress={this.deleteItem.bind(this)}>Supprimer</Text>
                         </TouchableOpacity>  */}
                        <View style={[styles.btnBlock,styles.spaceBitw]}> 
                            <LoaderButton load={this.state.load} onPress={()=>{this.deleteItem()}} BtnStyle={[styles.brandColor,styles._MT10_]}
                              text={'Supprimer'} textStyle={styles.genButton} />
                        </View>
                      </View>
                  </View>
            </View>
            <DropdownAlert
        ref={(ref) => this.dropdown = ref}
        onClose={(data) => {/*console.log(data);*/}} />
        </ScrollView>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(RemoveAd);
import React, { Component } from 'react';
import {
  Text,View,TouchableOpacity,ScrollView
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import { iconsMap, iconsLoaded } from '../../config/icons';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

class  Managead extends Component {
  constructor(props) {
    super(props);
    this.state={
        check1:false,
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
}

  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: true
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({title: "Gérer votre annonce"});
      this.props.navigator.setButtons({
         leftButtons: [{
       icon: iconsMap['back'],
       id: 'back2',
       title:'Back To Login',
     }],animated:false})
    }
    if(event.id=='didAppear'){

    }    
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }    
  }
  goto(page){
    if(page=='avant'|| page=='RemoveAd'){
      this.props.navigator.push({
        screen: page,
        passProps:{
          token:this.props.auth.token,
          ad_id:[this.props.data.Details.id]
        }
      });
    }else if(page=='Modifier' || page=='PromoteAd'){
      this.props.navigator.push({
        screen: page,
        passProps:{
          token:this.props.auth.token,
          data:this.props.data.Details,
          userdata:this.props.userdata,
        }
      });
    }    
  }
  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
           <View style={[styles.scrollInner,styles._HP15_,styles._PT15_]}>
                <View style={styles.blockmetter}>
                  {this.props.data.Details.is_active == 1 ? 
                    (
                      <View>                     
                      <TouchableOpacity onPress={()=>{this.goto('avant')}} activeOpacity={0.8} style={[styles.inlineDatarow,styles.ManageadRow]}>
                        <Text style={[styles.globalIcon,styles.specialIco]}>7</Text>
                        <Text style={[styles.labelText,styles.large]}>Remonter en tête de liste</Text>    
                    </TouchableOpacity>                                
                    <TouchableOpacity onPress={()=>{this.goto('Modifier')}} activeOpacity={0.8} style={[styles.inlineDatarow,styles.ManageadRow]}>
                        <Text style={[styles.globalIcon,styles.specialIco]}>h</Text>
                        <Text style={[styles.labelText,styles.large]}>Modifier l'annonce</Text>   
                    </TouchableOpacity></View>):null
                    }
                    <TouchableOpacity onPress={()=>{this.goto('RemoveAd')}} activeOpacity={0.8} style={[styles.inlineDatarow,styles.ManageadRow]}>
                        <Text style={[styles.globalIcon,styles.specialIco]}>8</Text>
                        <Text style={[styles.labelText,styles.large]}>Supprimer l'annonce</Text>    
                    </TouchableOpacity> 
                    {this.props.data.Details.is_active == 1 ?(                         
                    <TouchableOpacity onPress={()=>{this.goto('PromoteAd')}} activeOpacity={0.8} style={[styles.inlineDatarow,styles.ManageadRow]}>
                        <Text style={[styles.globalIcon,styles.specialIco]}>|</Text>
                        <Text style={[styles.labelText,styles.large]}>Mettre en avant</Text>   
                    </TouchableOpacity>):null
                    }
                </View>
           </View>
      </ScrollView>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Managead);





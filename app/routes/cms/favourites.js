import React, { Component } from 'react';
import {
  Text,View,TouchableOpacity,ScrollView,Alert
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {settings} from '../../config/settings';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
import Loader from '../../components/Loader/Loader';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
 const api= new ApiHelper;
class  favourites extends Component {
//   static navigatorButtons = {
//      leftButtons: [{
//        icon: images.back,
//        id: 'back',
//        title:'Back To Login',
//      }],
//   };
// static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
//     navBarHidden: false,
//     drawUnderTabBar:false
// };
constructor(props) {
    super(props);
    this.state={
        check1:false,
        searchData:[],
        searchLength:0,
        token:this.props.auth.token,
        load:false
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
}
  onNavigatorEvent(event) {
    //console.log(event) 
    if(event.id=='willAppear'){
    api.setAction(this.props.chatActions,this.props.authActions);
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleTabs({
        animated: true,
        to: 'shown'
     });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({title: "Recherche Préférée"});
      this.props.navigator.setButtons({
         leftButtons: [{
       icon: iconsMap['back'],
       id: 'back2',
       title:'Back To Login',
     }],animated:false})
    }
    if(event.id=='didAppear'){
        
    }
     
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }
componentDidMount(){    
    this.getUserFavsearch(); 
}
getUserFavsearch(){
    this.setState({load :true},()=>{
        let _this = this;
        let request={
            url:settings.endpoints.getUserFavsearch,
            method:'POST',
            params:{token:this.state.token}
        }   
        api.FetchData(request).then((result)=>{        
            if(result.status){
                _this.setState({
                    load :false,
                    searchData:result.searchList != undefined ? result.searchList : [],
                    searchLength:result.searchList.length != undefined ? result.searchList.length :0
                }); 
            }else{
                _this.setState({
                    load :false,                  
                })
            } 
        }).catch((error)=>{
            //console.log(error);
        });
    });
    
}
goto(page){
    this.props.navigator.push({
      screen: page
    });
}
deleteSearch(key){
    let _this = this;    
    Alert.alert(
    'effacer!!',
    'Êtes-vous sûr',
    [
        {text: 'Non', onPress: () => '', style: 'cancel'},
        {text: 'Oui', onPress: () => _this.deleteSe(key)},
    ],
    { cancelable: false }
    )
}
deleteSe(key){
    let _this = this;    
    let markers = this.state.searchData;
    let index = markers.indexOf(markers[key]);      
    let request={
        url:settings.endpoints.deleteUserFavSearch,
        method:'POST',
        params:{token:this.props.auth.token,id:markers[key].id}
    }
    api.FetchData(request).then((result)=>{
        if(result.status){
            if (index > -1) {
                markers.splice(index, 1);
                _this.setState({markers});
            }
        }else{
            alert('Somthing Went Wrong');
        }
    }).catch((error)=>{
        //console.log(error);
    });
}
edit(page,id){
    this.props.navigator.push({
    screen: 'CreatResearch',
    passProps:{
            token:this.props.auth.token, 
            userdata:this.props.auth.userdata,
            id:id
        } 
    });           
}
passData(dt){
    let obj={};
    obj.search_string=dt.keyword;
    obj.search_in_title=false;
    obj.search_urgent=false;
    obj.category_id=dt.category;
    obj.ad_type=dt.ad_type;
    obj.category_filter=typeof dt.filter != 'undefined' ? dt.filter.filter_data:{};
    obj.location=typeof dt.location != 'undefined' ? dt.location:{};
    this.props.navigator.push({
        screen: 'ProductgridContainer',
        passProps:{
            token:this.props.auth.token,
            userdata:this.props.auth.userdata, 
            params:obj,
            filterType:'advance'       
        }
    });
}
render() {
  let _this = this;
  let searchDetails = _this.state.searchLength > 0 ? (
        _this.state.searchData.map(function (item,key) {
           return (  <TouchableOpacity key={key+200}  style={[styles.inlineDatarow,styles.favorisCard,styles._MT0_]} onPress={()=>
               {_this.passData(item)}
               }>
                <View style={[styles.row]}>
                    <View style={styles._MR10_}>
                        <Text style={[styles.favouritesTitle,styles.bold,styles._PB5_]}>{item.name}</Text> 
                        {/* <Text style={[styles.contentfavCard,styles._FFM_]}>{item.fav_search_date}</Text>  */}
                        <Text style={[styles.contentfavCard,styles._FFM_]}>{item.summary}</Text>
                    </View>
                </View> 
                 <View style={[styles.inlineDatarow]}>
                    <TouchableOpacity  style={[styles._BR0_]} onPress={()=>{}}
                    //onPress={()=>_this.edit('favSearch',item.id)} activeOpacity={0.8}
                    >
                    <Icon name={'play-symbol'} color={'#888'} size={18} />
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={()=>_this.deleteSearch(key)} style={[styles.btnSquare]} activeOpacity={0.8}>
                        <Text style={[styles.globalIcon,styles.orangeColor,styles.xlIco]}>8</Text>
                    </TouchableOpacity> */}
                </View> 
            </TouchableOpacity>)
        })
        ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;;
    return (
        this.state.load ?<View style={[styles.Center]} ><Loader/></View>:
      <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
           <View style={[styles.scrollInner,styles._HP15_,styles._PT15_]}>
               {searchDetails}
           </View>
      </ScrollView>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(favourites);
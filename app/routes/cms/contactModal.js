import React, { Component } from 'react';
import {
  Text,View,TextInput,TouchableOpacity,ScrollView
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
// import CheckBox from 'react-native-check-box';
import CheckBox from '../../lib/react-native-check-box';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import DropdownAlert from 'react-native-dropdownalert';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;

class contactModal extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: require('../../images/back.png'),
  //      id: 'back',
  //      title:'Back To Login',
  //    }],
  // };
  static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    navBarTransparent: false,
    navBarTranslucent: false    
};

  constructor(props) {
    super(props);
    this.state={
        check1:false,
        button:'+',
        name:'',
        email:'',
        tel:'',
        message:''
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
  }

  setData(){
    if(api.checkLogin(this.props.auth.token)){
      console.log('dsjbfgi');
      let data = this.props.auth.userdata;
      this.setState({
        name:data.username,
        email:data.email,
        tel:data.phone
      });
    }
  }

  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
      api.setAction(this.props.chatActions,this.props.authActions);
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: true
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title: "Contact",
    });
     this.props.navigator.setButtons({
         leftButtons: [{
          icon: iconsMap['back'],
          id: 'back2',
          title:'Back To Login',
     }],animated:false})
    }
    if(event.id=='didAppear'){
      this.setData();
    }
    
    if(event.id == 'back2'){
      this.props.navigator.dismissAllModals({
        animationType: 'slide-down' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
      });
      // this.props.navigator.pop({
      //   animated: true // does the pop have transition animation or does it happen immediately (optional)
      // });
    }  
  }
  goto(page){
    this.props.navigator.push({
      screen: page
    });
  }
  SendDetail(){    
    let _this = this;    
    if(this.props.page == 'ProductDetail'){  
      if(this.state.name  == '' || this.state.email  == ''|| this.state.message  == ''|| this.state.tel  == '') {
        this.dropdown.alertWithType('error', 'Erreur', 'Remplissez tous les champs');
        return false;
      }
      if(this.state.email != ''){
          var atpos = this.state.email.indexOf("@");
          var dotpos =this.state.email.lastIndexOf(".");
          if (atpos<1 || dotpos<atpos+2 || dotpos+2>=this.state.email.length) {
              this.dropdown.alertWithType('error', 'Erreur', 'Adresse courriel invalide');
              return false;
          }
      } 
      _this.setState({load:true},()=>{
        let request={
          url:settings.endpoints.usercontact,
          method:'POST',
          params:{name:this.state.name,email:this.state.email,telephone:this.state.tel,message:this.state.message,ad_id:this.props.id,also_send_me:this.state.check1 ? 1 : 0}
        }
        api.FetchData(request).then((result)=>{ 
            _this.setState({load:false},()=>{
              if(result.status){
                this.dropdown.alertWithType('success', 'Succès', result.message);
              }else{
                this.dropdown.alertWithType('error', 'Erreur', result.message);
              }  
            });
                     
        }).catch((error)=>{
            console.log(error);
            _this.setState({load:false});
        });
      });  
      
    }
  }
  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
           <View style={[styles.scrollInner,styles._HP15_,styles._PT15_]}>
                  <Text style={styles.subTitle}>Pour contacter le vendeur, veuillez remplir le formulaire.</Text>
                  <FloatLabelTextInput value={this.state.name} style={[styles.genInput]} placeholder={"Votre nom"} underlineColorAndroid={'#eee'}
                  onChangeTextValue ={(val) => {
                  this.setState({name:val})}} 
                  />
                  <FloatLabelTextInput value={this.state.email} style={[styles.genInput]} placeholder={"Votre adresse email"} underlineColorAndroid={'#eee'}
                  onChangeTextValue ={(val) => {
                  this.setState({email:val})}} 
                  />
                  <FloatLabelTextInput value={this.state.tel} style={[styles.genInput]} placeholder={"Votre téléphone (facultatif)"} underlineColorAndroid={'#eee'}
                  onChangeTextValue ={(val) => {
                  this.setState({tel:val})}} 
                  />
                  <FloatLabelTextInput value={this.state.message} style={[styles.genInput]} placeholder={"Votre message"} underlineColorAndroid={'#eee'}
                  onChangeTextValue ={(val) => {
                  this.setState({message:val})}} 
                  />
                  <CheckBox style={[styles.genChk,styles._MT15_,styles._PB15_]}
                      onClick={(checked) =>{this.setState({check1:checked})}}
                      labelStyle={styles.midum} isChecked={this.state.check1}
                      rightText={'Recevoir une copie de ce message'}
                  />
                  {/* <TouchableOpacity activeOpacity={0.8} onPress={()=>this.SendDetail()}>
                      <Text style={[styles.smallBtn,styles.orange,styles.w120]}>Envoyer</Text>
                  </TouchableOpacity> */}
                  <LoaderButton load={this.state.load} onPress={()=>this.SendDetail()} BtnStyle={[styles.brandColor,styles._MT20_,styles.justCenter,{height:40,borderRadius:5}]} text={'Envoyer'} textStyle={styles.genButton2} />
              </View>
               <DropdownAlert
        ref={(ref) => this.dropdown = ref}
        onClose={(data) => {/*console.log(data);*/}} />
      </ScrollView>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(contactModal);

import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import Accordion from '../../lib/react-native-collapsible/Accordion';
import { iconsMap, iconsLoaded } from '../../config/icons';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;

class  information extends Component {
  _renderHeader(section) {
    return (
      <View style={styles.conditionsubTitle}>
        <Text style={styles.conditionsubtext}>{section.name}</Text>
        <Text style={styles.plusTab}>{'+'}</Text>
      </View>
    );
  }
  _renderContent(section) {
    return (
      <View style={styles.conditionContent}>
        <Text>{section.description}</Text>
      </View>
    );
  }
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: images.back,
  //      id: 'back',
  //      title:'Back To Login',
  //    }],
  // };

  constructor(props) {
    super(props);
    this.state={
        check1:false,
        button:'+',
        legalInfo:[]
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  static navigatorStyle = {
    // navBarBackgroundColor: colors.navBarBackgroundColor,
    // navBarTextColor: colors.navBarTextColor,
    // navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    // navBarButtonColor: colors.navBarButtonColor,
    // statusBarTextColorScheme: colors.statusBarTextColorScheme,
    // statusBarColor: colors.statusBarColor,
    // tabBarBackgroundColor: colors.tabBarBackgroundColor,
    // tabBarButtonColor: colors.tabBarButtonColor,
    // tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    // navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
    //tabBarHidden:true,
};

  onNavigatorEvent(event) {
    if(event.id=='willAppear'){
      api.setAction(this.props.chatActions,this.props.authActions);
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: false
      });
      this.props.navigator.toggleTabs({
        to: 'hidden',
        animated: false
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({title: "Mentions legales"});
      this.props.navigator.setButtons({
             leftButtons: [{
                icon: iconsMap['back'],
                id: 'back2',
                title:'back to welcome',
            }],
            animated:false
        });   
    }
    if(event.id=='didAppear'){

    }
    
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }
  goto(page){
    this.props.navigator.push({screen: page});
  }

  componentWillMount(){
      iconsLoaded.then(()=>{});
  }

  componentDidMount(){ 
    this.getTermsData();
  }
  getTermsData(){
      
      let request={
          url:settings.endpoints.legalInfo,
          method:'POST',
          params:{}
      }
      api.FetchData(request).then((result)=>{  
          if(result.status){
            this.setState({
              legalInfo:result.result.length > 0 ?result.result:[]
            });
          }    
      }).catch((error)=>{
          //console.log(error);
      });
  }
  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
           <View style={[styles.scrollInner,styles._HP15_,styles._PT15_]}>
                  <View style={styles.conditionMain}>
                      <Accordion
                      sections={this.state.legalInfo}
                      renderHeader={this._renderHeader}
                      renderContent={this._renderContent}
                    />     
                  </View>   
           </View>
      </ScrollView>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(information);





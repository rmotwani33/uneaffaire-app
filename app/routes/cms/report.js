import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch,Dimensions
} from 'react-native';
import { colors } from '../../config/styles';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import DropdownAlert from 'react-native-dropdownalert';
import { iconsMap, iconsLoaded } from '../../config/icons';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const { height, width } = Dimensions.get('window');
const api= new ApiHelper;

class Report extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: images.back,
  //      id: 'back',
  //      title:'back to welcome',
  //    }],
  // };
static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
    navBarTransparent: false,
    navBarTranslucent: false
};
  
  constructor(props) {
    super(props);
    this.state={
        value: '59240b33fb50563c24621cd6',
        text:'',
        radioArray : typeof this.props.data.radioArray != 'undefined' ? this.props.data.radioArray:[],
        Url:typeof this.props.data != 'undefined' ? this.props.data.Details.adurl :'',
        ad_id:typeof this.props.data != 'undefined' ? this.props.data.Details.id :'',
        token:typeof this.props.auth != 'undefined' ? this.props.auth.token :'',
        load1:false
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
}
  

  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
      api.setAction(this.props.chatActions,this.props.authActions);
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: false
      });
      this.props.navigator.toggleTabs({
          animated: false,
          to: 'hidden'
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true, navBarTransparent: false,
        navBarTranslucent: false});
      this.props.navigator.setTitle({
        title: "Signaler un problème",
      });
      this.props.navigator.setButtons({
         leftButtons: [{
        icon: iconsMap['back'],
        id: 'back2',
        title:'back to welcome',
      }],animated:false})
    }
    if(event.id=='didAppear'){

    }

    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }

  handleOnPress(value){
    this.setState({value:value})
 }

  goto(page){
    this.props.navigator.push({
      screen: page
    });

  }
  doReport(){ 
      this.setState({load1:true},()=>{
        let _this = this;
        let request={
            url:settings.endpoints.adAbuse,
            method:'POST',
            params:{token:this.state.token,ad_id:this.state.ad_id,abuse_cate_id:this.state.value,report:this.state.text,ad_link:this.state.Url}
        }      
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.dropdown.alertWithType('success', 'Success', result.message);
            }else{
                this.dropdown.alertWithType('error', 'Error', result.message);
            }   
            this.setState({load1 :false },()=>{
              setTimeout(() => {
                this.props.navigator.pop({
                  animated: true // does the pop have transition animation or does it happen immediately (optional)
                });
              }, 2500);
            });              
        }).catch((error)=>{
            this.setState({load1 :false });
        });  
      });     
      
  }
  render() {     
     let _this = this;
     let Radios =   this.state.radioArray.length > 0 ? ( 
           this.state.radioArray.map(function (item,key) {       
            return ( 
    <TouchableOpacity style={styles._PB10_} key={key} activeOpacity={0.8}><RadioButton currentValue={_this.state.value} value={item.id} onPress={_this.handleOnPress.bind(_this)}><Text style={[styles.readiText,styles._PR15_,styles._ML10_,styles._F16_]}>{item.name}</Text></RadioButton></TouchableOpacity>)
            })
        ):null;
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
            <View style={styles.scrollInner}>
                  <View style={[styles.productdataMain,styles._HP15_]}>
                      <View>                          
                          {Radios}
                      </View>
                      <View>
                         <TextInput underlineColorAndroid='#CCCCCC' style={styles.genTextarea} multiline={true} numberOfLines={5} placeholder={"Commentaires"}
                          onChangeText ={(val) => {
                          this.setState({text:val})}} 
                          />
                         {/* <TouchableOpacity style={[styles.brandColor,styles._MT10_]} activeOpacity={0.85} onPress={()=>this.doReport()}>
                            <Text style={styles.genButton} >Envoyer</Text>
                         </TouchableOpacity>  */}
                         <LoaderButton load={this.state.load1} onPress={()=>this.doReport()} BtnStyle={[styles.brandColor,styles._MT10_]} text={'Envoyer'} textStyle={[styles.genButton]} />
                      </View>
                  </View>
            </View>
            <DropdownAlert
        ref={(ref) => this.dropdown = ref}
        onClose={(data) => {/*console.log(data);*/}} />
        </ScrollView>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Report);
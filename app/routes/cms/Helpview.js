import React, { Component } from 'react';
import {Text,View} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import { iconsMap, iconsLoaded } from '../../config/icons';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

class  Helpview extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: images.back,
  //      id: 'back',
  //      title:'Back To Login',
  //    }],
  // };
  
// static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
// };

  constructor(props) {
    super(props);
    this.state={
        check1:false,
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
}

  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: true
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({title: "Macpro"});
      this.props.navigator.setButtons({
         leftButtons: [{
       icon: iconsMap['back'],
       id: 'back2',
       title:'Back To Login',
     }],animated:false})
    }
    if(event.id=='didAppear'){

    }
    
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    
  }
  goto(page){
    this.props.navigator.push({screen: page});
  }
  render() {
    return (
          <View style={[styles.main,styles._HP15_,styles._PT15_]}>
                <View style={styles.apropsMain}>
                    <Text style={styles.contentAprops}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                       it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, 
                       remaining essentially unchanged.</Text>
                    <Text style={styles.contentAprops}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                       it to make a type specimen book.</Text>   
                    <Text style={styles.contentAprops}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                      Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                       it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, 
                       remaining essentially unchanged.</Text>        
                </View>                 
            </View>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Helpview);





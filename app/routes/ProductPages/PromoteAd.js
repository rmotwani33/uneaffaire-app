import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,WebView,Alert
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
import CheckBox from '../../lib/react-native-check-box';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import { iconsMap, iconsLoaded } from '../../config/icons';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import Loader from '../../components/Loader/Loader';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import moment from 'moment';
import PayPalPayment from '../../redux/utils/paypal';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const api= new ApiHelper;

class PromoteAd extends Component {

  
static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    drawUnderTabBar:true,
};

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.state={
        value:1,
        value1:1,
        value2:1,
        scrollY: new Animated.Value(0),
        check1:false,
        //adList:[],
        //adListLen:0,
        btnLoad:false,
        priceData:{},
        adData:{},
        adId:null,
        loading:true,

        alreadyAd:false,
        alreadyList:false,
        
        isAdShowcase:false,
        isListShowcase:false,
        adShowcase:'',
        listShowCase:'',
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}
componentWillMount(){
    iconsLoaded.then(()=>{
        //this.getDataOfId();
    });
}

componentDidMount() {
    
}
onNavigatorEvent(event) {
    if(event.id=='willAppear'){
        // this.props.navigator.toggleNavBar({
        //     to: 'shown',
        //     animated: true
        // });
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
        this.props.navigator.toggleTabs({to:'hidden',animated:true});
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({title: "Promouvoir votre annonce"});  
        this.props.navigator.setButtons({
             leftButtons: [{
                icon: iconsMap['back'],
                id: 'back2',
                title:'back to welcome',
            }],
            animated:false
        });
        if(this.state.adId === null){
            this.getPriceData();
        }
            
    }
    if(event.id=='didAppear'){
       
    }

    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    if(event.id == 'menu'){
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }   
}
goto(page){   
    this.props.navigator.push({
        screen: page
    }); 
}
getData(data){
    let request={
        url:settings.endpoints.promotAd,
        method:'POST',
        params:{token:this.props.auth.token,ad_id:data.id}
    }
    api.FetchData(request).then((result)=>{           
            if(result.status){
                
                    let isAdShowcase=result.ad_obj.showcase_data.is_in_showcase == 1 ? true : false;
                    let isListShowcase=result.ad_obj.toplist_data.is_in_top_list == 1 ? true : false;
                    let adShowcase=isAdShowcase ? 
                    this.getAdDataById(result.ad_obj.showcase_data.showcase_duration_id,'showcase') : '';
    
                    let listShowCase=isListShowcase ? 
                    this.getAdDataById(result.ad_obj.toplist_data.top_list_duration_id,'toplist') : '';
                    
              
                
                this.setState({
                    adData:result.ad_obj,
                    adId:data.id,
                    isAdShowcase:isAdShowcase,
                    isListShowcase:isListShowcase,
                    adShowcase:adShowcase,
                    listShowCase:listShowCase,
                    alreadyAd:isAdShowcase,
                    alreadyList:isListShowcase,
                    loading:false});
            }else{
                this.setState({loading:false});
            }
        }).catch((error)=>{
            this.handleError('Impossible d\'obtenir des données');
        });
}
gettrans(bool){
    if(bool){
        return  this.state.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });
    }else{
        return  this.state.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [-50, 0],
            extrapolate: 'clamp',
        });  
    }
}
checked(c){
    this.setState({check1:c});
}

onPay(){
    this.setState({btnLoad:true},()=>{
        let highlight_data = null;
        if(this.state.alreadyAd && this.state.alreadyList){
            highlight_data = null;
        }else if(this.state.alreadyAd){
            highlight_data = {
                //is_in_showcase:this.state.isAdShowcase ? 1 : 0,
                //showcase_data_id:this.state.isAdShowcase ? this.state.adShowcase.id : '',
                is_in_top_list:this.state.isListShowcase ? 1:0,
                top_list_data_id:this.state.isListShowcase ? this.state.listShowCase.id : '',
            };
        }else if(this.state.alreadyList){
            highlight_data = {
                is_in_showcase:this.state.isAdShowcase ? 1 : 0,
                showcase_data_id:this.state.isAdShowcase ? this.state.adShowcase.id : '',
                //is_in_top_list:this.state.isListShowcase ? 1:0,
                //top_list_data_id:this.state.isListShowcase ? this.state.listShowCase.id : '',
            };  
        }else{
            highlight_data = {
                is_in_showcase:this.state.isAdShowcase ? 1 : 0,
                showcase_data_id:this.state.isAdShowcase ? this.state.adShowcase.id : '',
                is_in_top_list:this.state.isListShowcase ? 1:0,
                top_list_data_id:this.state.isListShowcase ? this.state.listShowCase.id : '',
            };
        }  
            if(highlight_data != null){
                let request = {
                    url:settings.endpoints.savePromotAd,
                    method:"POST",
                    params:{
                        token:this.props.auth.token,
                        ad_id:this.props.data.id,
                        highlight_data:highlight_data
                    }
                };
                api.FetchData(request).then((result)=>{
                    this.setState({btnLoad:false},()=>{
                        
                        let res = '';
                        if(result.status){                        
                            if(result.paypal){
                                //console.log(highlight_data); 
                                this.payWithPaypal(highlight_data,result.amount);  
                            }else{
                                res = 'Y';
                                this.props.navigator.push({
                                    screen: 'success',
                                    passProps:{
                                        status:res,
                                        message:result.message,
                                        token:this.props.auth.token,
                                        userdata:this.props.auth.userdata
                                    }
                                });
                            }                       
                        }else{
                            this.setState({btnLoad:false},()=>{
                                Alert.alert(
                                    'Alerte!',
                                    'Veuillez sélectionner une option puis payer',
                                    [
                                        {text: 'Ok', onPress: () => {
                                            this.handleAfter(result);
                                        }},
                                    ],
                                    { cancelable: false }
                                )
                                // alert('Veuillez sélectionner une option puis payer');
                            }); 
                        }                   
                    });                
                }).catch((err)=>{
                    this.setState({btnLoad:false},()=>{
                        console.log(err);
                        Alert.alert(
                            'Alerte!',
                            'Veuillez sélectionner une option puis payer',
                            [
                                {text: 'Ok'},
                            ],
                            { cancelable: false }
                        )
                    });        
                });    
            }
    });
    //let _this=this;
    //setTimeout(function(){
    //    _this.setState({btnLoad:false});
    //},200);

    /* 
    token,ad_id,is_in_showcase, showcase_data_id,is_in_top_list,
top_list_data_id
    
    */
    }
payWithPaypal(highlight_data,amount){
    let total = amount;
    if(total != '' && total != 0){
        let price = parseFloat(total).toFixed(2);
        let params = {
            price: price,
            currency: 'EUR',
            description: 'Promote Add',
            custom:'paypal_promot'
        };
        //console.log(""+price);return false;
        PayPalPayment(params).then(confirm => {
            if(confirm.response.state=='approved'){
                this.goSuccess(confirm,highlight_data);           
            }else{
                alert('Something went wrong in payment');
            }      
        }).catch(error => {console.log(error); alert('Something went wrong in payment')});        
    }
}
goSuccess(confirm,data){
    this.setState({loading:true},()=>{
        let request={
            url:settings.endpoints.hightlightAdTransaction,
            method:'POST',
            params:{
                token:this.props.auth.token,
                ad_id:this.props.data.id,
                highlight_data:data,
                paypal_id:confirm.response.id
            }
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({loading:false},()=>{
                    this.props.navigator.push({
                        screen: 'success',
                        passProps:{
                            status:'Y',
                            message:result.message,
                            token:this.props.auth.token,
                            userdata:this.props.auth.userdata
                        }
                    });
                });             
            }else{
                this.setState({loading:false},()=>{
                    alert('Quelque chose s\'est mal passé dans le paiement.')
                });
            } 
        }).catch((error)=>{
            console.log(error);
            this.setState({loading:false},()=>{
                alert('Quelque chose s\'est mal passé dans le paiement.')
            });
        });
    });
    
}

edit(data){
    this.props.navigator.push({
        screen: 'Modifier',
        passProps:{
        data:this.props.data,
        token:this.props.auth.token,
        userdata:this.props.auth.userdata,
        }
    }); 
}

getAdDataById(id,type){
    let ret={};
    let mapAry=[];
    if(type=='showcase'){
        mapAry=this.state.priceData.showcase_price;
    }else if(type=='toplist'){
        mapAry=this.state.priceData.toplist_price;
    }
    mapAry.map((data,i)=>{
        if(data.id == id){
            ret=data;
        }
    });
    return ret;
}

getPriceData(){
    this.setState({loading:true},()=>{
        if(this.state.priceData!== undefined && Object.keys(this.state.priceData).length === 0){
            let request={
            url:settings.endpoints.postPriceList,
            method:'POST',
            params:{}
        }
            api.FetchData(request).then((results)=>{
                if(results.status){
                    this.setState({priceData:results.post_prices},()=>{
                        if(this.props.data !== undefined && this.props.data !== null){
                            this.getData(this.props.data);
                        }
                    });
                }else{
                    this.handleError('Impossible d\'obtenir des données');
                }
            }).catch((error)=>{
                this.handleError('Impossible d\'obtenir des données');
            });
        } 
    });
    
}

getDispPrice(num){
    return num+'€';
}

calculatePrice(){
    let price=0;
    if(!this.state.alreadyAd && this.state.isAdShowcase){
        if(typeof this.state.adShowcase.price != 'undefined' && !isNaN(parseFloat(this.state.adShowcase.price))){
            price=price+this.state.adShowcase.price;
        }
    }
    if(!this.state.alreadyList && this.state.isListShowcase){
        if(typeof this.state.listShowCase.price != 'undefined' && !isNaN(parseFloat(this.state.listShowCase.price))){
            price=price+this.state.listShowCase.price;
        }
    }
    return price;
}

handleError(msg){
    Alert.alert(
            'Précurseur',
            msg,
            [
                {text: 'Annuler', onPress: () => {}, style: 'cancel'},
                {text: 'Approuvé', onPress: () => {this.goback()}},
            ],
            { cancelable: false }
        )
}

goback(){
    this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
    });
}

/* extra methods finish */

render() {
    const {adData , loading,priceData,alreadyAd,alreadyList}=this.state;
    //console.log(adData);
    let totPrice=0.00;
    if(!loading){
        totPrice = this.calculatePrice();
    }
    let dataDate = !loading ?   moment(adData.post_on).format("DD MMM [|] hh:mm a"):'';
    let data = !loading ? ( <View>             
               <Row size={12} style={[styles.proRow,styles.prolistRow,styles._MT0_,styles._PT0_,styles.Prolistgray]}>
                    <Col sm={5} md={4} lg={3} style={[styles.proCol,styles.listCol]} >
                        <View style={styles.imageCanvas}>
                            <View style={styles.imgInner}>
                                {adData.is_video_check == 1 ? <Image style={styles.hasVideo} source={images.video} />:null}
                                {adData.image_name != '' ? <Image style={[styles.proImg]} source={{uri:adData.image_name}} resizeMode="stretch"/>:null}
                            </View>
                        </View>
                        <View style={[styles.bottomDetail,styles.bottomCaption]}>
                            <Text style={[styles.leftAlign,styles.white]}>
                                <Text style={styles.small}>{dataDate}</Text>
                            </Text>
                            <View style={[styles.rightAlign]}>
                                <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}>s</Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>{adData.image_count}</Text></View> 
                            </View>
                        </View>
                        </Col>
                    <Col sm={7} md={8} lg={9} style={[styles.proCol,styles.listCol]}>
                        <View style={styles.proCaption}>
                                <Text style={[styles.proName,styles.big]} numberOfLines={1}>{adData.title}</Text>
                                <View style={styles.row}>
                                    <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                        <Text style={[styles.orangeColor,styles.bold]}>{adData.price} </Text>
                                    </View>
                                    <View style={[styles.inlineDatarow,styles._MT10_,styles._PB15_]}>
                                        {adData.is_urgent == 1 ?  <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>:null}
                                       {adData.is_direct_sale == 1 ? <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>:null}
                                        
                                    </View>
                                    <View style={[styles.inlineDatarow]}>
                                        <Text  style={[styles.globalIcon,styles.calenderAvant]}>e</Text><Text style={styles._PL5_}>{adData.days_remain} Jours Restants</Text> 
                                    </View>  
                                </View>
                               
                            </View>
                            
                    </Col>
                </Row>
                <TouchableOpacity style={styles._MT10_} onPress={()=>this.edit(adData)}>                            
                    <Text style={{fontSize:14}}><Text style={{fontSize:14,fontFamily:'icomoon',color:colors.navBarButtonColor}}>h</Text>  Modifier plus d’options : Texte, Photos, Video, Vente direct</Text>
                </TouchableOpacity>
                </View>
    ):<View style={[styles._MT15_,styles.Center]}><Text>Aucune donnée disponible</Text></View>;
    return (
        <View style={{flex:1,backgroundColor:"#fff"}}>
            {loading ? (<View style={[styles.Center]} ><Loader/></View>):(
            <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview,{flexGrow:1,padding:7}]}>
                <View style={styles.scrollInner}>
                    <View style={styles.proMain}>
                        {data}                      
                    </View>
                        <View style={styles._MT20_}>
                            <Text style={styles.blockTitle}>{'+ Mettre en avant'}</Text>
                            {/* <View style={[styles.row,styles._MT5_,styles._PB20_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check1}
                                        rightText={'Mon Annonce En Vitrine - Moins De 1€ / J'}
                                    />
                                    <View style={[styles._MT10_,styles.inlineDatarow]}>
                                            <Image style={styles.leftbnImg} source={images.envitrine} />
                                            <View style={styles._PL10_}>
                                                <RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PB10_]}>Durant 7 jours - 5,00 €</Text></RadioButton>
                                                <RadioButton currentValue={this.state.value} value={2} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PB10_]}>Durant 30 jours - 20,00 €</Text></RadioButton>
                                            </View>
                                    </View>
                            </View> */}

                            {/* Ad show case data */}
                            <View style={[styles.row,styles._PB20_]}>
                                    {alreadyAd ? (<View style={{position:'absolute',backgroundColor:"#0005",height:'100%',width:'100%',zIndex:22}}></View>):(null)}
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{
                                            if(!alreadyAd){
                                            let adShowcase='';
                                            if(checked){
                                                adShowcase=priceData.showcase_price[0];
                                            }
                                            this.setState({isAdShowcase:checked,adShowcase:adShowcase});
                                            }
                                            }}
                                        labelStyle={styles.midum} isChecked={this.state.isAdShowcase}
                                        rightText={'Mon Annonce En Vitrine - Moins De 1€ / J'}
                                    />
                                    <View style={[styles._MT10_,styles.inlineDatarow]}>
                                            <Image style={styles.leftbnImg} source={images.envitrine} />
                                            <View style={styles._PL10_}>
                                                {priceData.showcase_price.map((data,i)=>{
                                                    return <RadioButton key={'showcase_price'+i} currentValue={this.state.adShowcase} value={data} 
                                                onPress={(val)=>{ if(!alreadyAd){
                                                    this.setState({adShowcase:val,isAdShowcase:true})
                                                   }
                                                }}>
                                                    <Text style={[styles.readiText,styles._PB10_,styles._MT0_]}>{data.label}{' - '}{(data.price)}{'€'}</Text>
                                                </RadioButton>
                                                })}
                                            </View>
                                    </View>
                            </View>
                            {/* <View style={[styles.row,styles._PB20_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check1}
                                        rightText={'Mon Annonce En Tête De Liste'}
                                    />
                                    <View style={[styles._MT10_,styles.inlineDatarow]}>
                                        <Image style={styles.leftbnImg} source={images.bn1} />
                                        <View style={styles._PL10_}>     
                                            <RadioButton currentValue={this.state.value1} value={1} onPress={this.handleOnPress1.bind(this)}><Text style={[styles.readiText,styles._PB10_]}>Remonter maintenant - 1,00 €</Text></RadioButton>
                                        </View>
                                    </View>
                            </View> */}
                            {/* toplist data*/}
                            <View style={[styles.row,styles._PB20_]}>
                            {alreadyList ? (<View style={{position:'absolute',backgroundColor:"#0005",height:'100%',width:'100%',zIndex:22}}></View>):(null)}
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{
                                            if(!alreadyList){
                                            let listShowCase='';
                                            if(checked){
                                                listShowCase=typeof priceData.toplist_price[3] !='undefined' ? 
                                                priceData.toplist_price[3]:priceData.toplist_price[0];
                                            }
                                            this.setState({isListShowcase:checked,listShowCase:listShowCase});
                                            }
                                            }}
                                        labelStyle={styles.midum} isChecked={this.state.isListShowcase}
                                        rightText={'Mon Annonce En Tête De Liste - Moins De 1€ / J'}
                                    />
                                    <View style={[styles._MT10_,styles.inlineDatarow]}>
                                            <Image style={styles.leftbnImg} source={images.bn1} />
                                            <View style={styles._PL10_}>
                                                {typeof priceData.toplist_price[3] !='undefined' ? (
                                                    <RadioButton currentValue={this.state.listShowCase} value={priceData.toplist_price[3]} onPress={(val)=>
                                                        {if(!alreadyList){
                                                            this.setState({listShowCase:val,isListShowcase:true});
                                                        }
                                                        }}>
                                                    <Text style={[styles.readiText,styles._PB10_,styles._MT0_]}>{priceData.toplist_price[3].label}{' - '}{this.getDispPrice(priceData.toplist_price[3].price)}</Text>
                                                </RadioButton>):(null)}

                                                {typeof priceData.toplist_price[3] != 'undefined' ?(<View style={{height:1,backgroundColor:"#000",width:'100%',marginBottom:10}}></View>):(null)}

                                                {priceData.toplist_price.map((data,i)=>{
                                                    if(i<3){
                                                    return <RadioButton key={'toplist_price'+i} 
                                                    currentValue={this.state.listShowCase} value={data} 
                                                    onPress={(val)=>{
                                                    if(!alreadyList){
                                                        this.setState({listShowCase:val,isListShowcase:true})
                                                    }
                                                    }}>
                                                    <Text style={[styles.readiText,styles._PB10_,styles._MT0_]}>{data.label}{' - '}{this.getDispPrice(data.price)}</Text>
                                                </RadioButton>}else{
                                                    return null;
                                                }
                                                })}
                                            </View>
                                    </View>
                            </View>
                            
                            <View style={[styles._PB20_]}>
                                   <View style={[styles._MT10_,styles._PT15_,styles.flex]}>
                                            <View style={[styles.spaceBitw,styles.inlineDatarow]}><Text style={styles.bold}>Prix total :</Text><Text>{this.getDispPrice(totPrice)}</Text></View>
                                    </View>
                                    <View style={styles._MT15_}>
                                        <Text>En continuant, j'accepte les conditions d'utilisation de UneAffaire.fr (y compris la renonciation au droit de rétractation).</Text>
                                    </View>  
                                    {!this.state.alreadyAd || !this.state.alreadyList ? 
                                    <View style={{padding:7}}> 
                                        <LoaderButton load={this.state.btnLoad} onPress={()=>this.onPay()} BtnStyle={[styles.brandColor]} text={'Payer'} textStyle={styles.genButton} />
                                    </View>:null
                                    }
                            </View>
                        </View>
                </View>
            </ScrollView>)}
        </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(PromoteAd);
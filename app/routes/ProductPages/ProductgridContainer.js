import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,DrawerLayoutAndroid,Modal,ActivityIndicator,RefreshControl,Keyboard,Easing,StatusBar
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {SharedElementTransition} from 'react-native-navigation';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {settings} from '../../config/settings';
import Loader from '../../components/Loader/Loader';
import {GridItem,GridItemFull,ListItem} from '../../components/GridComponents'; 
import Tabs from '../../components/Tabs/Tabs';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
import startMain from '../../config/app-main';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const { height, width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const FORTAB = width < 1025 && width > 721;
const TABLANDSCAPE = width < 1025 && width > 768;
const TABPORTRAIT = width > 721 && width < 769;
const api= new ApiHelper;
const sensitivity= Platform.OS === 'ios' ? 60:40;


class ProductgridContainer extends Component {

static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    //navBarHidden:false,
    //tabBarHidden:false,
    //navBarHideOnScroll:true,
    drawUnderNavbar: true,
    drawUnderTabBar: true,
};
constructor(props) {
    super(props);
    //console.log(this.props);
    this.hs=0;
    this.loadingpage = false
    this.prevOffset=0;
    // if you want to listen on navigator events, set this up
    this.state={
        tabs:1,
        view:1,
        tabSelected:1,
        mainTab:4,
        like:1,
        scrolled:true,
        modalVisible: false,
        scrollY: new Animated.Value(0),
        mainItems:{ offers:{
            toutes:[],
            particulars:[],
            proffessional:[]
        }, demands:{
            toutes:[],
            particulars:[],
            proffessional:[]
        } },
        displayItems:[],
        load:true,
        refreshing:false,
        region:typeof this.props.region != 'undefined' ? this.props.region:'',
        basicSearch:typeof this.props.params != 'undefined' ? this.props.params:{},
        filterType:typeof this.props.filterType != 'undefined' ? this.props.filterType:{},
        type:typeof this.props.params != 'undefined' && this.props.params.ad_type != 'undefined' ? this.props.params.ad_type :  'offers',
        bookLoad:false,
        reload:false,
        name:'',
        searchString:typeof this.props.params != 'undefined'?typeof  this.props.params.searchTxt != 'undefined' ? this.props.params.searchTxt : this.props.params.search_string :'',
        more:true,
        page:1,
        sortBy:'date',
        tabShow: new Animated.Value(120),
        tabScale:new Animated.Value(1),
        navBarTabBar:true,
        bookMarkTitle : ''
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
   // console.log(this.props);
}

componentWillMount(){
    iconsLoaded.then(()=>{
        //this.getData();
        
    });
}

componentDidMount(){
    //this.getData(); 
    //console.log('did mount');
    this.handleNavBarAndTabBar(true);   
}
saveBookMark(){
    
    this.setState({bookLoad:true},()=>{
        let tab = this.state.tabs;
        if(tab==1){
            tabType='all'; 
        }
        else if(tab==2){
            tabType='particulars'; 
        }
        else if(tab==3){
            tabType='professional'; 
        }
        let params = this.props.params;
        let _this = this;
        let request={
             url:settings.endpoints.createFavSearch,
             method:'POST',
             params:{token:this.props.auth.token,fav_search_name: this.state.bookMarkTitle,fav_search_query:  this.state.basicSearch.search_string,fav_search_category:params.category_id,adtype:params.ad_type,usertype:tabType,attribute_id:params.category_filter,location:params.location}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                 this.setState({bookLoad :false},()=>this.setModalVisible(false));             
            }else{
                 this.setState({bookLoad :false});
            }               
        }).catch((error)=>{ 
             //console.log(error);
             this.setState({bookLoad :false});
        }); 
    });
    
}
_refreshControl(){
    return (
      <RefreshControl
        colors={[colors.navBarButtonColor]}
        tintColor={colors.navBarButtonColor}
        style={{alignItems: 'center', justifyContent: 'center', alignSelf: 'center'}}
        refreshing={this.state.refreshing}
        onRefresh={()=>this.tabchange(this.state.tabs)} progressViewOffset={80}/>
    )
}
sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
}
onNavigatorEvent(event) {
    console.log(event);
    if (event.id === 'bottomTabSelected') {
        if(event.selectedTabIndex == 3){
            this.props.navigator.popToRoot({
                animated: true,
            });
        }
    }
    if(event.id == 'willAppear'){   
        this.sendEventToDrawer();
        api.setAction(this.props.chatActions,this.props.authActions);
        //this.getTabData(this.state.mainTab,this.state.tabs)         //this.getData();
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
        //console.log('willAppear '+this.state.navBarTabBar);
        //this.handleNavBarAndTabBar(this.state.navBarTabBar);       
        // if(!this.state.navBarTabBar){
        //     this.props.navigator.setStyle({tabBarHidden: true});
        // }
        // this.props.navigator.toggleNavBar({to: 'shown',animated: true,});
        // this.props.navigator.toggleTabs({to: 'shown',animated: true,});
       
        // if(typeof this.state.displayItems != 'undefined' && this.state.displayItems.length > 4){
        //     this.props.navigator.setStyle({tabBarHidden: true});
        // }
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({title: "Annonces"});
        this.refreshNavigatorButtons();
     }
    if(event.id=='didAppear'){
        if(this.state.displayItems.length <= 0 && !this.state.refreshing){
            this.getTabData(4,this.state.tabs,false);
        }
        let bool = this.state.navBarTabBar;
        this.handleNavBarAndTabBar(bool);
        this.handleStatusBar(!bool);
        Keyboard.dismiss();
    }
    if(event.id == 'willDisappear'){
        this.handleStatusBar(false);
    }
    if(event.id == 'back'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    if(event.id == 'list'){
       if(this.state.view==1){
            this.setState({view:2});
            this.refreshNavigatorButtons();
       }else{
            this.setState({view:1});
            this.refreshNavigatorButtons();
       }
    }    
    if(event.id == 'menu'){
         console.log('menu pressed');
         console.log(this.props.navigator.toggleDrawer);
        this.props.navigator.toggleDrawer({
            side: 'left',
            //to:"open",
            animated: true,
        })
    }
    if(event.id =='euro'){
        if(this.state.sortBy != 'price'){
            this.handlePriceButttons('price');
        }else{
            this.handlePriceButttons(null);
        }
    }
    if(event.id == 'calendar'){
        if(this.state.sortBy != 'date'){
            this.handlePriceButttons('date');
        }else{
            this.handlePriceButttons(null);
        }
    }
    if(event.id == 'bookmark'){
        this.setModalVisible(true);
    }else{
        this.setModalVisible(false);
    }     
}
setModalVisible(visible) {
    this.setState({modalVisible: visible});
}
goto(page){ 
    // console.log(this.props);
    // console.log(this.props.fromHome);
    let paramss = {
        token:this.props.auth.token,
        userdata:this.props.auth.userdata,
        dataSearch:typeof this.props.params != 'undefined' ? this.props.params : {},
        region:typeof this.state.region != 'undefined' ? this.state.region:'',
        fromHome : typeof this.props.fromHome !== undefined ? this.props.fromHome : false
    }
    if(this.props.location !== undefined && this.props.location.isCurrent !== undefined && this.props.location.isCurrent){
        paramss.isCurrent = this.props.location.isCurrent;
    }
    this.props.navigator.push({
        screen: page,
        passProps:paramss
    }); 
}
navigateTo(page,data){
    let newData ={};
    if(data !== undefined && data !== null && Object.keys(data).length > 0){
        Object.keys(data).map((key)=>{
            newData[key] = data[key];
        });
    }
      if(page=='ProductDetail'){
        this.props.navigator.push({
        screen: page,
        sharedElements: ['SharedTextId'+data.id],
        passProps:{
          data:newData,
          token:this.props.auth.token,
          userdata:this.props.auth.userdata,  
        }
        }); 
      }else if(page=='Singlechat'){
       this.handleChat(newData);
     } else{
        this.props.navigator.push({
        screen: page,
        passProps:{
            token:this.props.auth.token,
            userdata:this.props.auth.userdata,
        }
        }); 
      }     
}

getShowcaseIds(){
    let ret=[];
    this.state.displayItems.map((d,i)=>{
        if(d.is_in_showcase==1){
            ret.push(d.id);
        }
    });
    return ret;
}

refreshNavigatorButtons(){
    let leftButtons = [{icon: iconsMap['menu'],id: 'menu',title:'Menu'}];
    let rightButtons = [{icon:this.state.view == 1 ? iconsMap['list'] : iconsMap['grid'],id: 'list',title:'grid'}];
    //let rightButtons =[];
    if(typeof this.props.params != 'undefined' && this.state.filterType == 'advance'){
         rightButtons = [
            {icon:this.state.view == 1 ? iconsMap['list'] : iconsMap['grid'],id: 'list',title:'grid'}];
            if(api.checkLogin(this.props.auth.token)){
                rightButtons.push( {icon:iconsMap['bookmark'],id: 'bookmark',title:'bookmark'});
            }
            // {icon:this.state.sortBy == 'price' ? iconsMap['euro-fill']:iconsMap['euro-outer'],id:'euro',title:'euro'},
            // {icon:this.state.sortBy == 'date' ?  iconsMap['calendar-fill']:iconsMap['calendar'],id:'calendar',title:'date'}
    }else{
        //  rightButtons = [
        //     {icon:this.state.view == 1 ? iconsMap['list'] : iconsMap['grid'],id: 'list',title:'grid'},
        //     {icon:this.state.sortBy == 'price' ? iconsMap['euro-fill']:iconsMap['euro-outer'],id:'euro',title:'euro'},
        //     {icon:this.state.sortBy == 'date' ?  iconsMap['calendar-fill']:iconsMap['calendar'],id:'calendar',title:'date'}
        // ];
    }
    this.props.navigator.setButtons({
        leftButtons : leftButtons,
        rightButtons : rightButtons,
        animated : true
    });
}

handlePriceButttons(pg){
    let p;
    if(this.state.sortBy != pg){
         p=pg;
        //this.handlePriceButttons('price');
    }else{
         p=null;
        //this.handlePriceButttons(null);
    }
    this.setState({sortBy:p},()=>{
        this.getTabData(4,this.state.tabs,true);
        this.refreshNavigatorButtons();
    });
}

getTabData(main,tab,refStatus){
    console.log(this.props.location);
    let adType='';
    let tabType='';
    if(main==4){
        adType='offers';
    }else if(main==5){
        adType='demands'
    }
    if(tab==1){
        tabType='toutes'; 
    }
    else if(tab==2){
        tabType='particulars'; 
    }
    else if(tab==3){
        tabType='professional'; 
    }
    let obj={
        ad_type:this.state.type,
        tab_type:tabType,
        token:this.props.auth.token=='Guest'?'':this.props.auth.token,
        region:this.state.region, 
    };
    if(Object.keys(this.state.basicSearch).length !== 0){
        Object.keys(this.state.basicSearch).map((key)=>{
            obj[key]=this.state.basicSearch[key];
        })
    }
    if(this.state.page != 1 && !refStatus){
        obj.page=this.state.page;
        obj.showcase_ids=this.getShowcaseIds();
    }
    if(this.state.sortBy != null){
        obj.sortby= this.state.sortBy;
    }
    if(this.props.location != null && this.props.location !== undefined){
        obj.location= this.props.location
    }
    /*  {ad_type:'offers',tab_type:'toutes',token:this.props.auth.token=='Guest'?'':this.props.auth.token,region:this.state.region,search_string:this.props.params.searchTxt,search_in_title:this.props.params.searchInTitle,search_urgent:this.props.params.urgent,in_history:this.props.params.in_history}*/
    if(refStatus){
        obj.in_history = false;
    }
    let request={
        url:settings.endpoints.itemList,
        method:'POST',
        params:obj
    }
    if(this.state.page == 1 || refStatus){
        this.setState({refreshing:true});  
    }   
    api.FetchData(request).then((result)=>{
    if(result.status){
        let mainItems=this.state.mainItems;
        mainItems[adType][tabType]=result.itemlist;
        let dI=[];
        if(this.state.page != 1 && !refStatus){
            dI=this.state.displayItems;
            dI.pop();
            dI=dI.concat(result.itemlist);
            if(result.itemlist.length > 0){
                this.loadingpage = false;
            }
        }else{
            dI=result.itemlist;
        }
        let pg=this.state.page;
        let mor= this.state.more;        
        if(result.itemlist.length > 0){
            pg+=1;
            mor=true;
        }else{
            mor=false;
        }
        if(refStatus){
            pg=2;
            mor=true;
            this.loadingpage=false;
        }
        this.setState({
            mainItems:mainItems,
            displayItems:dI,
            refreshing:false,
            reload:true,
            page:pg,
            more:mor
        });
    }else{

        let dI=this.state.displayItems;
        if(this.state.page != 1 && !refStatus){
            dI.pop();
        }
        this.setState({
            refreshing:false,
            more:false,
            displayItems:dI
        });
    }
    }).catch((error)=>{
        //console.log(error);
        this.setState({
            refreshing:false
        });
    });
}
tabchange(tab){
    if(tab==1 || tab==2 || tab==3){
        this.getTabData(this.state.mainTab,tab,true);
        this.setState({tabs:tab,tabSelected:tab,});
    }
    else if(tab==4 || tab==5){
        this.getTabData(tab,this.state.tabSelected,true);
        this.setState({mainTab:tab});
    }      
}
gettrans(bool){
    if(bool){
        return  this.state.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });
    }else{
        return  this.state.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [-50, 0],
            extrapolate: 'clamp',
        });  
    }
}
doFav(key,ss){
    if(!api.checkLogin(this.props.auth.token)){
         this.props.navigator.push({
            screen: 'WelcomeContainer',
            passProps:{
                token:this.props.auth.token,
                userdata:this.props.auth.userdata
            }           
         });
        return false;
    }
    let markers = this.state.displayItems;
    markers[key-1].havefave = ss == 0 ? 1 : 0;
    this.setState({markers},()=>{
        let _this = this;
        let request={
             url:settings.endpoints.favUnfavAd,
             method:'POST',
             params:{token:this.props.auth.token,id:markers[key-1].id}
        }
        api.FetchData(request).then((result)=>{
             if(result.status){
                 this.setState({load :false});
             }else{
                 alert('Something Went Wrong.');
             }               
        }).catch((error)=>{ 
             //console.log(error);
        }); 
    });
    
}

addLoader(){
    //console.log(this.props);
    let items = this.state.displayItems;
    items.push({loader:'loader'});
    this.setState({displayItems:items}); 
}

handleStatusBar(bool){
    setTimeout(()=>{
        if(Platform.OS === 'ios'){
            StatusBar.setHidden(bool,'slide');
        } 
   },50)
}

handleNavBarAndTabBar(bool){
    //console.log('navbar tab bar set to '+bool);
    this.setState({navBarTabBar:bool},()=>{
        this.props.navigator.toggleTabs({
            to: bool ? 'shown' : 'hidden',
            animated:true,
        });
        this.props.navigator.toggleNavBar({
            to: bool ? 'shown' : 'hidden',
            animated:true,
        });
    });
}

onScroll(e){
        var currentOffset = e.nativeEvent.contentOffset.y;
        this.hs=currentOffset;
        if(this.loadingpage == false 
            && this.state.more && 
            ((currentOffset + 350) > (this._contentHeight - this._componentHeight))){
            this.loadingpage = true;
            this.addLoader();
            this.getTabData(4,this.state.tabs,false);
     }
        if(Math.abs(currentOffset - this.prevOffset) > sensitivity){
            let _this = this;
            if(currentOffset > this.prevOffset){               
                Animated.parallel([
                    Animated.timing(_this.state.tabScale,{
                        toValue:0.5,
                        duration:50,
                    }),
                    Animated.timing(_this.state.tabScale,{
                        toValue:0.5,
                        duration:50,
                    }),
                    Animated.timing(_this.state.tabShow,{
                        toValue:0,
                        duration:50,
                    })
                ]).start(e => {
                    if(e.finished){
                        // console.log('In if');
                        // _this.props.navigator.toggleTabs({
                        //     to:'hidden',
                        //     animated:true,
                        // });
                        // _this.props.navigator.toggleNavBar({
                        //     to:'hidden',
                        //     animated:true,
                        // });
                        if(Platform.OS === 'android') {
                            this.handleNavBarAndTabBar(false);
                            _this.handleStatusBar(true);
                        }
                       
                    } 
                });                
            }else{
                // this.props.navigator.toggleTabs({
                //     to:'shown',
                //     animated:true,
                // });
                // this.props.navigator.toggleNavBar({
                //     to:'shown',
                //     animated:true,
                // });
                // console.log('In else');
                if(Platform.OS === 'android') {
                    this.handleNavBarAndTabBar(true);
                    this.handleStatusBar(false);
                }
                Animated.parallel([          // after decay, in parallel:
                    Animated.timing(_this.state.tabScale,{
                        toValue:1,
                        duration:50,
                        delay:200
                    }),
                    Animated.timing(_this.state.tabShow,{
                        toValue:1,
                        duration:150,
                        delay:200,
                        easing: Easing.linear,
                    }),
                ]).start();               
            }
            this.prevOffset = currentOffset;
        }
}

gotoMyProfile(){
    this.props.navigator.switchToTab({
        tabIndex:4
    })
    // this.props.navigator.push({
    //     screen: 'myAccount',
    //         passProps:{
    //             token:this.props.auth.token,
    //             userdata:this.props.auth.userdata,
    //             openProfile : 2
    //         },
    // });
}

handleChat(data){
    if(!api.checkLogin(this.props.auth.token)){
        startMain('',{});
    }else if(this.props.auth.userdata !== undefined && this.props.auth.userdata !== null && this.props.auth.userdata.profile_complete == 0){
        this.gotoMyProfile();
    }else{
        let newData = {};
        //console.log(data);
       newData.send_from_id = data.user_id;
       newData.ad = data.id;
       newData.token = this.props.auth.token;
       newData.name =  data.user_name;
       this.props.chatActions.openChat(newData);
       this.props.navigator.push({
           screen: 'Singlechat',
           passProps:{
               data:newData
           }
       });
    }
}

_renderProductData(data,i){
    i++;
    if(data.loader){
        return <Col sm={12} md={12} lg={12} style={styles.proCol} key={`list_item_loader`}><ActivityIndicator size="large" color={colors.navBarButtonColor}/><View style={{height:10}}></View></Col>
    }else{
        return(
            data.is_in_showcase==1 ?
            <GridItemFull data={data} vkey={i} key={i}
            onPress={(data)=>{this.navigateTo('ProductDetail',data)}}
            onFav={(i,fav)=>{this.doFav(i,fav)}}
            searchType = {this.state.type}
            currentUser={api.checkLogin(this.props.auth.token) ? this.props.auth.userdata:{id:'',token:''}}
            onChat={(data)=>{this.navigateTo('Singlechat',data)}} />
            : <ListItem data={data} vkey={i} key={i}
            onPress={(data)=>{this.navigateTo('ProductDetail',data)}}
            onFav={(i,fav)=>{this.doFav(i,fav)}}
            searchType = {this.state.type}
            currentUser={api.checkLogin(this.props.auth.token) ? this.props.auth.userdata:{id:'',token:''}}
            onChat={(data)=>{this.navigateTo('Singlechat',data)}} />
        ); 
    }
}

render() {
    const listHeight = Platform.OS === 'ios' ? height-40 : height-60;
    let popup =  <Modal style={styles.modalApear} animationType={"slide"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {alert("Modal has been closed.")}}>
            <View style={[styles.modalInner,styles.justCenter]}>
                {/* <Text style={[styles.globalIcon,styles.modalClose]} onPress={() => {this.setModalVisible(false)}}>{';'}</Text> */}
                <View style={styles.modalMain}>
                    <Text style={[styles.globalIcon,styles.modalClose]} onPress={() => {this.setModalVisible(false)}}>{';'}</Text>
                    <View style={[styles.modalBody]}>
                        <View><Text style={styles.subTitle}>Sauvegarder dans vos recherches préférées</Text></View>
                        <View style={[styles.inlineDatarow,styles._PR10_,{borderBottomWidth:0.5,borderBottomColor:"#eee"}]}>
                            <TextInput style={[styles.genInput,styles.flex]} placeholder={"Titre de la recherche"}  
                            value={this.state.bookMarkTitle}
                            // onChangeText={(text) => {
                            //     let basicSearch = {...this.state.basicSearch};
                            //     console.log(basicSearch.search_string)
                            //     basicSearch.search_string = text;
                            //     this.setState({ basicSearch })
                            // } }
                            onChangeText={(text)=>{
                                this.setState({
                                    bookMarkTitle : text
                                });
                            }}
                            />
                            <Text style={[styles.globalIcon,styles.absoluteBottomRight]}>h</Text>
                        </View>
                        <View style={styles._MT15_}>
                            <LoaderButton load={this.state.bookLoad} onPress={()=>{this.saveBookMark()}} BtnStyle={[styles.brandColor]} text={'Sauver'} textStyle={[styles.genButton,styles.smallBtn]} />
                        </View>
                    </View>
                </View>
            </View>
        </Modal>

     var navigationView = (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Text style={{margin: 10, fontSize: 15, textAlign: 'left'}}>I'm in the Drawer!</Text>
      </View>
    );
    const headerTranslate =  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    let product;
    let productListing=(this.state.view == 1 ? (
        <GridItem dataList={this.state.displayItems}
        onPress={(data)=>{this.navigateTo('ProductDetail',data)}}
        onFav={(i,fav)=>{this.doFav(i,fav)}}
        searchType = {this.state.type}
        currentUser={api.checkLogin(this.props.auth.token) ? this.props.auth.userdata:{id:'',token:''}}
        onChat={(id)=>{this.navigateTo('Singlechat',id)}} />):(<View style={styles.proMain}>
            {this.state.displayItems.map((data,i)=>{
            return this._renderProductData(data,i);
            })}
        </View>));
    if(this.state.tabs ==1 ||this.state.tabs ==2 ||this.state.tabs ==3  ){
        product = productListing;
    }else{
        product=null;
    }
    noItems=<View style={styles.noItemsCont}><Text style={styles.noItemsText}>Aucun élément trouvé</Text></View>;
    return (        
        <Animated.View style={[styles.productdataMain]} >
            <StatusBar/>
            <View style={[styles.searchHolder]}>  
                <Image source={images.companylogo} style={styles.uneSym}/>
                <View style={styles.searchinputHolder}>
                    <TextInput onFocus={()=>{
                        Keyboard.dismiss()
                        this.goto('FastSearch')}} style={[styles.searchInput,{marginLeft:5}]} placeholder={"Rechercher..."} underlineColorAndroid={'#0000'}
                    value={typeof this.state.searchString != 'undefined' ? this.state.searchString:''}    
                    />
                </View>
                <TouchableOpacity activeOpacity={0.8} onPress={()=>this.goto('FastSearch')} style={[styles.icoholderRight]}>
                    {/* <Text style={[styles.genIco,styles.searchIco]}>v</Text> */}
                    <Icon name="search" color={colors.navBarButtonColor} size={20} style={{marginRight:10}}/>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={{borderColor:'#2E89ED',borderWidth:1,padding:3,borderRadius:5}} onPress={()=>{this.goto('AdvanceSearch')}}>
                    {/* <Text style={[styles.searchholderIco,styles.genIco,styles.blueColor,styles.largeIco]} >r</Text> */}
                    {/* <Icon name="mixer" color={colors.navBarButtonColor} size={20} style={{marginRight:5}}/> */}
                            <Text color={colors.navBarButtonColor} style={{marginRight:2,fontSize:14,color:'#2E89ED'}}>Filtres</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={[this.state.tabs == 1 ?[styles.gridParent]:styles.listParent,{height:listHeight}]}
                showsVerticalScrollIndicator={false}
                scrollEventThrottle={5} refreshControl={this._refreshControl()}
                onScroll={this.onScroll.bind(this)}
                onContentSizeChange={( contentWidth, contentHeight ) => {
                            this._contentHeight = contentHeight;
                    }} 
                    onLayout={(event) => {
                                var {x, y, width, height} = event.nativeEvent.layout;
                            if(height!=0){
                                this._componentHeight = height;}
                    }} 
                >
                <View style={[styles.proMain,styles._MT10_]}>
                    <Tabs defaultTabIndex={0} tabList={['Toutes','Particuliers','Professionnels']}  
                    onTabPress={(i)=>{this.tabchange(i+1)}}/>
                      {this.state.refreshing ?
                       null:(product !=null && this.state.displayItems.length > 0 ? product: noItems)}
                      
                </View>
                
            </ScrollView>
            <Animated.View style={{width: 125,alignSelf:'center',position:'absolute',bottom:125,opacity:this.state.tabShow,transform: [{scale: this.state.tabScale}],backgroundColor: 'rgba(255,255,255,1)',borderWidth:1,borderColor: colors.navBarButtonColor,borderRadius:20, shadowColor: '#fff',
    shadowOffset: { width: 1, height: 1},
    shadowOpacity: 1,
    shadowRadius: 7,elevation:5}}>
                <View style={{flexDirection:'row',padding:5}}>
                <TouchableOpacity style={{width:55,flexDirection:'row'}} onPress={()=>this.handlePriceButttons('price')}>
                    <Icon name={this.state.sortBy == 'price' ? 'euro-fill':'euro-outer'} color={colors.navBarButtonColor} size={18} style={{alignSelf:'center'}}/>  
                    <Text style={{color:colors.navBarButtonColor,marginLeft:4}}>Prix</Text>
                </TouchableOpacity> 
                <TouchableOpacity style={{width:55,flexDirection:'row'}} onPress={()=>this.handlePriceButttons('date')}>
                    <Icon name={this.state.sortBy == 'date' ?  'calendar-fill':'calendar'} color={colors.navBarButtonColor} size={18} style={{alignSelf:'center'}}/>
                    <Text style={{color:colors.navBarButtonColor,marginLeft:5,fontSize:16}}>Date</Text>
                </TouchableOpacity>     
                </View>
            </Animated.View>  
            
            {popup}
        </Animated.View>        
    );
  }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductgridContainer);

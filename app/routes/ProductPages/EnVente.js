import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Platform,
    Animated,
    WebView,
} from 'react-native';
import styles from '../../config/genStyle';
// import CheckBox from 'react-native-check-box';
import CheckBox from '../../lib/react-native-check-box';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import {iconsMap, iconsLoaded} from '../../config/icons';
import Select from '../../components/Select/Select';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

class EnVente extends Component {
    static navigatorStyle = {
        // navBarBackgroundColor: colors.navBarBackgroundColor, navBarTextColor:
        // colors.navBarTextColor, navBarSubtitleTextColor:
        // colors.navBarSubtitleTextColor, navBarButtonColor: colors.navBarButtonColor,
        // statusBarTextColorScheme: colors.statusBarTextColorScheme, statusBarColor:
        // colors.statusBarColor, tabBarBackgroundColor: colors.tabBarBackgroundColor,
        // tabBarButtonColor: colors.tabBarButtonColor, tabBarSelectedButtonColor:
        // colors.tabBarSelectedButtonColor, navBarSubtitleColor:
        // colors.navBarSubtitleColor,
        drawUnderTabBar: true
    };

    constructor(props) {
        super(props);
        // if you want to listen on navigator events, set this up
        this.state = {
            listPrice:this.props.listPrice,
            totPrice:this.props.totPrice,
            userPrice:this.props.price,
            priceData:this.props.getPriceData(),
            alreadyIsDirectSell: false,
            isDirectSell: false,
            productName: '',
            productPrice: 0,
            itemQuantity: 0,
            shippingType: '', // 'variable'
            simpleShippingPrice: 0,
            variableShippingData:[
                {
                    from:1,
                    to:1,
                    price:0
                },
            ],
            paypalAddress: '',
            directSellData: [
                {
                    key:'',
                    value:'',
                },
                {
                    key:'',
                    value:'',
                },
                {
                    key:'',
                    value:'',
                }
            ]
        };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    componentWillMount() {
        iconsLoaded.then(() => {});
    }

    getDispPrice(num){
        //return num.toFixed(2).toString().replace('.',',')+' €';
        return num.toFixed(2).toString()+' €';
    }

    setData(data){
        let newData = data;
        this.setState(newData); 
    }

    onNavigatorEvent(event) {
        //console.log(event)
        if (event.id == 'willAppear') {
            this.props.chatActions.setScreen(this.props.testID);
            //console.log(this.props);
            this.props.navigator.toggleNavBar({to: 'shown', animated: true});
            this.props.navigator.toggleTabs({to: 'hidden', animated: true});
            this.props.navigator.setStyle({navBarTitleTextCentered: true});
            this.props.navigator.setTitle({title: "En Vente"});
            this.props.navigator.setButtons({
                    leftButtons: [
                        {
                            icon: iconsMap['back'],
                            id: 'back2',
                            title: 'back to welcome'
                        }
                    ],
                    animated: false
                });
                let EnVente=this.props.getEnVenteData(this.props.index);
                if(typeof EnVente != 'undefined' && Object.keys(EnVente).length !== 0){
                    let obj=EnVente;
                    obj.priceData=this.props.getPriceData();
                    obj.listPrice=this.props.listPrice;
                    obj.totPrice=this.props.totPrice;
                    obj.userPrice=this.props.price;
                    this.setData(obj);
                }
            
        }
        if (event.id == 'didAppear') {
            
        }

        if (event.id == 'back2') {
            this.props.setEnVenteData(this.props.index,this.state);
            this.props.navigator.pop({
                animated: true, // does the pop have transition animation or does it happen immediately (optional)
            });
        }

        if (event.id == 'menu') {
            this.props.navigator.toggleDrawer({
                    side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
                    animated: true, // does the toggle have transition animation or does it happen immediately (optional)
                    to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
                });
        }

    }
    handleOnPress(value) {
        this.setState({value: value})
    }
    handleOnPress1(value1) {
        this.setState({value1: value1})
    }
    handleOnPress2(value2) {
        this.setState({value2: value2})
    }

    getOptions(from,to){
        let retAry=[];
        if(from > to){
            retAry.push({
                id:to,
                value:to
            });
        }else{
            for(i=from;i<=to;i++){
                let retOpt={};
                retOpt.id=i;
                retOpt.value=i;
                retAry.push(retOpt);                                    
            }
        }
        return retAry;
    }

    calculatePrice(){
        let price = this.state.listPrice;
        let total = this.state.totPrice - price;
        if(this.state.isDirectSell && !this.state.alreadyIsDirectSell){
            let factor = (this.state.priceData.direct_sell_prcnt.price)/100;
            let plus = 0;
            if(parseInt(this.state.itemQuantity) !== NaN && parseFloat(this.state.productPrice) !== NaN){
                plus = this.state.priceData.direct_sell_price.price +( parseInt(this.state.itemQuantity) * parseFloat(this.state.productPrice) * factor);
            }
            price = price + plus;
        }
        return {
            price:price,
            total: total+price,
        };
    }

    resetDirectSell(){
        let newState = {
            listPrice:this.props.listPrice,
            totPrice:this.props.totPrice,
            userPrice:this.props.price,
            priceData:this.props.getPriceData(),
            alreadyIsDirectSell: false,
            isDirectSell: false,
            productName: '',
            productPrice: 0,
            itemQuantity: 0,
            shippingType: '', // 'variable'
            simpleShippingPrice: 0,
            variableShippingData:[
                {
                    from:1,
                    to:1,
                    price:0
                },
            ],
            paypalAddress: '',
            directSellData: [
                {
                    key:'',
                    value:'',
                },
                {
                    key:'',
                    value:'',
                },
                {
                    key:'',
                    value:'',
                }
            ]
        };
        if(this.props.index == null){
            let envData=this.props.getEnVenteData(this.props.index);
            if(envData.alreadyIsDirectSell){
                newState.alreadyIsDirectSell=true;
            }
        }
        // this.state = newState;
        this.setState(newState);

    }

    render() {
        let listTot=this.calculatePrice();
        let editEnable = this.state.isDirectSell && !this.state.alreadyIsDirectSell;
        return (
            <View style={[styles.main, styles._HP15_]}>
                <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                    <View style={styles.scrollInner}>
                        <View style={styles._MT10_}>
                            <View style={[styles._PB20_]}>
                                <CheckBox
                                    style={[styles.genChk, styles._MT5_]}
                                    disabled={this.state.alreadyIsDirectSell}
                                    onClick={(checked) => {
                                        if(!checked){
                                            this.resetDirectSell();
                                        }else{
                                            this.setState({isDirectSell:checked})
                                        }
                                }}
                                    labelStyle={styles.midum}
                                    isChecked={this.state.isDirectSell}
                                    rightText={'Votre annonce en vente directe - 1,00 € + 5% du prix par produit'}/>
                                <View style={styles._MT10_}>
                                    <FloatLabelTextInput
                                        style={styles.genInput}
                                        editable={editEnable}
                                        value={this.state.productName}
                                        onChangeTextValue={(value) => {
                                            this.setState({productName:value})
                                        }}
                                        placeholder={"Nom du produit*"}
                                        underlineColorAndroid={'#eee'}/>
                                    <FloatLabelTextInput
                                        style={styles.genInput}
                                        editable={editEnable}
                                        keyboardType={'numeric'}
                                        value={this.state.productPrice.toString()}
                                        onChangeTextValue={(value) => {
                                            value=Number(value);
                                            this.setState({productPrice:value})
                                        }}
                                        onFocus={()=>{
                                            const { productPrice } = this.state;
                                            if(productPrice == 0){
                                                this.setState({productPrice:''})
                                            }
                                        }}
                                        placeholder={"Prix unitaire*"}
                                        underlineColorAndroid={'#eee'}/>
                                    <FloatLabelTextInput
                                        style={styles.genInput}
                                        editable={editEnable}
                                        keyboardType={'numeric'}
                                        value={this.state.itemQuantity.toString()}
                                        onChangeTextValue={(value) => {
                                            value=Number(value);
                                            if(this.state.shippingType == 'Frais_port_variable'){
                                                let variableShippingData=[{from:1,to:1,price:0}];
                                                this.setState({itemQuantity:value,variableShippingData:variableShippingData});
                                            }else{
                                                this.setState({itemQuantity:value});
                                            }
                                        }}
                                        onFocus={()=>{
                                            const { itemQuantity } = this.state;
                                            if(itemQuantity == 0){
                                                this.setState({itemQuantity:''})
                                            }
                                        }}
                                        placeholder={"Quantité*"}
                                        underlineColorAndroid={'#eee'}/>
                                    {this.state.itemQuantity >= 1 ?
                                    (<View>
                                        <Select vkey={1} containerStyle={{ padding:5,width:180, marginTop: 5}}
                                        disabled={!editEnable}
                                        optionsList={[
                                            {
                                                id:'Frais_port_simple',
                                                value:'Frais de port simple'
                                            },{
                                                id:'Frais_port_variable',
                                                value:'Frais de port variable'
                                            }
                                        ]}
                                        selectedid={this.state.shippingType}
                                        header={'select frais'}
                                        onPress={(item) => { 
                                            if(item == 'Frais_port_simple'){
                                                let variableShippingData=[{from:1,to:1,price:0}];
                                                this.setState({shippingType:item,variableShippingData:variableShippingData});
                                            }else if(item == 'Frais_port_variable'){
                                                let simpleShippingPrice= 0;
                                                this.setState({shippingType:item,simpleShippingPrice:simpleShippingPrice});
                                            }
                                        }}/>
                                        {this.state.shippingType == "Frais_port_simple" ? (
                                        <View>
                                            <FloatLabelTextInput
                                                style={styles.genInput}
                                                editable={editEnable}
                                                keyboardType={'numeric'}
                                                value={this.state.simpleShippingPrice.toString()}
                                                onChangeTextValue={(value) => {
                                                    value=Number(value);
                                                    this.setState({simpleShippingPrice:value})
                                                }}
                                                placeholder={"Prix du port"}
                                                onFocus={()=>{
                                                    const { simpleShippingPrice } = this.state;
                                                    if(simpleShippingPrice == 0){
                                                        this.setState({simpleShippingPrice:''})
                                                    }
                                                }}
                                                underlineColorAndroid={'#eee'}/>
                                        </View>):(null)}

                                        {this.state.shippingType == "Frais_port_variable" ? (
                                            this.state.variableShippingData.map((data,i)=>{
                                                return (
                                                    <View key={'variable'+i} style={{flex:1,flexDirection:'row',
                                                    alignItems:'center',
                                                    justifyContent:'center'
                                                    }}>
                                                        <View style={{flex:1,flexDirection:'row',alignItems:'center',//justifyContent:'center'
                                                        }}>
                                                            <Text style={{marginRight:10}}>De</Text>
                                                            <Select vkey={'De'+i}
                                                            disabled={!editEnable} 
                                                        containerStyle={{ padding:5,width:70, marginTop: 5}}
                                                        optionsList={this.getOptions(data.from,data.from)}
                                                        header={'select frais'}
                                                        selectedid={data.from}
                                                        showClose = {false}
                                                        onPress={(item) => { 
                                                            let varData=this.state.variableShippingData;
                                                            varData[i].from=item;
                                                            this.setState({variableShippingData:varData});
                                                        }}/>
                                                        </View>
                                                        <View style={{flex:1,flexDirection:'row',alignItems:'center',//justifyContent:'center'
                                                        }}>
                                                            <Text style={{marginRight:10}}>à</Text>
                                                            <Select vkey={'à'+i} 
                                                            disabled={!editEnable}
                                                        containerStyle={{ padding:5,width:70, marginTop: 5}}
                                                        optionsList={this.getOptions((data.from+1),this.state.itemQuantity)}
                                                        header={'select frais'}
                                                        showClose = {false}
                                                        selectedid={data.to == 1 ? this.state.itemQuantity:data.to}
                                                        onPress={(item) => { 
                                                            let varData=this.state.variableShippingData;
                                                            varData[i].to=item;
                                                            if(typeof varData[i+1] == 'undefined' && i < 2 && item < this.state.itemQuantity){
                                                                let newvar={
                                                                    from:item+1,
                                                                    to:this.state.itemQuantity,
                                                                    price:0
                                                                };
                                                                varData.push(newvar);
                                                            }else if(typeof varData[i+1] != 'undefined' && i < 2 && item < this.state.itemQuantity){
                                                                varData[i+1].from=item+1;
                                                            }else{

                                                            }
                                                            this.setState({variableShippingData:varData});
                                                        }}/>
                                                        </View>
                                                        <View style={{flex:1}}>
                                                            <FloatLabelTextInput
                                                            style={styles.genInput}
                                                            editable={editEnable}
                                                            keyboardType={'numeric'}
                                                            value={this.state.variableShippingData[i].price.toString()}
                                                            onChangeTextValue={(value) => {
                                                                let varData=this.state.variableShippingData;
                                                                varData[i].price=value;
                                                                this.setState({variableShippingData:varData});
                                                            }}
                                                            placeholder={"Prix du port"}
                                                            onFocus={()=>{
                                                                let varData=this.state.variableShippingData;
                                                                varData[i].price='';
                                                                if(varData[i].price == 0){
                                                                    this.setState({variableShippingData:varData})
                                                                }
                                                            }}
                                                            underlineColorAndroid={'#eee'}/>
                                                        </View>
                                                        
                                        </View>
                                                );
                                            })
                                        ):(null)}

                                    </View>):(null)}



                                    <FloatLabelTextInput
                                        style={styles.genInput}
                                        editable={editEnable}
                                        value={this.state.paypalAddress}
                                        onChangeTextValue={(value) => {
                                            this.setState({paypalAddress:value})
                                        }}
                                        placeholder={"Votre adresse PayPal*"}
                                        underlineColorAndroid={'#eee'}/>


                                    {this.state.directSellData.map((data,i)=>{    
                                    return(<View key={'directSell'+i}>
                                            <FloatLabelTextInput
                                            style={styles.genInput}
                                            editable={editEnable}
                                            value={data.key}
                                            onChangeTextValue={(value) => {
                                                let newData= this.state.directSellData;
                                                newData[i].key = value;
                                                this.setState({directSellData:newData})
                                            }}
                                            placeholder={"Nom Option "+(i+1)}
                                            underlineColorAndroid={'#eee'}/>
                                            <FloatLabelTextInput
                                            style={styles.genInput}
                                            editable={editEnable}
                                            placeholder={"Valeur Option "+(i+1)}
                                            value={data.value}
                                            onChangeTextValue={(value) => {
                                                let newData= this.state.directSellData;
                                                newData[i].value = value;
                                                this.setState({directSellData:newData})
                                            }}
                                            underlineColorAndroid={'#eee'}/>
                                        </View>);
                                        }
                                    )}
                                </View>
                                <View style={[styles._MT10_, styles._PT15_, styles.flex]}>
                                    <View style={[styles.spaceBitw, styles.inlineDatarow]}>
                                        <Text style={styles.bold}>Prix total :</Text>
                                        <Text>{this.getDispPrice(listTot.price)}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.bottomFixdad}>
                    <View style={[styles._PB10_, styles._PT10_]}>
                        <Text style={[styles._F14_, styles.center]}>PRIX TOTAL DE MES ANNONCES :<Text style={[styles._F16_, styles.bold]}>
                                {this.getDispPrice(listTot.total)}
                            </Text>
                        </Text>
                    </View>
                </View>
            </View>
        );
    }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(EnVente);


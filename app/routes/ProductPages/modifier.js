import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Platform,
    Animated,
    WebView,
    Switch,
    ActivityIndicator,
    Alert,
    Keyboard,
    KeyboardAvoidingView,
    findNodeHandle
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import images from '../../config/images';
import styles from '../../config/genStyle';
import {colors} from '../../config/styles';
import {settings} from '../../config/settings';
import RadioButton from '../../lib/react-radio/index';
// import CheckBox from 'react-native-check-box';
import CheckBox from '../../lib/react-native-check-box';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ModalDropdown from '../../lib/react-native-modal-dropdown';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import Tabs from '../../components/Tabs/Tabs';
import LocationSelect from '../../components/LocationSelect/LocationSelect';
import ModalCat from '../../components/ModalCat/ModalCat';
import Select from '../../components/Select/Select';
import ImageBox from '../../components/ImageBox/Imagebox';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import ActionSheet from 'react-native-actionsheet';
import {iconsMap, iconsLoaded} from '../../config/icons';
import Icon from 'react-native-vector-icons/FontAwesome';
import Loader from '../../components/Loader/Loader';
import _ from 'lodash';
import { NetworkInfo } from 'react-native-network-info';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const {width, height} = Dimensions.get('window');
const vWidth = width;
const vHeight = (width * width / height);
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
let categoryContent;
const api = new ApiHelper;
const timeout = 1000;
class Modifier extends Component {

static navigatorStyle = {
    drawUnderTabBar: true,
};

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this up
    this.ActionSheetOptions = {
            CANCEL_INDEX: 2,
            DESTRUCTIVE_INDEX: 2,
            options: ['Prendre une photo', 'À partir de la bibliothèque', 'Annuler'],
            title: 'Ajouter des photos?'
        };
    this.ImageBox=null;
    this.ModalCat=null;
    this.LocationSelect=null;
    this.ActionSheets=null;
    this.refsT={};
    this.ScrollView=null;
    this.state={
            priceData:{},
            ipAddress:null,
            loading:true,
            Listing:{
                id:null,
                already:{
                    pack6:false,
                    isVideo:false,
                    isUrgent:false,
                    alreadyAd:false,
                    alreadyList:false,
                    isDirectSell:false
                },
                images: [],
                mainImage: '',
                upload:false,
                pack6: false,
                isVideo: false,
                videoUrl: '',
                avantData: {},
                EnVente: {},
                adType: 'offers',
                title: '',
                title_contre: '',
                description_contre:'',
                category: '',
                categoryLoad: true,
                attributeData: [],
                categoryFilterData: {},
                description: '',
                refer:'',
                price: 0,
                isUrgent: false,
                location:{},
                address:'',
                userType: '',
                mobile: '',
                email: '',
                username: '',
                isAccountData: true,
                hideMobile: false,
                showCategoryContent: false  
            },
            showVideo: false,
            check1: false,
            check2: false,
            checkbox: false,
            tabs: 1,
            tabSelected1: 1,
            tabSelected: 1,
            like: 1,
            scrolled: true,
            scrollY: new Animated.Value(0),
            value: 1,
            value1: 1,
            visible: 0,
            text: "",
            SRC: 'https://www.youtube.com/embed/0DI0WBuicV0',
            backArrow: true,
            onBack: null,
            sendLoad: false,
            validAry:{},
            doneBtn:false,
            mainViewHgt: 0,
            footerHgt: 0,
            keyboardOpen: false,
            bottom:0,
            refToSubmit:null
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

    componentWillMount() {
        iconsLoaded.then(() => {});
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

_keyboardDidShow = (e) => {
    if(Platform.OS === 'ios'){
        //console.log('keyboard open');
        //console.log(e);
        if(this.state.doneBtn){
            this.setState({keyboardOpen:true,bottom:e.endCoordinates.height});
        }
    }
}

_keyboardDidHide = (e) => {
    if(Platform.OS === 'ios'){
         //console.log('keyboard hide');
         //console.log(e);
            if(this.state.doneBtn){
                this.setState({keyboardOpen:false});
            }
    }
}

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }


getIp(){
    if(this.state.ipAddress == null){
        NetworkInfo.getIPAddress(ip => {
            this.setState({ipAddress:ip});
          });
    }
}

    setListingAttribute(object){
        let Listing = this.state.Listing;
        Object.keys(object).map((key) => {
            Listing[key] = object[key];
        });
        this.setState({Listing:Listing});
    }

    sendEventToDrawer() {
        this.props.navigator.handleDeepLink({
            link: 'Drawer',
            payload: {screen: this.props.testID}
        });
    }


 onNavigatorEvent(event) {
     if(event.id=='willAppear'){
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
        this.props.navigator.toggleTabs({
            animated: false,
            to: 'hidden'
        });
        this.props.navigator.toggleNavBar({
            to: 'shown',
            animated: true
        });
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({title: "Modifier votre annonce"});
        this.props.navigator.setButtons({
         leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title:'back to welcome',
            }],animated:false});
            this.getIp();
        if(this.state.Listing.id == null){
            this.getPriceData();
        }    
    }
    if(event.id=='didAppear'){
    }
     
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }

   goback(){
        if (this.state.onBack != null) {
            this.props.navigator.switchToTab({
                tabIndex: this.state.onBack // (optional) if missing, this screen's tab will become selected
            });
        } else {
            this.props.navigator.pop({
                animated: true // does the pop have transition animation or does it happen immediately (optional)
            });
        }
    }

    getPriceData(){
        this.setState({loading:true},()=>{
            if(this.state.priceData!== undefined && Object.keys(this.state.priceData).length === 0){
                let request={
                    url:settings.endpoints.postPriceList,
                    method:'POST',
                    params:{}
                }
                api.FetchData(request).then((results)=>{
                    if(results.status){
                        this.setState({priceData:results.post_prices},()=>{
                            if(this.props.data !== undefined && this.props.data !== null){
                                this.getData(this.props.data);
                            }
                        });
                    }else{
                        this.handleError('Impossible d\'obtenir des données');
                    }
                }).catch((error)=>{
                    this.handleError('Impossible d\'obtenir des données');
                });
            }else{
                if(this.props.data !== undefined && this.props.data !== null){
                    this.getData(this.props.data);
                }else{

                }
            }
        });        
    }    

    handleOnPress(value) {
        if (value == 'offers' || value == 'demands' || value === 'trocs') {
            this.setListingAttribute({adType:value});
        } else {

        }
    }

    goto(page) {
        let Listing=this.state.Listing;
        let envente=Listing.EnVente;
        let avantdata=Listing.avantData;
        let listingPrice=parseFloat(Listing.price);
        let listPrice=this.calculatePrice();
        //let totPrice=this.calculatePrice();
        let minusPrice=0;
        if(!Listing.already.isDirectSell && Listing.EnVente.isDirectSell){
            let factor = (this.state.priceData.direct_sell_prcnt.price)/100;
            minusPrice = this.state.priceData.direct_sell_price.price +( parseInt(Listing.EnVente.itemQuantity) * parseFloat(Listing.EnVente.productPrice) * factor);
            if(parseFloat(minusPrice) === NaN){
                minusPrice = 0;
            }
        }
        listPrice = listPrice - minusPrice;
        this.props.navigator.push({
            screen: page,
            passProps:{
                setEnVenteData:this.setEnVenteData.bind(this),
                setAdData:this.setAdData.bind(this),
                getEnVenteData:this.getEnVenteData.bind(this),
                getAdData:this.getAdData.bind(this),
                getPriceData:this.getPrices.bind(this),
                price:listingPrice,
                listPrice:listPrice,
                totPrice:listPrice,
                index:null
            }
        });
    }

    videoUrl() {
        if (this.state.Listing.videoUrl != "") {
            this.setState({showVideo: true});
        } else {
            alert("Please enter video url");
        }
    }

    addImage() {
        this.ActionSheets.show();
    }

    selectAction(i) {
        this.ImageBox.onActionSelect(i);
    }

    selectCat(item) {
        let Listing=this.state.Listing;
        Listing.category = item;
        Listing.categoryLoad = true;
        Listing.categoryFilterData = {};
        this.setState({
            Listing : Listing
        }, () => {
            this.loadAttributes(item);
        });
    }

    loadAttributesPromise(item){
        return new Promise((resolve,reject)=>{
            let request = {
            url: settings.endpoints.getAttributes,
            method: 'POST',
                params: {
                    category_id: item,
                    token: this.props.auth.token,
                    post:true,
                }
            }
            api.FetchData(request).then((results) => {
                let Listing=this.state.Listing;
                Listing.categoryLoad = false;
                Listing.attributeData = [];
                if (results.status) {
                    Object.keys(results.attrList).map((key) => {
                            if (results.attrList[key] != "") {
                                let attrib = {};
                                attrib.id = key;
                                attrib.type = results.attrList[key].type;
                                attrib.name = results.attrList[key].name;
                                if (results.attrList[key].type == 'dropdown') {
                                    attrib.options = results.attrList[key].predef;
                                    if(typeof results.attrList[key].predef[0] != 'undefined' && typeof results.attrList[key].predef[0].subAttr != 'undefined'){
                                        attrib.hasSub = true;
                                    }else{
                                        attrib.hasSub = false;
                                    }  
                                }
                                Listing.categoryFilterData[key] = '';
                                Listing.attributeData.push(attrib);
                            }
                        });
                }
                this.setState({Listing: Listing});
                resolve(true);
            })
            .catch((error) => {
                this.setListingAttribute({categoryLoad:false});
                this.handleError('Impossible d\'obtenir des données');
                reject(error);
            })
        });

    }

    loadAttributes(item){
        let request = {
            url: settings.endpoints.getAttributes,
            method: 'POST',
            params: {
                category_id: item,
                token: this.props.auth.token,
                post:true,
            }
        }
        api.FetchData(request).then((results) => {
                let Listing=this.state.Listing;
                Listing.categoryLoad = false;
                Listing.attributeData = [];
                if (results.status) {
                    Object.keys(results.attrList).map((key) => {
                            if (results.attrList[key] != "") {
                                let attrib = {};
                                attrib.id = key;
                                attrib.type = results.attrList[key].type;
                                attrib.name = results.attrList[key].name;
                                if (results.attrList[key].type == 'dropdown') {
                                    attrib.options = results.attrList[key].predef;
                                    if(typeof results.attrList[key].predef[0] != 'undefined' && typeof results.attrList[key].predef[0].subAttr != 'undefined'){
                                        attrib.hasSub = true;
                                    }else{
                                        attrib.hasSub = false;
                                    }  
                                }
                                Listing.categoryFilterData[key] = '';
                                Listing.attributeData.push(attrib);
                            }
                        });
                }
                this.setState({Listing: Listing});
                return true;
            })
            .catch((error) => {
                //console.log(error);
                this.setListingAttribute({categoryLoad:false});
                this.handleError('Impossible d\'obtenir des données');
                return false;
            })
    }

    validLocation(location){
         //console.log(location)
         if(location.isCurrent !== undefined && location.isCurrent !== null){
            if(location.lat !== undefined && location.long !== undefined){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    validateListing(listing) {
        //let valid=true; userType:'', email:'',
        //console.log(1);
        let validObj={
            login:true,
            videoUrl:true,
            title:true,
            category:true,
            title_contre:true,
            description_contre:true,
            categoryContent:true,
            description:true,
            avantData:true,
            directSell:true,
            adType:true,
            price:true,
            location:true,
            address:true,
            mobile:true,
        };

        if (!api.checkLogin(this.props.auth.token)) {
            validObj.login = false;
            //return false;
        }
        // images:[],        
        // if (typeof listing.images == 'undefined' || listing.images == null || listing.images.length < 1) {
        //     return false;
        // }
        // // mainImage:'',               
        // if (typeof listing.mainImage == 'undefined' || listing.mainImage == null) {
        //     return false;
        // }
        // isVideo:true, videoUrl:'https://www.youtube.com/embed/k-atPa3QUis',
        if (listing.isVideo && listing.videoUrl == '') {
            validObj.videoUrl = false;
            //return false;
        }
        // location:'', lat:'', log:'',
        if(!listing.isAccountData && (listing.location === undefined || listing.location === null || Object.keys(listing.location).length === 0|| !this.validLocation(listing.location))){
            validObj.location = false;
            //return false;
        }
        /*if (typeof listing.location == '' || typeof listing.lat == 'undefined' || typeof listing.log == 'undefined') {
            return false;
        }*/
        // title:'',
        if (listing.title == "") {
            validObj.title = false;
            //return false;
        }
        // description:'',
        if (listing.description == "") {
            validObj.description = false;
            //return false;
        }

        if(listing.adType === 'trocs') {
            if (listing.title_contre == "") {
                validObj.title_contre = false;
                //return false;
            }
            // description:'',
            if (listing.description_contre == "") {
                validObj.description_contre = false;
                //return false;
            } 
        }

        // category:'',
        //console.log(2);
        if (listing.category == "") {
            validObj.category = false;
            //return false;
        }
        // if(!listing.isAccountData && listing.address == ''){
        //     validObj.address = false;
        //     //return false;
        // }

        if(this.props.auth.userdata.usertype === 'professional' && (listing.address == '' || listing.address === undefined)) {
            validObj.address = false;
        }
        // avantData:{},
        if (!this.validateAvantData(listing.avantData)) {
            validObj.avantData = false;
            //return false;
        }
        //console.log(3);
        // EnVente:{},
        if (this.props.auth.userdata.usertype == 'professional' && !this.validateEnvente(listing.EnVente)) {
            validObj.directSell = false;
            //return false;
        }
        // adType:'offers',
        //console.log(4);
        if (typeof listing.adType == 'undefined' || listing.adType == null || listing.adType == "") {
            validObj.adType = false;
            //return false;
        }
        //  price:0,
        if (listing.price === NaN || listing.price <= 0) {
            validObj.price = false;
            //return false;
        }
        //console.log(5);
        if (!listing.isAccountData && (listing.mobile == '' || !this.validMobile(listing.mobile))) {
            validObj.mobile = false;
            //return false;
        }
        //
        //console.log(6);

        if (!this.validateCategoryData(listing.attributeData,listing.categoryFilterData)) {
            validObj.categoryContent = false;
            //return false;
        }
        //console.log(7);
        console.log
        (validObj);
        return validObj;
    }

    validMobile(mobile){
        if(isNaN(mobile.replace(/\s/g, ""))){
            //msg = "Le numéro de téléphone doit être un chiffre.";
            return false;
        } else if(mobile.charAt(0) != '0'){
            //msg = 'Le numéro de téléphone commence par 0!';
            return false;
        } else if(mobile.replace(/\s/g, "").length != 10){
            //msg = 'Le numéro de téléphone doit comporter 10 chiffres!';
            return false;
        }else{
            return true;
        }
    }


    validNumber(number){
        number=parseInt(number)
        if(number === NaN || number <= 0){
            return false;
        }
        return true;
    }

    validateEnvente(data){
        //console.log(data)
        if(typeof data == 'undefined' || data == null){
            return false;
        }
        if(Object.keys(data).length === 0){
            return true;
        }
        //console.log(11)
        if(data.isDirectSell !== true && data.isDirectSell !== false){
            return false;
        }

        if(!data.isDirectSell){
            return true;
        }
        if(data.productName == ''){
            return false;
        } 
        //console.log(12)
        if(!this.validNumber(data.productPrice)){
            return false;
        }
        if(!this.validNumber(data.itemQuantity)){
            return false;
        } 
        //console.log(13)
        let isSimple = data.shippingType == 'Frais_port_simple' ? true : false;
        let isVariable = data.shippingType == 'Frais_port_variable' ? true : false;
        let vL = isVariable ? data.variableShippingData.length : 0;
        if(!isSimple && !isVariable){
            return false;
        }
        //console.log(14)
        if(isSimple && !this.validNumber(data.simpleShippingPrice)){
            return false;
        }
        if(isVariable){
            let ret=true;
            data.variableShippingData.map((data,i)=>{
                if(!this.validNumber(data.price)){
                    ret=false;
                }
                if(data.to < 2){
                    ret=false;
                }
            });
            if(ret === false){
                return false;
            }
        }
        //console.log(15)
        let reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(reg.test(data.paypalAddress) == false){
            return false;
        }
        return true;
    }

    validateAvantData(data){
        if(typeof data == 'undefined' || data == null){
            return false;
        }
        if(Object.keys(data).length === 0){
            return true;
        }
        if(data.isAdShowcase === true){
            if(typeof data.adShowcase == 'undefined' || data.adShowcase == null || Object.keys(data.adShowcase).length === 0){
                return false;
            }
        }
        if(data.isListShowcase === true){
            if(typeof data.listShowCase == 'undefined' || data.listShowCase == null || Object.keys(data.listShowCase).length === 0){
                return false;
            }
        }
        return true;
    }

    validateCategoryData(attributeData,categoryFilterData) {
        // console.log(attributeData);
        // console.log(categoryFilterDaa);
        for (i = 0; i < categoryFilterData.length; i++) {
            let key = attributeData[i].id;
            if (typeof categoryFilterData[key] == 'undefined' || categoryFilterData[key] == null || categoryFilterData[key] == '') {
                return false;
            }
        }
        return true;
    }

    validateValidObj(validAry){
        //console.log(validAry);
        let ret = true;
        let obj = {};
       // validAry.map((d,i)=>{
            Object.keys(validAry).map((k)=>{
                if(ret === true && validAry[k]=== false){
                    obj.key=k;
                }
                ret = ret && validAry[k];
            });
       // });
        if(!ret){
            //console.log(obj);
            if(this.refsT !== undefined && this.refsT !== null && Object.keys(this.refsT).length > 0){
                //let newKey = this._getNextKey(index,key);
                //console.log('in next ')
                let newRef=null;
                if(obj.key == 'category'){
                    newRef = this.ModalCat;
                }else if(obj.key == 'location'){
                    newRef = this.LocationSelect;
                }else{
                    newRef = this.refsT[obj.key];
                    this._setFocus(newRef,true);
                }
                this.scrollToRef(newRef);
            }
        }
        console.log(ret);
        return ret;
    }

    scrollToRef(ref){
        if(this.ScrollView !== undefined && this.ScrollView !== null){
            if (ref && ref.inputRef) {
                ref.inputRef().measureLayout(findNodeHandle(this.ScrollView),(x,y)=>{
                    //console.log('x =>'+x);
                    //console.log('y =>'+y);
                    this.ScrollView.scrollTo({y: y-50,animated:true});
                });
            }
        }
    }

    validateAllListing(){
        let validAry={};
        /* uncomment this to start validation */
        validAry = this.validateListing(this.state.Listing);
        this.setState({validAry:validAry});
        console.log(validAry);
        return this.validateValidObj(validAry);        
    }

    convertToserver(listing){
        // console.log(listing.images);
        let obj={};
        let highlight={};
        let directsale={};
        obj.images=listing.images;
        obj.additional_picture=listing.pack6 ? 1 : 0;
        obj.additional_picture_price= obj.additional_picture ==1 ? {
            aditional_pic_price:this.state.priceData.aditional_pic_price.id
        }:"";
        obj.is_video_check=listing.isVideo ? 1 : 0;
        obj.video_price= obj.is_video_check == 1 ? {
            video_price:this.state.priceData.video_price.id
        }:"";
        obj.video=obj.is_video_check == 1 ? listing.videoUrl : "";

        if(typeof listing.avantData != 'undefined' && listing.avantData != null && Object.keys(listing.avantData).length !== 0){    
            highlight.is_in_showcase=listing.avantData.isAdShowcase ? 1:0;
            highlight.is_in_top_list=listing.avantData.isListShowcase ? 1:0;
            highlight.showcase_data=highlight.is_in_showcase == 1 ? listing.avantData.adShowcase : "";
            highlight.top_list_data=highlight.is_in_top_list == 1 ? listing.avantData.listShowCase : "";
            obj.highlight_data=highlight;
        }

        if(typeof listing.EnVente != 'undefined' && listing.EnVente != null && Object.keys(listing.EnVente).length !== 0){    
            directsale.is_direct_sale=listing.EnVente.isDirectSell ? 1:0;
            directsale.direct_sell_price_data=directsale.is_direct_sale==1 ? {
                direct_sell_price:this.state.priceData.direct_sell_price.id,
                direct_sell_prcnt:this.state.priceData.direct_sell_prcnt.id,
            }:"";
            directsale.product_name=listing.EnVente.productName;
            directsale.product_price=listing.EnVente.productPrice;
            directsale.item_quantity=listing.EnVente.itemQuantity;
            directsale.shipping_type=listing.EnVente.shippingType;
            directsale.shipping_value_simple=directsale.shipping_type == 'Frais_port_simple' ? listing.EnVente.simpleShippingPrice : "";
            directsale.shipping_value_variable=directsale.shipping_type != 'Frais_port_simple' ? listing.EnVente.variableShippingData : ""
            directsale.seller_paypal_url=listing.EnVente.paypalAddress;
            directsale.direct_sale_opt_data=listing.EnVente.directSellData;
            obj.direct_sale_data=directsale;
        }

        obj.ad_type=listing.adType;
        obj.title=listing.title;
        obj.title_contre=listing.title_contre;
        obj.description_contre=listing.description_contre;
        obj.category=listing.category;
        obj.categoryFilterData=listing.categoryFilterData;
        obj.description=listing.description;
        obj.reference= this.props.auth.userdata.usertype === 'professional' ? listing.refer : '';

        obj.price=listing.price;
        obj.is_urgent=listing.isUrgent ? 1 : 0;
        obj.urgent_price= obj.is_urgent == 1 ? {
            urgent_price:this.state.priceData.urgent_price.id
        }:"";
        obj.is_account_address=listing.isAccountData ? 1 : 0;
        obj.location=obj.is_account_address != 1 ? listing.location : '';
        obj.address=listing.address;
        obj.ad_user_type=listing.userType;
        obj.telephone=listing.mobile;
        obj.is_phone_hide=listing.hideMobile ? 1 : 0;
        return obj;
    }

    validate(){
        let imgbox = this.ImageBox.getImages();
        // console.log(imgbox);
        let LocationSelect =this.LocationSelect.getAddress();
            this.setListingAttribute({
                images:imgbox.images,
                //mainImage:imgbox.main,
                email:this.props.auth.userdata.email,
                userType: this.props.auth.userdata.usertype,
                location:LocationSelect,
                price : parseInt(this.state.Listing.price)
            });
        if (this.validateAllListing()) {
            return true;
            //this.addNewTab();
        } else {
            Alert.alert(
                'Alerte!',
                 'veuillez compléter tous le champs obligatoires',
                [
                    {text: 'Ok', onPress: () => {
                        //this.handleAfter(result);
                    }},
                ],
                { cancelable: false }
            )
            //alert('veuillez compléter tous le champs obligatoires');
            return false;
        }
    }

    handleAfter(result){
        this.setState({sendLoad:true},()=>{
        setTimeout(()=>{
            this.setState({sendLoad:false},()=>{
                this.props.navigator.push({
                    screen: 'Cart',
                    passProps:{
                        token:this.props.token,
                        userdata:this.props.userdata,
                        checkPop : true
                    }
                });
            // this.props.navigator.switchToTab({
            //     tabIndex:3
            //});
        });
        },2000);
    });
    }

    saveAllData(){
        let serverobj=this.convertToserver(this.state.Listing);
        /* end point remaining */
        serverobj.token=this.props.auth.token;
        serverobj.total_price=this.calculatePrice();
        serverobj.user_ip=this.state.ipAddress;
        serverobj.ad_id=this.state.Listing.id;
        let request = {
            url:settings.endpoints.updatePost,
            method:"POST",
            params:serverobj
        };
        console.log(serverobj);
        // return false;
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({sendLoad:false},()=>{
                    Alert.alert(
                        'Félicitations!',
                         result.message,
                        [
                            {text: 'Ok', onPress: () => {
                                this.handleAfter(result);
                            }},
                        ],
                        { cancelable: false }
                    )
                
                    //alert('Message édité avec succès');
                });
            }else{
                this.setState({sendLoad:false},()=>{
                    Alert.alert(
                        'Information !',
                        result.message,
                        [
                            {text: 'Ok', onPress: () => {
                                //this.props.navigator.pop({animated:true});
                            }},
                        ],
                        { cancelable: false }
                    )  
                });  
                //alert('Publié non édité');                
            }
        }).catch((err)=>{
            //alert('Quelque chose a mal tourné');
            this.setState({sendLoad:false},()=>{
                Alert.alert(
                    'Information !',
                    'Quelque chose a mal tourné',
                    [
                        {text: 'Approuvé', onPress: () => {
                            //this.props.navigator.pop({animated:true});
                        }},
                    ],
                    { cancelable: false }
                ) 
            });          
        });
        
    }

    submitAll() {
    if(this.validate()){
        if(this.ImageBox !== undefined && this.ImageBox !== null){
            this.setState({sendLoad:true},()=>{
                this.ImageBox.uploadImages(1).then((result)=>{
                    if(result.status){
                        let imgs=this.ImageBox.getImages();
                        console.log(imgs);
                        this.setListingAttribute({upload:true,images:imgs.images,mainImage:imgs.main});
                        setTimeout(()=>{
                            this.saveAllData();
                        },timeout);
                    }else{
                        setTimeout(()=>{
                            this.saveAllData();
                        },timeout);
                    }
                }).catch((err)=>{
                    //console.log(err)
                    // this.setState({sendLoad:true},()=>{
                    //     setTimeout(()=>{
                    //         this.saveAllData();
                    //     },timeout);
                    // });
                    this.setState({sendLoad:false},()=>{
                        alert(err.message);
                    })
                });
            });
        }
     }
    }

    calculatePrice() {
        let listing = this.state.Listing;
        console.log(listing.remain_days);
        let price = listing.remain_days <= 7 ? 0 : 1;
        if (!listing.already.pack6 && listing.pack6) {
            price += 1;
        }
        if (!listing.already.isVideo && listing.isVideo) {
            price += 1;
        }
        if (!listing.already.isUrgent && listing.isUrgent) {
            price += 1;
        }
        if(api.checkLogin(this.props.auth.token) && this.props.auth.userdata.usertype === 'professional' && this.ModalCat !== undefined && this.ModalCat !== null){
            let catP=this.ModalCat.getPrice();
            if(catP !== 0){
                price +=catP;
            }
        }
        if(typeof listing.avantData != 'undefined' && listing.avantData != null && Object.keys(listing.avantData).length !== 0){
            if(!listing.already.alreadyAd && listing.avantData.isAdShowcase){
                if(typeof listing.avantData.adShowcase.price && !isNaN(parseFloat(listing.avantData.adShowcase.price))){
                    price=price+listing.avantData.adShowcase.price;
                }
            }
            if(!listing.already.alreadyList && listing.avantData.isListShowcase){
                if(typeof listing.avantData.listShowCase.price && !isNaN(parseFloat(listing.avantData.listShowCase.price))){
                    price=price+listing.avantData.listShowCase.price;
                }
            }
        }
        if(typeof listing.EnVente != 'undefined' && listing.EnVente != null && Object.keys(listing.EnVente).length !== 0){
            if (typeof listing.EnVente.isDirectSell != 'undefined' && listing.EnVente.isDirectSell === true && !listing.already.isDirectSell){
                let factor = (this.state.priceData.direct_sell_prcnt.price)/100;
                price = price + this.state.priceData.direct_sell_price.price +( parseInt(listing.EnVente.itemQuantity) * parseFloat(listing.EnVente.productPrice) * factor);
                //price = price + this.state.priceData.direct_sell_price.price + (listing.price * factor);
            }
        }
        return price;
    }

    getDispPrice(num){
        //return num.toFixed(2).toString().replace('.',',')+' €';
        return num.toFixed(2).toString()+' €';
    }

    setEnVenteData(index,data){
        if(index== null){
            this.setListingAttribute({EnVente:data});
        } 
    }

    getEnVenteData(index){
        if(index == null){
            let ret=this.state.Listing.EnVente;
            if(Object.keys(ret).length > 0){
                ret.alreadyIsDirectSell = this.state.Listing.already.isDirectSell;
            }
            return ret;
        }
    }

    setAdData(index,data){
        if(index == null){
            this.setListingAttribute({avantData:data});
        }
    }

    getAdData(index){
        if(index == null){
            let ret=this.state.Listing.avantData;
            if(Object.keys(ret).length > 0){
                ret.alreadyAd = this.state.Listing.already.alreadyAd;
                ret.alreadyList = this.state.Listing.already.alreadyList;
            }
            return ret;
        }
    }

    getPrices(){
        return this.state.priceData;
    }

    getURL(url){
        if(url !== undefined && url !== null && url !== ""){
            let id = url.split('/').pop();
            if(id.indexOf('watch?v=') > -1){
                id=id.replace("watch?v=",'');
            }else {
                id = id;
            }
            return 'https://www.youtube.com/embed/'+id;
        }else {
            return '';
        }
    }


    _setFocus(ref,bool){
        if(ref !== undefined && ref !== null){
            if(bool){
                ref.focus();
                console.log(ref);
            }else{
                ref.blur();
            }
        }
    }

    _getNextKey(key){
        const {Listing} = this.state;
        switch(key){
            case 'videoUrl':
                return 'title';
            break;
            
            case 'title':
                return 'description';
            break;

            case 'description':
                if(this.props.auth.userdata.usertype == 'professional'){
                    return 'refer';
                }else{
                    return 'price';
                }
            break;

            case 'refer':
                return 'price';
            break;

            case 'price':
                if(Listing.isAccountData){
                    Keyboard.dismiss();
                    return null;
                }else{
                    return 'address';
                }
            break;

            case 'address':
                return 'mobile';
            break;

            case 'mobile':
                if(Listing.adType === 'trocs') {
                    return 'title_contre';
                } else{
                    Keyboard.dismiss();
                    return null;
                }
            break;
            case 'title_contre':
                return 'description_contre';
            break;
            case 'description_contre':
                Keyboard.dismiss();
                return null;
            break;
        }
    }

    getValidationStyle(key){
        const {validAry}=this.state;
        //console.log(key);
        if(validAry !== undefined && validAry !== null && validAry[key] !== undefined && validAry[key] !== null){
            if(validAry[key] === false){
                //console.log('red for '+ key );
                return {borderColor:"#F00",borderWidth:1};
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    _setNextFocus(key){
        if(this.refsT !== undefined && this.refsT !== null && Object.keys(this.refsT).length > 0){
            let newKey = this._getNextKey(key);
            if(newKey !== null){
            //console.log('in next ')
            this._setFocus(this.refsT[newKey],true);
            this.scrollToRef(this.refsT[newKey]);
            }
            //this._refStyleRed(this.refsT[index][key]);
        }
    }

    removeError(key){
        let validAry = this.state.validAry;
        if(validAry[key] !== undefined && validAry[key] !== null){
            if(validAry[key] === false){
                validAry[key] = true;
                this.setState({validAry:validAry});
            }
        }
    }

    getDiscount(){
        if(this.state.priceData !== undefined && this.state.priceData !== null && this.state.priceData.shop_data !== undefined){
            if(this.props.auth.userdata.have_shop === 1 && this.props.auth.userdata.shop_package_id !== undefined && this.props.auth.userdata.shop_package_id !== null && this.props.auth.userdata.shop_package_id !== '' ){
                let u = this.state.priceData.shop_data[this.props.auth.userdata.shop_package_id]
                if(u !== undefined && u !== null ){
                    if(u.type_of_discount === 'discount'){
                        return u.discount_price+'%';
                    }else{
                        return u.discount_price+' €';
                    }
                }
            }
        }
    }



    doneBtn(bool,key){
        if(Platform.OS==='ios'){
            //console.log(' done btn '+bool);
            if(bool){
                this.setState({doneBtn:bool, refToSubmit: this.refsT[key]});
            }else{
                this.setState({doneBtn:bool, refToSubmit: null});
            }
        }
    }

    triggerOnSubmit(){
        let refToSubmit = this.state.refToSubmit;
        //console.log(refToSubmit);
        if(refToSubmit !== undefined && refToSubmit !== null ){
            let inp = refToSubmit.inputRef();
            //console.log(inp);
            if(inp !== undefined && inp !== null && inp.props && inp.props.onSubmitEditing ){
                inp.props.onSubmitEditing();
            }
        }
    }

    _renderSubCategoryDropDown(attribute,selectedParent){
        let subattr=[];
        let subID='';
        attribute.options.map((data,i)=>{
            if(data.id === selectedParent){
                subattr = data.subAttr;
                subID = data.sub_id;
            }
        });

        return subattr.length > 0 && subID!='' ? <Select vkey={selectedParent} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                optionsList={subattr}
                header={'sélectionner'}
                selectedid={typeof this.state.Listing.categoryFilterData[subID] != 'undefined' ? this.state.Listing.categoryFilterData[subID]:''}
                onPress={(item) => {
                let listing = this.state.Listing;
                listing.categoryFilterData[subID] = item;
                this.setState({Listing: listing});
                }}/> : null;
    }

getAdDataById(id,type){
    let ret={};
    let mapAry=[];
    if(type=='showcase'){
        mapAry=this.state.priceData.showcase_price;
    }else if(type=='toplist'){
        mapAry=this.state.priceData.toplist_price;
    }
    mapAry.map((data,i)=>{
        if(data.id == id){
            ret=data;
        }
    });
    return ret;
}


getData(data){

    let request={
        url:settings.endpoints.editPost,
        method:"POST",
        params:{token:this.props.auth.token,ad_id:data.id}
    }
    api.FetchData(request).then((result)=>{
        if(result.status){
            this.loadAttributesPromise(result.ad_details.category).then(()=>{
                let serverList=result.ad_details;
                let Listing=this.state.Listing;
                Listing.id=serverList.id;
                Listing.remain_days=serverList.remain_days;
                Listing.images=serverList.images;
                /* set imageBox data here */
                Listing.mainImage='';
                Listing.pack6=serverList.additional_picture == 1 ? true : false;
                Listing.isVideo=serverList.is_video_check == 1 ? true : false;
                Listing.videoUrl=Listing.isVideo ? serverList.video : '';

                let avantdata={};
                avantdata.isAdShowcase=serverList.highlight_data.is_in_showcase == 1 ? true : false;
                avantdata.isListShowcase=serverList.highlight_data.is_in_top_list == 1 ? true : false;
                avantdata.adShowcase=avantdata.isAdShowcase ? 
                this.getAdDataById(serverList.highlight_data.showcase_data_id,'showcase') : '';

                avantdata.listShowCase=avantdata.isListShowcase ? 
                this.getAdDataById(serverList.highlight_data.top_list_data_id,'toplist') : '';
                Listing.avantData=avantdata;
                let EnVente={};
                /* set direct sell data here */
                if(this.props.auth.userdata.usertype == 'professional' && serverList.direct_sale_data.is_direct_sale == 1){
                    EnVente.isDirectSell=true;
                    EnVente.productName=serverList.direct_sale_data.product_name;
                    EnVente.productPrice=serverList.direct_sale_data.product_price;
                    EnVente.itemQuantity=serverList.direct_sale_data.item_quantity;
                    EnVente.shippingType=serverList.direct_sale_data.shipping_type;
                    EnVente.simpleShippingPrice = EnVente.shippingType == 'Frais_port_simple' ? serverList.direct_sale_data.shipping_value_simple :0 ;
                    EnVente.variableShippingData=[];
                    if(EnVente.shippingType == 'Frais_port_variable' && serverList.direct_sale_data.shipping_value_variable.length > 0){
                        EnVente.variableShippingData=serverList.direct_sale_data.shipping_value_variable;
                    } else{
                        EnVente.variableShippingData=[{from:1,to:1,price:0}];
                    }
                    EnVente.paypalAddress=serverList.direct_sale_data.seller_paypal_url;
                    EnVente.directSellData=[];
                    if(serverList.direct_sale_data.direct_sale_opt_data.length > 0){
                        for(i = 0;i < 3; i++){
                            if(serverList.direct_sale_data.direct_sale_opt_data[i] !== undefined){
                                EnVente.directSellData[i]=serverList.direct_sale_data.direct_sale_opt_data[i];
                            }else{
                                EnVente.directSellData[i]={key:'',value:''};
                            }       
                        }     
                    }else{
                        EnVente.directSellData=[{key:'',value:''},{key:'',value:''},{key:'',value:''}];
                    }
                }
                Listing.EnVente=EnVente;
                Listing.adType=serverList.ad_type;
                Listing.title=serverList.title;
                Listing.title_contre=serverList.title_contre;
                Listing.category=serverList.category;
                
                Listing.categoryFilterData=serverList.category_filter_data;
                
                Listing.description=serverList.description;
                Listing.description_contre=serverList.description_contre;
                Listing.refer= serverList.reference === undefined && serverList.reference !== '' ? serverList.reference :'';
                Listing.price=serverList.price;
                Listing.isUrgent=serverList.is_urgent == 1 ? true : false;

                Listing.location={};
                Listing.isAccountData=serverList.is_account_address == 1 ? true:false;
                if(Listing.isAccountData){
                    Listing.mobile=this.props.auth.userdata.phone;
                    Listing.address=this.props.auth.userdata.address1 + ', '+this.props.auth.userdata.address2;
                }else{
                    Listing.address=serverList.address;
                    Listing.mobile=serverList.telephone;
                }
                Listing.userType=this.props.auth.userdata.usertype;
                Listing.email=this.props.auth.userdata.email;
                Listing.username=this.props.auth.userdata.username;
                
                Listing.hideMobile=serverList.is_phone_hide == 1 ? true :false;
                Listing.showCategoryContent=false;
                Listing.already = {
                    pack6:Listing.pack6,
                    isVideo:Listing.isVideo,
                    isUrgent:Listing.isUrgent,
                    alreadyAd:avantdata.isAdShowcase ? true :false,
                    alreadyList:avantdata.isListShowcase ? true :false,
                    isDirectSell: EnVente.isDirectSell ? true : false,
                };
                this.setState({Listing:Listing,loading:false,showVideo: Listing.videoUrl != '' ? true : false },() => {
                    this.ImageBox.setImages(Listing.images);
                    this.ModalCat.setSelectedCat(serverList.categoryname);
                    if(Listing.location != ""){
                        Listing.location.isCurrent=false;
                        this.LocationSelect.setLocationData(Listing.location);
                    }
                });
            }).catch((err)=>{
                console.log(err)
            });
            
        }else{
            this.handleError('Aucune donnée disponible');
        }
    }).catch((err)=>{
        //console.log(err);
        this.handleError('Impossible d\'obtenir des données');
    })
}

handleError(msg){
    Alert.alert(
            'Précurseur',
            msg,
            [
                {text: 'Approuvé', onPress: () => {this.goback()}},
                {text: 'Annuler', onPress: () => {}, style: 'cancel'},
            ],
            { cancelable: false }
        )
}


_renderAnnounce() {
        const onTintColor = "#ffbf82";
        const thumbTintColor = "#ff7e00";
        const tintColor = "#b2b2b2";
        let listPrice = this.calculatePrice();
        const { Listing, priceData } = this.state;
        const isUserPro = this.props.auth.userdata.usertype == 'professional' ? true : false;
        return (
            <KeyboardAwareScrollView extraScrollHeight={50} key={'announce'} showsVerticalScrollIndicator={false}
                contentContainerStyle={[{flexGrow: 1,alignSelf: 'stretch',backgroundColor: '#f8f8f8'}]} innerRef={o=> this.ScrollView=o}>
                {/* <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'flex-end',paddingBottom: 5}}>
                    <Text style={[styles.orangeColor]}>{'Prix de I\'annonce N° '+(index+1)+'  :   '}{this.getDispPrice(listPrice)}{'   |    '}</Text>
                    {index > 0 ?
                    (<Text style={[styles.globalIcon,styles.orangeColor,styles._F20_]} onPress={()=>{this.removeListing(index)}}>8</Text>):(null)}
                </View> */}
                <View>
                    <ImageBox
                        token={this.props.auth.token}
                        ref={o => this.ImageBox = o}
                        onclickAdd={()=>{this.addImage()}}
                        incImage={()=>this.setListingAttribute({pack6:true})}
                        decImage={()=>{if(Listing.already.pack6 === false){this.setListingAttribute({pack6:false})}}}
                        isUserPro={isUserPro}
                        allowed={Listing.pack6 ? 10 : isUserPro ? 6 : 4 }/>
                </View>
                <View style={styles._MT10_}>
                    <View style={[styles.row,styles._MT5_]}>
                        <CheckBox style={[styles.genChk]} onClick={(checked) =>{this.setListingAttribute({pack6:checked});}}
                                        labelStyle={styles.midum}
                                        disabled={Listing.already.pack6}
                                        isChecked={Listing.pack6} 
                                        rightText={'Pack '+(isUserPro ? 4 : 6 )+' photos supplementaires - '+this.getDispPrice(priceData.aditional_pic_price.price)} />
                    </View>
                    <View>
                        <View style={[styles.inlineWithico, styles.justLeft, styles._MT5_]}>
                        <CheckBox
                            style={[styles.genChk]}
                            onClick={(checked) => {this.setListingAttribute({isVideo:checked});}}
                            labelStyle={styles.midum}
                            disabled={Listing.already.isVideo}
                            isChecked={Listing.isVideo}
                            rightText={'Video - '+this.getDispPrice(priceData.video_price.price)}/>
                    </View>
                    {Listing.isVideo ? 
                    <View>
                        <View style={styles.inlineDatarow}>
                            <FloatLabelTextInput
                                autoCapitalize="none" 
                                autoCorrect={false}
                                ref={o=>this.refsT['videoUrl']= o}
                                returnKeyType={'next'} 
                                onSubmitEditing={() => {this._setNextFocus('videoUrl')}}
                                fieldContainerStyle={[styles.genInput2,this.getValidationStyle('videoUrl')]}
                                placeholder={'Adresse URL YouTube'}
                                value={Listing.videoUrl}
                                onChangeTextValue={(text) => {
                                this.setListingAttribute({videoUrl:text});
                                this.removeError('videoUrl'); 
                            }}/>
                            <TouchableOpacity onPress={() => {this.videoUrl()}} activeOpacity={0.8}>
                                <Text style={[styles.globalIcon, styles.bigIco]}>h</Text>
                            </TouchableOpacity>
                        </View>
                        {this.state.showVideo ? Platform.OS === 'android' ?  (<WebView style={{width: vWidth,height: vHeight}}
                                source={{html: '<html><head></head><body style="margin: 0px;"><iframe style="border: 0;border-width: 0px;" width="' + vWidth + '" height="' + vHeight + '" src="' + this.getURL(Listing.videoUrl) + '" frameborder="0" allowfullscreen></iframe></body></html>'}}/>)  : <WebView style={{width: vWidth,height: vHeight,justifyContent:'center',alignSelf:'center'}}
                                source={{html: '<html><head></head><body style="margin: 0px auto;"><iframe style="border: 0;border-width: 0px;" width="800" height="500" src="' + this.getURL(Listing.videoUrl) + '" frameborder="0" allowfullscreen></iframe></body></html>'}}/>
                            : (<View style={styles.videoRow}>
                                <Text style={{justifyContent:'center',alignSelf:'center',fontSize:16,fontWeight:'bold'}}>Pour visualiser la video, cliquer sur le stylo !</Text>
                                <Image style={styles.videoIco} source={images.videoIco}/>
                            </View>)}
                            </View>
                            : (null)} 
                        </View>
                        </View>
                        <View style={styles._PTB10_}>
                        <View style={[styles.inlineDatarow, styles.spaceBitw, styles.borderBottom, styles._PB5_]}>
                            <TouchableOpacity activeOpacity={0.8}>
                                <Text style={[styles._F16_, styles.linkAvant]} onPress={() => {this.goto('mettreAvant')}}>Mettre en avant</Text>
                            </TouchableOpacity>
                            <Text style={[styles.globalIcon, styles.orangeColor]}>6</Text>
                        </View>
                        {api.checkLogin(this.props.auth.token) && isUserPro
                            ? (<View style={[styles.inlineDatarow, styles.spaceBitw, styles._MT5_]}>
                                    <TouchableOpacity activeOpacity={0.8}>
                                        <Text style={[styles._F16_, styles.linkAvant]} onPress={() => {this.goto('EnVente')}}>En Vente Direct</Text>
                                    </TouchableOpacity>
                                    <Text style={[styles.globalIcon, styles.orangeColor]}>6</Text>
                                </View>)
                            : (null)}
                    </View>
                    <View style = {[styles.inlineDatarow, styles._MT10_]}>
                        <RadioButton
                        currentValue={Listing.adType}
                        value={'offers'}
                        onPress={(value)=>this.handleOnPress(value)}
                        Text={'OFFRE'}>
                        </RadioButton>
                        <RadioButton
                        currentValue = {Listing.adType}
                        value = {'demands'}
                        onPress = {(value)=>this.handleOnPress(value)}
                        Text = {'DEMANDE'}>
                        </RadioButton>
                        <RadioButton
                        currentValue = {Listing.adType}
                        value = {'trocs'}
                        onPress = {(value)=>this.handleOnPress(value)}
                        Text = {'TROCS'}>
                        </RadioButton>
                    </View>
                <View style = {[styles._MT10_]}>
                    <Text style={[styles.subTitle2]}>Details</Text>
                    <View style={[styles.relative, styles._MT10_]}>
                    { Listing.adType === 'trocs' ? 
                        <View style={{flex:1,flexDirection:'row'}}>
                             <Icon name="mail-forward" color={'#34c24c'} size={20} style={{marginRight:10}}/>
                             <Text style={{color:'#34c24c'}}>Echange</Text>
                        </View>:null}
                        <View style={styles._MT10_}>
                        <FloatLabelTextInput
                        autoCapitalize="none" 
                                autoCorrect={false}
                            ref={o=>this.refsT['title']= o}
                            returnKeyType={'next'} 
                            onSubmitEditing={() => {this._setNextFocus('title')}}
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle('title')]}
                            editIco={'h'}
                            value={Listing.title}
                            onChangeTextValue={(value) => {
                            this.setListingAttribute({title:value});
                            this.removeError('title');
                        }}
                            placeholder={"Choisissiez un titre"}
                            underlineColorAndroid={'#eee'}/>
                            </View>
                    </View>
                    <View style = {styles._MT10_}>
                        <FloatLabelTextInput
                            autoCapitalize="none" 
                            autoCorrect={false}
                            ref={o=>this.refsT['description']= o}
                            // returnKeyType={'next'} 
                            // onSubmitEditing={() => {this._setNextFocus('description')}}
                            blurOnSubmit={true}
                            Description={true} editIco={'h'}
                            underlineColorAndroid='#eeeeee'
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle('description')]}
                            value={Listing.description}
                            onChangeTextValue={(value)=>{
                                this.setListingAttribute({description:value});
                                this.removeError('description');
                            }}
                            numberOfLines={6}
                            placeholder={"Faites un descriptif approprie de i ' annuonce"}/>
                    </View>
                    {
                        Listing.adType === 'trocs' ? 
                        <View style = {[styles._MT10_]}>
                         <View style={{flex:1,flexDirection:'row'}}>
                             <Icon name="reply" color={'#2e5f9b'} size={20} style={{marginRight:10}}/>
                             <Text style={{color:'#2e5f9b'}}>Contre</Text>
                        </View>
                        <View style={[styles.relative, styles._MT10_]}>
                        <FloatLabelTextInput
                        autoCapitalize="none" 
                                autoCorrect={false}
                            ref={o=>this.refsT['title_contre']= o}
                            returnKeyType={'next'} 
                            onSubmitEditing={() => {this._setNextFocus('title_contre')}}
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle('title_contre')]}
                            editIco={'h'}
                            value={Listing.title_contre}
                            onChangeTextValue={(value) => {
                            this.setListingAttribute({title_contre:value});
                            this.removeError('title_contre');
                        }}
                            placeholder={"Choisissiez un titre"}
                            underlineColorAndroid={'#eee'}/>
                    </View>
                    <View style = {styles._MT10_}>
                        <FloatLabelTextInput
                            autoCapitalize="none" 
                            autoCorrect={false}
                            ref={o=>this.refsT['description_contre']= o}
                            // returnKeyType={'next'} 
                            // onSubmitEditing={() => {this._setNextFocus('description_contre')}}
                            blurOnSubmit={true}
                            Description={true} editIco={'h'}
                            underlineColorAndroid='#eeeeee'
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle('description_contre')]}
                            value={Listing.description_contre}
                            onChangeTextValue={(value)=>{
                                this.setListingAttribute({description_contre:value});
                                this.removeError('description_contre');
                            }}
                            numberOfLines={6}
                            placeholder={"Faites un descriptif approprie de i ' annuonce"}/>
                    </View></View>: null
                    }

                    <View style = {[styles._MT10_,this.getValidationStyle('category')]}>
                        <ModalCat
                        SectionList={this.props.auth.categoryList}
                        ref={o => this.ModalCat = o}
                        containerStyle={{flex: 1}}
                        onPress={(item) => {
                            this.removeError('category');
                            this.selectCat(item)}}/></View>
                    {Listing.category !='' ? (
                        <View style = {[styles._MT10_,{borderColor:"#ddd",borderWidth:0.5,padding:7}]}>
                            {Listing.categoryLoad ? (
                                <ActivityIndicator animating={true} color={colors.navBarButtonColor} size="small"/>) : (Listing.attributeData.length > 0
                        ? (Listing.attributeData.map((attribute, i) => {
                            if (attribute.type != 'dropdown') {
                                return <View style={styles._MT10_} key={i}>
                                        <FloatLabelTextInput
                                            placeholder={attribute.name}
                                            keyboardType={attribute.type == 'number' ? 'numeric' : 'default'}
                                            fieldContainerStyle={styles.genInput2}
                                            value={typeof Listing.categoryFilterData[attribute.id] !='undefined' ? Listing.categoryFilterData[attribute.id].toString():''}
                                            onChangeTextValue={(value) => {
                                            let Listing = this.state.Listing;
                                            Listing.categoryFilterData[attribute.id] = value;
                                            this.setState({Listing: Listing});
                                        }}/>
                                    </View>;
                            } else {
                                return <View key={i}>
                                    <Select vkey={i} containerStyle={{ backgroundColor: "#fff", marginTop: 5}}
                                        optionsList={attribute.options}
                                        header={attribute.name+' choisir'}
                                        selectedid={typeof Listing.categoryFilterData[attribute.id] != 'undefined' ? Listing.categoryFilterData[attribute.id]:''}
                                        onPress={(item) => {
                                        let Listing = this.state.Listing;
                                        Listing.categoryFilterData[attribute.id] = item;
                                        this.setState({Listing: Listing});
                                    }}/>
                                    {attribute.hasSub && typeof Listing.categoryFilterData[attribute.id] != 'undefined' ? (
                                     this._renderSubCategoryDropDown(attribute,Listing.categoryFilterData[attribute.id])
                                    ):(null)}
                                </View>;
                            }
                        }))
                        : (
                            null
                        ))
                }
                </View>
                    ):(null)}  
                    {/* <View>
                                <Text>No Attributes Found</Text>
                            </View> */}
                    
                            {isUserPro ? <View style = {styles._MT10_}>
                        <FloatLabelTextInput
                        autoCapitalize="none" 
                        autoCorrect={false}
                        ref={o=>this.refsT['refer']= o}
                        returnKeyType={'next'} 
                        onSubmitEditing={() => {this._setNextFocus('refer')}}
                        underlineColorAndroid='#eeeeee'
                        fieldContainerStyle={[styles.genInput2,this.getValidationStyle('refer')]}
                        value={Listing.refer}
                        onChangeTextValue={(value)=>{
                            this.setListingAttribute({refer:value});
                            this.removeError('refer');
                        }}
                            placeholder={"Référence"}/>
                            </View> :null}

                            <View style = {styles._MT10_}>
                                <View style={[styles.inlineDatarow, styles._MT10_]}>
                    <FloatLabelTextInput
                    autoCapitalize="none" 
                    autoCorrect={false}
                    ref={o=>this.refsT['price']= o}
                    returnKeyType={'done'} 
                    onSubmitEditing={() => {this._setNextFocus('price')}}
                    fieldContainerStyle={[styles.genInput2,this.getValidationStyle('price')]}
                    placeholder={"Prix *"}
                    keyboardType={'numeric'}
                    value={Listing.price.toString()}
                    underlineColorAndroid={'#eee'}
                    onFocus={()=>{
                        this.doneBtn(true,'price');
                        if(Listing.price == 0){
                            this.setListingAttribute({price:''});
                        }
                    }}
                    onBlur={()=>{
                        this.doneBtn(false);
                        if(Listing.price == ''){
                            this.setListingAttribute({price:0});
                        }
                    }}
                    onChangeTextValue={(value) => {
                    this.setListingAttribute({price:value});
                    this.removeError('price');
                }}/>
                    <CheckBox
                        style={[styles.genChk]}
                        onClick={(checked) => { 
                            this.setListingAttribute({isUrgent:checked});
                            }}
                        labelStyle={[styles.midum, styles.orange]}
                        disabled={Listing.already.isUrgent}
                        isChecked={Listing.isUrgent}
                        rightText={'Option Urgent - '+this.getDispPrice(priceData.urgent_price.price)}/>
                </View>
                    <View style = {styles._MT10_}>
                        <Text style={[styles.subTitle2]}>Localisation</Text>
                        <View style={[styles.inlineDatarow, styles.spaceBitw, styles._MT15_]}>
                            <Text>Adresse du compte</Text>
                            <Switch
                                onTintColor={onTintColor}
                                thumbTintColor={thumbTintColor}
                                tintColor={tintColor}
                                onValueChange={(value) => {
                                if (value) {
                                    this.setListingAttribute({
                                        isAccountData:value,
                                        //username:this.props.auth.userdata.username,
                                        //mobile:this.props.auth.userdata.phone,
                                        //address:this.props.auth.userdata.address1+", "+this.props.auth.userdata.address2,
                                    });
                                    if(this.LocationSelect !== null && this.LocationSelect !== undefined) {
                                        this.LocationSelect.showCurrent(false);
                                    }
                                } else {
                                    this.setListingAttribute({isAccountData:value,mobile:'',address:''});
                                }
                            }}
                                value={Listing.isAccountData}/>
                        </View>
                            <LocationSelect  isEnable={true}
                            currentLocation={true}
                            postAd={true}
                            fieldContainerStyle={[styles.genInput2,this.getValidationStyle('location')]}
                            authActions={this.props.authActions}
                            removeError={()=>this.removeError('location')}
                            userdata={this.props.auth.userdata} 
                            isAccount={Listing.isAccountData}
                            ref = {o => this.LocationSelect = o} />
                                <View>
                                    <FloatLabelTextInput
                                autoCapitalize="none" 
                                autoCorrect={false}
                                ref={o=>this.refsT['address']= o}
                                returnKeyType={'next'} 
                                onSubmitEditing={() => {this._setNextFocus('address')}}
                                fieldContainerStyle={[styles.genInput2,this.getValidationStyle('address')]}
                                editable={!Listing.isAccountData}
                                placeholder={"Adresse postale*"}
                                underlineColorAndroid={'#eee'}
                                value={Listing.isAccountData ? this.props.auth.userdata.address1+", "+this.props.auth.userdata.address2 : Listing.address}
                                onChangeTextValue={(value) => {
                                this.setListingAttribute({address:value});
                                this.removeError(index,'address');
                            }}/>
                                </View>
                    </View>
                </View>
                </View>
                <View style={[styles._MT20_, styles._PB15_]}>
                    <Text style={[styles.subTitle2]}>Coordonnées</Text>
                    <View style={[styles.inlineDatarow, styles._MT10_]}>
                        {api.checkLogin(this.props.auth.token)
                            ? (
                                <Text style={[styles.subTitle]}>{isUserPro
                                        ? 'Professionnels'
                                        : 'Particuliers'}</Text>
                            )
                            : (null)}
                    </View>
                    <View style={styles._MT10_}>
                            {/* <FloatLabelTextInput
                                fieldContainerStyle={styles.genInput2}
                                editable={false}
                                placeholder={"Pseudo"}
                                underlineColorAndroid={'#eee'}
                                value={Listing.username}
                                onChangeTextValue={(value) => {
                                this.setListingAttribute({username:value});
                            }}/> */}
                        
                        {api.checkLogin(this.props.auth.token)
                            ? (<View style={styles._MT10_}><FloatLabelTextInput
                                fieldContainerStyle={styles.genInput2}
                                value={this.props.auth.userdata.email}
                                placeholder={"E-mail"}
                                editable={false}
                                underlineColorAndroid={'#eee'}/></View>)
                            : (null)}
                        <View style={[styles.relative,styles._MT10_]}>
                            <FloatLabelTextInput
                                autoCapitalize="none" 
                                autoCorrect={false}
                                ref={o=>this.refsT['mobile']= o}
                                returnKeyType={'next'} 
                                onSubmitEditing={() => {this._setNextFocus('mobile')}}
                                onFocus={()=>{this.doneBtn(true,'mobile')}}
                                onBlur={()=>{this.doneBtn(false)}}
                                fieldContainerStyle={[styles.genInput2,this.getValidationStyle('mobile')]}
                                placeholder={"Votre téléphone "}
                                maxLength={10}
                                editable={!Listing.isAccountData}
                                underlineColorAndroid={'#eee'}
                                value={Listing.mobile}
                                editIco={!Listing.isAccountData
                                ? 'h'
                                : ''}
                                keyboardType={'phone-pad'}
                                onChangeTextValue={(value) => {
                                this.setListingAttribute({mobile:value});
                                this.removeError('mobile');
                            }}/>
                        </View>

                        <View style={[styles.inlineDatarow, styles.spaceBitw, styles._MT15_]}>
                            <Text>Masquer mon téléphone</Text>
                            <Switch
                                onTintColor={onTintColor}
                                thumbTintColor={thumbTintColor}
                                tintColor={tintColor}
                                onValueChange={(value) => {
                                this.setListingAttribute({hideMobile:value})
                            }}
                                value={Listing.hideMobile}/>
                        </View>
                    </View>
                </View>

                <ActionSheet
                    ref={o => this.ActionSheets = o}
                    title={this.ActionSheetOptions.title}
                    options={this.ActionSheetOptions.options}
                    cancelButtonIndex={this.ActionSheetOptions.CANCEL_INDEX}
                    destructiveButtonIndex={this.ActionSheetOptions.DESTRUCTIVE_INDEX}
                    onPress={(i)=>{this.selectAction(i)}}/>
            </KeyboardAwareScrollView>
        );
    }

  render() {
    const onTintColor = "#ffa382";
    const thumbTintColor = "#f15a23";
    const tintColor = "#b2b2b2"; 
    const {loading, sendLoad,keyboardOpen, doneBtn,bottom} = this.state;
    let totPrice=0.00;
    if(!loading){
        totPrice = this.calculatePrice();
    }
    return (
        <View style={{flex: 1, backgroundColor: "#f8f8f8", padding: 7}}>
              {loading ? <View style={[styles.Center]}><Loader/></View> : this._renderAnnounce() }
                  {!loading ? (<View style={styles.bottomFixdad}>
                    <View style={styles._PB5_}>
                        <Text style={[styles._F14_,styles.center,styles._MT5_]}>Prix total de mes annonces :<Text style={[styles._F16_,styles.bold]}>{this.getDispPrice(totPrice)}</Text></Text>
                    </View>
                    <View style={[,styles._PB5_]}>
                        <TouchableOpacity activeOpacity={0.8} style={[styles.brandColor]} 
                        onPress={()=>{sendLoad? null:this.submitAll()}}>
                            {sendLoad ? 
                            (<View style={{padding:10}}><ActivityIndicator size="small" color="#fff" /></View>):
                            (<Text style={[styles.genButton]}>Valider</Text>)}
                        </TouchableOpacity>
                    </View>
                 </View>):(null)}


                 {doneBtn && keyboardOpen ? (<View style={{position:'absolute',backgroundColor:"#0008",flexDirection:'row',width:width,bottom:bottom,height:45,left:0}}>
                     <View style={{width:width-70,}}></View>
                     <TouchableOpacity style={{backgroundColor:colors.navBarButtonColor,width:70,padding:8,marginVertical:5,alignItems:'center',right:5,justifyContent:'center',borderRadius:5}} activeOpacity={0.8} onPress={()=>this.triggerOnSubmit()}><Text style={{color:"#fff",fontSize:16}}>{'Done'}</Text></TouchableOpacity></View>):(null)}
        </View>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Modifier);



/* <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
                     <View style={styles.scrollInner}>   
                        <View style={styles.postBlock}> 
                            <View style={styles.imgUploadView}>
                                <View style={styles.imgUpload}>
                                    <View>
                                        <View style={[styles.flex,styles._MT10_,styles.fullupImgView]}>
                                            <Image source={images.homestay} style={styles.fullupImg}/>
                                            <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                            </TouchableOpacity>
                                        </View>  
                                        <ScrollView  ref={(scrollView) => { _scrollView = scrollView; }} automaticallyAdjustContentInsets={false} showsHorizontalScrollIndicator={false} contentContainerStyle={styles.Carousel1}
                                            horizontal>
                                                    <View style={[styles.inlineDatarow,styles._MT5_]}>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.minic}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View style={styles.placeHolderview}><Image style={[styles.placeHolderImg]} source={images.IMG16}/>
                                                                <TouchableOpacity activeOpacity={0.8}  style={styles.cancelImg}>
                                                                    <Text style={styles.globalIcon}>{'}'}</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>    
                                        </ScrollView>
                                    </View>
                                </View>
                                <TouchableOpacity elevation={5}  activeOpacity={0.8} style={styles.postadcamera}>
                                    <Text style={[styles.globalIcon,styles.cameraAd]}>A</Text>
                                </TouchableOpacity>  
                            </View>
                        </View>
                        <View style={styles._MT10_}>
                            <View style={[styles.row,styles._MT5_]}>
                                    <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check2:!checked})}}
                                        labelStyle={styles.midum} isChecked={this.state.check2}
                                        rightText={'Pack 6 photos supplementaires - 1,00€'}
                                    />
                            </View>
                            <View>
                                <View style={[styles.inlineWithico,styles.justLeft,styles._MT5_]}>
                                        <CheckBox style={[styles.genChk]}
                                            onClick={this.videoShow.bind(this)}
                                            labelStyle={styles.midum} isChecked={this.state.check1}
                                            rightText={'Video - 1,00€'}
                                         />  
                                 </View>  
                                 {this.state.check1 ?
                                <View>
                                    <View style={styles.inlineDatarow}><FloatLabelTextInput placeholder={'Adresse URL YouTube'} onChangeTextValue={(text) => this.setState({text:text})}/><TouchableOpacity onPress={()=>{this.videoUrl()}} activeOpacity={0.8}><Text style={[styles.globalIcon,styles.bigIco]}>h</Text></TouchableOpacity></View>
                                    <View style={styles.videoRow}><Image style={styles.videoIco} source={images.videoIco} /></View>
                                 </View>:null}   
                            </View>   
                        </View>  
                        <View style={styles._PTB10_}>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles.borderBottom,styles._PB5_]}>
                                <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]} onPress={()=>{this.goto('mettreAvant')}}>Mettre en avant</Text></TouchableOpacity>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </View>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                <TouchableOpacity activeOpacity={0.8}><Text style={[styles._F16_,styles.linkAvant]} onPress={()=>{this.goto('EnVente')}}>En Vente Direct </Text></TouchableOpacity>
                                <Text style={[styles.globalIcon,styles.orangeColor]}>6</Text>
                            </View>
                        </View>
                        <View style={[styles.inlineDatarow,styles._MT10_]}>
                            <RadioButton currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>OFFRES</Text></RadioButton>
                            <RadioButton currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this)}><Text style={[styles.readiText,styles._PR15_,styles._ML5_]}>DEMANDES</Text></RadioButton>
                        </View>
                        <View style={[styles._MT20_]}>
                            <View style={[styles.subTitleBgView]}>
                                 <Text style={[styles.subTitleBg]}>Details</Text>
                            </View>
                            <View style={[styles.relative]}>
                                    <FloatLabelTextInput style={styles.genInput} placeholder={"Choisissiez un titre"} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                             </View>
                            <View style={styles._MT10_}>
                                <View style={styles.framePstad}>
                                    <View style={styles.relative}>
                                        <View style={[styles.relative,styles.Borderbtmframe,styles._MT5_]}>
                                            <TouchableOpacity activeOpacity={0.8} onPress={this.showhide.bind(this)}><Text style={styles._F14_}>{'Choisissiez la Category'}</Text></TouchableOpacity>
                                            <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
                                        </View>
                                        {categoryContent}
                                    </View>
                                    <View style={(this.state.visible == 0) ? styles.relative : [styles.relative,styles.txtareaOpen]}>
                                        <TextInput underlineColorAndroid='#eeeeee'  multiline={true} numberOfLines={2} placeholder={"Faites un descriptif approprie de i ' annuonce"} />
                                        <Text style={[styles.globalIcon,styles.textareaIco]}>h</Text>
                                    </View>
                                </View>
                                <View style={[styles.inlineDatarow,styles._MT10_]}>
                                    <FloatLabelTextInput style={[styles.genInput]}   placeholder={"Prix *"} underlineColorAndroid={'#eee'}/>
                                        <CheckBox style={[styles.genChk]}
                                        onClick={(checked) =>{this.setState({check1:!checked})}}
                                        labelStyle={[styles.midum,styles.orange]} isChecked={this.state.check1}
                                        rightText={'Option Urgent - 1,00€'}
                                    />
                                </View>
                                <View style={styles.relative}>
                                    <FloatLabelTextInput style={[styles.genInput,styles.absicoinput]} placeholder={"Ville ou code postal...."} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>t</Text>
                                </View>
                             </View>
                        </View>
                         <View style={[styles._MT20_,styles._PB15_]}>
                            <View style={[styles.subTitleBgView]}>
                                 <Text style={[styles.subTitleBg]}>Qui je suis</Text>
                            </View>
                            <View style={styles._MT10_}>
                                <View style={styles.tabParent}>
                                    <View style={[styles.tabs,styles._PB0_]}>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 1)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(1)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 1)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Particuliers</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity  style={(this.state.tabSelected1== 2)?[styles.tabSelected,styles.widthHalf]:[styles.tabnotSelected,styles.widthHalf]} onPress={()=>{this.tabchange1(2)}} activeOpacity={0.8}>
                                            <Text style={(this.state.tabSelected1== 2)?[styles.tabTextSelected,styles.orangeColor]:[styles.tabText]}>Professionnels</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.botomOrangeLine}></View>
                                </View>
                                <View style={[styles.relative]}>
                                    <FloatLabelTextInput style={styles.genInput} placeholder={"Pseudo"} underlineColorAndroid={'#eee'}/> 
                                    <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <View style={styles.relative}>
                                        <FloatLabelTextInput style={styles.genInput} placeholder={"Votre N° de TelePhone"} underlineColorAndroid={'#eee'}/> 
                                        <Text style={[styles.globalIcon,styles.dropabsIco,styles._T18_]}>h</Text>
                                </View>
                                <FloatLabelTextInput style={styles.genInput} placeholder={"Email"} underlineColorAndroid={'#eee'}/> 
                                <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT15_]}>
                                        <Text>Cacher le numero de telephone</Text>
                                        <Switch onTintColor={onTintColor}  thumbTintColor={thumbTintColor} tintColor={tintColor} onValueChange={(value) => this.setState({trueSwitchIsOn1: value})} value={this.state.trueSwitchIsOn1} /> 
                                </View>
                             </View>
                         </View>    
                    </View>
                </ScrollView>
 */






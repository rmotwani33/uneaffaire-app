import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Dimensions,Platform,Animated,DrawerLayoutAndroid,Modal,ActivityIndicator,RefreshControl,RefreshLayoutConsts,Keyboard
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';

import {Navigation,Screen,SharedElementTransition} from 'react-native-navigation';

import {Column as Col, Row} from 'react-native-flexbox-grid';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {settings} from '../../config/settings';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
// import { createIconSetFromIcoMoon } from '../../lib/react-native-vector-icons';
// import icoMoonConfig from '../../lib/react-native-vector-icons/glyphmaps/icomoon.json';
// const Icon = createIconSetFromIcoMoon(icoMoonConfig);
import moment from 'moment';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const { height, width } = Dimensions.get('window');
const HEADER_MAX_HEIGHT = 100;
const HEADER_MIN_HEIGHT = Platform.OS === 'ios' ? 50 : 50;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;
const FORTAB = width < 1025 && width > 721;
const TABLANDSCAPE = width < 1025 && width > 768;
const TABPORTRAIT = width > 721 && width < 769;
const api= new ApiHelper;
const sensitivity=40;


class UserAnnonces extends Component {
genModal(){
    //if(this.state.displayItems.length > 0){   
        Navigation.showModal({
            screen: "ShopDetail", // unique ID registered with Navigation.registerScreen
            title: "Détail du magasin", // title of the screen as appears in the nav bar (optional)
            passProps: {
                token:this.props.auth.userdata.token,
                shop_id:typeof this.props.data != 'undefined' ? this.props.data.UserDetail.shop_id:typeof this.props.params.shop_id != 'undefined' ? this.props.params.shop_id : '',
                yes:false
            }, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
            animationType: 'slide-up' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
        });
    //}
}

constructor(props) {
    super(props);
    // if you want to listen on navigator events, set this u
    this.prevOffset=0;
    this.state={
        tabs:1,
        view:1,
        tabSelected:1,
        like:1,
        scrolled:true,
        modalVisible: false,
        scrollY: new Animated.Value(0),
        iconHeart:'m',
        displayItems:[],
        load:true,
        shop_image:'',
        refreshing:false,
        token:typeof this.props.auth.token != 'undefined' ? this.props.auth.token:'',
        serachTxt:typeof this.props.params != 'undefined' ? this.props.params.search_string :''
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillMount(){
    iconsLoaded.then(()=>{});
}
static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    // navBarHidden:false,
    // tabBarHidden:false,
    //navBarHideOnScroll:true,
    drawUnderNavbar: true,
    drawUnderTabBar: true,
};

onScroll(e){
    var currentOffset = e.nativeEvent.contentOffset.y;
    if(Math.abs(currentOffset - this.prevOffset) > sensitivity){
        if(Platform.OS === 'android') {
            if(currentOffset > this.prevOffset){
                this.props.navigator.toggleTabs({
                    to:'hidden',
                    animated:true,
                });
                this.props.navigator.toggleNavBar({
                    to:'hidden',
                    animated:true,
                });
            }else{
                this.props.navigator.toggleTabs({
                    to:'shown',
                    animated:true,
                });
                this.props.navigator.toggleNavBar({
                    to:'shown',
                    animated:true,
                });
            }
        }
        this.prevOffset = currentOffset;
    }
}
_refreshControl(){
    return (
      <RefreshControl
        colors={[colors.navBarButtonColor]}
        refreshing={this.state.refreshing}
        onRefresh={()=>this.getAnnounceData()} />
    )
}
getAnnounceData(){
    
    let _this=this;
    _this.setState({refreshing:true},()=>{
        let obj={
            shop_id:this.props.data.UserDetail.shop_id != 'undefined' ? this.props.data.UserDetail.shop_id:this.props.auth.userdata.shop_id,token:this.props.auth.userdata.token != undefined ? this.props.auth.userdata.token : this.props.auth.token
        };
        if(typeof this.props.params != 'undefined' && Object.keys(this.props.params).length !== 0){
            Object.keys(this.props.params).map((key)=>{
                obj[key]=this.props.params[key];
            })
        }
        let request={
          url:settings.endpoints.shopDetails,
          method:'POST',
          params:obj
        }
        api.FetchData(request).then((result)=>{
          if(result.status){
              _this.setState({
                  displayItems:result.itemList,
                  refreshing:false,
                  shop_image:result.shop.shop_image
              });
              if(result.itemList.length <= 4){
                _this.props.navigator.toggleTabs( {
                    animated: true,
                    to: 'hidden'
                });
              }
          }else{
              _this.setState({
                  displayItems:[],
                  refreshing:false
              });
          }
        }).catch((error)=>{
          //console.log(error);
          _this.setState({
                  load:false
              });
        }); 
    });
    
}

 onNavigatorEvent(event) {
     if(event.id=='willAppear'){
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
        this.props.navigator.toggleTabs({
        animated: true,
        to: 'shown'
    });
   
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title:this.props.data.UserDetail.name,
    });
    this.props.navigator.setButtons({
         leftButtons: [{
    icon: iconsMap['back'],
    id: 'back2',
    title:'back to welcome',
    }],
      rightButtons: [
            {   
                icon:iconsMap['list'],
                id: 'list',
                title:'grid',
            },
            {   
                icon:iconsMap['information'],
                id: 'info',
                title:'grid',
            },
        ],animated:false})
     }
     if(event.id=='didAppear'){
        this.getAnnounceData();
     }
    
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
    if(event.id == 'list'){
        // console.log(this.state.view)
       if(this.state.view==1){
        this.setState({view:2},()=>{
            this.props.navigator.setButtons({
                rightButtons: [
                    {   
                        icon:iconsMap['grid'],
                        id: 'list',
                        title:'grid',
                    },
                    {   
                        icon:iconsMap['information'],
                        id: 'info',
                        title:'grid',
                    },
                ],
                animated: false // does the change have transition animation or does it happen immediately (optional)
            });
        });
       
       }else{
             this.setState({view:1},()=>{
                this.props.navigator.setButtons({
                    rightButtons: [
                        {   
                            icon:iconsMap['list'],
                            id: 'list',
                            title:'list',
                        },
                        {   
                            icon:iconsMap['information'],
                            id: 'info',
                            title:'grid',
                        },
                    ],
                        animated: false // does the change have transition animation or does it happen immediately (optional)
                });
             });
             //if(this.state.isStore != 1){
        }
    }
    
    if(event.id == 'info'){
        this.genModal()
    }   
  }
  setModalVisible(visible) {
        this.setState({modalVisible: visible});
  }
  goto(page){   
    this.props.navigator.push({
        screen: page,
        passProps:{
            token:this.props.auth.token ,
            userdata:this.props.auth.userdata,
            UserDetail:this.props.data.UserDetail,
            params:typeof this.props.params != 'undefined'? this.props.params:[]
        }
    }); 
  }

  navigateTo(page,data){
      if(page=='ProductDetail'){
            this.props.navigator.push({
                screen: page,
                sharedElements: ['SharedTextId'+data.id],
                passProps:{
                data:data,
                token:this.props.auth.userdata.token != undefined ? this.props.auth.userdata.token:this.props.auth.token ,
                userdata:this.props.auth.userdata,  
            }            
       });
      }else{
            this.props.navigator.push({
                screen: page,
                passProps:{
                data:data,
                token:this.props.auth.token != undefined ? this.props.auth.token:this.props.auth.userdata.token,
                userdata:this.props.auth.userdata,
            }
    }); 
      }     
  }
  
// onScroll(event){
//         var currentOffset = event.nativeEvent.contentOffset.y;
//         var direction = currentOffset > this.offset ? this.setState({scrolled:true}) : this.setState({scrolled:false});
//         this.offset = currentOffset;       
// }

gettrans(bool){
if(bool){
      return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    }else{
       return  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [-50, 0],
        extrapolate: 'clamp',
    });  
    }
}
doFav(key,ss){
    if(!api.checkLogin(this.props.auth.token)){
         this.props.navigator.push({
            screen: 'WelcomeContainer',
            passProps:{
                token:this.props.auth.token,
                userdata:this.props.auth.userdata
            }                      
         });
        return false;
    }
    let markers  = this.state.displayItems;    
    markers[key].havefave = ss == 0 ? 1 : 0; 
    this.setState({markers},()=>{
        let _this = this;
        let request={
             url:settings.endpoints.favUnfavAd,
             method:'POST',
             params:{token:typeof this.props.auth.token != 'undefined' ? this.props.auth.token :this.props.auth.userdata.token,id:markers[key].id}
        }
        api.FetchData(request).then((result)=>{
             if(result.status){
                 if(this.state.iconHeart == 'm'){ 
                     this.setState({iconHeart :'n'});
                 }
                 else{
                     this.setState({heart:1,iconHeart :'m'});
                 }
             }else{
                 alert('Something Went Wrong.');
             }               
        }).catch((error)=>{ 
             //console.log(error);
        }); 
    });
    
}
_renderSharedElement(){
    let ret =[];  
    if(this.state.view != 1 && this.state.displayItems.length > 0 ){
        this.state.displayItems.map((item,key)=>{ 
           ret.push(<View key={'hidden'+item.id} style={{height:0,width:0}}>
                    <SharedElementTransition sharedElementId={'SharedTextId'+item.id}>
                        <Image style={[styles.proImg,styles.proImgsmall]} source={{uri:item.image}} />
                    </SharedElementTransition>
                </View>);
        });
    }
    
    if(ret.length > 0){
        return ret;
    }else{
        return null;
    }
}
_renderProductData(data,i){
    let _this = this;
    let dataDate = moment(data.post_on).format("DD MMM [|] HH:MM");
        if(this.state.view==1){           
        return (
        <Col sm={6} md={4} lg={3} style={[styles.proCol,{paddingBottom:5}]} key={i}>
            <View style={styles.proList}>
                <TouchableOpacity onPress={()=>{this.navigateTo('ProductDetail',data)}} activeOpacity={0.8}>
                    <View style={styles.canvasContainer}>
                        <View style={styles.imgInner}>
                            <SharedElementTransition sharedElementId={'SharedTextId'+data.id}>
                            <Image
                                style={[styles.proImg,styles.progridWidth]}
                                source={{uri:data.single_image}}
                            />
                            </SharedElementTransition>
                            <Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />
                            <View style={styles.absoluteBottomRight}>
                            {data.is_urgent == 1 ?  <Text style={[styles.globalIcon,styles.red]}> y</Text> : null }
                                    {data.is_direct_sale == 1 ?  <Text style={[styles.globalIcon,styles.red]}> w </Text> : null }
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
                <View style={styles.proCaption}>
                    <TouchableOpacity onPress={()=>{this.navigateTo('ProductDetail',data)}} activeOpacity={0.8}>
                        <Text style={[styles.proName,styles._F14_]} numberOfLines={1}>{data.title}</Text>
                    </TouchableOpacity>
                    <View style={[styles.bottomDetail]}>
                        <View style={[styles.inlineDatarow]}>
                                <Text style={[styles.orangeColor,styles.bold,styles._F14_,styles._MR10_]}>{data.price}</Text>
                                <View style={[styles.rightAlign,styles._PB5_]}>
                                <View style={[styles.relative]}>
                                    <Text style={[styles.globalIcon,styles.camIco]}>s</Text>
                                    <Text style={[styles.bold,styles.abscamtxt,styles.small]}>{data.totalImageCount}</Text>
                                </View> 
                                </View>
                        </View>    
                        <View style={[styles.inlineDatarow,styles.justRight]}>
                            <View style={[styles.inlineDatarow]}>
                                <View style={styles._MR5_}>
                                    <Text style={data.havefave==1 ? [styles.globalIcon,styles.xlIco,styles.orangeColor]:[styles.globalIcon,styles.xlIco]} 
                                                    onPress={()=>_this.doFav(i,data.havefave)}>
                                                    {data.havefave==1 ? 'n':'m'}</Text>
                                </View>
                                <TouchableOpacity style={styles.userChat} activeOpacity={0.8} 
                                onPress={()=>{this.navigateTo('Singlechat',data.id)}}>
                                    <View style={[styles.relative]}>
                                        <Text style={[styles.globalIcon,styles.xlIco]}> f</Text>
                                        <Text style={[styles.circle,styles.online,styles.absCircle]}> </Text>
                                    </View>
                                </TouchableOpacity>
                            </View> 
                        </View>    
                    </View>
                </View>
            </View>  
        </Col>
        );
      }else{
        return(
            data.user_type !='professional' ?
            <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_,styles.Prolistgray]}  key={i}>
                <Col sm={12} md={12} lg={12} style={[styles.proCol,styles.listCol]} >
                    <View style={styles.proList}>
                        <TouchableOpacity onPress={()=>{this.navigateTo('ProductDetail',data)}} activeOpacity={0.8}>
                            <View style={styles.canvasContainer}>
                                <View style={styles.imgInner}>
                                        <SharedElementTransition sharedElementId={'SharedTextId'+data.id}>
                                    <Image
                                        style={[styles.proImg,styles.progridWidth]}
                                        source={{uri:data.single_image}}
                                    />
                                    </SharedElementTransition>
                                    <Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />
                                    <View style={styles.absoluteBottomRight}>
                                    {data.is_urgent == 1 ?  <Text style={[styles.globalIcon,styles.red]}> y</Text> : null }
                                    {data.is_direct_sale == 1 ?  <Text style={[styles.globalIcon,styles.red]}> w </Text> : null }
                                       
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.proCaption}>
                            <TouchableOpacity onPress={()=>{this.navigateTo('ProductDetail',data)}} activeOpacity={0.8}><Text style={[styles.proName,styles._F14_]} numberOfLines={1}>{data.title}</Text></TouchableOpacity>
                            <View style={styles.bottomDetail}>
                                <Text style={styles.leftAlign}>
                                    <Text style={[styles.orangeColor,styles.bold,styles._F14_,styles._MR10_]}>{data.price}</Text>
                                    <Text><Text style={[styles.globalIcon,styles.camIco,styles.big]}> {'['} </Text>
                                    <Text style={styles.bold}>3</Text></Text>
                                </Text>    
                                <View style={[styles.inlineDatarow,styles.justRight]}>
                                    <View style={[styles.inlineDatarow]}>
                                        <View style={styles._MR5_}>
                                           <Text style={data.havefave==1 ? [styles.globalIcon,styles.xlIco,styles.orangeColor]:[styles.globalIcon,styles.xlIco]} 
                                                    onPress={()=>_this.doFav(i,data.havefave)}>
                                                    {data.havefave==1 ? 'n':'m'}</Text></View>
                                        <TouchableOpacity style={styles.userChat} activeOpacity={0.8} onPress={()=>{this.navigateTo('Singlechat',data.id)}}><View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View></TouchableOpacity>
                                    </View> 
                                </View>    
                            </View>
                        </View>
                    </View>  
                </Col>
            </Row> :
             <Row size={12} style={[styles.proRow,styles.prolistRow,styles._MT10_,styles._PT0_,styles.Prolistgray]}>
                <Col sm={5} md={3} lg={2} style={[styles.proCol,styles.listCol]} >
                    <View style={styles.imageCanvas}>
                        <View style={styles.imgInner}>
                        {typeof data.video != 'undefined' && data.video != "" ?
                            <Image style={styles.hasVideo} source={images.video} /> : null 
                        }
                            <Image style={styles.proImg} source={{uri:data.single_image}} resizeMode="stretch"/>
                        </View>
                    </View>
                    <View style={[styles.bottomDetail,styles.bottomCaption]}>
                        <Text style={[styles.leftAlign,styles.white]}>
                            <Text style={styles.small}>{dataDate}</Text>
                            {/*{data.post_on}*/}
                        </Text>
                        <View style={[styles.rightAlign]}>
                            <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}>s</Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>3</Text></View> 
                        </View>
                    </View>
                </Col>
                <Col sm={7} md={9} lg={10} style={[styles.proCol,styles.listCol]}>
                    <View style={styles.proCaption}>
                        <Text style={[styles.proName,styles.big]} numberOfLines={1}>{data.title}</Text>
                        <View style={styles.row}>
                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                <Text style={[styles.orangeColor,styles.bold]}>{data.price}</Text>
                                <View style={[styles.inlineDatarow,styles.justRight]}>
                                    <View style={[styles.inlineDatarow]}>
                                        <View style={styles._MR5_}><Text style={data.havefave==1 ? [styles.globalIcon,styles.xlIco,styles.orangeColor]:[styles.globalIcon,styles.xlIco]} 
                                                    onPress={()=>_this.doFav(i,data.havefave)}>
                                                    {data.havefave==1 ? 'n':'m'}</Text></View>
                                        <TouchableOpacity style={styles.userChat} activeOpacity={0.8} onPress={()=>{this.navigateTo('Singlechat',data.id)}}><View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View></TouchableOpacity>
                                    </View> 
                                </View>  
                            </View>
                            <View style={[styles.inlineDatarow,styles._MT10_,styles._PB15_]}>
                            {data.is_urgent == 1 ? 
                                <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text> : null}
                             {data.is_direct_sale == 1 ?   <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text> : null}
                            </View>
                            <View style={[styles.inlineDatarow]}>
                            {data.user_type=='professional' ? (
                                    <Text style={styles.small}>{data.categoryname} <Image style={styles.proSign} source={images.pro} /></Text>
                                ):(
                                    <Text style={styles.small}>{data.categoryname} </Text>
                                )}
                            </View>  
                            <View><Text style={styles.small}>{data.town} {data.town != '' && data.department != ''? '/' :null} {data.department}</Text></View>
                                    <View>
                                        <Text style={styles.small}>{dataDate}</Text>
                                    </View>
                        </View>
                    </View>
                </Col>
            </Row>
        );
      }
  }

  render() {
    let popup =  <Modal style={styles.modalApear} animationType={"slide"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {alert("Modal has been closed.")}}>
                    <View style={[styles.modalInner,styles.justCenter]}>
                        <Text style={[styles.globalIcon,styles.modalClose]} onPress={() => {this.setModalVisible(false)}}>{';'}</Text>
                        <View style={styles.modalMain}>
                            <View style={[styles.modalBody]}>
                                <View><Text style={styles.subTitle}>Sauvegarder dans vos recherches préférées</Text></View>
                                <View style={[styles.inlineDatarow,styles._PR10_]}>
                                     <TextInput style={[styles.genInput,styles.flex]} placeholder={"Titre de la recherche"}  underlineColorAndroid={'#eee'}/><Text style={[styles.globalIcon,styles.absoluteBottomRight]}>h</Text>
                                </View>
                                <View style={styles._MT15_}>
                                    <TouchableOpacity style={[styles.brandColor]} activeOpacity={0.8}><Text style={[styles.genButton,styles.smallBtn]}>Sauver</Text></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>

    const listHeight = Platform.OS === 'ios' ? height : height-60;
    const headerTranslate =  this.state.scrollY.interpolate({
        inputRange: [0, 50],
        outputRange: [0, -50],
        extrapolate: 'clamp',
    });
    let product;
    let productListing=(this.state.view == 1 ? (
        <Row size={12} style={[styles.proRow]}>                          
            {this.state.displayItems.map((data,i)=>{
                return this._renderProductData(data,i);
            })}
        </Row>):(<View style={styles.proMain}>
            {this.state.displayItems.map((data,i)=>{
            return this._renderProductData(data,i);
            })}
    </View>));
    if(this.state.tabs ==1){
        product = productListing;
    }else{
        product=null;
    }
    console.log(this.props.data.UserDetail.usertype);
    return (
        <View style={[styles.productdataMain]}>
            {this._renderSharedElement()}
            <Animated.View style={[styles.searchHolder,
                    {
                        transform: [{ translateY: headerTranslate },
                        {
                            scale: this.state.scrollY.interpolate({
                                inputRange: [-50 , 0, 50],
                                outputRange: [2, 1, 1]
                            })
                        }]
                    },
                ]}>
                <Image source={images.companylogo} style={styles.uneSym}/>
                        <View style={styles.searchinputHolder}>
                            <TextInput onFocus={()=>{Keyboard.dismiss();this.goto('ShopSearch')}} style={[styles.searchInput,{marginLeft:5}]} placeholder={"Rechercher..."} underlineColorAndroid={'#0000'}
                            value={this.state.serachTxt} 
                        />
                        </View>
                        <TouchableOpacity onPress={()=>{this.goto('ShopSearch')}} activeOpacity={0.8} style={[styles.icoholderRight]}>
                            {/* <Text style={[styles.genIco,styles.searchIco]}>v</Text> */}
                            <Icon name="search" color={colors.navBarButtonColor} size={20} style={{marginRight:5}}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity activeOpacity={0.8} onPress={()=>{this.goto('AdvanceSearch')}}>
                            <Text style={[styles.searchholderIco,styles.genIco,styles.blueColor,styles.largeIco]} >r</Text
                            <Icon name="mixer" color={"#4789bf"} size={20} style={{marginRight:5}}/>
                        </TouchableOpacity>> */}
            </Animated.View>
            <View style={[this.state.tabs == 1 ?styles.gridParent:styles.listParent,{height:listHeight}]}>
                <View style={[{borderColor:'#fff',}]}>
                    <View style={styles.tabParent}>
                        <View style={styles.tabs}>
                            <TouchableOpacity  style={(this.state.tabSelected== 1)?[styles.tabSelected,styles.widthThree]:[styles.tabnotSelected,styles.widthThree]}
                             //onPress={()=>this.tabchange(2)} 
                             activeOpacity={0.8}>
                                <Text style={(this.state.tabSelected== 1)?[styles.tabTextSelected,styles.orangeColor,{fontSize:12,paddingBottom:5}]:[styles.tabText]}>{
                                    this.props.data.UserDetail.usertype == 'personal' ? 'PARTICULIER':'PROFESSIONNEL'}</Text>
                            </TouchableOpacity>
                        </View>    
                        <View style={[styles.botomOrangeLine,styles.forproduct]}></View>
                    </View>
                    <ScrollView
                    style={[this.state.tabs == 1 ?[styles.gridParent]:styles.listParent]}
                    contentContainerStyle={{flexGrow:1,paddingBottom:50}}
                    showsVerticalScrollIndicator={false}
                    scrollEventThrottle={5}
                    onScroll={this.onScroll.bind(this)}
                    refreshControl={this._refreshControl()}>
                    {this.state.refreshing ?(
                        <View style={{
                            height:Dimensions.get('window').height,
                        }}>
                        </View>
                    ):this.state.displayItems.length >  0 ? product :<View style={{marginTop:15}}><Text style={{fontSize:12,textAlign:'center'}}>Aucun Enregistrement Trouvé</Text></View>}
                    </ScrollView>
                    {/*{product}*/}
                </View>
            </View>            
            {popup}
        </View>    
    );
  }
}


function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAnnonces);

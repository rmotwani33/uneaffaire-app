import React, { Component } from 'react';
import {Image,Text,View,ScrollView,TextInput,TouchableOpacity} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import DropdownAlert from 'react-native-dropdownalert';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import { iconsMap, iconsLoaded,Icon } from '../../config/icons';
import startMain from '../../config/app-main';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;

class success extends Component {
    static navigatorStyle = {
        drawUnderTabBar:true,
        //tabBarHidden:true,
    };
    constructor(props) {
        super(props);
        this.state={
            message:typeof this.props.message != 'undefined' ? this.props.message:'',
            load:false,
            status:typeof this.props.status != 'undefined' ? this.props.status:'N'
        };
        // if you want to listen on navigator events, set this up
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }
    componentWillMount(){
        iconsLoaded.then(()=>{});
    }
    onNavigatorEvent(event) {
    //console.log(event)
        if(event.id=='willAppear'){
            this.props.chatActions.setScreen(this.props.testID);
            this.props.navigator.toggleNavBar({
            to: 'shown',
            animated: false
        });
        this.props.navigator.toggleTabs({
            to: 'hidden',
            animated: false
        });
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({title: "Panier"});
        this.props.navigator.setButtons({
                leftButtons: [{
                   // icon: iconsMap['back'],
                    id: 'back2',
                    title:'',
                }],
                animated:false
            });   
        }
        if(event.id=='didAppear'){

        }
        // if(event.id == 'back2'){
        // this.props.navigator.pop({
        //     animated: true // does the pop have transition animation or does it happen immediately (optional)
        // });
        // }
    }
    goto(page){
        startMain(this.props.auth.token,this.props.auth.userdata);
    }
    render() {
        return (        
           <View style={[styles.scrollInner,{alignItems:'center',justifyContent:'center'}]}> 
                <View style={{flex:2,justifyContent:'center',alignSelf:'center'}}>
                    <View style={{height:100,width:100,backgroundColor:'#F15A25',borderRadius:50,justifyContent:'center',alignItems:'center',}}>
                        {this.state.status =='Y' ? 
                        (
                            <Icon name="tick" color={'#fff'}  size={60}/>):(<Icon name="close" color={'#fff'}  size={50}/>)
                        }
                    </View>
                </View>
                <View style={{flex:1,alignItems:'center'}}>
                    <Text style={{fontSize:22,fontWeight:'bold',color:'#000'}}>{this.state.status =='Y' ? 'Paiement effectué avec succès !':'Paiement échoué !'}</Text>
                    <Text style={{fontSize:16,color:'#000',textAlign:'center'}}>{this.state.message}</Text>
                </View>
                <View style={{flex:1,alignItems:'center'}}>                    
                    <TouchableOpacity style={[styles.justCenter,{height:45,borderRadius:22.5,borderColor:'#F15A25',borderWidth:2,padding:10,justifyContent:'center',alignItems:'center'
                    }]} activeOpacity={0.85} onPress={()=>this.goto('HomeContainer')}>
                        <Text style={{fontSize:16,color:'#F15A25'}}>Retour à la page d'acceuil</Text>
                    </TouchableOpacity>
                </View>
                <DropdownAlert ref={(ref) => this.dropdown = ref} onClose={(data) => {}} />
           </View>
        );
    }
}
function mapStateToProps(state, ownProps) {
  return {
         chat:state.chat,
         auth:state.auth,
  };
 }
 
 function mapDispatchToProps(dispatch) {
  return {
   chatActions: bindActionCreators(chatActions, dispatch),
   authActions: bindActionCreators(authActions, dispatch),
  };
 }
 
 export default connect(mapStateToProps, mapDispatchToProps)(success);
import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch,Dimensions,Alert,AsyncStorage
} from 'react-native';
import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {settings} from '../../config/settings';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import Loader from '../../components/Loader/Loader';
import { iconsMap, iconsLoaded } from '../../config/icons';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import DropdownAlert from 'react-native-dropdownalert';
import PayPalPayment from '../../redux/utils/paypal';
//import Spinner from'rn-spinner';
import {Column as Col, Row} from 'react-native-flexbox-grid';

// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

const { height, width } = Dimensions.get('window');
const api= new ApiHelper;

class shopCart extends Component {

static navigatorStyle = {
     navBarBackgroundColor: colors.navBarBackgroundColor,
     navBarTextColor: colors.navBarTextColor,
     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
     navBarButtonColor: colors.navBarButtonColor,
     statusBarTextColorScheme: colors.statusBarTextColorScheme,
     statusBarColor: colors.statusBarColor,
     tabBarBackgroundColor: colors.tabBarBackgroundColor,
     tabBarButtonColor: colors.tabBarButtonColor,
     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
     navBarSubtitleColor: colors.navBarSubtitleColor,
     navBarTextFontFamily: colors.navBarTextFontFamily,
    //tabBarHidden: false,
     drawUnderTabBar:true
};  
constructor(props) {
    super(props);
    this.state={
        cartData:[],
        coupon:'',
        ucredit:this.props.auth.userdata.user_credit,
        shopAmount:0,
        discount:0,
        days:0,
        UserDetail:[]
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
}

componentWillMount(){
    iconsLoaded.then(()=>{});
}
sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
}

onNavigatorEvent(event) {
    //console.log(event)
    if (event.id === 'bottomTabSelected') {
       this.setState({onBack:event.unselectedTabIndex});
    }
    if(event.id=='willAppear'){       
        this.sendEventToDrawer();
        api.setAction(this.props.chatActions,this.props.authActions);
        this.props.chatActions.setScreen(this.props.testID);
        //console.log(this.props);
        this.props.navigator.toggleNavBar({
            to: 'shown',
            animated: true
        });
        this.props.navigator.toggleTabs({
            animated: true,
            to: 'hidden'
        });
        this.props.navigator.setStyle({navBarTitleTextCentered: true});
        this.props.navigator.setTitle({title: 'Votre Commande'});
        this.props.navigator.setButtons({
                leftButtons: [{
                    icon: iconsMap['back'],
                    id: 'back2',
                    title: 'back to welcome'
                }],animated:false
            });
    }
    if(event.id=='didAppear'){
         this.getCartData();
        //  AsyncStorage.getItem("userdata").then((value) => {
        //     let p = JSON.parse(value);
        //     this.setState({UserDetail: p});
        // }).done();
    }       
    if(event.id == 'back2'){
        if(this.state.onBack!=null){
            this.props.navigator.switchToTab({
                tabIndex: this.state.onBack // (optional) if missing, this screen's tab will become selected
            });
        }else{
            this.props.navigator.pop({
                animated: true // does the pop have transition animation or does it happen immediately (optional)
            });
        }     
    }
  }
doDiscount(){
    if(this.state.coupon == ''){
        this.dropdown.alertWithType('error', 'Error','Entrez le code de coupon');
        return false;
    }
    let _this = this;
    // if(_this.state.shopAmount <= _this.state.ucredit){
    //      _this.dropdown.alertWithType('error', 'Error','Code de Coupon Invalide');
    //     return false;
    // }
    _this.setState({load1:true},()=>{
        let request={
            url:settings.endpoints.getCoupanPrice,
            method:'POST',
            params:{token:this.props.auth.token != undefined ? this.props.auth.token:'',shop_package_id:this.props.id,coupon:this.state.coupon}
        }
        api.FetchData(request).then((result)=>{       
            if(result.status){
                _this.setState({
                    discount:result.disscount_price,
                    load1 :false
                });
                 _this.dropdown.alertWithType('success', 'Success','Coupon appliqué'); 
            }else{
                _this.setState({                
                    load1 :false,
                    discount:0
                });
                 _this.dropdown.alertWithType('error', 'Error',result.message);
            }
        }).catch((error)=>{
            //console.log(error);
        });
    });
}
makeShop(){
    let _this = this;
    _this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.checkWallet,
            method:'POST',
            params:{token:this.props.auth.token != undefined ? this.props.auth.token:'',shop_package_id:this.props.id,type:'shop',coupon_code:this.state.coupon != '' ? this.state.coupon:''}
        }
        api.FetchData(request).then((result)=>{ 
            _this.setState({load :false});  
            console.log(result);           
            if(result.status){
                if(result.paypal){
                    _this.payWithPaypal(result);
                }else{
                    let np = _this.props.auth.userdata;
                    let p ={};
                    Object.keys(np).map((kk) => {
                        p[kk] = np[kk];
                    });
                    p.have_shop = 1;
                    p.shop_id =result.shop_id;
                    p.shop_package_id = _this.props.id;
                    _this.props.authActions.setUserData(_this.props.auth.token,p);
                    AsyncStorage.setItem("userdata", JSON.stringify(p));
                   _this.props.navigator.push({
                       screen: 'success',
                       passProps:{
                           status:'Y',
                           message:result.message,
                           token:_this.props.auth.token,
                           userdata:_this.props.auth.userdata
                       }
                   }); 
                }                        
            }else{
                _this.setState({                
                    load :false
                });            
            }
        }).catch((error)=>{
            console.log(error);
        });
    });
    
}
payWithPaypal(result){
    let total = result.amount;
    if(total != '' && total != 0){
        let price = parseFloat(total).toFixed(2);
        //console.log(""+price);return false;
        let params = {
            price: price,
            currency: 'EUR',
            description: 'Buy a Shop',
            custom:'paypal_shop'
        };
        PayPalPayment(params).then(confirm => {
            if(confirm.response.state=='approved'){
                this.goSuccess(confirm);           
            }        
        }).catch(error => console.log(error));        
    }
}
goSuccess(confirm){
    this.setState({load:true},()=>{
        let _this = this;
        let request={
            url:settings.endpoints.shopPackageTransaction,
            method:'POST',
            params:{token:this.props.auth.token != undefined ? this.props.auth.token:'',shop_package_id:this.props.id,coupon_code:this.state.coupon != '' ? this.state.coupon:'',paypal_id:confirm.response.id}
        }
        api.FetchData(request).then((result)=>{
            if(result.status){
                this.setState({load:false},()=>{
                    let np = _this.props.auth.userdata;
                    let p ={};
                    Object.keys(np).map((kk) => {
                        p[kk] = np[kk];
                    });
                    p.have_shop = 1;
                    p.shop_id =result.shop_id;
                    p.shop_package_id = _this.props.id;
                    _this.props.authActions.setUserData(_this.props.auth.token,p);
                    AsyncStorage.setItem("userdata", JSON.stringify(p));
                    _this.props.navigator.push({
                        screen: 'createShop',
                        passProps:{
                            id:_this.props.id,
                            UserDetail : p,
                            days:result.days
                        }
                    });
                });
            }else{
                this.setState({load:false},()=>{
                    alert('Quelque chose s\'est mal passé dans le paiement.')
                });
            } 
        }).catch((error)=>{
            console.log(error);
            this.setState({load:false},()=>{
                alert('Quelque chose s\'est mal passé dans le paiement.')
            });
        });
    });
    
}
getCartData(){
    let _this = this;
    _this.setState({load:true},()=>{
        let request={
            url:settings.endpoints.shopCart,
            method:'POST',
            params:{token:this.props.auth.token != undefined ? this.props.auth.token:'',shop_package_id:this.props.id,type:'',days:''}
        }
        api.FetchData(request).then((result)=>{       
            if(result.status){
                _this.setState({
                    cartData:result.packageDetails != undefined ? result.packageDetails :[] ,
                    shopAmount:result.packageDetails.amount,
                    days:result.packageDetails.days,
                    load :false
                },//()=>_this.showAlert(result.status,result.message)
                ); 
            }else{
                _this.setState({                
                    load :false
                },()=>_this.showAlert(result.status,result.message));
            }
        }).catch((error)=>{
            console.log(error);
        });
    });
    
}

showAlert(status,message){
    console.log(message);
    if(status){
        this.dropdown.alertWithType('success', 'Succès', message); 
    }else{
        this.dropdown.alertWithType('error', 'Erreur', message);
    }     
}
render() {
    let _this = this;
    let type = '';            
    let data =<Row size={12}  style={[styles.proRow,styles.prolistRow,styles._MT0_,styles._PT0_]}>
        <Col sm={5} md={3} lg={2} style={[styles.proCol,styles.listCol,{alignItems:'center',justifyContent:'center'}]} >
            <View style={[styles.colInner]}>
                <View style={[]}>
                    <Image style={{height:100,width:100,resizeMode:'cover'}}  source={images.store} />
                </View>
            </View>            
        </Col>
        <Col sm={7} md={9} lg={10} style={[styles.proCol,styles.listCol]}>
            <View style={[styles.proCaption]}>
                <Text style={[styles.proName,styles.big,styles._PB10_]} numberOfLines={1}>{this.state.cartData.name}</Text>
                    <View style={styles.row}>
                        <View style={[styles.dataRow,styles._PB10_]}>
                            <View style={styles.inlineWithico}>
                                <Text>{this.state.cartData.description}</Text>
                            </View>                                           
                        </View> 
                        <View style={[styles.dataRow,styles._PB10_]}>
                            <View style={styles.inlineWithico}>
                                <Text>{this.state.cartData.amount+colors.euro}</Text>
                            </View>                                           
                        </View>
                    </View>
            </View>
        </Col>
    </Row>;                     
    return (  
        this.state.load ? <View style={[styles.Center]} ><Loader/></View> :           
        <View style={[styles.main,styles._PT15_,styles._HP15_]}>
            <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
                <View style={styles.scrollInner}>                    
                    <View style={styles.proMain}>
                        {data}
                    </View>
                    <View style={{paddingVertical:10,flexDirection:'row',backgroundColor:"#fff"
                    }}>
                    <FloatLabelTextInput  style={styles.genInput} placeholder={"Code de coupon"}  underlineColorAndroid={'#eee'}
                    onChangeTextValue ={(val) =>this.setState({coupon:val})} 
                    value={this.state.coupon}/>
                    <View style={{
                        alignItems:'center',justifyContent:'center',
                    }} >
                        <LoaderButton onPress={()=>this.doDiscount()} load={this.state.load1} BtnStyle={[styles.brandColor,styles.justCenter,{padding:5}]} text={'Appliquer'} textStyle={[styles.genButton,{padding:5,fontSize:14}]} />       
                    </View>
                    </View>
                    <View style={mainStyle.cont}>
                        <View style={mainStyle.row}>               
                            <Text style={mainStyle.text}>Sous total</Text>               
                            <Text style={mainStyle.text}>{colors.euro}{' '}{this.state.cartData.amount}</Text>
                        </View>
                        <View  style={mainStyle.row}>                
                            <Text style={mainStyle.text}>Remise</Text>               
                            <Text style={mainStyle.text}>{colors.euro}{' '}{this.state.discount}</Text>
                        </View>
                        <View style={mainStyle.row}>                
                            <Text style={mainStyle.text}>Credit</Text>                
                            <Text style={mainStyle.text}>{colors.euro}{' '}{this.state.shopAmount<this.state.ucredit ? this.state.shopAmount-this.state.discount : this.state.ucredit}</Text>               
                        </View> 
                        <View style={mainStyle.row}>                
                            <Text style={mainStyle.text}>Prix total</Text>                
                            <Text style={mainStyle.text}>{colors.euro}{' '}{this.state.shopAmount>this.state.ucredit ? (this.state.shopAmount-this.state.ucredit)-this.state.discount : 0}</Text> 
                        </View>
                    </View>
                </View>    
            </ScrollView>            
            <View style={[styles.bottomFixed,styles.justCenter,styles.stretch,styles._MLR14DEC_]}> 
                <View style={[styles.stretch]}>
                    <TouchableOpacity style={[styles.brandColor,styles._MLR5_]} onPress={()=>this.makeShop()} activeOpacity={0.8}>
                        <Text style={[styles.genButton,styles._F16_]}>Acheter maintenant</Text>
                    </TouchableOpacity>                       
                </View>
            </View>
               <DropdownAlert
        ref={(ref) => this.dropdown = ref}
        onClose={(data) => {/*console.log(data);*/}} />  
            
        </View>        
    );
  }
}
const mainStyle = StyleSheet.create({
    cont:{
        flex:1,
        padding:10,
    },
    row:{
        flex:1,
        alignItems:'center',
        justifyContent:'space-between',
        flexDirection:'row',
        paddingTop:10,
        paddingBottom:10,
        borderColor:"#ddd",
        borderBottomWidth:1, 
    },
    col:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',       
    },
    text:{
        color:"#666",
        fontSize:18,
    }
})


function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(shopCart);
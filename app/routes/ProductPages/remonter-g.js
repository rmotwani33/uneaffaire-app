
import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import RadioButton from '../../lib/react-radio/index';
import {Navigation,Screen} from 'react-native-navigation';
import CheckBox from 'react-native-check-box';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import Dimensions from 'Dimensions';
const { height, width } = Dimensions.get('window');
import {Column as Col, Row} from 'react-native-flexbox-grid';
export default class remonter extends Component {
//   static navigatorButtons = {
//      leftButtons: [{
//        icon: require('../../images/back.png'),
//        id: 'back',
//        title:'back to welcome',
//      }],
//   };
// static navigatorStyle = {
//     navBarBackgroundColor: colors.navBarBackgroundColor,
//     navBarTextColor: colors.navBarTextColor,
//     navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
//     navBarButtonColor: colors.navBarButtonColor,
//     statusBarTextColorScheme: colors.statusBarTextColorScheme,
//     statusBarColor: colors.statusBarColor,
//     tabBarBackgroundColor: colors.tabBarBackgroundColor,
//     tabBarButtonColor: colors.tabBarButtonColor,
//     tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
//     navBarSubtitleColor: colors.navBarSubtitleColor,
// };
  
  constructor(props) {
    super(props);
    //console.log(this.props);
    this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        trueSwitchIsOn: true,
        falseSwitchIsOn: false,
        value: 0,
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  

  onNavigatorEvent(event) {
    //console.log(event)
    this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: true
    });
    this.props.navigator.toggleTabs({
        animated: false,
        to: 'hidden'
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title: "Remonter",

    });
    if(event.id == 'back'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }

  handleOnPress(value){
        this.setState({value:value})
 }

  goto(page){
    this.props.navigator.push({
      screen: page
    });

  }
  tabchange(tab){
          this.setState({tabs:tab,tabSelected:tab});
  }
  render() {
    return (
        <ScrollView showsVerticalScrollIndicator={false} style={[styles.mainScollview]}>
            {/* <View style={styles.scrollInner}>
                <View style={[styles.main,styles._HP15_]}>
                        <Text style={styles.blockTitle}>Remonter en tête de liste</Text>
                        <View style={styles.proMain}>
                            <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_]}>
                                <Col sm={4} md={4} lg={2} style={[styles.proCol,styles.listCol]} >
                                    <View style={styles.imageCanvas}>
                                        <View style={styles.imgInner}>
                                            <Image style={[styles.proImg,styles.proImgremonter]} source={images.minic} />
                                        </View>
                                    </View>
                                </Col>
                                <Col sm={8} md={8} lg={10} style={[styles.proCol,styles.listCol]}>
                                    <View style={styles.proCaption}>
                                        <Text style={[styles.proName,styles.big]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
                                        <Text style={[styles.smallDesc,styles.small]}>Lorem ipsum dolor sit amet, consectetur</Text>
                                        <View style={styles.row}>
                                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT10_]}>
                                                <Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text>
                                                <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>
                                                <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>
                                            </View>
                                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                                <TouchableOpacity style={[styles.inlineDatarow,styles.discuterbtn]}activeOpacity={0.8}>
                                                   <Text style={[styles.inlineWithico,styles._MR0_,styles._F12_]}>Hors ligne</Text><Text style={[styles.circle,styles.offline,styles.inlineWithico]}></Text>
                                                </TouchableOpacity> 
                                                <Text style={styles.fullDate}>9 dec, 2016</Text>
                                            </View>
                                        </View>
                                    </View>
                                </Col>
                            </Row> 
                             <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_]}>
                                <Col sm={4} md={4} lg={2} style={[styles.proCol,styles.listCol]} >
                                    <View style={styles.imageCanvas}>
                                        <View style={styles.imgInner}>
                                            <Image style={[styles.proImg,styles.proImgremonter]} source={images.minic} />
                                        </View>
                                    </View>
                                </Col>
                                <Col sm={8} md={8} lg={10} style={[styles.proCol,styles.listCol]}>
                                    <View style={styles.proCaption}>
                                        <Text style={[styles.proName,styles.big]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
                                        <Text style={[styles.smallDesc,styles.small]}>Lorem ipsum dolor sit amet, consectetur</Text>
                                        <View style={styles.row}>
                                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT10_]}>
                                                <Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text>
                                                <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>
                                                <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>
                                            </View>
                                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                                <TouchableOpacity style={[styles.inlineDatarow,styles.discuterbtn]}activeOpacity={0.8}>
                                                   <Text style={[styles.inlineWithico,styles._MR0_,styles._F12_]}>Discuter</Text><Text style={[styles.circle,styles.online,styles.inlineWithico]}></Text>
                                                </TouchableOpacity> 
                                                <Text style={styles.fullDate}>9 dec, 2016</Text>
                                            </View>
                                        </View>
                                    </View>
                                </Col>
                            </Row> 
                            <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_]}>
                                <Col sm={4} md={4} lg={2} style={[styles.proCol,styles.listCol]} >
                                    <View style={styles.imageCanvas}>
                                        <View style={styles.imgInner}>
                                            <Image style={[styles.proImg,styles.proImgremonter]} source={images.minic} />
                                        </View>
                                    </View>
                                </Col>
                                <Col sm={8} md={8} lg={10} style={[styles.proCol,styles.listCol]}>
                                    <View style={styles.proCaption}>
                                        <Text style={[styles.proName,styles.big]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
                                        <Text style={[styles.smallDesc,styles.small]}>Lorem ipsum dolor sit amet, consectetur</Text>
                                        <View style={styles.row}>
                                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT10_]}>
                                                <Text style={[styles.orangeColor,styles.bold]}>12,550 €</Text>
                                                <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small]}>Urgent<Text style={styles.globalIcon}> x</Text></Text>
                                                <Text style={[styles.inlineWithico,styles.red,styles.small]}>Achat Ditect<Text style={styles.globalIcon}> w</Text></Text>
                                            </View>
                                            <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT5_]}>
                                                <TouchableOpacity style={[styles.inlineDatarow,styles.discuterbtn]}activeOpacity={0.8}>
                                                   <Text style={[styles.inlineWithico,styles._MR0_,styles._F12_]}>Hors ligne</Text><Text style={[styles.circle,styles.offline,styles.inlineWithico]}></Text>
                                                </TouchableOpacity> 
                                                <Text style={styles.fullDate}>9 dec, 2016</Text>
                                            </View>
                                        </View>
                                    </View>
                                </Col>
                            </Row> 
                        </View>
                        <Text style={[styles.genText,styles._MT15_]}>En continuant, j'accepte les conditions d'utilisation de UneAffaire.fr (y compris la renonciation au droit de rétractation).</Text>
                        <View style={[styles.inlineDatarow,styles.spaceBitw,styles._MT15_,styles._PB15_]}>
                            <TouchableOpacity activeOpacity={0.8}>
                                <Text style={[styles.smallBtn,styles.slatebgColor,styles.w120]}>Annuler</Text>
                            </TouchableOpacity>
                            <TouchableOpacity activeOpacity={0.8}>
                                <Text style={[styles.smallBtn,styles.w120]}>Valider</Text>
                            </TouchableOpacity>
                        </View>
                </View>
            </View> */}
        </ScrollView>
    );
  }

}






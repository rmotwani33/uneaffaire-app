import React, {Component} from 'react';
import {
    StyleSheet,
    Image,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    Platform,
    Animated,
    WebView
} from 'react-native';

import {colors} from '../../config/styles';
import styles from '../../config/genStyle';
import { iconsMap, iconsLoaded } from '../../config/icons';
import Tabs from '../../components/Tabs/Tabs';
import moment from 'moment';
import startMain from '../../config/app-main';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
import Swipeout from 'react-native-swipeout';
import Icon from 'react-native-vector-icons/FontAwesome';
import {settings} from '../../config/settings';
var _ = require('lodash');
import 'moment/locale/fr'  // without this line it didn't work
moment.locale('fr')
const api = new ApiHelper;

class Chat extends Component {
  
    static navigatorStyle = {
        navBarBackgroundColor: colors.navBarBackgroundColor,
        navBarTextColor: colors.navBarTextColor,
        navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
        navBarButtonColor: colors.navBarButtonColor,
        statusBarTextColorScheme: colors.statusBarTextColorScheme,
        statusBarColor: colors.statusBarColor,
        tabBarBackgroundColor: colors.tabBarBackgroundColor,
        tabBarButtonColor: colors.tabBarButtonColor,
        tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
        navBarSubtitleColor: colors.navBarSubtitleColor
    };

    constructor(props) {
        super(props);
        // if you want to listen on navigator events, set this up
        this.state = {
            check1: false,
            tabs: 1,
            tabSelected: 1,
            like: 1,
            scrolled: true,
            scrollY: new Animated.Value(0),
            onBack: null,
            activeRowNoti : null,
        };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    componentDidMount(){
        iconsLoaded.then(()=>{});
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.chat.unread != nextProps.chat.unread){
            //console.log(nextProps.chat); 
        }
    }

    
    sendEventToDrawer(){
        this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
            screen:this.props.testID
        }});
    }

    handleOpen(){
        if(!api.checkLogin(this.props.auth.token)){
            startMain('',{});
        }else if(!api.isProfileComplete(this.props.auth.userdata)){
            this.props.navigator.switchToTab({
                tabIndex:4
            });
        }else{
            this.sendEventToDrawer();
            api.setAction(this.props.chatActions,this.props.authActions);
            //this.props.navigator.switchToTab({})
            this.props.chatActions.setScreen(this.props.testID);
            //console.log(this.props);
            this.props.navigator.toggleNavBar({to: 'shown', animated: true});
            this.props.navigator.toggleTabs({to: 'shown', animated: true});
            this.props.navigator.setStyle({navBarTitleTextCentered: true});
            this.props.navigator.setTitle({title: "Messagerie"});
            this.props.navigator.setButtons({
                leftButtons: [{
                    icon: iconsMap['back'],
                    id: 'back2',
                    title: 'back to welcome'
                }],animated:false
            });
        }
    }
    

    onNavigatorEvent(event) {
        if (event.id === 'bottomTabSelected' && event.selectedTabIndex == 1) {
            //console.log('bottomtabselected  on back state == '+this.state.onBack)
            if(this.state.onBack == null){
                //console.log('on back go to tab '+(event.unselectedTabIndex+1));
                if(!api.isProfileComplete(this.props.auth.userdata)){
                    this.setState({onBack: 0});
                }else{
                    this.setState({onBack: event.unselectedTabIndex});
                }
            }
        }
        if (event.id == 'willAppear') {
            this.handleOpen();
        }
        if (event.id == 'willDisappear') {
            //this.setState({onBack: null});
        }
        

        if (event.id == 'back2') {
            this.goback()
            // this.props.navigator.pop({
            //         animated: true // does the pop have transition animation or does it happen immediately (optional)
            //     });
        }


        if (event.id == 'menu') {
            this.props.navigator.toggleDrawer({
                    side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
                    animated: true, // does the toggle have transition animation or does it happen immediately (optional)
                    to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
                });
        }
    }
    goback(){
        if (this.state.onBack != null) {
            let onBack = this.state.onBack
            this.setState({onBack: null},()=>{
                this.props.navigator.switchToTab({
                    tabIndex: onBack // (optional) if missing, this screen's tab will become selected
                });
            });
        } else {
            this.props.navigator.switchToTab({
                tabIndex: 0 // (optional) if missing, this screen's tab will become selected
            });
        }
    }

    goto(page,data) {
        let newData = {};
        newData.send_from_id = this.props.auth.userdata.id == data.from_id ? data.to_id : data.from_id;
        newData.ad = data.ad;
        newData.token = this.props.auth.token;
        newData.name =  data.fullname;
        this.props.chatActions.openChat(data);
        this.props.navigator.push({
            screen: page,
            passProps:{
                data:newData
            }
        });
    }
    tabchange(tab) {
        this.setState({tabs: tab, tabSelected: tab});
    }

    onScroll(event) {
        var currentOffset = event.nativeEvent.contentOffset.y;
        var direction = currentOffset > this.offset
            ? this.setState({scrolled: true})
            : this.setState({scrolled: false});
        this.offset = currentOffset;
    }

    deleteNotification = (item) => {
        if(_.isObject(item) && !_.isEmpty(item)) {
            let request={
                url:settings.endpoints.deleteChat,
                method:'POST',
                params:{token:typeof this.props.auth.token != 'undefined' ? this.props.auth.token :this.props.auth.userdata.token,ad_id:item.ad}
           }
            api.FetchData(request).then((result)=>{
                if(result.status){
                   
                }else{
                    alert('Something Went Wrong.');
                }               
            }).catch((error)=>{ 
                    //console.log(error);
            }); 
        }
    }

    _renderChat(data,i,tab){
        let swipeoutNoti = [
            {
              component: <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}><Icon name="trash-o" style={{ color: '#fff', fontSize: 28 }} /></View>,
              type: 'delete',
              onPress: () => { this.deleteNotification(data) }
            }
          ];
        let row=null;
        if(tab==1 || (tab == 2 && data.userOnline)){
            row=
            <Swipeout
            right={swipeoutNoti}
            autoClose={true}
            rowID={i}
            backgroundColor={data.is_read === 'Y' ? "#FFF" : '#DCDCDC'}
            close={this.state.activeRowNoti !== i}
            onOpen={(secId, rowId, direction) => {
              if (typeof direction !== "undefined" && this.state) {
                this.setState({ activeRowNoti: rowId });                  
              }
            }}
            key={`swipeListNoti_${i}`} >
            <TouchableOpacity key={`chat_${tab}_${i}`}
            style={StyleSheet.flatten(styles.chatview)}
            activeOpacity={0.7}
            onPress={() => {this.goto('Singlechat',data)}}>
            <View style={StyleSheet.flatten(styles.leftview)}>
                <Image source={{uri:data.avatar}} style={StyleSheet.flatten(styles.img)}></Image>
                {data.userOnline ? 
                <View style={{position:'absolute',bottom:10,right:14,backgroundColor:"#34C24C",height:10,width:10,borderRadius:5}}></View> : null}
            </View>
            <View style={StyleSheet.flatten(styles.centerview)}>
                <View style={StyleSheet.flatten(styles.chatname_date)}>
                    <Text style={StyleSheet.flatten(styles.namefont)}>{data.fullname}</Text>
                    <Text style={styles.dateTXT}>{moment(data.createdAt).fromNow()}</Text>
                </View>
                <Text style={StyleSheet.flatten(styles.chatfont)} numberOfLines={1}>{data.adName}</Text>
                {data.typing ? <Text style={StyleSheet.flatten(styles.chatfont)} numberOfLines={1}>{'écrit...'}</Text>:
                <Text style={StyleSheet.flatten(styles.chatfont)} numberOfLines={1}>{data.message}</Text>}
                {data.msg_count > 0 ? 
                <View style={{position:'absolute',right:20,top:25,backgroundColor:"#FF7E00",height:20,width:20,borderRadius:10,alignItems:'center',justifyContent:'center'}}><Text style={{color:"#FFF"}}>{data.msg_count}</Text></View> : null}
            </View>
        </TouchableOpacity></Swipeout>;
        }
        return row;
    }

    render() {
        const {conv}=this.props.chat;
        const {tabs}=this.state;
        let noChat=<View style={{alignItems:'center',justifyContent:'center',flex:1}}><Text style={{fontSize:24,textAlign:'center'}}>{tabs !=2 ? 'Aucun message récent n\'a été trouvé':'Aucun utilisateur en ligne n\'a été trouvé'}</Text></View>;
        let dataD=[];
        if(typeof conv != 'undefined' && conv.length > 0 ){
            conv.map((data,i)=>{
                let ele = this._renderChat(data,i,tabs);
                if(ele != null){ dataD.push(ele) }
            });
        }
        let ChatData=dataD.length >  0 ? (
            <ScrollView showsVerticalScrollIndicator={false} bounces={false} style={styles.mainScollview}>
            <View style={styles.scrollInner}>
            {dataD}
            </View>
            </ScrollView>) : (<View style={styles.scrollInner}>{noChat}</View>);
        return (
            <View style={[styles.main1,{padding:7,paddingTop:15}]}>
                <Tabs defaultTabIndex={0} tabList={['Toutes les conversations','En ligne']}  onTabPress={(i)=>{this.tabchange(i+1)}}/>
                {ChatData}
            </View>
        );
    }
}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);

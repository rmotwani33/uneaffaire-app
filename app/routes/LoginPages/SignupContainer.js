import React, {Component} from 'react';
import {
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Platform,
  Keyboard,
  KeyboardAvoidingView
} from 'react-native';

import {colors} from '../../config/styles';
//import styles from './styles';
import styles from '../../config/genStyle';
import {settings} from '../../config/settings';

import CheckBox from '../../lib/react-native-check-box';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import DropdownAlert from 'react-native-dropdownalert';
import DeviceInfo from 'react-native-device-info';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import messages from '../../config/messages';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {iconsMap, iconsLoaded} from '../../config/icons';
import Tabs from '../../components/Tabs/Tabs';
import CKeyboardSpacer from '../../components/CKeyboardSpacer';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
// redux specific
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api = new ApiHelper;
//const Devmode=true;

class SignupContainer extends Component {

  static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    navBarTextFontFamily: colors.navBarTextFontFamily,
    drawUnderNavBar: false,
    drawUnderTabBar: true
  };

  constructor(props) {
    super(props);
    //console.log(this.props);
    this.refsT={};
    this.state = {
      tabs: 1,
      tabSelected: 1,
      noofcompany: '',
      cname: '',
      uname: '',
      password: '',
      repass: '',
      email: '',
      terms: true,
      load: false
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount() {
    iconsLoaded.then(() => {});
  }

  onNavigatorEvent(event) {
    //console.log(event)
    if (event.id == 'willAppear') {
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({to: 'shown', animated: false});
      this.props.navigator.setStyle({navBarTitleTextCentered: true, drawUnderNavBar: false});
      this.props.navigator.setTitle({title: "S’inscrire"});
      this.props.navigator.toggleTabs({animated: false, to: 'hidden'});
      this.props.navigator.setButtons({
          leftButtons: [
            {
              icon: iconsMap['back'],
              id: 'back2',
              title: 'back to welcome'
            }
          ],
          animated: false
        });
    }
    if (event.id == 'didAppear') {}

    if (event.id == 'back2') {
      this.props.navigator.pop({
          animated: true // does the pop have transition animation or does it happen immediately (optional)
        });
    }
  }

  getDeviceInfo() {
    let data = {
      uuid: DeviceInfo.getUniqueID(),
      manufacturer: DeviceInfo.getManufacturer(),
      brand: DeviceInfo.getBrand(),
      model: DeviceInfo.getModel(),
      id: DeviceInfo.getDeviceId(),
      SystemName: DeviceInfo.getSystemName(),
      SystemVersion: DeviceInfo.getSystemVersion(),
      tablet: DeviceInfo.isTablet(),
      Locale: DeviceInfo.getDeviceLocale(),
      Country: DeviceInfo.getDeviceCountry(),
      Timezone: DeviceInfo.getTimezone()
    }
    return data;
  }

  displayNoInternet() {
    this.props.authActions.setNetDial(true);
    this.props.navigator.showLightBox({
        screen: "netOnOffModal", // unique ID registered with Navigation.registerScreen
        passProps: {
          dismiss: this.dismiss.bind(this)
        }, // simple serializable object that will pass as props to the lightbox (optional)
        style: {
          backgroundBlur: "dark", // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
          backgroundColor: "#0008" // tint color for the background, you can specify alpha here (optional)
        },
        adjustSoftInput: "resize", // android only, adjust soft input,
        modes: 'nothing',
        // 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
      });
  }
  dismiss() {
    api.checkNet().then(() => {
        this.props.authActions.setNet(false);
        this.props.authActions.setNetDial(false);
        this.props.navigator.dismissLightBox();
      })
      .catch((err) => {
        //console.log('Error while dismissing modal'); console.log(err);
        this.props.authActions.setNet(true);
        this.props.authActions.setNetDial(false);
        // this.displayNoInternet();
      });
  }

  signUp() {
    //console.log(Platform)
    api.checkNet().then(() => {
        let params = {};
        if (this.state.tabSelected == 1) {
          params = {
            username: this.state.uname,
            usertype: 'personal',
            email: this.state.email,
            password: this.state.password,
            device: Platform.OS,
            device_data: this.getDeviceInfo()
          }
        } else if (this.state.tabSelected == 2) {
          params = {
            numofcompany: this.state.noofcompany,
            username: this.state.cname,
            usertype: 'professional',
            email: this.state.email,
            password: this.state.password,
            device: Platform.OS,
            device_data: this.getDeviceInfo()
          }
        }
        let request = {
          url: settings.endpoints.signup,
          method: 'POST',
          params: params
        }
        //if(Devmode){ console.log(request); }else{
        this.setState({load: true},()=>{
          api.FetchData(request).then((result) => {
            //console.log(result);
            this.setState({load: false},()=>{
              if (result.status) {
                this
                  .dropdown
                  .alertWithType('success', 'Félicitations!', result.message);
                setTimeout(() => {
                  this.goto('SigninContainer');
                }, 3000);
                
              } else {
                this
                  .dropdown
                  .alertWithType('error', 'Error', result.message);
              }
            });
          })
          .catch((error) => {
            this.setState({load: false});
            //console.log(error);
          });
        });
        
      })
      .catch((err) => {
        this.displayNoInternet();
      });

    // }
  }

  textFeldValidate() {
    let {
      email,
      uname,
      password,
      noofcompany,
      cname,
      repass,
      terms,
      tabSelected
    } = this.state;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    Keyboard.dismiss();
    let valid = true;

    if (tabSelected == 1) {
      if (uname == "") {
        this.dropdown.alertWithType('error', 'Error', 'Entrez Pseudo');
        valid = false;
      }
    } else if (tabSelected == 2) {
      if ((!(parseInt(noofcompany) > 0)) || noofcompany == "") {
        this.dropdown.alertWithType('error', 'Error', 'Entrez le nombre entier');
        valid = false;
      } else if (noofcompany.length < 14) {
        this.dropdown.alertWithType('error', 'Error', 'Number Siret au moins 14 numéros');
        valid = false;
      }
      if (cname == "") {
        this.dropdown.alertWithType('error', 'Error', 'Entrez le nom de l\'entreprise');
        valid = false;
      }
    }
    if (email.length === 0 || password.length === 0) {
      this.dropdown.alertWithType('error', 'Error', messages.noUsername);
      valid = false;
    } else if (reg.test(email) == false) {
      this.dropdown.alertWithType('error', 'Error', messages.validEmail);
      valid = false;
    } else if (password.length < 8) {
      this.dropdown.alertWithType('error', 'Error', messages.password8);
      valid = false;
    } else if (password.length > 16) {
      this.dropdown.alertWithType('error', 'Error', messages.password16);
      valid = false;
    } else if (password !== repass) {
      this.dropdown.alertWithType('error', 'Error', messages.repass);
      valid = false;
    } else if (!terms) {
      this.dropdown.alertWithType('error', 'Error', 'Vous devez accepter les conditions générales de vente.');
      valid = false;
    }

    if (valid) {
      this.signUp();
    }
  }

  goto(page) {
    this.props.navigator.push({screen: page});
  }
  tabchange(tab) {
    this.setState({tabs: tab, tabSelected: tab});
  }

  _setNextFocus(key){
    if(this.refsT !== undefined && this.refsT !== null){
    let nextKey = ''
      if(key == 'uname'){
        nextKey = 'email';
      }else if(key == 'noofcompany'){
        nextKey = 'cname';
      }else if(key == 'cname'){
        nextKey = 'email';
      }else if(key == 'email'){
        nextKey = 'password';
      }else if(key == 'password'){
        nextKey = 'repass';
      }
      if(nextKey !== '' && this.refsT[nextKey] !== undefined && this.refsT[nextKey] !== null){
        inp = this.refsT[nextKey].inputRef();
        if(inp !== undefined && inp !== null){
          inp.focus();
        } 
      }
    }
  }

  renderIos(){
    let signcontent = this.state.tabs == 1
    ? (
      <View style={styles.flex}>
        <FloatLabelTextInput
          forIco={"Ico"}
          icon={'2'}
          style={styles.genInput}
          placeholder={"Pseudo"}
          underlineColorAndroid={'#eee'}
          autoCapitalize="none" 
          autoCorrect={false}
          ref={o=>this.refsT['uname']= o}
          returnKeyType={'next'}
          onSubmitEditing={()=>this._setNextFocus('uname')} 
          value={this.state.uname}
          onChangeTextValue={uname => this.setState({uname: uname})}
          /></View>
    )
    : (
      <View style={styles.proCont}>

        <FloatLabelTextInput
          forIco={"Ico"}
          icon={'T'}
          style={styles.genInput}
          placeholder={"Numéro de siret"}
          keyboardType={'numeric'}
          underlineColorAndroid={'#eee'}
          autoCapitalize="none" 
          autoCorrect={false}
          ref={o=>this.refsT['noofcompany']= o}
          returnKeyType={'next'}
          onSubmitEditing={()=>this._setNextFocus('noofcompany')} 
          value={this.state.noofcompany}
          maxLength={14}
          onChangeTextValue={noofcompany => this.setState({noofcompany: noofcompany})}
          />

        <FloatLabelTextInput
          forIco={"Ico"}
          icon={'B'}
          style={styles.genInput}
          placeholder={"Nom de la société"}
          underlineColorAndroid={'#eee'}
          autoCapitalize="none" 
          autoCorrect={false}
          ref={o=>this.refsT['cname']= o}
          returnKeyType={'next'}
          onSubmitEditing={()=>this._setNextFocus('cname')} 
          value={this.state.cname}
          onChangeTextValue={cname => this.setState({cname: cname})}
          />
      </View>
    );

  return (
    <KeyboardAwareScrollView contentContainerStyle={[styles.main, styles.proMain,styles._HP15_,{flexGrow:1}]}>
      <Tabs defaultTabIndex={0} tabList={['Particulier','Professionnel']}  
                  onTabPress={(i)=>{this.tabchange(i+1)}}/>
      <View style={styles.flex}>

        <View
          style={{
          flex: 2,
          alignSelf: 'stretch'
        }}>
          {signcontent}
        
          <FloatLabelTextInput
            forIco={"Ico"}
            icon={'9'}
            style={styles.genInput}
            placeholder={"Entrer votre adresse email"}
            underlineColorAndroid={'#eee'}
            autoCapitalize="none" 
            autoCorrect={false}
            ref={o=>this.refsT['email']= o}
            returnKeyType={'next'}
            onSubmitEditing={()=>this._setNextFocus('email')} 
            value={this.state.email}
            onChangeTextValue={email => this.setState({email: email})}
            />

          <FloatLabelTextInput
            forIco={"Ico"}
            icon={'K'}
            style={styles.genInput}
            placeholder={"Mot de passe"}
            underlineColorAndroid={'#eee'}
            secureTextEntry={true}
            autoCapitalize="none" 
            autoCorrect={false}
            ref={o=>this.refsT['password']= o}
            returnKeyType={'next'}
            onSubmitEditing={()=>this._setNextFocus('password')} 
            value={this.state.password}
            onChangeTextValue={(password) => this.setState({password: password})}
            />

          <FloatLabelTextInput
            forIco={"Ico"}
            icon={'K'}
            style={styles.genInput}
            placeholder={"Confirmer le mot de passe"}
            underlineColorAndroid={'#eee'}
            secureTextEntry={true}
            autoCapitalize="none" 
            autoCorrect={false}
            ref={o=>this.refsT['repass']= o}
            returnKeyType={'go'}
            onSubmitEditing={()=>this.textFeldValidate()} 
            value={this.state.repass}
            onChangeTextValue={(repass) => this.setState({repass: repass})}
            />

          <View>

            <CheckBox
              style={[styles.genChk, styles._MT10_]}
              onClick={(checked) => this.setState({terms: checked})}
              isChecked={this.state.terms}
              rightText={"J’accepte les conditions générales de ventes."}/>
          </View>
        </View>
        <View style={[styles.btnBlock, styles.spaceBitw]}>

          <LoaderButton
            load={this.state.load}
            onPress={() => {
            this.textFeldValidate()
          }}
            BtnStyle={[styles.brandColor, styles.signupButton]}
            text={'S’inscrire'}
            textStyle={styles.genButton}/>
        </View>
        <View>
          <Text style={styles.signupTxt}>Vous avez déjà un compte?
            <Text
              style={styles.signTxt}
              onPress={() => {
              this.goto('SigninContainer')
            }}>Se connecter</Text>
            Ici</Text>
        </View>
      </View>
      <DropdownAlert ref={(ref) => this.dropdown = ref} onClose={(data) => {}}/>
    </KeyboardAwareScrollView>
  );
  }

  renderAndroid(){
    let signcontent = this.state.tabs == 1
    ? (
      <View style={styles.flex}>
        <FloatLabelTextInput
          forIco={"Ico"}
          icon={'2'}
          style={styles.genInput}
          placeholder={"Pseudo"}
          underlineColorAndroid={'#eee'}
          autoCapitalize="none" 
          autoCorrect={false}
          ref={o=>this.refsT['uname']= o}
          returnKeyType={'next'}
          onSubmitEditing={()=>this._setNextFocus('uname')} 
          value={this.state.uname}
          onChangeTextValue={uname => this.setState({uname: uname})}
          /></View>
    )
    : (
      <View style={styles.proCont}>

        <FloatLabelTextInput
          forIco={"Ico"}
          icon={'T'}
          style={styles.genInput}
          placeholder={"Numéro de siret"}
          keyboardType={'numeric'}
          underlineColorAndroid={'#eee'}
          autoCapitalize="none" 
          autoCorrect={false}
          ref={o=>this.refsT['noofcompany']= o}
          returnKeyType={'next'}
          onSubmitEditing={()=>this._setNextFocus('noofcompany')} 
          value={this.state.noofcompany}
          maxLength={14}
          onChangeTextValue={noofcompany => this.setState({noofcompany: noofcompany})}
          />

        <FloatLabelTextInput
          forIco={"Ico"}
          icon={'B'}
          style={styles.genInput}
          placeholder={"Nom de la société"}
          underlineColorAndroid={'#eee'}
          autoCapitalize="none" 
          autoCorrect={false}
          ref={o=>this.refsT['cname']= o}
          returnKeyType={'next'}
          onSubmitEditing={()=>this._setNextFocus('cname')} 
          value={this.state.cname}
          onChangeTextValue={cname => this.setState({cname: cname})}
          />
      </View>
    );

  return (
    <View style={[styles.main, styles.proMain,styles._HP15_]}>
      <Tabs defaultTabIndex={0} tabList={['Particulier','Professionnel']}  
                  onTabPress={(i)=>{this.tabchange(i+1)}}/>
      <View style={styles.flex}>

        <View
          style={{
          flex: 2,
          alignSelf: 'stretch'
        }}>
          {signcontent}
        
          <FloatLabelTextInput
            forIco={"Ico"}
            icon={'9'}
            style={styles.genInput}
            placeholder={"Entrer votre adresse email"}
            underlineColorAndroid={'#eee'}
            autoCapitalize="none" 
            autoCorrect={false}
            ref={o=>this.refsT['email']= o}
            returnKeyType={'next'}
            onSubmitEditing={()=>this._setNextFocus('email')} 
            value={this.state.email}
            onChangeTextValue={email => this.setState({email: email})}
            />

          <FloatLabelTextInput
            forIco={"Ico"}
            icon={'K'}
            style={styles.genInput}
            placeholder={"Mot de passe"}
            underlineColorAndroid={'#eee'}
            secureTextEntry={true}
            autoCapitalize="none" 
            autoCorrect={false}
            ref={o=>this.refsT['password']= o}
            returnKeyType={'next'}
            onSubmitEditing={()=>this._setNextFocus('password')} 
            value={this.state.password}
            onChangeTextValue={(password) => this.setState({password: password})}
            />

          <FloatLabelTextInput
            forIco={"Ico"}
            icon={'K'}
            style={styles.genInput}
            placeholder={"Confirmer le mot de passe"}
            underlineColorAndroid={'#eee'}
            secureTextEntry={true}
            autoCapitalize="none" 
            autoCorrect={false}
            ref={o=>this.refsT['repass']= o}
            returnKeyType={'go'}
            onSubmitEditing={()=>this.textFeldValidate()} 
            value={this.state.repass}
            onChangeTextValue={(repass) => this.setState({repass: repass})}
            />

          <View>

            <CheckBox
              style={[styles.genChk, styles._MT10_]}
              onClick={(checked) => this.setState({terms: checked})}
              isChecked={this.state.terms}
              rightText={"J’accepte les conditions générales de ventes."}/>
          </View>
        </View>
        <View style={[styles.btnBlock, styles.spaceBitw]}>

          <LoaderButton
            load={this.state.load}
            onPress={() => {
            this.textFeldValidate()
          }}
            BtnStyle={[styles.brandColor, styles.signupButton]}
            text={'S’inscrire'}
            textStyle={styles.genButton}/>
        </View>
        <View>
          <Text style={styles.signupTxt}>Vous avez déjà un compte?
            <Text
              style={styles.signTxt}
              onPress={() => {
              this.goto('SigninContainer')
            }}>Se connecter</Text>
            Ici</Text>
        </View>
      </View>
      <DropdownAlert ref={(ref) => this.dropdown = ref} onClose={(data) => {}}/>
    </View>
  );
  }


  render() {
      return (
        Platform.OS === 'ios' ? this.renderIos() : this.renderAndroid()
      )
  }

}

function mapStateToProps(state, ownProps) {
  return {chat: state.chat, auth: state.auth};
}

function mapDispatchToProps(dispatch) {
  return {
    chatActions: bindActionCreators(chatActions, dispatch),
    authActions: bindActionCreators(authActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupContainer);

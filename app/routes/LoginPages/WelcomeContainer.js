import React, {Component} from 'react';
import {StyleSheet,Image,Text,View,TextInput,TouchableOpacity,NativeModules,Platform,AsyncStorage,Modal,Keyboard} from 'react-native';
import {colors} from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import {LoginManager, GraphRequest, GraphRequestManager,AccessToken} from 'react-native-fbsdk';
import {GoogleSignin} from 'react-native-google-signin';
const { RNTwitterSignIn } = NativeModules;
import oauthconfig from '../../config/oauthconfig';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import {Navigation, Screen} from 'react-native-navigation';
import DeviceInfo from 'react-native-device-info';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {settings} from '../../config/settings';
import { Icon } from '../../config/icons';
import startMain from '../../config/app-main';
import ModalDropdown from '../../lib/react-native-modal-dropdown';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';

GoogleSignin.configure({
  scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  iosClientId: oauthconfig.google.iosClientId, // only for iOS
  webClientId: oauthconfig.google.webClientId, // client ID of type WEB for your server (needed to verify user ID and offline access)
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: '', // specifies a hosted domain restriction
  loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
  accountName: '', // [Android] specifies an account name on the device that should be used
 // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});
const api= new ApiHelper;
class WelcomeContainer extends Component {
  static navigatorStyle = Platform.OS === 'ios' ? {
    // navBarHidden: true,
    // tabBarHidden: true,
    disableOpenGesture: true,
    drawUnderTabBar: true
  }:{
    navBarHidden: true,
    tabBarHidden: true,
    disableOpenGesture: true,
    drawUnderTabBar: true
  };

  constructor(props) {
    super(props);
    this.regionOption = {'Perticulaire':'Perticulaire','Professionnel':'Professionnel'};
    this.state ={
      modalVisible: false,
      socialUser:{},
      prevParams:{},
      usertype:'personal',
      load1:false,
      email:'',
      userInfo:{}
    }
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  sendEventToDrawer(){
    this.props.navigator.handleDeepLink({link: 'Drawer',payload:{
        screen:this.props.testID
    }});
  }

  onNavigatorEvent(event) {
    //console.log(event.id);
    if(event.id == 'willAppear'){
      this.sendEventToDrawer();
      this.props.chatActions.setScreen(this.props.testID);
      this.props.authActions.setUserData(this.props.token,this.props.userdata);
      //console.log(this.props);
      this.props.navigator.toggleTabs({animated: false, to: 'hidden'});
      this.props.navigator.toggleNavBar({to: 'hidden', animated: false});
    }
    //     this.props.navigator.setDrawerEnabled({         side: 'left',
    // enabled: false });
  }

  goto(page) {
    this.props.navigator.push({screen: page});
  }

  SocialLogin(userdata) {
    userdata.device=Platform.OS;
    userdata.device_data=this.getDeviceInfo();
    let request={
      url:settings.endpoints.social,
      method:'POST',
      params:userdata
    }
    api.FetchData(request).then((result)=>{
      if(result.status){
        if(result.first_login){
          this.setState({modalVisible:true,socialUser:result.user_data,prevParams:request.params});
        }else{
        AsyncStorage.multiSet([['token',result.user_data.token],['userdata',JSON.stringify(result.user_data)]]).then((r)=>{
          console.log(r);
          startMain(result.user_data.token,result.user_data);
        }).catch((e)=>{
          console.log(e);
        });
          //this.props.api.setToken(result.user_data.token,result.user_data);
          //console.log(result.user_data);
          
        }
      }else{
        alert(result.message);
      }
    }).catch((error)=>{
      alert('Quelque chose a mal tourné');
      //console.log(error);
    });
  }

  getDeviceInfo(){
    let data={
      uuid:DeviceInfo.getUniqueID(),
      manufacturer:DeviceInfo.getManufacturer(),
      brand:DeviceInfo.getBrand(),
      model:DeviceInfo.getModel(),
      id:DeviceInfo.getDeviceId(),
      SystemName:DeviceInfo.getSystemName(),
      SystemVersion:DeviceInfo.getSystemVersion(),
      tablet:DeviceInfo.isTablet(),
      Locale:DeviceInfo.getDeviceLocale(),
      Country:DeviceInfo.getDeviceCountry(),
      Timezone:DeviceInfo.getTimezone(),
    }
    return data;
  }

  FacebookLogin() {
    let _this=this;
    LoginManager.logInWithReadPermissions(['public_profile', 'email'])
      .then(function (result) {
        if (result.isCancelled) {
          //tthis.LC.setState({loading: false, facebookLoading: false});
        } else {
          let infoRequest = new GraphRequest('/me?fields=id,name,first_name,last_name,email,picture.height(2000)', null, 
          function (error, result) {
            AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    api.xConsole(data);
                  }
                )
            if (error) {
             api.xConsole(error);
            } else {
              let userdata = {
                provider: 'Facebook',
                uid: result.id,
                uname: result.name,
                email: typeof result.email != 'undefined' ? result.email : '',
                firstname: result.first_name,
                lastname: result.last_name,
                profile: result.picture.data.url
              }
              _this.SocialLogin(userdata);
            }
          },);
          new GraphRequestManager().addRequest(infoRequest).start();
        }
      }, function (error) {
        api.xConsole(error)
      });
  }

  GoogleLogin = async () => {
    let _this = this;
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo);
      this.setState({ userInfo },()=>{
        let userdata = {
          provider: 'Google',
          uid: userInfo.user.id,
          uname: userInfo.user.familyName,
          email: typeof userInfo.user.email != "undefined" ? userInfo.user.email : '',
          firstname: typeof userInfo.user.givenName != 'undefined' ? userInfo.user.givenName : userInfo.user.name,
          lastname: typeof userInfo.user.familyName != 'undefined' ? userInfo.user.familyName : '',
          profile: userInfo.user.photo != null ? userInfo.user.photo : 'avatar.png'
        }
        api.xConsole(userInfo);
        _this.SocialLogin(userdata);
      });
    } catch (error) {
      console.log(error);
      api.xConsole(error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  // GoogleLogin() {
  //   let _this = this;
  //   console.log(GoogleSignin);
  //   GoogleSignin.configure({
  //     // scopes: ["https://www.googleapis.com/auth/drive.readonly"], // what API you
  //     // want to access on behalf of the user, default is email and profile
  //     iosClientId: oauthconfig.google.iosClientId, // only for iOS
  //     webClientId: oauthconfig.google.webClientId, // client ID of type WEB for your server (needed to verify user ID and offline access)
  //     offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  //     forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login
  //     accountName: '', // [Android] specifies an account name on the device that should be used
  //     hostedDomain: '' // [Android] specifies a hosted domain restriction
  //   }).then(() => {
  //       GoogleSignin.signIn().then((user) => {
  //           let userdata = {
  //             provider: 'Google',
  //             uid: user.id,
  //             uname: user.name,
  //             email: typeof user.email != "undefined" ? user.email : '',
  //             firstname: typeof user.givenName != 'undefined' ? user.givenName : user.name,
  //             lastname: typeof user.familyName != 'undefined' ? user.familyName : '',
  //             profile: user.photo != null ? user.photo : 'avatar.png'
  //           }
  //           api.xConsole(user);
  //           _this.SocialLogin(userdata);
  //         }).catch((err) => {
  //           api.xConsole(err);
  //         }).done();
  //     });
  // }

  TwitterLogin() {
    let _this = this;
    console.log(NativeModules);
    console.log(RNTwitterSignIn);
    if (Platform.OS === 'ios') {
      RNTwitterSignIn.init(oauthconfig.twitter.consumer_key, oauthconfig.twitter.consumer_secret);
      RNTwitterSignIn.logIn()
      .then((loginData) => {
        console.log(loginData)
        const { authToken, authTokenSecret } = loginData
        if (authToken && authTokenSecret) {
          let userdata = {
            provider: 'Twitter',
            uid: loginData.id,
            uname: loginData.name,
            email: loginData.email == "COULD_NOT_FETCH" ? '' : loginData.email,
            firstname: '',
            lastname: '',
            profile: loginData.profile_image_url_https
          }
          api.xConsole(loginData);
          _this.SocialLogin(userdata);
        }
      })
      .catch((error) => {
        console.log(error);
        api.xConsole(error);
      })
      // RNTwitterSignIn.logIn(oauthconfig.twitter.consumer_key, oauthconfig.twitter.consumer_secret, (error, loginData) => {
      //   if (!error) {
      //     let userdata = {
      //       provider: 'Twitter',
      //       uid: loginData.id,
      //       uname: loginData.name,
      //       email: loginData.email == "COULD_NOT_FETCH" ? '' : loginData.email,
      //       firstname: '',
      //       lastname: '',
      //       profile: loginData.profile_image_url_https
      //     }
      //     api.xConsole(loginData);
      //     _this.SocialLogin(userdata);
      //   } else {
      //     api.xConsole(error);
      //   }
      // });
    } else {
      RNTwitterSignIn.init(oauthconfig.twitter.consumer_key, oauthconfig.twitter.consumer_secret).then(() => {
          return RNTwitterSignIn.logIn();
        }).then((loginData) => {
          let userdata = {
            provider: 'Twitter',
            uid: loginData.userID,
            uname: loginData.name,
            email: loginData.email == "COULD_NOT_FETCH" ? '' : loginData.email,
            firstname: '',
            lastname: '',
            profile: 'https://www.1plusx.com/app/mu-plugins/all-in-one-seo-pack-pro/images/default-user-image.png'
          }
          _this.SocialLogin(userdata);
        }).catch((error) => {
          api.xConsole(error);
        });
    }
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  skip(){
    let _this=this;
    AsyncStorage.multiSet([['token','Guest'],['userdata',JSON.stringify({})]]).then((r)=>{
      console.log(r);
      startMain('Guest',{});
    }).catch((e)=>{
      console.log(e);
    });
    //_this.goto('HomeContainer');
    
    // AsyncStorage.setItem('token', 'Guest', () => {
    //   //_this.goto('HomeContainer');
    //   startMain('Guest',{});
    // });
  }
  getRegions(){
    let ret = [];
    Object.keys(this.regionOption).map((key)=>{
        ret.push(key);
    });
    return ret;
  }
  selectRegion(rr){
    console.log(rr);
  }

  validateModal(){
    let {email,usertype,socialUser}=this.state;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    Keyboard.dismiss(); 
    let valid = true;

    if(socialUser.email == "" && (reg.test(email) == false || email == "")){
      //this.dropdown.alertWithType('error', 'Error', messages.validEmail);
      alert('Enter valid email ')
      valid = false;
    }else if(usertype != "personal" && usertype !="professional"){
      alert('Enter valid user type ')
      valid = false;
    }

    if(valid){
      this.sendUserType();
    }
  }

  afterLogin(result){
    let socialUser = this.state.socialUser;
    if(socialUser.email == ""){
      socialUser = result.user_data;
    }else{
      socialUser.usertype = this.state.usertype;
    }
    AsyncStorage.multiSet([['token',socialUser.token],['userdata',JSON.stringify(socialUser)]]).then((r)=>{
      console.log(r);
      startMain(socialUser.token,socialUser);
    }).catch((e)=>{
      console.log(e);
    });
  }

  sendUserType(){

    // endpoint : app/updateUserType
    // param : token,usertype
    // usertype : personal/professional
    let params={
      token:this.state.socialUser.token,
      usertype:this.state.usertype,
    };
    if(this.state.socialUser.email == ""){
      params.email=this.state.email;
      params.prev_data = this.state.prevParams;
    }
    let request={
      url:settings.endpoints.updateUserType,
      method:'POST',
      params:params
    }
    this.setState({load1:true},()=>{
      api.FetchData(request).then((result)=>{
        if(result.status){
          //this.setState({modalVisible:false,load1:false});
          if(Platform.OS === 'ios'){
            this.afterLogin(result);
          }else{
            this.setState({modalVisible:false,load1:false},()=>{
              this.afterLogin(result);
            });
          }
        }else{
          this.setState({load1:false},()=>{
            alert(result.message);
          });
        }
      }).catch((error)=>{
        this.setState({load1:false},()=>{
          alert('Quelque chose a mal tourné');
        });
        //console.log(error);
      });
    });
    
  }

  render() {
    let showEmail = this.state.socialUser.email == "";
    //let showEmail = true;
    let popup =  <Modal style={styles.modalApear} animationType={"slide"} transparent={true} visible={this.state.modalVisible} onRequestClose={() => {alert("Modal has been closed.")}}>
    <View style={[styles.modalInner,styles.justCenter]}>
        <View style={styles.modalMain}>
            {/* <Text style={[styles.globalIcon,styles.modalClose]} onPress={() => {this.setModalVisible(false)}}>{';'}</Text> */}
            <View style={[styles.justCenter]}>
            {showEmail ? (<Text style={styles.subTitle}>Entrez vos coordonnées</Text>):(<Text style={styles.subTitle}>Vous êtes</Text>)}
            {showEmail ? (
              <View style={{height:50,width:200}}>
                
                <FloatLabelTextInput style={{fontSize:12}}
                    value={this.state.email}
                    onChangeTextValue={(value) => {
                      this.setState({email:value});
                    }}
                    placeholder={"E-mail"}
                    underlineColorAndroid={'#eee'}/>
                </View>):(null)}
                {showEmail ? (<View style={styles.justCenter}>
                  <Text style={styles.subTitle}>Vous êtes</Text>
              </View>):(null)}
              <View style={[styles.justCenter,{flexDirection:'row'}]}>
                
                <TouchableOpacity style={[styles.modalButton,this.state.usertype == "personal" ? styles.modalButtonSelected : null]}
                onPress={()=>{this.setState({usertype:"personal"})}}>
                  <Text style={this.state.usertype == "personal" ? styles.modalButtonSelectedText : styles.modalButtonText }>Particulier</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.modalButton,this.state.usertype == "professional" ? styles.modalButtonSelected : null]}
                onPress={()=>{this.setState({usertype:"professional"})}}>
                <Text style={this.state.usertype == "professional" ? styles.modalButtonSelectedText : styles.modalButtonText }>Professionnel</Text>
                </TouchableOpacity>
                </View>
                 {/* <View>
                  <Text style={styles.subTitle}>Sélectionner le genre</Text>
                  </View>
                  <View style={[styles.relative,styles._w220_,styles._MT5_,{padding:5}]}>
                    <View>
                  <ModalDropdown  defaultValue={'Perticulaire'} options={this.getRegions()}  textStyle={[styles.dropdown_2_text,styles._F16_,styles.grayColor]} dropdownStyle={{
                                            //bottom:0,
                                            width:200,
                                            top:-50,
                                            height:80,
                                        }} onSelect={(region)=>this.selectRegion(region)}/>
                   <Text style={[styles.globalIcon,styles.dropabsIco,{padding:5}]}>g</Text> 
                  </View>
            </View>  */}
                <View style={[styles._MT15_,{width:220}]}>
                    <LoaderButton load={this.state.load1} onPress={()=>this.validateModal()} BtnStyle={[styles.brandColor]} 
                    text={'Sauver'} textStyle={[styles.genButton2,styles.smallBtn]} />
                </View>
            </View>
        </View>
    </View>
</Modal>
    return (
      <View style={[styles.main, styles._HP15_]}>
        <View style={[styles.logoCont, styles.FLONEHALF]}>
          <Image source={images.companylogomain} style={styles.logo}></Image>
        </View>
        <View style={styles.btnBlock}>
          <TouchableOpacity
            style={styles.outlineBtn}
            activeOpacity={0.8}
            onPress={() => {this.goto('SigninContainer')}}>
            <Text style={[styles.genButton, styles.orangeColor]}>Se connecter</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.outlineBtn, styles._MT10_]}
            activeOpacity={0.8}
            onPress={() => {this.goto('SignupContainer')}}>
            <Text style={[styles.genButton, styles.orangeColor]}>S’inscrire</Text>
          </TouchableOpacity>
          <Text style={styles.signsocialTxt}>{'  --------- OU Connectez-vous avec ---------  '}</Text>
        </View>
        <View style={styles.bottomView}>
          <TouchableOpacity style={[styles.SocialBtn,styles.faceBookBtn]} onPress={() => this.FacebookLogin()}>
            {/* <Text style={styles.Social}>c</Text> */}
            <Icon name="facebook-logo" color={"#fff"} size={25}/>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.SocialBtn,styles.googleBtn]} onPress={() => this.GoogleLogin()}>
            {/* <Text style={styles.Social}>b</Text> */}
            <Icon name="google-glass-logo" color={"#fff"} size={25}/>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.SocialBtn,styles.twitterBtn]} onPress={() => this.TwitterLogin()}>
            {/* <Text style={styles.Social}>a</Text> */}
            <Icon name="twitter" color={"#fff"} size={25}/>
          </TouchableOpacity>
        </View>
        {popup}
        <View>
          <TouchableOpacity style={{paddingTop: 5}} onPress={() => {this.skip()}} activeOpacity={0.8}>
            <Text style={[styles.skipText]}>Ignorer</Text>
          </TouchableOpacity>
          {/* <View>
            <Text style={[styles.signupTxt,{fontSize:11}]}>{'Version '+settings.version[Platform.OS]}</Text>
          </View> */}
        </View>
      </View>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeContainer);

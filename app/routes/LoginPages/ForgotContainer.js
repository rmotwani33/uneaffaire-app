import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity
} from 'react-native';

import { colors } from '../../config/styles';
import images from '../../config/images';
import styles from '../../config/genStyle';
import ApiHelper from '../../lib/ApiHelper/ApiHelper';
import {settings} from '../../config/settings';
import {Navigation,Screen} from 'react-native-navigation';
import DropdownAlert from 'react-native-dropdownalert';
import LoaderButton from '../../components/LoaderButton/LoaderButton';
import { iconsMap, iconsLoaded } from '../../config/icons';
// redux specific
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as chatActions from '../../redux/reducers/chat';
import * as authActions from '../../redux/reducers/auth';
const api= new ApiHelper;

class ForgotContainer extends Component {
  // static navigatorButtons = {
  //    leftButtons: [{
  //      icon: require('../../images/back.png'),
  //      id: 'back',
  //      title:'Back To Login',
  //    }],
  // };
  static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
    tabBarHidden:true
};

  constructor(props) {
    super(props);
    this.state={
        check1:false,
        email:"",
        emailCondition:true,
        errorTxt:'',
        emailRespCondition:false,
        load:false
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  componentWillMount(){
    iconsLoaded.then(()=>{});
}
dismiss(){   
  api.checkNet().then(()=>{
      this.props.authActions.setNet(false);
      this.props.authActions.setNetDial(false);
      this.props.navigator.dismissLightBox();
  }).catch((err)=>{
      //console.log('Error while dismissing modal');
      //console.log(err);
      this.props.authActions.setNet(true);
      this.props.authActions.setNetDial(false);
      // this.displayNoInternet();       
  });
}

displayNoInternet(){
  this.props.authActions.setNetDial(true);
  //console.log('Show lightbox called');
  //console.log(this.props.navigator);
  this.props.navigator.showLightBox({
      screen: "netOnOffModal", // unique ID registered with Navigation.registerScreen
      passProps: {
          dismiss:this.dismiss.bind(this)
      }, // simple serializable object that will pass as props to the lightbox (optional)
      style: {
        backgroundBlur: "dark", // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
        backgroundColor: "#0008" // tint color for the background, you can specify alpha here (optional)
      },
      //adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
     });
}


  onNavigatorEvent(event) {
    //console.log(event)
    if(event.id=='willAppear'){
      api.setAction(this.props.chatActions,this.props.authActions);
      this.props.chatActions.setScreen(this.props.testID);
      //console.log(this.props);
      this.props.navigator.toggleNavBar({
        to: 'shown',
        animated: true
      });
      this.props.navigator.setStyle({navBarTitleTextCentered: true});
      this.props.navigator.setTitle({title: "MOT DE PASSE OUBLIÉ"});
      this.props.navigator.setButtons({
          leftButtons: [{
            icon: iconsMap['back'],
            id: 'back2',
            title:'Back To Login',
          }],
          animated:false
      });
    }
   
    if(event.id == 'back2'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }

  }
  goto(page){
    this.props.navigator.push({
      screen: page
    });

  }
  

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  };
  resetPassword=()=>{
    api.checkNet().then(()=>{
      this.props.authActions.setNetDial(false);
        this.setState({load:true});
        const {email} = this.state.email;
        if(this.state.email == ''){
            this.setState({errorTxt:'Entrez votre email'});
            this.dropdown.alertWithType('error', 'Error', 'Entrez votre email');
            this.setState({load:false});
        }else if (!this.validateEmail(this.state.email) ) {
            this.dropdown.alertWithType('error', 'Error', 'Entrez un email valide');          
            this.setState({emailCondition :true,errorTxt:'Entrez un email valide'});
            this.setState({load:false});
        }else {
            //console.log(settings.endpoints.forgot);
            let request={
              url:settings.endpoints.forgot,
              method:'POST',
              params:{email:this.state.email}
            }
            api.FetchData(request).then((result)=>{
               this.setState({load:false,email:''});
              if(result.status){
                 this.dropdown.alertWithType('success', 'Success', result.message);
              }else{
                this.dropdown.alertWithType('error', 'Error', result.message);
              }
            }).catch((error)=>{
              //console.log(error);
            });
            this.setState({emailCondition :false});
        }
      }).catch((err)=>{
        this.displayNoInternet();
    });
  }
  render() {
      
     // Screen.togg(this, {})
    return (
          <View style={[styles.main,styles._HP15_,styles._PT15_]}>
                <View style={styles.forgotMain}>
                    <Text style={styles.forgotEmailtxt}>Entrez votre adresse e-mail, nous vous enverrons un lien pour réinitialiser votre mot de passe.</Text>
                    <TextInput style={[styles.genInput,styles._PL5_]} 
                    placeholder="Email" 
                    value={this.state.email}
                    underlineColorAndroid={!this.validateEmail(this.state.resetPassword == false) ?'#f00':'#eee'} 
                    onChangeText={(email) => this.setState({email:email})} />
                </View>    
                <View style={styles.btnRow}>
                    {/* <TouchableOpacity onPress={this.resetPassword} style={[styles.brandColor,styles._MT20_]} activeOpacity={0.8}>
                        <Text style={styles.genButton}>Envoyer un courriel</Text>
                    </TouchableOpacity> */}
                    <LoaderButton load={this.state.load} onPress={this.resetPassword} BtnStyle={[styles.brandColor,styles._MT20_]} 
                        text={'S\'identifier'} textStyle={styles.genButton} />
                </View>
                <DropdownAlert ref={(ref) => this.dropdown = ref} onClose={(data) => {}} />
          </View>
    );
  }

}

function mapStateToProps(state, ownProps) {
	return {
        chat:state.chat,
        auth:state.auth,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		chatActions: bindActionCreators(chatActions, dispatch),
		authActions: bindActionCreators(authActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotContainer);


import React, { Component } from 'react';
import {
  StyleSheet,
  Image,Text,View,TextInput,TouchableOpacity,ScrollView,Switch
} from 'react-native';

import images from '../../config/images';
import styles from '../../config/genStyle';
import { colors } from '../../config/styles';
import RadioButton from '../../lib/react-radio/index';
import {Navigation,Screen} from 'react-native-navigation';
import CheckBox from 'react-native-check-box';
import FloatLabelTextInput from '../../lib/react-native-floating-label-text-input';
import Dimensions from 'Dimensions';
const { height, width } = Dimensions.get('window'); 
import {Column as Col, Row} from 'react-native-flexbox-grid';
import ModalDropdown from '../../lib/react-native-modal-dropdown';
export default class favoris extends Component {
//   static navigatorButtons = {
//      leftButtons: [{
//        icon: require('../../images/back.png'),
//        id: 'back',
//        title:'back to welcome',
//      }],
//   };
/*static navigatorStyle = {
    navBarBackgroundColor: colors.navBarBackgroundColor,
    navBarTextColor: colors.navBarTextColor,
    navBarSubtitleTextColor: colors.navBarSubtitleTextColor,
    navBarButtonColor: colors.navBarButtonColor,
    statusBarTextColorScheme: colors.statusBarTextColorScheme,
    statusBarColor: colors.statusBarColor,
    tabBarBackgroundColor: colors.tabBarBackgroundColor,
    tabBarButtonColor: colors.tabBarButtonColor,
    tabBarSelectedButtonColor: colors.tabBarSelectedButtonColor,
    navBarSubtitleColor: colors.navBarSubtitleColor,
};*/

  constructor(props) {
    super(props);
    //console.log(this.props);
    this.state={
        check1:false,
        tabs:1,
        tabSelected:1,
        trueSwitchIsOn: true,
        falseSwitchIsOn: false,
        value: 0,
         iconHeart:'m'
    };
    // if you want to listen on navigator events, set this up
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  

  onNavigatorEvent(event) {
    //console.log(event)
    this.props.navigator.toggleNavBar({
      to: 'shown',
      animated: true
    });
    this.props.navigator.setStyle({navBarTitleTextCentered: true});
    this.props.navigator.setTitle({
      title: "favoris",

    });
    if(event.id == 'back'){
      this.props.navigator.pop({
        animated: true // does the pop have transition animation or does it happen immediately (optional)
      });
    }
  }

  handleOnPress(value){
        this.setState({value:value})
 }

  goto(page){
    this.props.navigator.push({
      screen: page
    });

  }
  tabchange(tab){
          this.setState({tabs:tab,tabSelected:tab});
  }
   heartchange(){
      if(this.state.iconHeart == 'm'){ 
            this.setState({iconHeart :'n'});
      }
      else{
           this.setState({heart:1,iconHeart :'m'});
      }
      //alert(this.state.iconHeart)
  };
  render() {
     const onTintColor = "#ffa382";
     const thumbTintColor = "#ececec";
     const tintColor = "#b2b2b2";
    //  if(this.state.tabs == 1){
    //     myAccountData = 
    //     <ScrollView>
    //             <View style={styles.scrollInner}>
    //                 <View style={styles.myAccountData}>
    //                     <View>
    //                         <Text style={styles.blockTitle}>Vos Favoris</Text>  
    //                     </View>
    //                     <View style={styles.accountdataBlock}>
    //                         <View style={[styles.inlineDatarow]}>
    //                             <View style={[styles.inlineWithico]}>
    //                                 <CheckBox
    //                                     style={[styles.genChk]}
    //                                     onClick={(checked) =>{
    //                                     this.setState({check1:!checked})}}
    //                                     labelStyle={styles.midum}
    //                                     isChecked={this.state.check1}
    //                                     rightText={'Tout selectionner'}
    //                                 />
    //                             </View>
    //                             <View style={[styles.inlineWithico,styles.flexStart]}>
    //                                 <Text style={[styles.globalIcon,styles.bigIco]}>8</Text>
    //                                 <Text style={[styles.icoText,styles.suprimeTitle]}>Supprimer la selection</Text>
    //                             </View>
    //                         </View>
    //                         <View style={[styles._PTB10_]}>
    //                             <View style={[styles.inlineDatarow]}>
    //                                 <Text style={[styles.globalIcon,styles.bigIco]}>8</Text>
    //                                 <Text style={[styles.icoText,styles.suprimeTitle]}>Partager la selection</Text>
    //                             </View>
    //                         </View>
    //                         <View style={[styles.relative,styles._w220_,styles.dropBorder,styles._MT5_]}>
    //                             <ModalDropdown options={['Choisissiez la Category', 'One']} style={[styles.dropdown_2,styles.dropDown,styles.dropDownHome]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} />
    //                             <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
    //                         </View>                            
    //                     </View>
    //                     <View style={[styles.accountdataBlock]}>
    //                         <View style={styles.proMain}>
    //                                 <Row size={12} style={[styles.proRow,styles.prolistRow,styles._MT0_,styles._PT0_]}>
    //                                     <Col sm={4} md={4} lg={4} style={[styles.proCol,styles.listCol]} >
    //                                         <View style={styles.colInner}>
    //                                             <View style={styles.imageCanvas}>
    //                                                 <View style={styles.imgInner}>
    //                                                     <Image style={styles.hasVideo} source={images.video} />
    //                                                     <Image style={[styles.proImg,styles.proImgsmall]} source={images.minic} />
    //                                                     <CheckBox
    //                                                         style={[styles.genChk,styles.absoluteCheck]}
    //                                                         onClick={(checked) =>{
    //                                                         this.setState({check1:!checked})}}
    //                                                         labelStyle={styles.midum}
    //                                                         isChecked={this.state.check1}
    //                                                     />
    //                                                 </View>
    //                                             </View>
    //                                             <View style={[styles.bottomDetail,styles.bottomCaption,styles.relativeCaption]}>
    //                                                 <Text style={[styles.leftAlign,styles.white]}>
    //                                                     <Text style={styles.small}>19 Dec | 09h09</Text>
    //                                                 </Text>
    //                                                 <View style={[styles.rightAlign]}>
    //                                                     <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>3</Text></View> 
    //                                                 </View>
    //                                             </View>
    //                                         </View>     
    //                                     </Col>
    //                                     <Col sm={8} md={8} lg={8} style={[styles.proCol,styles.listCol]}>
    //                                         <View style={styles.proCaption}>
    //                                             <Text style={[styles.proName,styles.big,styles._PB10_]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
    //                                             <View style={styles.row}>
    //                                                 <View style={[styles.inlineDatarow,styles._PB10_]}>
    //                                                     <View style={styles.inlineWithico}>
    //                                                         <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small,styles._MPR5_]}><Text style={styles.globalIcon}>x</Text> Urgent</Text>
    //                                                         <Text style={[styles.inlineWithico,styles.red,styles.small]}><Text style={styles.globalIcon}>w</Text> Achat Ditect</Text>
    //                                                     </View>
    //                                                 </View> 
    //                                                 <View style={[styles.row,styles._PB10_]}>
    //                                                     <Text style={[styles.orangeColor,styles.bold,styles.midum]}>12,550 €</Text>  
    //                                                 </View>
    //                                                 <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                                     <View style={[styles.inlineDatarow,styles._MR10_]}>
    //                                                         <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                                         <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                                     </View> 
    //                                                 </View>
                                                    
    //                                             </View>
    //                                         </View>
    //                                     </Col>
    //                                 </Row>
    //                                 <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_]}>
    //                                     <Col sm={4} md={4} lg={4} style={[styles.proCol,styles.listCol]} >
    //                                         <View style={styles.colInner}>
    //                                             <View style={styles.imageCanvas}>
    //                                                 <View style={styles.imgInner}>
    //                                                     <Image style={styles.hasVideo} source={images.video} />
    //                                                     <Image style={[styles.proImg,styles.proImgsmall]} source={images.minic} />
    //                                                     <CheckBox
    //                                                         style={[styles.genChk,styles.absoluteCheck]}
    //                                                         onClick={(checked) =>{
    //                                                         this.setState({check1:!checked})}}
    //                                                         labelStyle={styles.midum}
    //                                                         isChecked={this.state.check1}
    //                                                     />
    //                                                 </View>
    //                                             </View>
    //                                             <View style={[styles.bottomDetail,styles.bottomCaption,styles.relativeCaption]}>
    //                                                 <Text style={[styles.leftAlign,styles.white]}>
    //                                                     <Text style={styles.small}>19 Dec | 09h09</Text>
    //                                                 </Text>
    //                                                 <View style={[styles.rightAlign]}>
    //                                                     <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>3</Text></View> 
    //                                                 </View>
    //                                             </View>
    //                                         </View>     
    //                                     </Col>
    //                                     <Col sm={8} md={8} lg={8} style={[styles.proCol,styles.listCol]}>
    //                                         <View style={styles.proCaption}>
    //                                             <Text style={[styles.proName,styles.big,styles._PB10_]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
    //                                             <View style={styles.row}>
    //                                                 <View style={[styles.inlineDatarow,styles._PB10_]}>
    //                                                     <View style={styles.inlineWithico}>
    //                                                         <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small,styles._MPR5_]}><Text style={styles.globalIcon}>x</Text> Urgent</Text>
    //                                                         <Text style={[styles.inlineWithico,styles.red,styles.small]}><Text style={styles.globalIcon}>w</Text> Achat Ditect</Text>
    //                                                     </View>
    //                                                 </View> 
    //                                                 <View style={[styles.row,styles._PB10_]}>
    //                                                     <Text style={[styles.orangeColor,styles.bold,styles.midum]}>12,550 €</Text>  
    //                                                 </View>
    //                                                 <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                                     <View style={[styles.inlineDatarow,styles._MR10_]}>
    //                                                         <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                                         <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                                     </View> 
    //                                                 </View>
    //                                             </View>
    //                                         </View>
    //                                     </Col>
    //                                 </Row>
    //                                 <Row size={12} style={[styles.proRow,styles.prolistRow,styles._PT0_]}>
    //                                     <Col sm={4} md={4} lg={4} style={[styles.proCol,styles.listCol]} >
    //                                         <View style={styles.colInner}>
    //                                             <View style={styles.imageCanvas}>
    //                                                 <View style={styles.imgInner}>
    //                                                     <Image style={styles.hasVideo} source={images.video} />
    //                                                     <Image style={[styles.proImg,styles.proImgsmall]} source={images.minic} />
    //                                                     <CheckBox
    //                                                         style={[styles.genChk,styles.absoluteCheck]}
    //                                                         onClick={(checked) =>{
    //                                                         this.setState({check1:!checked})}}
    //                                                         labelStyle={styles.midum}
    //                                                         isChecked={this.state.check1}
    //                                                     />
    //                                                 </View>
    //                                             </View>
    //                                             <View style={[styles.bottomDetail,styles.bottomCaption,styles.relativeCaption]}>
    //                                                 <Text style={[styles.leftAlign,styles.white]}>
    //                                                     <Text style={styles.small}>19 Dec | 09h09</Text>
    //                                                 </Text>
    //                                                 <View style={[styles.rightAlign]}>
    //                                                     <View style={styles.relative}><Text style={[styles.globalIcon,styles.camIco,styles.white,styles.camIcoAbs]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.white,styles.small]}>3</Text></View> 
    //                                                 </View>
    //                                             </View>
    //                                         </View>     
    //                                     </Col>
    //                                     <Col sm={8} md={8} lg={8} style={[styles.proCol,styles.listCol]}>
    //                                         <View style={styles.proCaption}>
    //                                             <Text style={[styles.proName,styles.big,styles._PB10_]} numberOfLines={1}>3008 familiale grise à restaurer</Text>
    //                                             <View style={styles.row}>
    //                                                 <View style={[styles.inlineDatarow,styles._PB10_]}>
    //                                                     <View style={styles.inlineWithico}>
    //                                                         <Text style={[styles.inlineWithico,styles.borderRight,styles.red,styles.small,styles._MPR5_]}><Text style={styles.globalIcon}>x</Text> Urgent</Text>
    //                                                         <Text style={[styles.inlineWithico,styles.red,styles.small]}><Text style={styles.globalIcon}>w</Text> Achat Ditect</Text>
    //                                                     </View>
    //                                                 </View> 
    //                                                 <View style={[styles.row,styles._PB10_]}>
    //                                                     <Text style={[styles.orangeColor,styles.bold,styles.midum]}>12,550 €</Text>  
    //                                                 </View>
    //                                                 <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                                     <View style={[styles.inlineDatarow,styles._MR10_]}>
    //                                                         <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                                         <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                                     </View> 
    //                                                 </View>
    //                                             </View>
    //                                         </View>
    //                                     </Col>
    //                                 </Row>
    //                         </View>
    //                     </View>
    //                 </View>
    //             </View>
    //     </ScrollView>
    //  }else if(this.state.tabs == 2){
    //     myAccountData = 
    //         <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
    //             <View style={styles.scrollInner}>
    //                 <View style={styles.myAccountData}>
    //                     <View style={styles.accountdataBlock}>
    //                         <Text style={styles.blockTitle}>Gérer mes alertes de notifications</Text>
    //                         <View style={styles.blockmetter}>
    //                             <View style={[styles.rowBtnMain,styles.rowMesalerts]}>
    //                                 <Text>Une annonce a été publiée</Text>    
    //                                  <Switch onTintColor={onTintColor} thumbTintColor={thumbTintColor} tintColor={tintColor} onValueChange={(value) => this.setState({trueSwitchIsOn1: value})} value={this.state.trueSwitchIsOn1} />     
    //                             </View>                                
    //                             <View style={[styles.rowBtnMain,styles.rowMesalerts]}>
    //                                 <Text>une annonce a été supprimée</Text>   
    //                                  <Switch onTintColor={onTintColor} thumbTintColor={thumbTintColor} tintColor={tintColor}  onValueChange={(value) => this.setState({trueSwitchIsOn2: value})}  value={this.state.trueSwitchIsOn2} />
    //                             </View>
    //                             <View style={[styles.rowBtnMain,styles.rowMesalerts]}>
    //                                 <Text>une annonce arrive à son terme</Text>    
    //                                  <Switch onTintColor={onTintColor} thumbTintColor={thumbTintColor} tintColor={tintColor} onValueChange={(value) => this.setState({trueSwitchIsOn3: value})} value={this.state.trueSwitchIsOn3} />     
    //                             </View>                                
    //                             <View style={[styles.rowBtnMain,styles.rowMesalerts]}>
    //                                 <Text>une annonce a été remontée en tête de liste</Text>   
    //                                  <Switch onTintColor={onTintColor} thumbTintColor={thumbTintColor} tintColor={tintColor}  onValueChange={(value) => this.setState({trueSwitchIsOn4: value})}  value={this.state.trueSwitchIsOn4} />
    //                             </View> 
    //                         </View>
    //                     </View>
                       
    //                     <View style={[styles.accountdataBlock,styles.infoForm]}>
    //                          <Text style={styles.blockTitle}>Gérer mes alertes de notifications</Text>
    //                           <TouchableOpacity style={[styles.brandColor,styles._w150_]} activeOpacity={0.8}>
    //                              <Text style={[styles.genButton,styles.smallBtn]}>Créer une alerte</Text>
    //                           </TouchableOpacity>
    //                           <View style={[styles._MT15_,styles.cardNotiAlert]}>
    //                             <View style={[styles.inlineDatarow,styles.spaceBitw,styles._PB10_]}>
    //                                 <Text style={styles.notiCardTitle}>Voiture grise</Text>
    //                                 <Text style={styles.notiCardDate}>19 décembre | 09:00</Text>
    //                             </View>
    //                             <View style={[styles.inlineDatarow,styles.spaceBitw]}>
    //                                 <Switch onTintColor={onTintColor} thumbTintColor={thumbTintColor} tintColor={tintColor}  onValueChange={(value) => this.setState({switchnoti1: value})}  value={this.state.switchnoti1} />
    //                                 <View style={[styles.relative,styles._w120_,styles.dropBorder,styles._MT5_]}>
    //                                     <ModalDropdown options={['Choisissiez la Category', 'One']} style={[styles.dropdown_2,styles.dropDown,styles.dropDownHome]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} />
    //                                     <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
    //                                 </View>
    //                                 <View style={styles.dataRow}>
    //                                     <TouchableOpacity style={[styles.icNoti,styles._BR1_]} activeOpacity={0.8}>
    //                                         <Text style={[styles.globalIcon,styles.orangeColor,styles.big]}>h</Text>
    //                                     </TouchableOpacity>
    //                                     <TouchableOpacity style={styles.icNoti}  activeOpacity={0.8}>
    //                                         <Text style={[styles.globalIcon,styles.orangeColor,styles.big]}>8</Text>
    //                                     </TouchableOpacity>
    //                                  </View>
    //                             </View>
    //                           </View>
    //                     </View>
    //                 </View>
    //             </View>
    //         </ScrollView>
    //  }else if(this.state.tabs == 3){
    //     myAccountData = 
    //         <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
    //             <View style={styles.scrollInner}>
    //                  <View style={styles.myAccountData}>
    //                     <View>
    //                         <Text style={styles.blockTitle}>Vos abonnements</Text>  
    //                     </View>
    //                     <View style={styles.accountdataBlock}>
    //                         <View style={[styles.inlineDatarow]}>
    //                             <View style={[styles.inlineWithico]}>
    //                                 <CheckBox
    //                                     style={[styles.genChk]}
    //                                     onClick={(checked) =>{
    //                                     this.setState({check1:!checked})}}
    //                                     labelStyle={styles.midum}
    //                                     isChecked={this.state.check1}
    //                                     rightText={'Tout selectionner'}
    //                                 />
    //                             </View>
    //                             <View style={[styles.inlineWithico,styles.flexStart]}>
    //                                 <Text style={[styles.globalIcon,styles.bigIco]}>8</Text>
    //                                 <Text style={[styles.icoText,styles.suprimeTitle]}>Supprimer la selection</Text>
    //                             </View>
    //                         </View>
    //                         <View style={[styles._PTB10_]}>
    //                             <View style={[styles.inlineDatarow]}>
    //                                 <Text style={[styles.globalIcon,styles.bigIco]}>8</Text>
    //                                 <Text style={[styles.icoText,styles.suprimeTitle]}>Partager la selection</Text>
    //                             </View>
    //                         </View>
    //                         <View style={[styles.relative,styles._w220_,styles.dropBorder,styles._MT5_]}>
    //                             <ModalDropdown options={['Choisissiez la Category', 'One']} style={[styles.dropdown_2,styles.dropDown,styles.dropDownHome]} textStyle={[styles.dropdown_2_text,styles._F14_,styles.grayColor]} dropdownStyle={styles.dropdown_2_dropdown} />
    //                             <Text style={[styles.globalIcon,styles.dropabsIco]}>g</Text>
    //                         </View>                            
    //                     </View>
    //                     <View style={styles.accountdataBlock}>
    //                         <Row size={12} style={styles.proRow}>
    //                             <Col sm={6} md={4} lg={3} style={[styles.proCol]}>
    //                                 <View style={styles.proList}>
    //                                         <View style={styles.canvasContainer}>
    //                                             <View style={styles.imgInner}>
    //                                                 <Image
    //                                                     style={[styles.proImg,styles.progridWidth]}
    //                                                     source={images.minic}
    //                                                     />
    //                                                 <CheckBox
    //                                                     style={[styles.genChk,styles.gridCheck]}
    //                                                     onClick={(checked) =>{
    //                                                     this.setState({check1:!checked})}}
    //                                                     labelStyle={styles.midum}
    //                                                     isChecked={this.state.check1}
    //                                                 />
    //                                                 <Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />
    //                                                 <View style={styles.absoluteBottomRight}>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> y</Text>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> w </Text>
    //                                                 </View>
    //                                             </View>
    //                                         </View>
    //                                     <View style={styles.proCaption}>
    //                                             <TouchableOpacity onPress={()=>{this.goto('ProductDetail')}} activeOpacity={0.8}><Text style={[styles.proName,styles._F14_]} numberOfLines={1}>3008 familiale grise à restaurer</Text></TouchableOpacity>
    //                                                 <View style={[styles.bottomDetail]}>
    //                                             <View style={[styles.inlineDatarow]}>
    //                                                 <Text style={[styles.orangeColor,styles.bold,styles._F14_]}>12,550 €</Text>
    //                                                 <View style={[styles.rightAlign,styles._PB5_]}>
    //                                                     <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.camIco]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.small]}>3</Text></View> 
    //                                                     </View>
    //                                             </View>    
    //                                             <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                                     <View style={[styles.inlineDatarow]}>
    //                                                         <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                                         <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                                     </View> 
    //                                              </View>   
    //                                          </View>
    //                                      </View>
    //                                 </View>
    //                             </Col>
    //                             <Col sm={6} md={4} lg={3} style={styles.proCol}>
    //                                 <View style={styles.proList}>
    //                                         <View style={styles.canvasContainer}>
    //                                             <View style={styles.imgInner}>
    //                                                 <Image
    //                                                     style={[styles.proImg,styles.progridWidth]}
    //                                                     source={images.minic}
    //                                                     />
    //                                                 <CheckBox
    //                                                     style={[styles.genChk,styles.gridCheck]}
    //                                                     onClick={(checked) =>{
    //                                                     this.setState({check1:!checked})}}
    //                                                     labelStyle={styles.midum}
    //                                                     isChecked={this.state.check1}
    //                                                 />
    //                                                 <Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />
    //                                                 <View style={styles.absoluteBottomRight}>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> y</Text>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> w </Text>
    //                                                 </View>
    //                                             </View>
    //                                         </View>
    //                                         <View style={styles.proCaption}>
    //                                             <TouchableOpacity onPress={()=>{this.goto('ProductDetail')}} activeOpacity={0.8}><Text style={[styles.proName,styles._F14_]} numberOfLines={1}>3008 familiale grise à restaurer</Text></TouchableOpacity>
    //                                                 <View style={[styles.bottomDetail]}>
    //                                             <View style={[styles.inlineDatarow]}>
    //                                                 <Text style={[styles.orangeColor,styles.bold,styles._F14_]}>12,550 €</Text>
    //                                                 <View style={[styles.rightAlign,styles._PB5_]}>
    //                                                     <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.camIco]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.small]}>3</Text></View> 
    //                                                     </View>
    //                                             </View>    
    //                                            <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                                     <View style={[styles.inlineDatarow]}>
    //                                                         <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                                         <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                                     </View> 
    //                                              </View>     
    //                                             </View>
    //                                         </View>
    //                                 </View>
    //                             </Col>
    //                             <Col sm={6} md={4} lg={3} style={styles.proCol}>
    //                                 <View style={styles.proList}>
    //                                         <View style={styles.canvasContainer}>
    //                                             <View style={styles.imgInner}>
    //                                                 <Image
    //                                                     style={[styles.proImg,styles.progridWidth]}
    //                                                     source={images.minic}
    //                                                     />
    //                                                 <CheckBox
    //                                                     style={[styles.genChk,styles.gridCheck]}
    //                                                     onClick={(checked) =>{
    //                                                     this.setState({check1:!checked})}}
    //                                                     labelStyle={styles.midum}
    //                                                     isChecked={this.state.check1}
    //                                                 />
    //                                                 <Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />
    //                                                 <View style={styles.absoluteBottomRight}>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> y</Text>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> w </Text>
    //                                                 </View>
    //                                             </View>
    //                                         </View>
    //                                         <View style={styles.proCaption}>
    //                                             <TouchableOpacity onPress={()=>{this.goto('ProductDetail')}} activeOpacity={0.8}><Text style={[styles.proName,styles._F14_]} numberOfLines={1}>3008 familiale grise à restaurer</Text></TouchableOpacity>
    //                                                 <View style={[styles.bottomDetail]}>
    //                                             <View style={[styles.inlineDatarow]}>
    //                                                 <Text style={[styles.orangeColor,styles.bold,styles._F14_]}>12,550 €</Text>
    //                                                 <View style={[styles.rightAlign,styles._PB5_]}>
    //                                                     <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.camIco]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.small]}>3</Text></View> 
    //                                                     </View>
    //                                             </View>    
    //                                             <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                                     <View style={[styles.inlineDatarow]}>
    //                                                         <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                                         <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                                     </View> 
    //                                              </View>     
    //                                         </View>
    //                                     </View>
    //                                 </View>
    //                             </Col>
    //                             <Col sm={6} md={4} lg={3} style={styles.proCol}>
    //                                 <View style={styles.proList}>
    //                                         <View style={styles.canvasContainer}>
    //                                             <View style={styles.imgInner}>
    //                                                 <Image
    //                                                     style={[styles.proImg,styles.progridWidth]}
    //                                                     source={images.minic}
    //                                                     />
    //                                                 <CheckBox
    //                                                     style={[styles.genChk,styles.gridCheck]}
    //                                                     onClick={(checked) =>{
    //                                                     this.setState({check1:!checked})}}
    //                                                     labelStyle={styles.midum}
    //                                                     isChecked={this.state.check1}
    //                                                 />
    //                                                 <Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />
    //                                                 <View style={styles.absoluteBottomRight}>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> y</Text>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> w </Text>
    //                                                 </View>
    //                                             </View>
    //                                         </View>
    //                        <View style={styles.proCaption}>
    //                                 <TouchableOpacity onPress={()=>{this.goto('ProductDetail')}} activeOpacity={0.8}><Text style={[styles.proName,styles._F14_]} numberOfLines={1}>3008 familiale grise à restaurer</Text></TouchableOpacity>
    //                                      <View style={[styles.bottomDetail]}>
    //                                     <View style={[styles.inlineDatarow]}>
    //                                         <Text style={[styles.orangeColor,styles.bold,styles._F14_]}>12,550 €</Text>
    //                                         <View style={[styles.rightAlign,styles._PB5_]}>
    //                                             <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.camIco]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.small]}>3</Text></View> 
    //                                          </View>
    //                                     </View>    
    //                                     <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                                     <View style={[styles.inlineDatarow]}>
    //                                                         <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                                         <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                                     </View> 
    //                                              </View>     
    //                                 </View>
    //                             </View>
    //                                 </View>
    //                             </Col>
    //                             <Col sm={6} md={4} lg={3} style={styles.proCol}>
    //                                 <View style={styles.proList}>
    //                                         <View style={styles.canvasContainer}>
    //                                             <View style={styles.imgInner}>
    //                                                 <Image
    //                                                     style={[styles.proImg,styles.progridWidth]}
    //                                                     source={images.minic}
    //                                                     />
    //                                                 <CheckBox
    //                                                     style={[styles.genChk,styles.gridCheck]}
    //                                                     onClick={(checked) =>{
    //                                                     this.setState({check1:!checked})}}
    //                                                     labelStyle={styles.midum}
    //                                                     isChecked={this.state.check1}
    //                                                 />
    //                                                 <Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />
    //                                                 <View style={styles.absoluteBottomRight}>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> y</Text>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> w </Text>
    //                                                 </View>
    //                                             </View>
    //                                         </View>
    //                                     <View style={styles.proCaption}>
    //                                 <TouchableOpacity onPress={()=>{this.goto('ProductDetail')}} activeOpacity={0.8}><Text style={[styles.proName,styles._F14_]} numberOfLines={1}>3008 familiale grise à restaurer</Text></TouchableOpacity>
    //                                      <View style={[styles.bottomDetail]}>
    //                                     <View style={[styles.inlineDatarow]}>
    //                                         <Text style={[styles.orangeColor,styles.bold,styles._F14_]}>12,550 €</Text>
    //                                         <View style={[styles.rightAlign,styles._PB5_]}>
    //                                             <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.camIco]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.small]}>3</Text></View> 
    //                                          </View>
    //                                     </View>    
    //                                    <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                                     <View style={[styles.inlineDatarow]}>
    //                                                         <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                                         <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                                     </View> 
    //                                              </View>     
    //                                 </View>
    //                             </View>
    //                                 </View>
    //                             </Col>
    //                             <Col sm={6} md={4} lg={3} style={styles.proCol}>
    //                                 <View style={styles.proList}>
    //                                         <View style={styles.canvasContainer}>
    //                                             <View style={styles.imgInner}>
    //                                                 <Image
    //                                                     style={[styles.proImg,styles.progridWidth]}
    //                                                     source={images.minic}
    //                                                     />
    //                                                 <CheckBox
    //                                                     style={[styles.genChk,styles.gridCheck]}
    //                                                     onClick={(checked) =>{
    //                                                     this.setState({check1:!checked})}}
    //                                                     labelStyle={styles.midum}
    //                                                     isChecked={this.state.check1}
    //                                                 />
    //                                                 <Image style={[styles.proSign,styles.proSignAbsolute]} source={images.pro} />
    //                                                 <View style={styles.absoluteBottomRight}>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> y</Text>
    //                                                     <Text style={[styles.globalIcon,styles.red]}> w </Text>
    //                                                 </View>
    //                                             </View>
    //                                         </View>
    //                                     <View style={styles.proCaption}>
    //                                 <TouchableOpacity onPress={()=>{this.goto('ProductDetail')}} activeOpacity={0.8}><Text style={[styles.proName,styles._F14_]} numberOfLines={1}>3008 familiale grise à restaurer</Text></TouchableOpacity>
    //                                      <View style={[styles.bottomDetail]}>
    //                                     <View style={[styles.inlineDatarow]}>
    //                                         <Text style={[styles.orangeColor,styles.bold,styles._F14_]}>12,550 €</Text>
    //                                         <View style={[styles.rightAlign,styles._PB5_]}>
    //                                             <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.camIco]}> s </Text><Text style={[styles.bold,styles.abscamtxt,styles.small]}>3</Text></View> 
    //                                          </View>
    //                                     </View>    
    //                                     <View style={[styles.inlineDatarow,styles.justRight]}>
    //                                         <View style={[styles.inlineDatarow]}>
    //                                             <View><Text style={this.state.iconHeart == 'm' ? [styles.globalIcon,styles.xlIco]:[styles.globalIcon,styles.xlIco,styles.orangeColor]} onPress={this.heartchange.bind(this)}>{this.state.iconHeart}</Text></View>
    //                                             <View style={[styles.relative]}><Text style={[styles.globalIcon,styles.xlIco]}> f</Text><Text style={[styles.circle,styles.online,styles.absCircle]}> </Text></View>
    //                                         </View> 
    //                                     </View>     
    //                                 </View>
    //                             </View>
    //                                 </View>
    //                             </Col>
    //                         </Row>
    //                     </View>
    //                 </View>
    //             </View>
    //         </ScrollView>
    //  }else if(this.state.tabs == 4){
    //     myAccountData = 
    //     <ScrollView showsVerticalScrollIndicator={false} style={styles.mainScollview}>
    //         <View style={styles.scrollInner}>
    //             <View style={styles.myAccountData}>
    //                     <View style={styles.accountdataBlock}>
    //                         <Text style={styles.blockTitle}>Nos offres pour la boutique</Text>
    //                         <View style={[styles.contBlock,styles.borderBottom]}>
    //                             <View style={styles.contData}>
    //                                 <Text style={styles.smallDesc}>Créer une recherche favorite, une fois que vous avez affinez et 
    //                                     enregistrez une recherche, vous pourrez accéder directement aux derniers résultats correspondants.  </Text> 
    //                             </View>
    //                             <View style={styles.row}>
    //                                 <TouchableOpacity style={[styles._MR10_]} activeOpacity={0.8}>
    //                                     <Text style={[styles.smallBtn]}>Créer une recherche favorite</Text>
    //                                 </TouchableOpacity>
    //                             </View>
    //                         </View>
    //                         <View style={[styles.contBlock,styles._MT10_]}>
    //                             <Text style={styles.subTitle}>3008 familiable grise</Text>
    //                             <View style={styles.contData}>
    //                                 <Text style={styles.smallDesc}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
    //                             </View>
    //                             <View style={styles.rowBtnMain}>
    //                                  <TouchableOpacity  style={[styles.btnSquare,styles._BR0_]} activeOpacity={0.8}>
    //                                     <Text style={[styles.globalIcon,styles.orangeColor,styles.big]}>h</Text>
    //                                 </TouchableOpacity>
    //                                 <TouchableOpacity  style={[styles.btnSquare]} activeOpacity={0.8}>
    //                                     <Text style={[styles.globalIcon,styles.orangeColor,styles.big]}>8</Text>
    //                                 </TouchableOpacity>
    //                             </View>
    //                         </View>
    //                         <View style={[styles.contBlock,styles._MT10_]}>
    //                             <Text style={styles.subTitle}>3008 familiable grise</Text>
    //                             <View style={styles.contData}>
    //                                 <Text style={styles.smallDesc}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
    //                             </View>
    //                             <View style={styles.rowBtnMain}>
    //                                  <TouchableOpacity  style={[styles.btnSquare,styles._BR0_]} activeOpacity={0.8}>
    //                                     <Text style={[styles.globalIcon,styles.orangeColor,styles.big]}>h</Text>
    //                                 </TouchableOpacity>
    //                                 <TouchableOpacity  style={[styles.btnSquare]} activeOpacity={0.8}>
    //                                     <Text style={[styles.globalIcon,styles.orangeColor,styles.big]}>8</Text>
    //                                 </TouchableOpacity>
    //                             </View>
    //                         </View>
    //                     </View>
    //             </View>
    //         </View>
    //     </ScrollView>
    //  }else{
    //    null
    //  }

     

    return (
       <View style={[styles.main,styles._HP15_]}>
            {/* <View style={styles.tabs}>
                <TouchableOpacity  style={(this.state.tabSelected== 1)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>{this.tabchange(1)}} activeOpacity={0.8}>
                    <Text style={(this.state.tabSelected== 1)?[styles.tabText,styles.orangeColor]:[styles.tabText]}>Gerers mes{"\n"}favoris</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={(this.state.tabSelected== 2)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(2)} activeOpacity={0.8}>
                    <Text style={(this.state.tabSelected== 2)?[styles.tabText,styles.orangeColor]:[styles.tabText]}>Gerer mes{"\n"}alertes</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={(this.state.tabSelected== 3)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(3)} activeOpacity={0.8}>
                    <Text style={(this.state.tabSelected== 3)?[styles.tabText,styles.orangeColor]:[styles.tabText]}>Recherches{"\n"}favorites</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={(this.state.tabSelected== 4)?[styles.tabSelected,styles.widthQuater]:[styles.tabnotSelected,styles.widthQuater]} onPress={()=>this.tabchange(4)} activeOpacity={0.8}>
                    <Text style={(this.state.tabSelected== 4)?[styles.tabText,styles.orangeColor]:[styles.tabText]}> Vos{"\n"}Abonnement</Text>
                </TouchableOpacity>
            </View> */}
            {/* {myAccountData} */}
       </View>
    );
  }

}






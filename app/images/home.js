import React, { Component } from 'react';
import { Container, Content, Form, Item, Input, Label,Button,Icon,Text , Header, Left, Right, Body, Title, Tabs , Tab,TabHeading,DatePicker,Footer,FooterTab } from 'native-base';
import {
Image,
  StyleSheet, View
} from 'react-native';

    export default class HomePage extends Component {

        render() {
            return (
                  <Image source={require('./../../img/back.jpg')} style={styles.container}>
						
						<Container>
							<Header>
									<Left>
											<Button transparent>
												<Image source={require('./../../img/menu.png')} style={styles.menuIco}></Image> 
											</Button>
										</Left>
										
											<Title>MYCRIBOOKING</Title>
									
										<Right>
											<Button transparent>
												<Image source={require('./../../img/companylogo.jpg')} style={styles.headerLogo}></Image> 
											</Button>
										</Right>
									</Header>
							<Content>
									
									<View style={styles.genView}>  
										<Form style={StyleSheet.flatten(styles.viewInner)}>
												<Tabs hasTabs style={styles.tabParent}>
													<Tab heading={<TabHeading><Image source={require('./../../img/home.svg')} style={styles.tabIco}></Image></TabHeading>}>
														 
													</Tab>
													<Tab heading={<TabHeading><Image source={require('./../../img/menu.png')} style={styles.tabIco}></Image></TabHeading>}>
													</Tab>
													<Tab heading={<TabHeading><Image source={require('./../../img/airplane-around-earth.png')} style={styles.tabIco}></Image></TabHeading>}>
													</Tab>
													<Tab heading={<TabHeading><Image source={require('./../../img/man-cycling.png')} style={styles.tabIco}></Image></TabHeading>}>
													</Tab>
													<Tab heading={<TabHeading><Image source={require('./../../img/car.png')} style={styles.tabIco}></Image></TabHeading>}>
													</Tab>
													<Tab heading={<TabHeading><Image source={require('./../../img/location.png')} style={styles.tabIco}></Image></TabHeading>}>
													</Tab>
												</Tabs>
												<Item>
													<Icon name="search" />
													<Input placeholder="Search" style={StyleSheet.flatten(styles.searchInput)}/>
												</Item>
												<Button success block >
													<Text>Search</Text>
												</Button>
												 <Footer>
													<FooterTab>
														<Button>
															<Text>Home</Text>
														</Button>
														<Button>
															<Text>Favorate</Text>
														</Button>
														<Button active>
															<Text>Review</Text>
														</Button>
														<Button>
															<Text>Notification</Text>
														</Button>
														<Button>
															<Text>Profile</Text>
														</Button>
													</FooterTab>
												</Footer>
										</Form>
									</View>
							</Content>
						</Container>
                  </Image>
            );
        }
    }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: null,
  },
  brandLogo:{
      height:80,
      width:80
  },
  centerView:{
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  signUp:{
      width: 100,
      height:40,
      backgroundColor:'#b881fc'
  },
  menuIco:{
	  width:30,
	  height:30,
  },
  headerLogo:{
	 width:45, 
  },
  genView: {
		backgroundColor:'rgba(0,0,0,0)',  
		marginTop:25,
		paddingLeft:10,
		paddingRight:10,
   },
   tabIco:{
		width:27,   
		height:24,
   },
   tabParent:{
		backgroundColor:'rgba(0,0,0,0)',    
   },
   viewInner:{
	   backgroundColor:'rgba(0,0,0,0.6)',    
   },
   searchInput:{
	 	 color:'#ffffff', 
   },
   
})
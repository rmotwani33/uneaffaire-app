
// original //
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Animated,
  Platform
} from 'react-native';
import genStyles from '../../config/genStyle';
import Icon from 'react-native-vector-icons/FontAwesome';
class FloatingLabel extends Component {
  constructor(props) {
    super(props);

    let initialPadding = 9;
    let initialOpacity = 0;

    if (this.props.visible) {
      initialPadding = 5;
      initialOpacity = 1;
    }

    this.state = {
      paddingAnim: new Animated.Value(initialPadding),
      opacityAnim: new Animated.Value(initialOpacity)
    }
  }

  componentWillReceiveProps(newProps) {
    Animated.timing(this.state.paddingAnim, {
      toValue: newProps.visible ? 5 : 9,
      duration: 230
    }).start();

    return Animated.timing(this.state.opacityAnim, {
      toValue: newProps.visible ? 1 : 0,
      duration: 230
    }).start();
  }

  render() {
    return (
      <Animated.View style={[styles.floatingLabel, { paddingTop: this.state.paddingAnim, opacity: this.state.opacityAnim }]}>
        {this.props.children}
      </Animated.View>
    );
  }
}

class TextFieldHolder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marginAnim: new Animated.Value(this.props.withValue ? 10 : 0)
    }
  }

  componentWillReceiveProps(newProps) {
    return Animated.timing(this.state.marginAnim, {
      toValue: newProps.withValue ? 10 : 0,
      duration: 230
    }).start();
  }

  render() {
    return (
      <Animated.View style={{ marginTop: this.state.marginAnim,flexDirection:'row' }}>
        {this.props.children}
      </Animated.View>
    );
  }
}

class FloatLabelTextField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      text: this.props.value
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps.hasOwnProperty('value') && newProps.value !== this.state.text) {
      this.setState({ text: newProps.value })
    }
  }

  withBorder() {
    if (!this.props.noBorder) {
      return styles.withBorder;
    }
  }

  render() {
    return (
      <View style={[styles.container,this.props.Description ? {height:125}:null]}>
        <View style={styles.viewContainer}>
          {this.props.forIco == 'Ico' ? (<View style={styles.iconCont}>
            {this.props.IconProvider ? (
              <Icon name="location-arrow" size={30} color={"#ccc"}/>
            ):(
             <Text style={[genStyles.globalIcon,genStyles.fieldIco]}>{this.props.icon ? this.props.icon: null}</Text>)}
             </View>):(null)}
          <View style={styles.paddingView} />
          <View style={[styles.fieldContainer, this.withBorder(), this.props.fieldContainerStyle ? this.props.fieldContainerStyle : null]}>
            <FloatingLabel visible={this.state.text}>
              <Text style={[styles.fieldLabel, this.labelStyle()]}>{this.placeholderValue()}</Text>
            </FloatingLabel>
            <TextFieldHolder withValue={this.state.text}>
              {this.props.Description ? (<TextInput {...this.props}
                ref='input'
                underlineColorAndroid="transparent"
                style={[styles.valueDesc]}
                multiline={true}
                defaultValue={this.props.defaultValue}
                value={this.state.text}
                placeholderTextColor={this.props.placeholderTextColor ? this.props.placeholderTextColor : '#ccc'}
                numberOfLines={6}
                autoCapitalize={'none'} 
                // returnKeyType={'go'}
                autoCorrect={false}
                maxLength={this.props.maxLength}
                onFocus={() => this.setFocus()}
                onBlur={() => this.unsetFocus()}
                onChangeText={(value) => this.setText(value)}
                />):(
                  
              <TextInput {...this.props}
                ref='input'
                underlineColorAndroid="transparent"
                style={[styles.valueText]}
                defaultValue={this.props.defaultValue}
                value={this.state.text}
                autoCapitalize={'none'} autoCorrect={false}
                maxLength={this.props.maxLength}
                onFocus={() => this.setFocus()}
                onBlur={() => this.unsetFocus()}
                onChangeText={(value) => this.setText(value)}
                />
                )}
                {this.props.editIco ? (
            <View style={[styles.rightIco,this.props.Description ? null:styles.rightIcoTop]}>
                <Text style={[styles.editIco,this.props.Description ? styles.editIcoTop:null]}>{this.props.editIco}</Text>
            </View>):(null)}
            </TextFieldHolder>
            
          </View>
        </View>
      </View>
    );
  }

  inputRef() {
    return this.refs.input;
  }

  focus() {
    this.inputRef().focus();
  }

  blur() {
    this.inputRef().blur();
  }

  isFocused() {
    return this.inputRef().isFocused();
  }

  clear() {
    this.inputRef().clear();
  }

  setFocus() {
    this.setState({
      focused: true
    });
    try {
      return this.props.onFocus();
    } catch (_error) { }
  }

  unsetFocus() {
    this.setState({
      focused: false
    });
    try {
      return this.props.onBlur();
    } catch (_error) { }
  }

  labelStyle() {
    if (this.state.focused) {
      return styles.focused;
    }
  }

  placeholderValue() {
    if (this.state.text) {
      return this.props.placeholder;
    }
  }

  setText(value) {
    this.setState({
      text: value
    });
    try {
      return this.props.onChangeTextValue(value);
    } catch (_error) { }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 45,
    backgroundColor: '#0000',
    justifyContent: 'center'
  },
  viewContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  paddingView: {
    //width: 15
  },
  floatingLabel: {
    position: 'absolute',
    top: 0,
    left: 0
  },
  fieldLabel: {
    height: 15,
    fontSize: 10,
    color: '#f15a23'
  },
  fieldContainer: {
    flex: 1,
    justifyContent: 'center',
    position: 'relative'
  },
  withBorder: {
    borderBottomWidth: 1 / 2,
    borderColor: '#C8C7CC',
  },
  valueText: {
    flex:1,
    height: (Platform.OS == 'ios' ? 20 : 60),
    fontSize: 16,
    color: '#333'
  },
  focused: {
    color: "#f15a23"
  },
  iconCont:{
     width:30,
     alignItems:'center',
     justifyContent:'space-around'
   },
  valueDesc:{
    flex:1,
    fontSize: 16,
    color: '#333',
    textAlignVertical:'top',
    height:(Platform.OS == 'ios' ? 100 : 120),
  },
  rightIco:{
    width:20,
  },
  rightIcoTop:{
    alignItems:'center',
    justifyContent:'center',
  },
  editIco:{
    fontFamily:'icomoon',
    fontSize:16,
    justifyContent:'center'
  },
  editIcoTop:{
    zIndex:1,
    paddingTop:15,
  }
});

export default FloatLabelTextField;
/* bakup gaurang
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Animated,
  Platform,Dimensions
} from 'react-native'
const { height, width } = Dimensions.get('window');
import genStyles from '../../config/genStyle';
class FloatingLabel extends Component {
  constructor(props) {
    super(props);

    let initialPadding = 9;
    let initialOpacity = 0;

    if (this.props.visible) {
      initialPadding = 5;
      initialOpacity = 1;
    }

    this.state = {
      paddingAnim: new Animated.Value(initialPadding),
      opacityAnim: new Animated.Value(initialOpacity)
    }
  }

  componentWillReceiveProps(newProps) {
    Animated.timing(this.state.paddingAnim, {
      toValue: newProps.visible ? 5 : 9,
      duration: 230
    }).start();

    return Animated.timing(this.state.opacityAnim, {
      toValue: newProps.visible ? 1 : 0,
      duration: 230
    }).start();
  }

  render() {
      return (
        <Animated.View style={[styles.floatingLabel, { paddingTop: this.state.paddingAnim, opacity: this.state.opacityAnim }]}>
          {this.props.children}
        </Animated.View>
      );
    }
  }

class TextFieldHolder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marginAnim: new Animated.Value(this.props.withValue ? 10 : 0)
    }
  }

  componentWillReceiveProps(newProps) {
    return Animated.timing(this.state.marginAnim, {
      toValue: newProps.withValue ? 10 : 0,
      duration: 230
    }).start();
  }

  render() {
    return (
      <Animated.View style={{ flex:1,marginTop: this.state.marginAnim }}>
        {this.props.children}
      </Animated.View>
    );
  }
}

class FloatLabelTextField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      focused: false,
      text: this.props.value
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps.hasOwnProperty('value') && newProps.value !== this.state.text) {
      this.setState({ text: newProps.value })
    }
  }

  withBorder() {
    if (!this.props.noBorder) {
      return styles.withBorder;
    }
  }

  render() {
    return (*/
      // <View style={styles.container}>
      //   <View style={styles.viewContainer}>
      //     {this.props.forIco == 'Ico' ? (<View style={styles.iconCont}>
      //       <Text style={[genStyles.globalIcon,genStyles.fieldIco]}>{this.props.icon ? this.props.icon: null}</Text>
      //       </View>):(null)}
          
      //     <View style={styles.paddingView} />
      //     <View style={[styles.fieldContainer, this.withBorder()]}>
      //       <FloatingLabel visible={this.state.text}>
      //         <Text style={[styles.fieldLabel, this.labelStyle()]}>{this.placeholderValue()}</Text>
      //       </FloatingLabel>
      //       <TextFieldHolder withValue={this.state.text}>
      //         <TextInput {...this.props}
      //           ref='input'
      //           underlineColorAndroid="transparent"
      //           style={[styles.valueText]}
      //           defaultValue={this.props.defaultValue}
      //           value={this.state.text}
      //           maxLength={this.props.maxLength}
      //           onFocus={() => this.setFocus()}
      //           onBlur={() => this.unsetFocus()}
      //           onChangeText={(value) => this.setText(value)}
      //           />
      //           {/* {paddingLeft:this.props.forIco == 'Ico' ? 30 : 0} */}
      //       </TextFieldHolder>
      //     </View>
      //   </View>
      // </View>
  //   );
  // }
/*
  inputRef() {
    return this.refs.input;
  }

  focus() {
    this.inputRef().focus();
  }

  blur() {
    this.inputRef().blur();
  }

  isFocused() {
    return this.inputRef().isFocused();
  }

  clear() {
    this.inputRef().clear();
  }

  setFocus() {
    this.setState({
      focused: true
    });
    try {
      return this.props.onFocus();
    } catch (_error) { }
  }

  unsetFocus() {
    this.setState({
      focused: false
    });
    try {
      return this.props.onBlur();
    } catch (_error) { }
  }

  labelStyle() {
    if (this.state.focused) {
      return styles.focused;
    }
  }

  placeholderValue() {
    if (this.state.text) {
      return this.props.placeholder;
    }
  }

  setText(value) {
    this.setState({
      text: value
    });
    try {
      return this.props.onChangeTextValue(value);
    } catch (_error) { }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0)',
    justifyContent: 'center'
  },
  viewContainer: {
    flex: 1,
    flexDirection: 'row',
    //paddingTop:15,
    alignItems:'center',
  },
  paddingView: {
   // width: 15
  },
  floatingLabel: {
    position: 'absolute',
    top: 0,
    left: 0
  },
  fieldLabel: {
    height: 15,
    fontSize: 10,
    color: '#f15a23',
  },
  fieldContainer: {
    flex: 1,
    justifyContent: 'center',
    position: 'relative',
    height: (Platform.OS == 'ios' ? 40 : 45),
  },
  withBorder: {
    //borderBottomWidth: 1 / 2,
    //borderColor: '#C8C7CC',
  },
  valueText: {
    height: (Platform.OS == 'ios' ? 40 : 45),
    fontSize: (width < 1025 && width > 721) ? 16:14,
    color: '#333',
    //paddingTop:15,
    paddingLeft:0,
  },
  focused: {
    color: "#f15a23"
  },
  iconCont:{
    width:30,
  }
});

export default FloatLabelTextField;*/

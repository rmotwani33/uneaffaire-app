import React, {Component} from 'react';
import {AsyncStorage,NetInfo} from 'react-native';
import {settings} from '../../config/settings';
import startMain from '../../config/app-main';
const isConn = true;

export default class ApiHelper extends Component {
    constructor(props) {
        super(props);
        this.chatAction = null;
        this.authAction = null;
    }

    xConsole = (str) => {
        if (settings.devMode) {
            console.log(str);
        }
    }
    checkLogin(str) {
        if (typeof str != 'undefined' && str != null && str != '' && str != 'Guest') {
            return true;
        } else {
            return false;
        }
    }

    setAction(c,a){
        this.chatAction = c;
        this.authAction = a;
    }

    checkNet(){
        return new Promise((resolve, reject) => {
            NetInfo.isConnected.fetch().then(isConnected => {
                    var random_boolean = Math.random() >= 0.5;
                    console.log('Net is ' + random_boolean);
                    if(random_boolean || 1 == 1){
                        resolve(true);
                    }else{
                        reject(false);
                    }
            });
        });        
    }

    isProfileComplete(userdata){
        if(userdata !== undefined && userdata !== null && userdata.profile_complete == 1){
            return true;
        }else{
            return false;
        }
    }

    logOut=()=>{
        AsyncStorage.multiRemove(['token','userdata'],function(err){
        }).then(res=>{
            startMain('',{});
        });
    }
    FetchData = (request) => {

        const apiCall = (resolve, reject) => {
            fetch(settings.api + request.url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(request.params)
            }).then((response) => response.json()).then((responseJson) => {
                if(responseJson.status===false && responseJson.login === false){
                    this.logOut();
                }else{
                    this.xConsole(responseJson);
                    resolve(responseJson);
                }
            }).catch((error) => {
                this.xConsole(error);
                reject(error);
            });
        };

        return new Promise((resolve, reject) => {
            this.checkNet().then(()=>{
                this.xConsole(request);
                this.xConsole(JSON.stringify(request.params));
                this.xConsole(settings.api + request.url);
                apiCall(resolve, reject);
            }).catch((error)=>{
                if(this.authAction !== undefined && this.authAction != null){
                    this.authAction.setNet(true, request, resolve);
                }
            }); 
        });
    }

    uploadData = (request, progress_cb) => {
        return new Promise((resolve, reject) => {
            const url = settings.api + request.url
            var oReq = new XMLHttpRequest();
            oReq.upload.addEventListener("progress", function (event) {
                    if (event.lengthComputable) {
                        var progress = (event.loaded * 100) / event.total;
                        progress = parseInt(progress);
                        progress_cb(progress);
                    } else {
                        progress_cb(null);
                        // Unable to compute progress information since the total size is unknown
                    }
                });
            var params = request.query;
            oReq.open(request.method, settings.api + request.url, true);
            oReq.send(params);
            oReq.onreadystatechange = function () {
                if (oReq.readyState == XMLHttpRequest.DONE) {
                    try {
                        console.log(oReq.responseText);
                        let data = JSON.parse(oReq.responseText);
                        resolve(data);
                    } catch (exe) {
                        reject(exe);
                        console.log(exe);
                    }
                    
                }
            }
        });
    }
}
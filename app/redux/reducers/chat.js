//import * as types from '../../constants/actionTypes';
//import initialState from './initialState';
import moment from 'moment';

const types ={
    LOAD:'chat/LOAD',
    SET_SOCKET:'chat/SET_SOCKET',
    SET_DATA:'chat/SET_DATA',
    SET_SCREEN:'chat/SET_SCREEN',
    SET_CONV:'chat/SET_CONV',
    SET_NEW_MESSAGE:'chat/SET_NEW_MESSAGE',
    SET_DETAIL:'chat/SET_DETAIL',
    OPEN_CHAT:'chat/OPEN_CHAT',
    SET_CONV_DETAIL_ID:'chat/SET_CONV_DETAIL_ID'
}



const initialState={
    load:false,
    conv:[],
    data:{},
    convDetailId:null,
    convDetail:[],
    detailMsgCount:-1, 
    unread:0,
    notification:{},
    socket:null,
    screen:null,
}

export default function reducer(state = initialState, action) {
    //console.log('reducer called:' + action.type)
	switch (action.type) {

		case types.LOAD:
			return {
				...state,
				load: true
			};
        case types.SET_SOCKET:
			return {
				...state,
				socket: action.socket
            };
        case types.SET_CONV:
			return {
				...state,
				conv:action.conv
            };
		case types.SET_DATA:
			return {
				...state,
                data:action.data,
                unread:action.unread
            };
        case types.SET_DETAIL:
			return {
				...state,
                convDetail:action.convDetail,
                detailMsgCount:action.detailMsgCount,
                convDetailId:action.convDetailId
            };

        case types.SET_NEW_MESSAGE:
        return {
            ...state,
            unread:action.unread,
            conv:action.conv,
            convDetail:action.convDetail,
            detailMsgCount:action.detailMsgCount,
            notification:action.notification
        };    
            
        case types.SET_SCREEN:
			return {
				...state,
				screen:action.screen
            };
        
        case types.OPEN_CHAT:
			return {
				...state,
                conv:action.conv,
                unread:action.unread,
                convDetailId:action.convDetailId
            };

            case types.SET_CONV_DETAIL_ID:
			return {
				...state,
                convDetailId:action.convDetailId
            };
		default:
			return state;
	}
}

export function load(){
    return (dispatch) =>
    dispatch({
        type:types.LOAD
    })
}

export function setSocket(socket){
    return (dispatch) => {
        dispatch({
            type:types.SET_SOCKET,
            socket
        })
    }
}

export function setData(data){
    return (dispatch) =>
    dispatch({
        type:types.SET_DATA,
        data:data,
        unread:data.badge
    })
}

export function setScreen(screen){
    return (dispatch) =>
    dispatch({
        type:types.SET_SCREEN,
        screen
    })
}

export function setConv(conv){
    let newConv = conv;
    sortArray(newConv);
    return (dispatch) =>
    dispatch({
        type:types.SET_CONV,
        conv:newConv
    })
}

export function setUserStatus(uid,status){    
    return (dispatch, getState) => {
        let newConv = getState().chat.conv;
        if(newConv.length > 0){
            newConv.map((data,i)=>{
                if(data.from_id == uid || data.to_id == uid){
                    data.userOnline=status;
                }
            });
        }
        dispatch({
        type:types.SET_CONV,
        conv:newConv
    })
    }
}

export function setUserTyping(ad,status){    
    return (dispatch, getState) => {
        let newConv = getState().chat.conv;
        if(newConv.length > 0){
            newConv.map((data,i)=>{
                if(data.ad === ad){
                    data.typing=status;
                }else if(data.typing === undefined){
                    data.typing=false;
                }
            });
        }
        dispatch({
            type:types.SET_CONV,
            conv:newConv
        })
        }
}

export function onNewMessage(msg){
    return (dispatch, getState) => {
        let newConv = getState().chat.conv;
        //let unreadCount=getState().chat.unread;
        let unreadCount = 0;
        let uid= getState().chat.data.uid;
        let convDetail = getState().chat.convDetail;
        let notification = getState().chat.notification;
        //const len = convDetail.length -1;
        //const last = JSON.parse(JSON.stringify(convDetail[len]));
        // console.log('======= last item =========')
        // console.log(last);
        // console.log('=======  item to push  =========')
        // console.log(msg);
        msg.createdAt=msg.updatedAt;
        const nmsg = JSON.parse(JSON.stringify(msg));
        let adId = getState().chat.convDetailId;
        if(adId !== null && adId === nmsg.ad){
            convDetail.push(nmsg);
        }
        if(nmsg.from_id !== undefined && nmsg.from_id !== uid){
            notification = nmsg;
        }
        if(newConv.length > 0){
            let ret=false;
            newConv.map((data,i)=>{
                if(data.ad == msg.ad){
                    let c = data.msg_count;
                    if(uid !== undefined && msg.from_id == uid){
                        msg.msg_count = 0;
                        newConv[i].createdAt=msg.createdAt;
                        newConv[i].from_id=msg.from_id;
                        newConv[i].id=msg.id;
                        newConv[i].is_read=msg.is_read;
                        newConv[i].message=msg.message;
                        newConv[i].msg_count=msg.msg_count;
                        newConv[i].typing=msg.typing;
                        newConv[i].updatedAt=msg.updatedAt;
                    }else{
                        //console.log(c);
                        //console.log(msg.msg_count);
                        // console.log(unreadCount);
                        msg.msg_count+=c;
                        unreadCount+=msg.msg_count;
                        //msg.createdAt=msg.updatedAt;
                        newConv[i]=msg;
                        //unreadCount+=msg.msg_count;
                    }
                    ret=true;
                }else{
                    unreadCount+=data.msg_count;
                }
            });
            if(!ret){
                newConv.push(msg);
            }else{
                // newConv.sort(function(a1,a2){
                //     let v1=moment(a1.createdAt);
                //     let v2=moment(a2.createdAt);
                //     return v1 < v2;
                // });
                sortArray(newConv);
            }
        }else{
            newConv.push(msg);
        }
        //console.log(unreadCount);
        dispatch({
        type:types.SET_NEW_MESSAGE,
        conv:newConv,
        unread:unreadCount,
        convDetail:convDetail,
        detailMsgCount:convDetail.length,
        notification:notification
    })
    }
}

export function setDetail(convDetail){
    return (dispatch, getState) => {
        let flag = convDetail[0] !== undefined && convDetail[0] !== null && convDetail[0].ad !== undefined && convDetail[0].ad !== null && convDetail[0].ad !== '';
        let adId = getState().chat.convDetailId;
        if(convDetail[0] !== undefined && convDetail[0] !== null && convDetail[0].ad !== undefined && convDetail[0].ad !== null && convDetail[0].ad !== ''){
            adId = convDetail[0].ad;
        }
        //console.log('convDetail set to '+adId+' by SET_DETAIL')
        dispatch({
            type:types.SET_DETAIL,
            convDetail:convDetail,
            detailMsgCount:convDetail.length,
            convDetailId:adId
        });
    }
}

export function setConvDetailId(id){
    //console.log('convDetail set to '+id+' by SET_CONV_DETAIL_ID')
    return (dispatch) =>
    dispatch({
        type:types.SET_CONV_DETAIL_ID,
        convDetailId:id
    })
}

export function openChat(d){
    return (dispatch, getState) => {
        let newConv = getState().chat.conv;
        let unreadCount=getState().chat.unread;
        newConv.map((data,i)=>{
            if(data.ad == d.ad){
                //if(d.msg_count > 0){
                    unreadCount=unreadCount-data.msg_count;
                //}
                data.msg_count=0;
            }
        });
        //console.log('convDetail set to '+d.ad+' by OPEN_CHAT')
        dispatch({
            type:types.OPEN_CHAT,
            conv:newConv,
            unread:unreadCount,
            convDetailId:d.ad !== undefined && d.ad !== null && d.ad !== '' ? d.ad : null
        })
    }
}


function sortArray(arr){
    arr.sort(function(a1,a2){
        let v1=moment(a1.createdAt);
        let v2=moment(a2.createdAt);
        return v1 < v2;
    });
}
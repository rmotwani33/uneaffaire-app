import PayPal from 'react-native-paypal-wrapper';
import oauthconfig from '../../config/oauthconfig';

export default async function PayPalPayment(params) {
    return new Promise((resolve,reject)=>{
        PayPal.initialize(oauthconfig.payPalMode, oauthconfig.paypalClientId);
        PayPal.pay(params).then((confirm) => {
            resolve(confirm);
        }).catch((error) => {
            reject(error);
        });
    });
}
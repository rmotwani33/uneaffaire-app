import {Platform, PermissionsAndroid} from 'react-native';
export default async function tryGetLocation(authActions) {
    return new Promise((resolve, reject) => {
      if (Platform.OS === 'android') {
        PermissionsAndroid
          .check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION).then((result) => {
            console.log('result');
            console.log(result);
            if (result === false) {
              try {
                PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                  .then((granted) => {
                    console.log('granted');
                    console.log(granted);
                    console.log( PermissionsAndroid.RESULTS.GRANTED);
                    try {
                      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        authActions.setPermission(true);
                        getLocation(true, authActions).then((result1) => {
                          console.log(result1);
                          resolve(result1);
                        }).catch((err) => {
                          reject(err);
                          console.log('rejected in 54');
                          authActions.setPermission(false);
                        });
                      } else {
                        console.log('rejected in 58');
                        authActions.setPermission(false);
                      }
                    } catch (err) {
                      console.log(err);
                      console.log('rejected in 63');
                      authActions.setPermission(false);
                      reject(err);
                    }
                  }).catch((e) => {
                    console.log(e);
                    console.log('rejected in 69');
                    authActions.setPermission(false);
                  });
              } catch (err) {
                console.log(err);
                console.log('rejected in 74');
                authActions.setPermission(false);
              }
            } else if (result) {
              getLocation(true, authActions).then((result1) => {
                resolve(result1);
                // setWatchID(authActions);
              }).catch((err) => {
                reject(err);
                console.log(err);
                // setWatchID(authActions);
                console.log('rejected in 82');
                authActions.setPermission(false);
              });
            }
          })
          .catch((err) => {
            reject(err);
            console.log('rejected in 89');
            authActions.setPermission(false);
          });
      } else {
        getLocation(true, authActions).then((result) => {
          authActions.setPermission(true);
          resolve(result);
          // setWatchID(authActions);
        }).catch((err) => {
          reject(err);
          console.log('rejected in 98');
          // setWatchID(authActions);
          authActions.setPermission(false);
        });
      }
    });
  }


  function getLocation(bool, authActions) {
    return new Promise((resolve, reject) => {
      if (bool || Platform.OS === 'ios') {
        navigator.geolocation.getCurrentPosition((position) => {
          console.log('Current Position');
          // console.log(position);
          const locationData = position.coords;
          locationData.mocked = position.mocked;
          locationData.timestamp = position.timestamp;
          authActions.setLocation(locationData);
          console.log(locationData);
          // authActions.setLocation(locationData);
          // setWatchID(authActions);
          resolve(locationData);
        }, (error) => {
          console.log(error);
          console.log('rejected in 28');
          reject(error);
        }, {
          enableHighAccuracy: Platform.OS === 'ios',
          timeout: 20000,
          maximumAge: 3600000,
          // distanceFilter: 1,
        });
      }
    });
  }

export  async function tryCamera() {
    return new Promise((resolve,reject)=>{
        if (Platform.OS === 'android') {
            PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then((result) => {
                PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then((result) => {
                    if (result === false) {
                       // try {
                            //const granted = 
                            PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA,PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE])
                            .then((granted)=>{
                                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                                    //authActions.setPermission(true);
                                   
                                        resolve(true);                                   
                                } else {
                                    reject(false);
                                    //authActions.setPermission(false);
                                }
                            }).catch((e)=>{
                                reject(e);
                                //authActions.setPermission(false);
                                 //getLocation(false, authActions);
                            });
    
                        //} catch (err) {
                            //getLocation(false, authActions);
                            //this.setState({accessLocation:false});
                        //}
                    }else if(result){
                        resolve(true);
                    }
                })
                .catch((err) => {
                    reject(err);
                });
                    //this.setState({accessLocation: result});
                })
                .catch((err) => {
                    reject(err);
                });
        } else {
           resolve(true);
        }
    });
}

